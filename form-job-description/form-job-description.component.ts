import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {GeneralService} from '../../shared/service/general.service';
import {Observable} from 'rxjs/Rx';
import {AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AppForm2} from '../../shared/model/form-models/formTow-model/pr-req-and-app-page-1.model';
import {OtherCredentialForm} from '../../shared/model/form-models/other-application-form.model';
import {isNullOrUndefined} from 'util';
import {DatePipe} from '@angular/common';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {AppFormJobDescription} from '../../shared/model/form-models/formJobDescription-model/formJobDescription.model';
import {Http} from '@angular/http';
import {FormJobDescriptionService} from './form-job-description.service';
import {FormNewApproachModel} from '../../shared/model/form-models/formNewApproach.model';
import {AppFormMarketingQuestion} from '../../shared/model/form-models/formMarketingQuestion-model/formMarketingQuestion.model';
@Component({
  selector: 'app-form-job-description',
  templateUrl: './form-job-description.component.html',
  styleUrls: ['./form-job-description.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FormJobDescriptionComponent implements OnInit {
    @Input()
    formNewApproachId: any;
    appFormJobDescription: AppFormJobDescription = new AppFormJobDescription();
    formAppJobDescriptions: AppFormJobDescription[];
    isSaving: boolean;
    emailBody: any;
    signDateDb: any;
    approvedCMODateDb: any;
    approvedBodyMemberDateDb: any;
    confirmSaveMsgForm: boolean = false;
    errorSaveMsgForm: boolean = false;
    confirmSubmitFormMsg: boolean = false;
    errorSubmitFormMsg: boolean = false;
    pageLoader: boolean = false;
    isSubmitted: boolean = false;
    duplicatedName = false;
    formNewAppId: any;
    formJobDescription: FormGroup;

    printedNameJobDescription: AbstractControl;
    dateJobDescription: AbstractControl;


    constructor(private formJobDescriptionService: FormJobDescriptionService, private modalService: NgbModal,
              private generalService: GeneralService, private spinner: NgxSpinnerService,
              private  fb: FormBuilder, private http: Http,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              public toastrService: ToastrService,) {
        this.formJobDescription = fb.group({
            'printedNameJobDescription': ['', Validators.compose([])],
            'dateJobDescription': ['', Validators.compose([])],

        });
        this.printedNameJobDescription = this.formJobDescription.controls['printedNameJobDescription'];
        this.dateJobDescription = this.formJobDescription.controls['dateJobDescription'];
    }

  ngOnInit() {
      this.spinner.show();
      this.activatedRoute
          .queryParams
          .subscribe(params => {
              // Defaults to 0 if no query param provided.
              this.formNewAppId = params['formNewAppId'];
              //alert(this.formNewAppId)
          });

      this.formJobDescriptionService.getCurrentForm(this.formNewAppId).subscribe(res => {

          // form is saved
          if(!isNullOrUndefined(res)) {
              this.appFormJobDescription = res;
              if((<FormNewApproachModel>this.appFormJobDescription.formNewApproach).submit === true)
                  this.isSubmitted = true;

              let dp = new DatePipe(navigator.language);
              let p = 'y-MM-dd'; // YYYY-MM-DD
              let dtr = dp.transform(this.appFormJobDescription.dateJobDescription, p);
              this.formJobDescription.get('dateJobDescription').setValue(dtr);
          }
          this.spinner.hide();
      }, rej =>{
          this.spinner.hide();
      })
  }

    save(){
        this.spinner.show();

        this.isSaving = true;

        console.log(this.appFormJobDescription);

        if (!isNullOrUndefined(this.appFormJobDescription.id)){
            this.subscribeToSaveResponse(
                this.formJobDescriptionService.update(this.appFormJobDescription));
        } else {
            this.subscribeToSaveResponse(
                this.formJobDescriptionService.create(this.appFormJobDescription,this.formNewAppId));
        }
    }


    private subscribeToSaveResponse(result: Observable<AppFormJobDescription>) {
        result.subscribe((res: AppFormJobDescription) =>
            this.onSaveSuccess(res), (res: Response) => {
            this.onSaveError();
            if (res.status === 400) {
                this.duplicatedName = true;
            }
            this.onSaveError()
        });
    }

    private onSaveSuccess(result: AppFormJobDescription) {

        this.appFormJobDescription= result;
        // alert(JSON.stringify(result.formNewApproach))
        this.isSaving = false;
        this.confirmSaveMsgForm = true;
        this.errorSaveMsgForm = false;
        this.spinner.hide();
    }

    private onSaveError() {
        this.isSaving = false;
        this.confirmSaveMsgForm = false;
        this.errorSaveMsgForm = true;
        this.spinner.hide();
    }

    private submit() {

        this.spinner.show();


        this.formJobDescriptionService.submit(this.appFormJobDescription).subscribe(
            res => {
                //this.confirmSubmitFormMsg =true;
                this.errorSubmitFormMsg= false;
                this.isSubmitted= true;
                this.toastrService.success('The Form Job Description\n' , 'Successfully Submitted !');
                this.router.navigate([''])
                this.spinner.hide()},
            rej =>{this.errorSubmitFormMsg=true;
                this.confirmSubmitFormMsg =false;
                this.spinner.hide()})

    }

}
