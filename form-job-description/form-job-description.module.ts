import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import {CommonModule} from '@angular/common';
import {HttpModule} from '@angular/http';
import {AgGridModule} from 'ag-grid-angular';
import {HttpClientModule} from '@angular/common/http';
import {MultiselectDropdownModule} from 'angular-2-dropdown-multiselect';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormsModule, ReactiveFormsModule, Validators} from '@angular/forms';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {NgJhipsterModule} from 'ng-jhipster';
import {customHttpProvider} from '../../shared/interceptor/http.provider';
import {FormJobDescriptionComponent} from './form-job-description.component';
// import {FormMarketingService} from './form-marketing.service';
import {FormJobDescriptionService} from './form-job-description.service';


export const routes = [
    {
        path: '', component: FormJobDescriptionComponent, pathMatch: 'full',

    }
];

@NgModule({
    imports: [
        CommonModule, HttpModule, HttpClientModule, FormsModule, NgbModule.forRoot(), MultiselectDropdownModule
        , AngularMultiSelectModule, NgJhipsterModule, RouterModule.forChild(routes),
        ReactiveFormsModule,
        //AppRoutingModule,

    ],
    declarations: [
      FormJobDescriptionComponent
    ],

    exports: [FormJobDescriptionComponent]
    ,

    providers: [  FormJobDescriptionService,
        customHttpProvider()

    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FormJobDescriptionModule {

}
