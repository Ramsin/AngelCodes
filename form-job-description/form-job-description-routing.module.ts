import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {FormJobDescriptionComponent} from './form-job-description.component';


export const appRoutes: Routes = [

    { path: '',   redirectTo: '/pages/formJobDescription', pathMatch: 'full' },
    { path: 'formJobDescription',   redirectTo: '/pages/formJobDescription', pathMatch: 'full' },
    // {path: 'formSix', redirectTo:'/pages/formSix', pathMatch: 'full'},
    // 6th Route
    { path: '**', component: FormJobDescriptionComponent}
];

@NgModule({
    imports: [RouterModule.forChild(appRoutes)],
    exports: [RouterModule],
    providers: []
})

export class AppRoutingModule {}
