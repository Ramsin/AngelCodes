import { Injectable } from '@angular/core';
import {BaseRequestOptions, Http, RequestOptions, Response, URLSearchParams} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import {ResponseWrapper} from '../../shared/model/response-wrapper.model';
import {createRequestOption} from '../../shared/model/request-util';
import {JhiDateUtils} from 'ng-jhipster';
import {SERVER_API_URL} from '../../app.constants';
import {AppFormJobDescription} from '../../shared/model/form-models/formJobDescription-model/formJobDescription.model';
import {AppForm2} from '../../shared/model/form-models/formTow-model/pr-req-and-app-page-1.model';

@Injectable()
export class FormJobDescriptionService {

    private resourceUrl = SERVER_API_URL + 'api';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }
    create(appFormJobDescription: AppFormJobDescription, formNewAppId: any): Observable<AppFormJobDescription> {
        const copy = this.convert(appFormJobDescription);

        let option: RequestOptions;
        const options: BaseRequestOptions = new BaseRequestOptions();
        const params: URLSearchParams = new URLSearchParams();
        params.set('formNewApproachId' , formNewAppId )
        options.params = params;

        return this.http.post(this.resourceUrl + '/FormJobDescription', copy, options).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(appFormJobDescription: AppFormJobDescription): Observable<AppFormJobDescription> {
        const copy = this.convert(appFormJobDescription);
        return this.http.put(this.resourceUrl + '/FormJobDescription', copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    submit(appFormJobDescription: AppFormJobDescription): Observable<AppFormJobDescription>{
        const copy = this.convert(appFormJobDescription);
        return this.http.post(this.resourceUrl + '/FormJobDescription/submit', copy).map((res:Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => res);
    }

    getCurrentForm(formNewAppId: any): Observable<AppFormJobDescription>
    {
        let params = new URLSearchParams();
        params.append('formNewAppId', formNewAppId);

        return this.http.get(this.resourceUrl + '/getCurrentUserLatestFormJobDescription', {params: params})
            .map((res: Response) => {

                const jsonResponse = res.json();

                return this.convertItemFromServer(jsonResponse);


            });
    }

    submitForm(): Observable<ResponseWrapper>{
        return this.http.get(this.resourceUrl + '/XXXXX').map(res => res);
    }


    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }
    private convertItemFromServer(json: any): AppForm2 {
        const entity: AppForm2 = Object.assign(new AppForm2(), json);
        // entity.creationDate = this.dateUtils
        //     .convertLocalDateFromServer(json.creationDate);
        return entity;
    }
    private convert(appForm2: AppForm2): AppForm2 {
        const copy: AppForm2 = Object.assign({}, appForm2);
        // copy.signDate = this.dateUtils
        //     .convertLocalDateToServer(appForm2.signDate);
        // copy.approvedCMODate = this.dateUtils
        //     .convertLocalDateToServer(appForm2.approvedCMODate);
        // copy.approvedBodyMemberDate = this.dateUtils
        //     .convertLocalDateToServer(appForm2.approvedBodyMemberDate);

        return copy;
    }

}
