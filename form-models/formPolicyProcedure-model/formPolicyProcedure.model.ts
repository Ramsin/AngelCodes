import {BaseEntity} from '../../base-entity';


export class AppFormPolicyProcedure implements BaseEntity {
    constructor(
        public id?: number,
        public clinicPolicy?: string,
        public employeeNamePolicy?: any,
        public datePolicyProcedure?: string,
        public formPolicy?: BaseEntity,
        public formNewApproach?: BaseEntity
    ) {
    }
}
