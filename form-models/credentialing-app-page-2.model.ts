import {BaseEntity} from '../base-entity';
import {CredentialAOSM} from './credential-aosm.model';

export class Page2 implements BaseEntity {
    constructor(
        public id?: number,
        public collegeUniversity?: string,
        public degreesHonorsPreMedical?: string,
        public dateOfGraduationPreMedical?: any,
        public addressPreMedical?: string,
        public cityPreMedical?: string,
        public statePreMedical?: string,
        public zipPreMedical?: string,
        public professionalSchoolmedical?: string,
        public degreesHonorsMedical?: string,
        public dateOfGraduationMedical?: any,
        public addressMedical?: string,
        public cityMedical?: string,
        public stateMedical?: string,
        public zipMedical?: string,
        public nameInstitution?: string,
        public degreesHonorsPofessionalEdu?: string,
        public dateOfGraduationPofessionalEdu?: any,
        public addressPofessionalEdu?: string,
        public cityPofessionalEdu?: string,
        public statePofessionalEdu?: string,
        public zipPofessionalEdu?: string,
        public credentialAOSMS?: BaseEntity[],
        public credentialInternshipS?: BaseEntity[],
        public credentialApplicationForm?: BaseEntity,
       // public credentialAsom =  new Array<CredentialAOSM>()  //CredentialAOSM[] = new CredentialAOSM()[10], //public messages: CredentialAOSM[] = []

        // search parameters
        public nameInstitutionInternship?: string,
        public typeInternship?: string
    ) {

    }
}
