import {BaseEntity} from '../../base-entity';


export class AppFormStatementHealth implements BaseEntity {
    constructor(
        public id?: number,
        public nameStatementHealth?: string,
        public dateStatementHealth?: string,
        public formPolicy?: BaseEntity,
        public formNewApproach?: BaseEntity
    ) {
    }
}
