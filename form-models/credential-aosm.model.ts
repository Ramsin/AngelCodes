
import {BaseEntity} from '../base-entity';

export class CredentialAOSM implements BaseEntity {
    constructor(
        public id?: number,
        public stateLicenseNumber?: string,
        public licenseNumberMedical?: number,
        public originalDateOfIssue?: any,
        public currentlyPracticeState?: boolean,
        public naOtherStateMedicalLicenses?: boolean,
        public explain?: string,
        public aosmOrderId?: number,
        public credentialingAppPage2?: BaseEntity,
    ) {
        this.currentlyPracticeState = false;
        this.naOtherStateMedicalLicenses = false;
    }
}
