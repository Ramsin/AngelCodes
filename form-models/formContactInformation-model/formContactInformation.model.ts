import {BaseEntity} from '../../base-entity';

// export const enum JobContactInformation {
//     'Doctor',
//     'Md',
//     'Do',
// }


export class AppFormContactInformation implements BaseEntity {
    constructor(
        public id?: number,
        public jobContactInformation?: string,
        public drNameContactInformation?: string,
        public addressOfficeContactInformation?: string,
        public cityOfficeContactInformation?: string,
        public stateOfficeContactInformation?: string,
        public zipOfficeContactInformation?: string,
        public addressHomeContactInformation?: string,
        public cityHomeContactInformation?: string,
        public stateHomeContactInformation?: string,
        public zipHomeContactInformation?: string,
        public cellContactInformation?: string,
        public homeContactInformation?: string,
        public faxContactInformation?: string,
        public emailContactInformation?: string,
        public phonePreferredContactInformation?: boolean,
        public emailPreferredContactInformation?: boolean,
        public explainPreferredContactInformation?: string,
        public nameEmergencyContactInformation?: string,
        public cellEmergencyContactInformation?: string,
        public homeEmergencyContactInformation?: string,
        public otherEmergencyContactInformation?: string,
        public topContactInformation?: string,
        public bottomContactInformation?: string,
        public labCoatContactInformation?: string,
        public laserEyeWearContactInformation?: string,
        public surgicalGlovesContactInformation?: string,
        public formJob?: BaseEntity,
        public formNewApproach?: BaseEntity
    ) {

    }
}
