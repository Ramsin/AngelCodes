
import {BaseEntity} from '../base-entity';

export class CredentialInternship implements BaseEntity {
    constructor(
        public id?: number,
        public nameInstitutionInternship?: string,
        public datesAttendendInternship?: any,
        public datesAttendendInternshipEnd?: any,
        public addressInternship?: string,
        public programDirectorNameInternship?: string,
        public typeInternship?: string,
        public isProgramSuccessfullyCompletedInternship?: boolean,
        public isSubjectOfDisciplinaryActionInternship?: boolean,
        public rotatingOrStraight?: boolean,
        public isNaInternship?: boolean,
        public programSuccessfullyCompletedInternship?: string,
        public subjectOfDisciplinaryActionInternship?: string,
        public listSpeciality?: string,
        public internshipOrderId?: number,
        public credentialingAppPage2?: BaseEntity,
    ) {
        this.isProgramSuccessfullyCompletedInternship = false;
        this.isNaInternship = false;
        this.isSubjectOfDisciplinaryActionInternship = false;
        this.rotatingOrStraight = false;
    }
}
