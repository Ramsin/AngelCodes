import {BaseEntity} from '../../base-entity';


export class AppFormJobDescription implements BaseEntity {
    constructor(
        public id?: number,
        public printedNameJobDescription?: string,
        public dateJobDescription?: any,
        public formJob?: BaseEntity,
        public formNewApproach?: BaseEntity
    ) {
    }
}
