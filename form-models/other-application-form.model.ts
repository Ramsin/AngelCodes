import {BaseEntity} from '../base-entity';

export class OtherCredentialForm implements BaseEntity {
    constructor(
        public id?: number,
        public prReqAndAppAbvlPage1?: BaseEntity,
        public privilReqAndAppPage1?: BaseEntity,
        public privReqAndAppReapp?: BaseEntity
    ) {
    }
}
