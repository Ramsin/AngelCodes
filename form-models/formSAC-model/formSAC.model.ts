import {BaseEntity} from '../../base-entity';


export class AppFormSAC implements BaseEntity {
    constructor(
        public id?: number,
        public nameSAC?: string,
        public dateSAC?: any,
        public formJob?: BaseEntity,
        public formNewApproach?: BaseEntity
    ) {
    }
}
