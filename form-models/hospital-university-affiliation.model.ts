import {BaseEntity} from "../base-entity";

export class HospitalUniversityAffiliation implements BaseEntity {
    constructor(
        public id?: number,
        public nameOfInstitutionHospitalAndUniversity?: string,
        public addressHospitalAndUniversity?: string,
        public cityHospitalAndUniversity?: string,
        public stateHospitalAndUniversity?: string,
        public zipHospitalAndUniversity?: string,
        public datesAffiliateHospitalAndUniversity?: any,
        public datesAffiliateHospitalAndUniversityEnd?: any,
        public departmentHospitalAndUniversity?: string,
        public membershipStatusHospitalAndUniversity?: string,
        public departmentChiefHospitalAndUniversity?: string,
        public privilegesAtInstitutionHospitalAndUniversity?: boolean,
        public typePrivilegesGrantedHospitalAndUniversity?: string,
        public hospitalOrderId?: number,
        public credentialingAppPage4?: BaseEntity,
    ) {
        this.privilegesAtInstitutionHospitalAndUniversity = false;
    }
}
