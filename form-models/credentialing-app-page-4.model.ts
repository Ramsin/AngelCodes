
import {BaseEntity} from "../base-entity";

export class Page4 implements BaseEntity {
    constructor(
        public id?: number,
        public hospitalUniversityAffiliations?: BaseEntity[],
        public credentialApplicationForm?: BaseEntity,
    ) {
    }
}
