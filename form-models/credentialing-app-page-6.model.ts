import {BaseEntity} from "../base-entity";

export class Page6 implements BaseEntity {
    constructor(
        public id?: number,
        public professionalPeerReferenceLastName1?: string,
        public professionalPeerReferenceFirstName1?: string,
        public professionalPeerReferenceMiddleName1?: string,
        public professionalPeerReferenceDegree1?: string,
        public professionalPeerReferenceTitle1?: string,
        public professionalPeerReferenceRelationShip1?: string,
        public professionalPeerReferenceSpecialities1?: string,
        public professionalPeerReferenceYearsKnown1?: string,
        public professionalPeerReferenceAddress1?: string,
        public professionalPeerReferenceCity1?: string,
        public professionalPeerReferenceState1?: string,
        public professionalPeerReferenceZip1?: string,
        public professionalPeerReferencePhone1?: string,
        public professionalPeerReferencefax1?: string,
        public professionalPeerReferenceEmail1?: string,
        public professionalPeerReferenceLastName2?: string,
        public professionalPeerReferenceFirstName2?: string,
        public professionalPeerReferenceMiddleName2?: string,
        public professionalPeerReferenceDegree2?: string,
        public professionalPeerReferenceTitle2?: string,
        public professionalPeerReferenceRelationShip2?: string,
        public professionalPeerReferenceSpecialities2?: string,
        public professionalPeerReferenceYearsKnown2?: string,
        public professionalPeerReferenceAddress2?: string,
        public professionalPeerReferenceCity2?: string,
        public professionalPeerReferenceState2?: string,
        public professionalPeerReferenceZip2?: string,
        public professionalPeerReferencePhone2?: string,
        public professionalPeerReferencefax2?: string,
        public professionalPeerReferenceEmail2?: string,
        public professionalPeerReferenceLastName3?: string,
        public professionalPeerReferenceFirstName3?: string,
        public professionalPeerReferenceMiddleName3?: string,
        public professionalPeerReferenceDegree3?: string,
        public professionalPeerReferenceTitle3?: string,
        public professionalPeerReferenceRelationShip3?: string,
        public professionalPeerReferenceSpecialities3?: string,
        public professionalPeerReferenceYearsKnown3?: string,
        public professionalPeerReferenceAddress3?: string,
        public professionalPeerReferenceCity3?: string,
        public professionalPeerReferenceState3?: string,
        public professionalPeerReferenceZip3?: string,
        public professionalPeerReferencePhone3?: string,
        public professionalPeerReferenceFax3?: string,
        public professionalPeerReferenceEmail3?: string,
        public credentialApplicationForm?: BaseEntity,
    ) {
    }
}
