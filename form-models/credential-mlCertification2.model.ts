
import {BaseEntity} from '../base-entity';

export class CredentialMLCertification2 implements BaseEntity {
    constructor(
        public id?: number,
        public deaRegistrationNumber?: string,
        public expiresDate3?: any,
        public isNaMLCertification2?: boolean,
        public certification2OrderId?: number,
        public credentialingAppPage1?: BaseEntity,
    ) {
        this.isNaMLCertification2 = false;

    }
}
