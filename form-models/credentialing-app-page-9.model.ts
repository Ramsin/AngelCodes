import {BaseEntity} from "../base-entity";

export class Page9 implements BaseEntity {
    constructor(
        public id?: number,
        public personalSanctionsCurrentlyEngagedYes?: boolean,
        public personalSanctionsCurrentlyEngagedNo?: string,
        public personalSanctionsChemicalSubstancesYes?: boolean,
        public personalSanctionsChemicalSubstancesNo?: string,
        public personalSanctionsIfYesExplain?: string,
        public date?: any,
        public printName?: string,
        public credentialApplicationForm?: BaseEntity,
    ) {
        this.personalSanctionsCurrentlyEngagedYes = true;
        this.personalSanctionsChemicalSubstancesYes = true;
    }
}
