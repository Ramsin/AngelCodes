import {BaseEntity} from '../../base-entity';


export class AppFormNonDisclosure implements BaseEntity {
    constructor(
        public id?: number,
        public printedNameSignerDisclosure?: string,
        public dateSignerDisclosure?: any,
        public printedNameCompanyDisclosure?: string,
        public dateCompanyDisclosure?: any,
        public formDisclosure?: BaseEntity,
        public formNewApproach?: BaseEntity
    ) {
    }
}
