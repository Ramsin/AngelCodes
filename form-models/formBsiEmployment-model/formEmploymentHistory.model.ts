
import{BaseEntity} from "../../base-entity";

export class AppFormEmploymentHistory implements BaseEntity {
    constructor(
        public id?: number,
        public nameEmployee?: string,
        public addressEmployee?: string,
        public dateEmployeeFrom?: any,
        public dateEmployeeTo?: any,
        public typeBusiness?: string,
        public positionTitle?: string,
        public salaryStarting?: string,
        public salaryFinal?: string,
        public managersName?: string,
        public managersTitle?: string,
        public phoneOne?: string,
        public phoneTwo?: string,
        public reasonLeaving?: string,
        public historyOrderId?: number,
        public formBsiEmployment?: BaseEntity,
    ) {

    }
}
