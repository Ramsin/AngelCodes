
import {BaseEntity} from "../base-entity";

export class TrainingFellowships implements BaseEntity {
    constructor(
        public id?: number,
        public nameOfInstitutionTrainingFellowship?: string,
        public addressTrainingFellowship?: string,
        public cityTrainingFellowship?: string,
        public stateTrainingFellowship?: string,
        public zipTrainingFellowship?: string,
        public datesAttendedTrainingFellowship?: any,
        public nameOfImmediateSupervisorTrainingFellowship?: string,
        public typeOfFellowship?: string,
        public explanationForNoCoProTrainingFellowship?: string,
        public explanationForDisciplinaryTrainingFellowship?: string,
        public isProgramSuccessfullyCompletedTrainingFellowship?: boolean,
        public isNaTrainingFellowship?: boolean,
        public programSuccessfullyCompletedTrainingFellowship?: string,
        public disciplinaryTrainingFellowship?: boolean,
        public attachAnExplanationTrainingFellowship?: string,
        public trainingOrderId?: number,
        public credentialingAppPage3?: BaseEntity,
    ) {
        this.isProgramSuccessfullyCompletedTrainingFellowship = false;
        this.isNaTrainingFellowship = false;
        this.disciplinaryTrainingFellowship = false;
    }
}
