import {BaseEntity} from '../base-entity';

export class CredentialApplicationForm implements BaseEntity {
    constructor(
        public id?: number,
        public isSubmit?: boolean,
        public credentialingAppPage1?: BaseEntity,
        public credentialingAppPage2?: BaseEntity,
        public credentialingAppPage3?: BaseEntity,
        public credentialingAppPage4?: BaseEntity,
        public credentialingAppPage5?: BaseEntity,
        public credentialingAppPage6?: BaseEntity,
        public credentialingAppPage7?: BaseEntity,
        public credentialingAppPage8?: BaseEntity,
        public credentialingAppPage9?: BaseEntity,
        public formNewApproach?: BaseEntity

    ) {
    }
}
