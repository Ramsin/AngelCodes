

import {BaseEntity} from "../base-entity";

export class Page7 implements BaseEntity {
    constructor(
        public id?: number,
        public professionalLiabilityInsuranceCarrier?: string,
        public professionalLiabilityAddress?: string,
        public professionalLiabilityCity?: string,
        public professionalLiabilityState?: string,
        public professionalLiabilityZip?: string,
        public professionalLiabilityPolicyLimits?: string,
        public professionalLiabilityPerOccurrence?: string,
        public professionalLiabilityAggregate?: string,
        public professionalLiabilityPolicyNumber?: string,
        public professionalLiabilityAgent?: string,
        public professionalLiabilityEffectiveDate?: any,
        public professionalLiabilityExpirationDate?: any,
        public professionalLiabilityClaimsMade?: boolean,
        public professionalLiabilityLawSuitAgainstYouYes?: boolean,
        public professionalLiabilityLawSuitAgainstYouNo?: string,
        public lawSuitAgainstYouYesExplain?: boolean,
        public professionalLiabilityAnyNowPendingYes?: boolean,
        public professionalLiabilityAnyNowPendingNo?: string,
        public anyNowPendingYesExplain?: boolean,
        public professionalLiabilityAnyJudgementOrPaymentOrClaimYes?: boolean,
        public professionalLiabilityAnyJudgementOrPaymentOrClaimNo?: string,
        public anyJudgementOrPaymentOrClaimYesExplain?: boolean,
        public professionalLiabilityDeniedProfessionalInsuranceYes?: boolean,
        public professionalLiabilityDeniedProfessionalInsuranceNo?: string,
        public deniedProfessionalInsuranceYesExplain?: boolean,
        public credentialApplicationForm?: BaseEntity,
    ) {
        this.professionalLiabilityClaimsMade = false;
        // this.professionalLiabilityOccurrence = false;
        this.professionalLiabilityLawSuitAgainstYouYes = true;
        // this.lawSuitAgainstYouYesExplain = false;
        this.professionalLiabilityAnyNowPendingYes = true;
        // this.anyNowPendingYesExplain = false;
        this.professionalLiabilityAnyJudgementOrPaymentOrClaimYes = true;
        // this.anyJudgementOrPaymentOrClaimYesExplain = false;
        this.professionalLiabilityDeniedProfessionalInsuranceYes = true;
        // this.deniedProfessionalInsuranceYesExplain = false;
    }
}
