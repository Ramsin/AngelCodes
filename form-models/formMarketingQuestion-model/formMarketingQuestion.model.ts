import {BaseEntity} from '../../base-entity';


export class AppFormMarketingQuestion implements BaseEntity {
    constructor(
        public id?: number,
        public nameMarketing?: string,
        public whenBornMarketing?: any,
        public whereBornMarketing?: string,
        public childrenMarketing?: string,
        public familyMarketing?: string,
        public languagesMarketing?: string,
        public funFactMarketing?: string,
        public awardHonorsMarketing?: string,
        public medicalPhilosophyMarketing?: string,
        public hobbiesInternetMarketing?: string,
        public favoriteCharityMarketing?: string,
        public submitStatus?: string,
        public formMarketingQuestion?: BaseEntity,
        public formNewApproach?: BaseEntity
    ) {
    }
}
