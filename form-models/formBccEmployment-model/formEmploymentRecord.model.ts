import {BaseEntity} from '../../base-entity';


export class AppFormEmploymentRecord implements BaseEntity {
    constructor(
        public id?: number,
        public ifCurrentlyEmployedEmployment?: boolean,
        public employerEmployment?: string,
        public typeBusinessEmployment?: string,
        public phoneOneEmployment?: string,
        public cityEmployment?: string,
        public stateEmployment?: string,
        public faxEmployment?: string,
        public jobTitleEmployment?: string,
        public supervisorEmployment?: string,
        public phoneTwoEmployment?: string,
        public dateEmployedFromEmployment?: any,
        public dateEmployedToEmployment?: any,
        public reasonLeavingEmployment?: string,
        public dutiesEmployment?: string,
        public recordOrderId?: number,
        public formBccEmployment?: BaseEntity
    ) {
    }
}
