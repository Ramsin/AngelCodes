import {BaseEntity} from '../base-entity';
import {CredentialMLCertification1} from './credential-mlCertification1.model';
import{CredentialMLCertification2} from './credential-mlCertification2.model';

export class Page1 implements BaseEntity {
    constructor(
        public id?: number,
        public firstName1?: string,
        public lastName1?: string,
        public middle1?: string,
        public firstName2?: string,
        public lastName2?: string,
        public middle2?: string,
        public ss?: string,
        public npiNumber?: string,
        public dateOfBirth?: any,
        public placeOfBirth?: string,
        public citizenship?: string,
        public primaryProfessionalGNA?: string,
        public yearsAssociated?: any,
        public yearsAssociatedEnd?: any,
        public cityInfo1?: string,
        public stateInfo1?: string,
        public zipInfo1?: string,
        public telephoneNumber?: string,
        public faxInfo1?: string,
        public emailInfo1?: string,
        public homeAddress?: string,
        public homeTelNumber?: string,
        public cityInfo2?: string,
        public stateInfo2?: string,
        public zipInfo2?: string,
        public stateAndLicenseNumber?: string,
        public licenseNumberMedicalLicensure?: string,
        public originalDateIssue?: any,
        public expiresDate1?: any,
        // public deaRegistrationNumber?: string,
        // public expiresDate3?: any,
        public credentialMLCertification1S?: BaseEntity[],
        public credentialMLCertification2S?: BaseEntity[],
        public credentialApplicationForm?: BaseEntity,

        // search fields
        public birthDateSearchFrom?: string,
        public birthDateSearchTo?: string,
        public expiresDate1SearchFrom?: string,
        public expiresDate1SearchTo?: string,
        public controlledRCertificationNumber?: string,
        public expiresDate2SearchFrom?: string,
        public expiresDate2SearchTo?: string,
        public deaRegistrationNumber?: string,
        public expiresDate3SearchFrom?: string,
        public expiresDate3SearchTo?: string) {
    }
}
