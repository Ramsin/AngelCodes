

import {BaseEntity} from "../base-entity";

export class Page8 implements BaseEntity {
    constructor(
        public id?: number,
        public personalSanctionsLicenseToPracticeDeniedYes?: boolean,
        public personalSanctionsLicenseToPracticeDeniedNo?: string,
        public personalSanctionsLicenseToReprimandedSubjectYes?: boolean,
        public personalSanctionsLicenseToReprimandedSubjectNo?: string,
        public personalSanctionsLostBoardYes?: boolean,
        public personalSanctionsLostBoardNo?: string,
        public personalSanctionsCapitalCertifyingYes?: boolean,
        public personalSanctionsCapitalCertifyingNo?: string,
        public personalSanctionsInformationPertainingYes?: boolean,
        public personalSanctionsInformationPertainingNo?: string,
        public personalSanctionsFederalDEANumberYes?: boolean,
        public personalSanctionsFederalDEANumberNo?: string,
        public personalSanctionsHospitalAmbulatoryYes?: boolean,
        public personalSanctionsHospitalAmbulatoryNo?: string,
        public personalSanctionsVoluntarilyAndInVoluntarilyYes?: boolean,
        public personalSanctionsVoluntarilyAndInVoluntarilyNo?: string,
        public personalSanctionsDisciplinaryActionYes?: boolean,
        public personalSanctionsDisciplinaryActionNo?: string,
        public personalSanctionsCensuredOrExcludeOrSuspendActionYes?: boolean,
        public personalSanctionsCensuredOrExcludeOrSuspendActionNo?: string,
        public personalSanctionsMedicareMedicaidChampusYes?: boolean,
        public personalSanctionsMedicareMedicaidChampusNo?: string,
        public personalSanctionsDeniedMembershipSubjectYes?: boolean,
        public personalSanctionsDeniedMembershipSubjectNo?: string,
        public personalSanctionsWithdrawnAnApplicationYes?: boolean,
        public personalSanctionsWithdrawnAnApplicationNo?: string,
        public personalSanctionsChargedOrConvictedForACrimeYes?: boolean,
        public personalSanctionsChargedOrConvictedForACrimeNo?: string,
        public personalSanctionsSubjectOfCivilOrCriminalYes?: boolean,
        public personalSanctionsSubjectOfCivilOrCriminalNo?: string,
        public personalSanctionsHaveMedicalConditionYes?: boolean,
        public personalSanctionsHaveMedicalConditionNo?: string,
        public personalSanctionsUnableToPerformEssentialYes?: boolean,
        public personalSanctionsUnableToPerformEssentialNo?: string,
        public personalSanctionsAnyReligiousYesRestrictionYes?: boolean,
        public personalSanctionsAnyReligiousYesRestrictionNo?: string,
        public personalSanctionsAnyReligiousYesExplain?: string,
        public credentialApplicationForm?: BaseEntity,
    ) {
        this.personalSanctionsLicenseToPracticeDeniedYes = true;
        this.personalSanctionsLicenseToReprimandedSubjectYes = true;
        this.personalSanctionsLostBoardYes = true;
        this.personalSanctionsCapitalCertifyingYes = true;
        this.personalSanctionsInformationPertainingYes = true;
        this.personalSanctionsFederalDEANumberYes = true;
        this.personalSanctionsHospitalAmbulatoryYes = true;
        this.personalSanctionsVoluntarilyAndInVoluntarilyYes = true;
        this.personalSanctionsDisciplinaryActionYes = true;
        this.personalSanctionsCensuredOrExcludeOrSuspendActionYes = true;
        this.personalSanctionsMedicareMedicaidChampusYes = true;
        this.personalSanctionsDeniedMembershipSubjectYes = true;
        this.personalSanctionsWithdrawnAnApplicationYes = true;
        this.personalSanctionsChargedOrConvictedForACrimeYes = true;
        this.personalSanctionsSubjectOfCivilOrCriminalYes = true;
        this.personalSanctionsHaveMedicalConditionYes = true;
        this.personalSanctionsUnableToPerformEssentialYes = true;
        this.personalSanctionsAnyReligiousYesRestrictionYes = true;
    }
}
