import {BaseEntity} from '../base-entity';


export class FormNewApproachModel implements BaseEntity {
    constructor(
        public id?: number,
        public formType?: string,
        public submit?: boolean,
        public status?: string
    ) {

    }
}
