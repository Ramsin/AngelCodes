

import {BaseEntity} from "../base-entity";

export class Page3 implements BaseEntity {
    constructor(
        public id?: number,
        public nameInstitutionResidencyProgram?: string,
        public datesAttendendResidencyProgram?: any,
        public addressResidencyProgram?: string,
        public cityResidencyProgram?: string,
        public stateResidencyProgram?: string,
        public zipResidencyProgram?: string,
        public typeOfResidency?: string,
        public fullNameOfProgramDirectorResidencyProgram?: string,
        public isProgramSuccessfullyCompletedResidency?: boolean,
        public isNaResidencyPrograms?: boolean,
        public programSuccessfullyCompletedResidency?: string,
        public trainingFellowships?: BaseEntity[],
        public credentialApplicationForm?: BaseEntity,

        // search parameters
        public nameOfInstitutionTrainingFellowship?: string,
        public typeOfFellowship?: string

) {
        this.isProgramSuccessfullyCompletedResidency = true;
        this.isNaResidencyPrograms = false;
    }
}
