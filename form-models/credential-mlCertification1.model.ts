
import {BaseEntity} from '../base-entity';

export class CredentialMLCertification1 implements BaseEntity {
    constructor(
        public id?: number,
        public controlledRCertificationNumber?: string,
        public expiresDate2?: any,
        public isNaMLCertification1?: boolean,
        public certification1OrderId?: number,
        public credentialingAppPage1?: BaseEntity,
    ) {
        this.isNaMLCertification1 = false;
    }
}
