import {Component, EventEmitter, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {Patient} from "../../shared/model/patient.model";
import {Subscription} from 'rxjs/Subscription';
import {ResponseWrapper} from '../../shared/model/response-wrapper.model';
import {ActivatedRoute, Router} from '@angular/router';
import {ITEMS_PER_PAGE} from '../../shared/constants/pagination.constants';
import {PatientService} from "./patient.service";
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {GeneralService} from '../../shared/service/general.service';
import {DocumentVersionComponent} from '../document-version/document-version.component';
import {FormBuilder} from '@angular/forms';
import { PatientDetailComponent } from "./patient-detail.component";
import { PatientDialogComponent} from "./patient-dialog.component";
import {PatientDeleteDialogComponent} from "./patient-delete-dialog.component";
import {Position} from '../../shared/model/position.model';
import {EmployeeDialogComponent} from '../employee/employee-dialog.component';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.scss'],
  encapsulation: ViewEncapsulation.None
})
 export class PatientComponent implements OnInit {
    @ViewChild('component1') component1: DocumentVersionComponent;
    @Output() myEvent = new EventEmitter();

    currentAccount: any;
    patients: Patient[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;


  constructor(private patientService:PatientService, private modalService: NgbModal,
              private generalService: GeneralService,
              fb:FormBuilder,
              private activatedRoute: ActivatedRoute,
              private router: Router,) {
      this.page = 1;
      this.itemsPerPage = 10; // ITEMS_PER_PAGE;
      this.reverse = 'asc';
      // The field to be sorted
      this.predicate = 'firstname';
  }

    loadAll() {
        this.patientService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort:  this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/pages/patientComponent'], {queryParams:
            {
                page: this.page,
                // size: this.itemsPerPage,
                size:this.itemsPerPage,
               sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/pages/PatientComponent', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
  ngOnInit() {

      this.loadAll();
      // this.generalService.queryState()
      //     .subscribe((res: ResponseWrapper) => {
      //     }, (res: ResponseWrapper) => this.onError(res.json));

  }
    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;}


onSuccess(data, headers) {

        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.patients = data;

    }
    deletePatient(id) {
      //alert('id =' + id);
        const modalRef = this.modalService.open(PatientDeleteDialogComponent, {windowClass: 'lg' , backdrop : 'static'});
        modalRef.componentInstance.patientDelete = id;
        // callback of the popup
        modalRef.result.then(res => {
            console.log(res + ' the result of popup');
            this.loadAll()
        }, rejected => {
            console.log(' rejected: ' + rejected.toString());
            this.loadAll()
        });
    }

    addPatient() {
        const r: Patient =  new Patient(1, 'jj');
        const modalRef = this.modalService.open(PatientDialogComponent, {windowClass: 'lg' ,backdrop: 'static'});

        //callback of the popup
        modalRef.result.then(res => {
            console.log(res + ' the result of popup');
            this.loadAll()
        }, rejected => {
            console.log(' rejected: ' + rejected.toString());
            this.loadAll()
        });

    }


    private onError(error) {
        // this.jhiAlertService.error(error.message, null, null);
    }
    detailPatient(id) {
        //alert(' id =' + id);
        const modalRef = this.modalService.open(PatientDetailComponent, {windowClass: 'lg' , backdrop: 'static'});
        modalRef.componentInstance.patientDetail = id;
        // callback of the popup
        modalRef.result.then(res => {
            console.log(res + ' the result of popup');
            this.loadAll()
        }, rejected => {
            console.log(' rejected: ' + rejected.toString());
            this.loadAll()
        });
    }

    editPatient(id) {
        // alert(' in edit id =' + id);
        const modalRef = this.modalService.open(PatientDialogComponent, {windowClass: 'lg' , backdrop: 'static'});
        modalRef.componentInstance.patientEdit = id;
        // callback of the popup
        modalRef.result.then(res => {
            console.log(res + ' the result of popup');
            this.loadAll()
        }, rejected => {
            console.log(' rejected: ' + rejected.toString());
            this.loadAll()
        });
    }

}
