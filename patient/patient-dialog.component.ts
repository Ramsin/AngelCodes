import {Component, OnInit, OnDestroy, Input} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {PatientService} from "./patient.service";
import {DocumentTypeService} from '../document-type/document-type.service';
import {ResponseWrapper} from '../../shared/model/response-wrapper.model';
import {Patient} from "../../shared/model/patient.model";

import {GeneralService} from '../../shared/service/general.service';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Position} from '../../shared/model/position.model';

@Component({
    selector: 'app-patient-dialog',
    templateUrl: './patient-dialog.component.html',
    styleUrls: ['./patient-dialog.component.css'],
})
export class PatientDialogComponent implements OnInit {
    @Input() patientEdit;

    patient: Patient = new Patient();
    isSaving: boolean;
    firstname: AbstractControl;
    id: AbstractControl;
    lastname: AbstractControl;
    middlename: AbstractControl;
    postfix: AbstractControl;
    prefix: AbstractControl;
    dateofbirth: AbstractControl;
    age: AbstractControl;
    height: AbstractControl;
    weight: AbstractControl;
    gender: AbstractControl;
    displayName: AbstractControl;
    altEmail: AbstractControl;
    primaryEmail: AbstractControl;
    address1: AbstractControl;
    address2: AbstractControl;
    city: AbstractControl;
    country: AbstractControl;
    suitNumber: AbstractControl;
    emergencyContactAddress: AbstractControl;
    emergencyContactCity: AbstractControl;
    emergencyContactState: AbstractControl;
    emergencyContactZip: AbstractControl;
    emergencyContactPhone: AbstractControl;
    emergencyContactName: AbstractControl;
    cellPhone: AbstractControl;
    officePhone: AbstractControl;
    otherPhone: AbstractControl;
    homePhone: AbstractControl;
    primaryPhone: AbstractControl;
    fax: AbstractControl;
    zip: AbstractControl;
    emailAddress: AbstractControl;
    website: AbstractControl;
    form: FormGroup;
    stateDropdownSettings = {};
    selectedItems = [];
    documenttypes: DocumentType[];
    creationDateDp: any;
    duplicatedName: boolean = false;

    constructor(public activeModal: NgbActiveModal,
                private patientService: PatientService,
                private generalService: GeneralService,
                fb: FormBuilder) {

        this.form = fb.group({
            'firstname': ['', Validators.compose([Validators.required])],
            'id': ['', Validators.compose([])],
            'lastname': ['', Validators.compose([Validators.required])],
            'middlename': ['', Validators.compose([])],
            'emergencyContactName': ['', Validators.compose([])],
            'postfix': ['', Validators.compose([])],
            'prefix': ['', Validators.compose([])],
            'dateofbirth': ['', Validators.compose([Validators.required])],
            'age': ['', Validators.compose([Validators.required])],
            'weight': ['', Validators.compose([Validators.required])],
            'height': ['', Validators.compose([Validators.required])],
            'address1': ['', Validators.compose([Validators.required])],
            'address2': ['', Validators.compose([])],
            'city': ['', Validators.compose([])],
            'zip': ['', Validators.compose([])],
            'suitNumber': ['', Validators.compose([])],
            'emergencyContactAddress': ['', Validators.compose([])],
            'emergencyContactCity': ['', Validators.compose([])],
            'emergencyContactState': ['', Validators.compose([])],
            'emergencyContactZip': ['', Validators.compose([])],
            'emergencyContactPhone': ['', Validators.compose([])],
            'country': ['', Validators.compose([])],
            'cellPhone': ['', Validators.compose([])],
            'officePhone': ['', Validators.compose([])],
            'homePhone': ['', Validators.compose([])],
            'otherPhone': ['', Validators.compose([])],
            'primaryPhone': ['', Validators.compose([])],
            'fax': ['', Validators.compose([])],
            'altEmail': ['', Validators.compose([])],
            'website': ['', Validators.compose([])],
            'primaryEmail': ['', Validators.compose([])],
            'displayName': ['', Validators.compose([])],
            'gender': ['', Validators.compose([Validators.required])],
            'emailAddress': ['', Validators.compose([])]
        });

        this.firstname = this.form.controls['firstname'];
        this.id = this.form.controls['id'];
        this.lastname = this.form.controls['lastname'];
        this.middlename = this.form.controls['middlename'];
        this.emergencyContactName = this.form.controls['emergencyContactName'];
        this.postfix = this.form.controls['postfix'];
        this.prefix = this.form.controls['prefix'];
        this.dateofbirth = this.form.controls['dateofbirth'];
        this.age = this.form.controls['age'];
        this.height = this.form.controls['height'];
        this.weight = this.form.controls['weight'];
        this.gender = this.form.controls['gender'];
        this.altEmail = this.form.controls['altEmail'];
        this.primaryEmail = this.form.controls['primaryEmail'];
        this.displayName = this.form.controls['displayName'];
        this.website = this.form.controls['website'];
        this.address1 = this.form.controls['address1'];
        this.address2 = this.form.controls['address2'];
        this.city = this.form.controls['city'];
        this.country = this.form.controls['country'];
        this.zip = this.form.controls['zip'];
        this.suitNumber = this.form.controls['suitNumber'];
        this.emergencyContactAddress = this.form.controls['emergencyContactAddress'];
        this.emergencyContactCity = this.form.controls['emergencyContactCity'];
        this.emergencyContactZip = this.form.controls['emergencyContactZip'];
        this.emergencyContactState = this.form.controls['emergencyContactState'];
        this.emergencyContactPhone = this.form.controls['emergencyContactPhone'];
        this.cellPhone = this.form.controls['cellPhone'];
        this.officePhone = this.form.controls['officePhone'];
        this.otherPhone = this.form.controls['otherPhone'];
        this.homePhone = this.form.controls['homePhone'];
        this.primaryPhone = this.form.controls['primaryPhone'];
        this.fax = this.form.controls['fax'];
        this.emailAddress = this.form.controls['emailAddress'];
    }

    loadPatient(id) {
        //  alert('id=' + id);
        this.patientService.find(id).subscribe((patient) => {
            this.patient = patient;
        });

    }

    ngOnInit() {
        // alert(' selecteStateList==' + this.selecteStateList);
        // this.isSaving = false;
        // this.documentTypeService.query()
        //     .subscribe((res: ResponseWrapper) => {
        //         this.documenttypes = res.json;
        //     }, (res: ResponseWrapper) => this.onError(res.json));
        if (this.patientEdit) {

            this.loadPatient(this.patientEdit);
        }
        this.stateDropdownSettings = {
            singleSelection: false,
            text: 'Select State',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: 'myclass custom-class'
        };
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.patient.id !== undefined) {
            this.subscribeToSaveResponse(
                this.patientService.update(this.patient));
        } else {
            this.subscribeToSaveResponse(
                this.patientService.create(this.patient));
        }
    }

    private subscribeToSaveResponse(result: Observable<Patient>) {
        result.subscribe((res: Position) =>
            this.onSaveSuccess(res), (res: Response) => {
            this.onSaveError();
            if (res.status === 400) {
                this.duplicatedName = true;
            }
            this.onSaveError()
        });
    }

    private onSaveSuccess(result: Patient) {
        // this.eventManager.broadcast({ name: 'clinicListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        // this.jhiAlertService.error(error.message, null, null);
    }


    trackDocumentTypeById(index: number, item: DocumentType) {
        // return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }


    onItemSelect(item: any) {
        console.log(item);
        console.log(this.selectedItems);
    }

    OnItemDeSelect(item: any) {
        console.log(item);
        console.log(this.selectedItems);
    }

    onSelectAll(items: any) {
        console.log(items);
    }

    onDeSelectAll(items: any) {
        console.log(items);
    }

    validationReset() {
        this.duplicatedName = false;
    }
}

