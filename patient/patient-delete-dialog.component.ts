import { Component, OnInit, OnDestroy , Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PatientService } from "./patient.service";
import {Patient} from "../../shared/model/patient.model";

@Component({
    selector: 'app-patient-delete-dialog',
    templateUrl: './patient-delete-dialog.component.html'
})
export class PatientDeleteDialogComponent {
    @Input() patientDelete;
    patient: Patient;

    constructor(
        private patientService: PatientService,
        public activeModal: NgbActiveModal,
        // private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
       // alert("id=" + id);
        this.patientService.delete(id).subscribe((response) => {
            this.activeModal.dismiss(true);
        });
    }
}
