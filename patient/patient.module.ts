import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import {CommonModule} from '@angular/common';
import {HttpModule} from '@angular/http';
import {AgGridModule} from 'ag-grid-angular';
import {HttpClientModule} from '@angular/common/http';
import {MultiselectDropdownModule} from 'angular-2-dropdown-multiselect';
import {PatientComponent} from './patient.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormsModule, ReactiveFormsModule, Validators} from '@angular/forms';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {NgJhipsterModule} from 'ng-jhipster';
import {PatientDetailComponent}  from './patient-detail.component';
import {PatientService} from './patient.service';
import {PatientDialogComponent} from './patient-dialog.component';
import {PatientDeleteDialogComponent} from './patient-delete-dialog.component'
import {AppRoutingModule} from './patient-routing.module';
import {customHttpProvider} from '../../shared/interceptor/http.provider';
import {GeneralService} from '../../shared/service/general.service';

export const routes = [
    {
        path: '', component: PatientComponent, pathMatch: 'full',
        // children: [
        //     {path: '', redirectTo: '/personal', pathMatch: 'full'},
        //
        //     // {path: '/personal', component: PersonalComponent}
        // ]
    }
];

@NgModule({
    imports: [
        CommonModule, HttpModule, HttpClientModule, RouterModule.forChild(routes), FormsModule, NgbModule.forRoot(), MultiselectDropdownModule
        , AngularMultiSelectModule, NgJhipsterModule,
        ReactiveFormsModule,
      AppRoutingModule
    ],
    declarations: [
        PatientComponent, PatientDetailComponent, PatientDialogComponent, PatientDeleteDialogComponent
    ],
    entryComponents: [
         PatientDetailComponent, PatientDialogComponent, PatientDeleteDialogComponent
    ],
    providers: [  PatientService, customHttpProvider(), GeneralService

    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PatientModule {



}
