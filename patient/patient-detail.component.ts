import {Component, OnInit, OnDestroy, Input} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { PatientService } from "./patient.service";
import {Patient} from "../../shared/model/patient.model";
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-patient-detail',
    templateUrl: './patient-detail.component.html',
    styleUrls: ['./patient-detail.component.css'],
})
export class PatientDetailComponent implements OnInit, OnDestroy {
    @Input() patientDetail;

    patient: Patient;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private patientService: PatientService,
        private route: ActivatedRoute,
        public activeModal: NgbActiveModal
    ) {}


    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(this.patientDetail);
        });
        // this.registerChangeInClinics();
    }

    load(id) {
        this.patientService.find(id).subscribe((patient) => {
            this.patient = patient;
        });
    }
    // previousState() {
    //     window.history.back();
    // }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    // registerChangeInClinics() {
    //
    // }
    clear() {
        this.activeModal.dismiss('cancel');
    }

}
