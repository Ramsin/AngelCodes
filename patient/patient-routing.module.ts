import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
// import {PersonalComponent} from '../surgery-note/personal/personal.component';
// import {WorkflowGuard} from '../surgery-note/workflow/workflow-guard.service';
import {EmployeeService} from '../employee/employee.service';
import {PatientComponent} from './patient.component';

export const appRoutes: Routes = [

    // 1st Route
    // { path: 'personal',  component: PersonalComponent },
    // { path: 'demographics',  component: DemographicsComponent , data: { breadcrumb: 'Demographics' }},
    // // { path: 'demographics/:id/:clinic',  component: DemographicsComponent , data: { breadcrumb: 'Demographics' }},
    // { path: 'demographics/:patient/:clinic/:id',  component: DemographicsComponent , data: { breadcrumb: 'Demographics' }},
    // { path: 'preoperative/:patient/:clinic/:id',  component: PreOperativeComponent , data: { breadcrumb: 'Pre Operative' }},
    // { path: 'allergies/:patient/:clinic/:id',  component: AllergiesComponent , data: { breadcrumb: 'Allergies & Medication ' }},
    // { path: 'tumescent/:patient/:clinic/:id',  component: TumescentComponent , data: { breadcrumb: 'Tumescent Anesthesia' }},
    // { path: 'verification/:patient/:clinic/:id',  component: VerificationComponent , data: { breadcrumb: 'Verification' }},
    // { path: 'aspirate/:patient/:clinic/:id',  component: AspirateComponent , data: { breadcrumb: 'Aspirate' }},
    // { path: 'surgeonAffrimation/:patient/:clinic/:id',  component: SurgeonAffrimationComponent , data: { breadcrumb: 'Surgeon Affrimation' }},
    // { path: 'surgeryDetail/:patient/:clinic/:id',  component: SurgeryDetailComponent , data: { breadcrumb: 'Surgery Detail' }},
    // { path: 'attachment/:patient/:clinic/:id',  component: AttachmentComponent , data: { breadcrumb: 'VS Attachment' }},
    // { path: 'recoveryDischarge/:patient/:clinic/:id',  component: RecoveryDischargeComponent , data: { breadcrumb: 'Recovery & Discharge' }},
    // { path: 'vitalSigns/:patient/:clinic/:id',  component: VitalSignsComponent , data: { breadcrumb: 'Select Vital Signs' }},
    // { path: 'safetyTimeOut/:patient/:clinic/:id',  component: SafetyTimeOutComponent , data: { breadcrumb: 'Safety Time Out' }},
    // { path: 'diabetic/:patient/:clinic/:id',  component: DiabeticComponent , data: { breadcrumb: 'Diabetic' }},
    // 2nd Route
    // { path: 'work',  component: WorkComponent, canActivate: [WorkflowGuard] , data: { breadcrumb: 'Pre Operative' }},
    // // 3rd Route
    // { path: 'address',  component: AddressComponent, canActivate: [WorkflowGuard] },
    // // 4th Route
    // { path: 'address2',  component: Address2Component, canActivate: [WorkflowGuard] },
    // { path: 'address3',  component: Address3Component, canActivate: [WorkflowGuard] },
    // { path: 'result',  component: ResultComponent, canActivate: [WorkflowGuard] },
    // 5th Route
    { path: '',   redirectTo: '/pages/patient', pathMatch: 'full' },
    { path: 'patient',   redirectTo: '/pages/patient', pathMatch: 'full' },
    // 6th Route
    { path: '**', component: PatientComponent}
];

@NgModule({
    imports: [RouterModule.forChild(appRoutes)],
    exports: [RouterModule],
    providers: [EmployeeService,]
})

export class AppRoutingModule {}





//
//
// import { NgModule }             from '@angular/core';
// import { Routes, RouterModule } from '@angular/router';
//
// import { PersonalComponent }    from './personal/personal.component';
// import { WorkComponent }        from './work/work.component';
// import { AddressComponent }     from './address/address.component';
// import { ResultComponent }      from './result/result.component';
//
// import { WorkflowGuard }        from './workflow/workflow-guard.service';
// import { WorkflowService }      from './workflow/workflow.service';
// import {Address2Component} from './address2/address2.component';
//
//
// export const appRoutes: Routes = [
//     // 1st Route
//     { path: 'personal',  component: PersonalComponent },
//     // 2nd Route
//     { path: 'work',  component: WorkComponent, canActivate: [WorkflowGuard] },
//     // 3rd Route
//     { path: 'address',  component: AddressComponent, canActivate: [WorkflowGuard] },
//     // 4th Route
//     { path: 'address2',  component: Address2Component, canActivate: [WorkflowGuard] },
//
//     { path: 'result',  component: ResultComponent, canActivate: [WorkflowGuard] },
//     // 5th Route
//     { path: '',   redirectTo: '/personal', pathMatch: 'full' },
//     // 6th Route
//     { path: '**', component: PersonalComponent }
// ];
//
// @NgModule({
//     imports: [RouterModule.forRoot(appRoutes, { useHash: true} )],
//     exports: [RouterModule],
//     providers: [WorkflowGuard]
// })
//
// export class SurgeryNoteRoutingModule {}
