import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {BaseRequestOptions, Http, RequestOptions, Response, URLSearchParams} from '@angular/http';
import {Employee} from '../../shared/model/employee.model';
import {GeneralService} from '../../shared/service/general.service';
import {EmployeeService} from '../employee/employee.service';
import {ResponseWrapper} from '../../shared/model/response-wrapper.model';
import {FormManagementService} from './form.management.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
//import {IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts} from 'angular-2-dropdown-multiselect';


@Component({
    selector: 'app-assignForm',
    templateUrl: './assignForm.popup.component.html',
  //  styleUrls: ['./profile.component.scss'],
 //   encapsulation: ViewEncapsulation.None
})
export class AssignFormPopupComponent implements OnInit {
   @Input() employees: Employee[];
    selecteEmployees: Employee[];
 //  @Input() public employeesList = [];
    public employeesList = []
   @Input() public formName: any;
    itemsPerPage: number = 10;
    employeesDropdownSettings = {};
    ckeditorContent: string =' Context ';
 //   readonly bufferSize: number = 10;
    loading = false;

  //
  //
  //   msSettings: IMultiSelectSettings = {
  //       pullRight: false,
  //       enableSearch: true,
  //       checkedStyle: 'fontawesome',
  //       buttonClasses: 'btn btn-default btn-secondary',
  //       selectionLimit: 0,
  //       closeOnSelect: false,
  //       autoUnselect: false,
  //       showCheckAll: true,
  //       showUncheckAll: true,
  //       fixedTitle: true,
  //       dynamicTitleMaxItems: 3,
  //       maxHeight: '300px',
  //       isLazyLoad: true,
  //       loadViewDistance: 1,
  //       stopScrollPropagation: true,
  //       selectAddedValues: true
  //   };
  //
  //   myTexts: IMultiSelectTexts = {
  //       checkAll: 'Select all',
  //       uncheckAll: 'Unselect all',
  //       checked: 'item selected',
  //       checkedPlural: 'items selected',
  //       searchPlaceholder: 'Find',
  //       searchEmptyResult: 'Loading...',
  //       searchNoRenderText: 'Type in search box to see results...',
  //       defaultTitle: 'Select',
  //       allSelected: 'All selected',
  //   };
  //
  //   loadingTexts:  IMultiSelectTexts = Object.assign({},this.myTexts);
  //   noresultTexts:  IMultiSelectTexts = Object.assign({},this.myTexts);
  //
  // //  myOptions: IMultiSelectOption[] = this.employees;
  //   initLoadNum = 20;
  //   lastFilter: string = '';
  //   lazyLoadNum: number = 10;
  //   searchOption: EmployeeOption[] = new EmployeeOption[10]();





    constructor(private spinner: NgxSpinnerService , private modalService: NgbActiveModal, private http: Http, private toasterService: ToastrService,
                private generalService: GeneralService, private formService: FormManagementService) {

    }


    ngOnInit() {

        this.employeesDropdownSettings = {
            singleSelection: false,
            text: 'Select Employess',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            lazyLoading: true,
            loading: true,
            classes: 'myclass custom-class'
        };

     // this.myOptions = this.employees
       // this.employees.entries()

    }

    clear() {
        this.modalService.dismiss('cancel');
    }

    save(){
     //   alert(' --- >>>>>>  ' +  JSON.stringify(this.selecteEmployees))
      //  this.rejectEmail(this.employee).subscribe(res => {console.log(' this is the output ' +  res)})
        this.spinner.show();
        this.formService.assignFormsToEmployees(this.selecteEmployees, this.formName
        ).map(res => res).subscribe(res =>
        {
        this.spinner.hide();
        this.toasterService.success('The Form has been assigned successfully');
        this.modalService.close();
        },
                rej => {this.spinner.hide();
                    this.toasterService.error('Problem assigning Form');
                    this.modalService.close();
                })
    }

    onItemSelect(event: any){
    }

    OnItemDeSelect(event: any){
    }

    onSelectAll(event: any){
    }

    onDeSelectAll(event: any){
    }

    fetchMore(event: any) {
        //   alert('this event ------ >>>>>>  ' + JSON.stringify(event))
        console.log(' ----- >>>>>>>   ' + JSON.stringify(event))
       // event.end - event.start

        // this.generalService.queryEmployee({
        //     page: 0,
        //     size: this.itemsPerPage,
        //     //        sort: this.sort()
        // })
        //     .subscribe((res: ResponseWrapper) => {
        //         // this.employees = res.json;
        //         this.employeesList.push(res.json)
        //     }, (res: ResponseWrapper) => this.onError(res.json));


//        if (event.end === this.employeesList.length - 1) {
 //           this.loading = true;
 //           alert(' This is the test ------ >>>>>>>>>   ')

            // this.appService.getChunkData(this.itemList.length, this.bufferSize).then(chunk => {
            //     this.itemList = this.itemList.concat(chunk);
            //     this.loading = false;
            // }, () => this.loading = false);
 //       }
    }

    // filter(event: any){
    //     //alert('  --- >>>>>>   key up ' + JSON.stringify(event) + ' inserted value : ' + event.target.value + " 000 ---- >> " + event.keyCode)
    //     if(event.keyCode == 13) {
    //         this.spinner.show();
    //         this.generalService.searchEmployeeByNameAndFamily(event.target.value).subscribe(res => {
    //             this.spinner.hide();
    //             //alert(' ----- >>>>>>>> ' + res.json)
    //             this.employeesList = <Employee[]>res.json;
    //         })
    //     }
    //     }

        test(event: any){
        alert(' ------ >>>> >>>>> >>>>>>>  ' + JSON.stringify(event))
        }

    private rejectEmail(employee: Employee){

        // sendHtmlToserver(html: string): Observable<Response> {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        //  let params = new URLSearchParams();
        //  params.set("html", html);

     //   alert('test');
        let option: RequestOptions;
        const options: BaseRequestOptions = new BaseRequestOptions();
        const params: URLSearchParams = new URLSearchParams();
        params.set('emailContent',this.ckeditorContent); ///'emailContent', '"' + 'This is the email content' + '"')
        params.set('employeeId' , employee.id.toString() )
        options.params = params;

        return this.http.post('http://localhost:8080/api/rejectProfile',  params).map((res: Response) =>
        {const jsonResponse = res;
            this.modalService.close();
            return jsonResponse;
        });
        // }



    }

    // sort() {
    //     const result = ['itemName,asc'];
    //     // const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    //     // if (this.predicate !== 'id') {
    //     //     result.push('id');
    //     // }
    //     return result;
    // }

    onError(msg : any){

    }

    loadData(event: any){
        alert(' This is the test' + ' uuuuuu  ');
    }

}
