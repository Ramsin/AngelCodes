import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {ResponseWrapper} from '../../shared/model/response-wrapper.model';
import {ActivatedRoute, Router} from '@angular/router';
import {ITEMS_PER_PAGE} from '../../shared/constants/pagination.constants';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {GeneralService} from '../../shared/service/general.service';
import {DocumentVersionComponent} from '../document-version/document-version.component';
import {BaseRequestOptions, Http, RequestOptions, Response, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService, JhiDataUtils, JhiDateUtils} from 'ng-jhipster';
import {AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Position} from '../../shared/model/position.model';
// import {Car} from "../../shared/model/form-models/car.model";
import {CredentialAOSM} from '../../shared/model/form-models/credential-aosm.model';
// import {Forms} from '../../shared/model/form.model';
import {Page1} from '../../shared/model/form-models/credentialing-app-page-1.model';
import {Page2} from '../../shared/model/form-models/credentialing-app-page-2.model';
import {forEach} from '@angular/router/src/utils/collection';
import {BaseEntity} from '../../shared/model/base-entity';
import {until} from 'selenium-webdriver';
import alertIsPresent = until.alertIsPresent;
import {Page3} from "../../shared/model/form-models/credentialing-app-page-3.model";
import {TrainingFellowships} from "../../shared/model/form-models/training-fellowships.model";
import {Page5} from "../../shared/model/form-models/credentialing-app-page-5.model";

import {HospitalUniversityAffiliation} from "../../shared/model/form-models/hospital-university-affiliation.model";
import {Page4} from 'app/shared/model/form-models/credentialing-app-page-4.model';
import {Page6} from "../../shared/model/form-models/credentialing-app-page-6.model";
import {Page7} from "../../shared/model/form-models/credentialing-app-page-7.model";
import {Page8} from "../../shared/model/form-models/credentialing-app-page-8.model";
import {Page9} from 'app/shared/model/form-models/credentialing-app-page-9.model';
import {DatePipe} from '@angular/common';
import {isNullOrUndefined} from 'util';
import {CredentialApplicationForm} from '../../shared/model/form-models/credential-application-form.model';
import {EmployeeService} from '../employee/employee.service';
import {Employee} from '../../shared/model/employee.model';
import {V} from '@angular/core/src/render3';
import {NgxSpinnerService} from 'ngx-spinner';
import {FormManagementService} from './form.management.service';
import {AppForm2} from '../../shared/model/form-models/formTow-model/pr-req-and-app-page-1.model';
import {EmployeeDialogComponent} from '../employee/employee-dialog.component';
import {AssignFormPopupComponent} from './assignForm.popup.component';
import {AssignFormPositionPopupComponent} from './assignFormPosition.popup.component';
import {Clinic} from '../../shared/model/clinic.model';
import {PositionService} from '../position/position.service';
import {ClinicService} from '../clinic/clinic.service';
import {AssignFormClinicPopupComponent} from './assignFormClinic.popup.component';
import {Form_Constants} from '../../shared/model/Form_Constants';
// import * as html2canvas from 'html2canvas'
// import * as jsPDF from 'jspdf';

@Component({
  selector: 'app-form-management',
  templateUrl: './form.management.component.html',
  styleUrls: ['./form.management.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class FormManagementComponent implements OnInit {

    form2s: AppForm2[];
    employees: Employee[];
    positionList = [];
    positions: Position[];
    clinics: Clinic[];
    public employeesList = [];
    itemsPerPage: number = 1000;
    formNames: any =
        [{id: 1, itemName: 'Credential-Application-Form'}, {id: 2, itemName: 'Privilege-Request-and-Approval-Obalon'}, {id: 3, itemName: 'Privilege-Request-and-Approval-Physician'},
        {id: 4, itemName: 'Reappointment-Privilege-Form'}, {id: 5, itemName: Form_Constants.Form_Job_Description}, {id: 6, itemName: Form_Constants.Form_Marketing_Question}
        ,{id: 7, itemName: Form_Constants.Form_Policy_Procedure}, {id: 8, itemName: Form_Constants.Form_Statement_Health}, {id: 9, itemName: Form_Constants.Form_S_AC}, {id:10, itemName: Form_Constants.Form_Contact_Information}
        ];


    constructor(private spinner: NgxSpinnerService ,private formManagmenetService : FormManagementService, private modalService: NgbModal, private generalService:GeneralService, private positionService: PositionService, private clinicService: ClinicService) {

    }

    ngOnInit() {

      this.spinner.show();

      let   employee: Employee = new Employee();

    //  employee.id = 96836;

        // this.formManagmenetService.getListOfabvls(employee).subscribe(res => { //this.form2s = (AppForm2[])res;
        //
        //     this.form2s = <AppForm2[]>res.json;
        //     // let aosmArrayTemp: any = <AppForm2[]>res.json;
        //     this.form2s = <AppForm2[]>res;
        //    // alert(' this is the lenght of the form2   ' + (<AppForm2[]>res))
        // });

        this.clinicService.query({
            page: 0,
            size: 200
        }).subscribe(pos => {this.clinics = pos.json;})

        this.positionService.query({
            page: 0,
            size: 200
        }).subscribe(pos => {this.positionList = pos.json;})

        this.generalService.query({
            page: 0,
            size: this.itemsPerPage,
            // sort: this.sort()
        }).subscribe((res: ResponseWrapper) => {
            this.employees = res.json;
            this.spinner.hide();
        }, (res: ResponseWrapper) =>{
            this.spinner.hide();
            this.onError(res.json)});

    }

    assignPopUp(formName: any)  {

        //    this.modalService.open()
        const modalRef = this.modalService.open(AssignFormPopupComponent  , {windowClass: 'lg', backdrop: 'static'});
        for (let x = 0; x < this.employees.length; x++) {
            const temp = {id: this.employees[x].id, itemName: this.employees[x].firstName + ' - '  + this.employees[x].lastName, };
            this.employeesList[x] = temp;
        }
        modalRef.componentInstance.employeesList = this.employeesList;
        modalRef.componentInstance.formName = formName

    }

    assignPositionPopUp(formName: any){

        const modalRef = this.modalService.open(AssignFormPositionPopupComponent  , {windowClass: 'lg', backdrop: 'static'});
        // for (let x = 0; x < this.positions.length; x++) {
        //     const temp = {id: this.positions[x].id, itemName: this.positions[x].itemName + ' - '  + this.employees[x].lastName, };
        //     this.positionList[x] = temp;
        // }
        modalRef.componentInstance.positionsList = this.positionList;
        modalRef.componentInstance.formName = formName


    }

    assignClinicPopUp(formName: any){

        const modalRef = this.modalService.open(AssignFormClinicPopupComponent  , {windowClass: 'lg', backdrop: 'static'});
        // for (let x = 0; x < this.employees.length; x++) {
        //     const temp = {id: this.employees[x].id, itemName: this.employees[x].firstName + ' - '  + this.employees[x].lastName, };
        //     this.employeesList[x] = temp;
        // }
        modalRef.componentInstance.clinicsList = this.clinics;
        modalRef.componentInstance.formName = formName


    }

    onError(json : any){

    }
}
