import {NgModule, CUSTOM_ELEMENTS_SCHEMA, Directive} from '@angular/core';
import { RouterModule } from '@angular/router';
import {CommonModule} from '@angular/common';
import {HttpModule} from '@angular/http';
import {AgGridModule} from 'ag-grid-angular';
import {HttpClientModule} from '@angular/common/http';
//import {MultiselectDropdownModule} from 'angular-2-dropdown-multiselect';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormsModule, ReactiveFormsModule, Validators} from '@angular/forms';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {NgJhipsterModule} from 'ng-jhipster';
import {FormManagementComponent} from './form.management.component';
import {FormManagementService} from './form.management.service';
import {AssignFormPopupComponent} from './assignForm.popup.component';
import {AssignFormPositionPopupComponent} from './assignFormPosition.popup.component';
import {AssignFormClinicPopupComponent} from './assignFormClinic.popup.component';
import {ClinicService} from '../clinic/clinic.service';
import {MultiselectDropdownModule} from 'angular-2-dropdown-multiselect';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';







@NgModule({
    imports: [
        CommonModule, HttpModule, HttpClientModule, RouterModule, FormsModule, NgbModule.forRoot() , MultiselectDropdownModule
        , AngularMultiSelectModule, NgJhipsterModule,
        ReactiveFormsModule,
        Ng2SmartTableModule,
        AgGridModule.withComponents(
            []
        )
    ],
    declarations: [
        FormManagementComponent, AssignFormPopupComponent, AssignFormPositionPopupComponent, AssignFormClinicPopupComponent
    ],
    entryComponents: [
        FormManagementComponent, AssignFormPopupComponent, AssignFormPositionPopupComponent, AssignFormClinicPopupComponent
    ],
    providers: [ FormManagementService, ClinicService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FormManagementModule {

}
