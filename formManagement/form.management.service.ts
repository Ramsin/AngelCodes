import {Injectable} from '@angular/core';
import {BaseRequestOptions, Http, RequestOptions, Response, URLSearchParams} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import {JhiDateUtils} from 'ng-jhipster';
import {SERVER_API_URL} from '../../app.constants';
import {Employee} from '../../shared/model/employee.model';
import {Clinic} from '../../shared/model/clinic.model';


@Injectable()
export class FormManagementService {

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }




    getListOfabvls(employee : Employee): Observable<any> {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');


        let option: RequestOptions;
        const options: BaseRequestOptions = new BaseRequestOptions();
        const params: URLSearchParams = new URLSearchParams();
        params.set('employeeId',   employee.id.toString() )
        options.params = params;

          //return this.http.post(SERVER_API_URL + 'api/getAllPrReqAndAppAbvlPage1OfUser', params)
        return this.http.post(SERVER_API_URL + 'api/getUSerWithAllForms', params)
            .map((res: Response) => {
                const jsonResponse = res.json();
                return jsonResponse;
            });
    }


    assignFormsToEmployees(employee: Employee[], formName: any): Observable<any> {

       // alert(' this is the employess === >>>>  ' + JSON.stringify(employee))



        return this.http.post(SERVER_API_URL + 'api/assignform/' + formName , employee).map(res => res)

    }

    assignFormsToPositions(positions: Position[], formName: any): Observable<any>{

        return this.http.post(SERVER_API_URL + 'api/assignform/position/' + formName , positions).map(res => res)

    }

    assignFormsToClinics(clinics: Clinic[], formName: any): Observable<any>{

        return this.http.post(SERVER_API_URL + 'api/assignform/clinic/' + formName , clinics).map(res => res)

    }

}
