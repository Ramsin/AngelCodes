import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {BaseRequestOptions, Http, RequestOptions, Response, URLSearchParams} from '@angular/http';
import {Employee} from '../../shared/model/employee.model';
import {GeneralService} from '../../shared/service/general.service';
import {EmployeeService} from '../employee/employee.service';
import {ResponseWrapper} from '../../shared/model/response-wrapper.model';
import {FormManagementService} from './form.management.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {PositionService} from '../position/position.service';
import {Clinic} from '../../shared/model/clinic.model';
import {ToastrService} from 'ngx-toastr';


@Component({
    selector: 'app-assignFormClinic',
    templateUrl: './assignFormClinic.popup.component.html',
  //  styleUrls: ['./profile.component.scss'],
 //   encapsulation: ViewEncapsulation.None
})
export class AssignFormClinicPopupComponent implements OnInit {
   @Input() clinics: Clinic[];
    selectedClinics: Clinic[];
   @Input() public clinicsList = [];
   @Input() public formName: any;
    itemsPerPage: number = 1000;
    clinicsDropdownSettings = {};


    constructor(private spinner: NgxSpinnerService , private toasterService: ToastrService , private modalService: NgbActiveModal, private http: Http, private generalService: GeneralService, private formService: FormManagementService, private positionService: PositionService) {

    }


    ngOnInit() {

        this.clinicsDropdownSettings = {
            singleSelection: false,
            text: 'Select Clinics',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: 'myclass custom-class'
        };


     }

    clear() {
        this.modalService.dismiss('cancel');
    }

    save(){

        this.spinner.show();
    //    alert(this.formName + ' 0000000000 ');
        this.formService.assignFormsToClinics(this.selectedClinics, this.formName
        ).map(res => res).subscribe(res =>
        {
           this.spinner.hide();
            this.toasterService.success('The Form has been assigned successfully');
            this.modalService.close();
        },
                rej => {
                this.spinner.hide();
                this.toasterService.error('Problem assigning Form');
                this.modalService.close();
                })
    }


    onItemSelect(){
    }

    OnItemDeSelect(){
    }

    onSelectAll(){
    }

    onDeSelectAll(){
    }




}
