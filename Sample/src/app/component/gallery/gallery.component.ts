import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
// import {config} from "rxjs";



@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss'],
})
export class GalleryComponent implements OnInit {
  showNavigationArrows = false;
  showNavigationIndicators = false;
  public imageSrc = 'https://image.freepik.com/free-icon/upload-arrow_318-26670.jpg';

  constructor(config: NgbCarouselConfig) {
    // customize default values of carousels used by this component tree
    config.interval = 10000;
    config.wrap = false;
    config.keyboard = false;

    config.showNavigationArrows = true;
    config.showNavigationIndicators = true;

  }

  ngOnInit() {
    //.....start carousel...............
    $( '.img-wrapper' ).hover(
      function() {
        $(this).find('.img-overlay').animate({opacity: 1}, 600);
      }, function() {
        $(this).find('.img-overlay').animate({opacity: 0}, 600);
      }
    );

// ... start gallery Lightbox
    let $overlay = $('<div id="overlay" style="background: rgba(0,0,0,0.7);width: 100%;height: 100%;position: fixed;top: 0;left: 0;display: flex; justify-content: center;align-items: center; z-index: 999;-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;"></div>');
    const $image = $('<img style=" margin: 0; width: 80%;height: auto;object-fit: contain;padding: 5%;@media screen and (min-width:768px) {  width: 60%;} @media screen and (min-width:1200px) {  width: 50%;}">' );
    const $prevButton = $('<div id="prevButton" style="color: #fff;font-size: 2em;transition: opacity 0.8s;margin-right: 20px;@media screen and (min-width:768px) {font-size: 3em;};"><i class="fa fa-chevron-left"></i></div>');
    const $nextButton = $('<div id="nextButton" style="color: #fff;font-size: 2em;transition: opacity 0.8s;margin-left: 20px;@media screen and (min-width:768px) {font-size: 3em; }"><i class="fa fa-chevron-right"></i></div>');
    const $exitButton = $('<div id="exitButton" style="color: #fff;font-size: 2em;transition: opacity 0.8s;position: absolute;top: 15px;right: 15px ;@media screen and (min-width:768px) {font-size: 3em;}"><i class="fa fa-times"></i></div>');

// Add overlay
    $overlay.append($image).prepend($prevButton).append($nextButton).append($exitButton);
    $('#gallery').append($overlay);

// Hide overlay on default
    $overlay.hide();

// When an image is clicked
    $('.img-overlay').click(function(event) {

      // Prevents default behavior
      event.preventDefault();
      // Adds href attribute to variable
      const imageLocation = $(this).prev().attr('href');
      // Add the image src to $image
      $image.attr('src', imageLocation);
      // Fade in the overlay
      $overlay.fadeIn('slow');
    });

// When the overlay is clicked
    $overlay.click(function() {
     // alert('hiii')
      // Fade out the overlay
      $(this).fadeOut('slow');
    });

// When next button is clicked
    $nextButton.click(function(event) {
      // Hide the current image
      $('#overlay img').hide();
      // Overlay image location
      const $currentImgSrc = $('#overlay img').attr('src');
      // Image with matching location of the overlay image
      const $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
      // Finds the next image
      const $nextImg = $($currentImg.closest('.image').next().find('img'));
      // All of the images in the gallery
      const $images = $('#image-gallery img');
      // If there is a next image
      if ($nextImg.length > 0) {
        // Fade in the next image
        $('#overlay img').attr('src', $nextImg.attr('src')).fadeIn(800);
      } else {
        // Otherwise fade in the first image
        $('#overlay img').attr('src', $($images[0]).attr('src')).fadeIn(800);
      }
      // Prevents overlay from being hidden
      event.stopPropagation();
    });

// When previous button is clicked
    $prevButton.click(function(event) {
      // Hide the current image
      $('#overlay img').hide();
      // Overlay image location
      const $currentImgSrc = $('#overlay img').attr('src');
      // Image with matching location of the overlay image
      const $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
      // Finds the next image
      const $nextImg = $($currentImg.closest('.image').prev().find('img'));
      // Fade in the next image
      $('#overlay img').attr('src', $nextImg.attr('src')).fadeIn(800);
      // Prevents overlay from being hidden
      event.stopPropagation();
    });

// When the exit button is clicked
    $exitButton.click(function() {
      // Fade out the overlay
      $('#overlay').fadeOut('slow');
    });
    //........start multi select...............

    function buildDropDown(name, index, array) {
      const opt = document.createElement('option');
      opt.value = name;
      opt.innerHTML = name;
      dropdown.appendChild(opt);
    }
    const container = document.getElementById('dynaflex'),
      array = container.getElementsByTagName('figure'),
      selectionBar = document.getElementById('selectionbar'),
      categories = [];
    for (let i = 0; i < array.length ; i++) {
      categories[i] = array[i].dataset.group;
    }
    const unique = categories.filter(function(item, i, ar) { return ar.indexOf(item) === i; });
    unique.reverse();
    unique.unshift('all');

    const dropdown = document.createElement('select');
    dropdown.id = 'categories';
    const dropdownLabel = document.createElement('label');
    dropdownLabel.htmlFor = dropdown.id;
    dropdownLabel.innerHTML = 'Show : ';
    unique.forEach(buildDropDown);
    selectionBar.appendChild(dropdownLabel);
    selectionBar.appendChild(dropdown);

    dropdown.addEventListener('change', function() {
        console.log('Change');
        if (this.value === 'all') {
          for (let i = 0; i < array.length; ++i) {	array[i].classList.remove('diminish');
          }
        } else {
          const hide = document.querySelectorAll('#dynaflex figure:not([data-group="' + this.value + '"])');
          for (let j = 0; j < hide.length; ++j) {
            hide[j].classList.add('diminish');
          }
          const show = document.querySelectorAll('#dynaflex figure[data-group="' + this.value + '"]');
          for (let k = 0; k < show.length; ++k) {	show[k].classList.remove('diminish');
          }
        }
      }
    );

    // drag and drop component........................
    // Call & init
    $(document).ready(function() {
      var each = $('.ba-slider').each(function() {
        var cur = $(this);
        // Adjust the slider
        var width = cur.width() + 'px';
        cur.find('.resize img').css('width', width);
        // Bind dragging events
        drags(cur.find('.handle'), cur.find('.resize'), cur);
      });
    });

// Update sliders on resize.
// Because we all do this: i.imgur.com/YkbaV.gif
    $(window).resize(function() {
      $('.ba-slider').each(function() {
        var cur = $(this);
        var width = cur.width() + 'px';
        cur.find('.resize img').css('width', width);
      });
    });

    function drags(dragElement, resizeElement, container) {

      // Initialize the dragging event on mousedown.
      dragElement.on('mousedown touchstart', function(e) {

        dragElement.addClass('draggable');
        resizeElement.addClass('resizable');

        // Check if it's a mouse or touch event and pass along the correct value
        var startX = (e.pageX) ? e.pageX : e.originalEvent.touches[0].pageX;

        // Get the initial position
        var dragWidth = dragElement.outerWidth(),
          posX = dragElement.offset().left + dragWidth - startX,
          containerOffset = container.offset().left,
          containerWidth = container.outerWidth();

        // Set limits
        var minLeft;
        minLeft = containerOffset + 10;
        var maxLeft;
        maxLeft = containerOffset + containerWidth - dragWidth - 10;

        // Calculate the dragging distance on mousemove.
        dragElement.parents().on('mousemove touchmove', function(e) {

          // Check if it's a mouse or touch event and pass along the correct value
          var moveX = (e.pageX) ? e.pageX : e.originalEvent.touches[0].pageX;

          var leftValue;
          leftValue = moveX + posX - dragWidth;

          // Prevent going off limits
          if ( leftValue < minLeft) {
            leftValue = minLeft;
          } else if (leftValue > maxLeft) {
            leftValue = maxLeft;
          }

          // Translate the handle's left value to masked divs width.
          var widthValue;
          widthValue = (leftValue + dragWidth / 2 - containerOffset) * 100 / containerWidth + '%';

          // Set the new values for the slider and the handle.
          // Bind mouseup events to stop dragging.
          $('.draggable').css('left', widthValue).on('mouseup touchend touchcancel', function() {
            $(this).removeClass('draggable');
            resizeElement.removeClass('resizable');
          });
          $('.resizable').css('width', widthValue);
        }).on('mouseup touchend touchcancel', function() {
          dragElement.removeClass('draggable');
          resizeElement.removeClass('resizable');
        });
        e.preventDefault();
      }).on('mouseup touchend touchcancel', function(e) {
        dragElement.removeClass('draggable');
        resizeElement.removeClass('resizable');
      });
    }
  }
  onDropHandler(object) {
    console.log('event' + JSON.stringify(object));
    this.imageSrc = object.event.target.result;
  }

  allowDrop(ev) {
    ev.preventDefault();
  }

  drag(ev) {
    // alert('drag=====>' + ev.target.id)
    ev.dataTransfer.setData('text', ev.target.id);
  }

  drop(ev) {
    ev.preventDefault();
    const data = ev.dataTransfer.getData('text');
    const x = ev.target.appendChild(document.getElementById(data));
    if (x && x.style) {
      x.style.height = '400px';
      x.style.width = '800px';
    }
  }

  //...........................filter new...............

}
// filterSelection("all")
// function filterSelection(c) {
//   var x, i;
//   x = document.getElementsByClassName("column");
//   console.log('dddddddd==>>.' + x)
//   if (c == "all") c = "";
//   for (i = 0; i < x.length; i++) {
//     w3RemoveClass(x[i], "show");
//     if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
//   }
// }
//
// function w3AddClass(element, name) {
//   var i, arr1, arr2;
//   arr1 = element.className.split(" ");
//   arr2 = name.split(" ");
//   for (i = 0; i < arr2.length; i++) {
//     if (arr1.indexOf(arr2[i]) == -1) {element.className += " " + arr2[i];}
//   }
// }
//
// function w3RemoveClass(element, name) {
//   var i, arr1, arr2;
//   arr1 = element.className.split(" ");
//   arr2 = name.split(" ");
//   for (i = 0; i < arr2.length; i++) {
//     while (arr1.indexOf(arr2[i]) > -1) {
//       arr1.splice(arr1.indexOf(arr2[i]), 1);
//     }
//   }
//   element.className = arr1.join(" ");
// }
//
//
// // Add active class to the current button (highlight it)
// var btnContainer = document.getElementById("myBtnContainer");
// var btns = btnContainer.getElementsByClassName("btn");
// for (var i = 0; i < btns.length; i++) {
//   btns[i].addEventListener("click", function(){
//     var current = document.getElementsByClassName("active");
//     current[0].className = current[0].className.replace(" active", "");
//     this.className += " active";
//   });
// }
