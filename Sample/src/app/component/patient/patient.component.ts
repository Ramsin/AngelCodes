import { debounceTime } from 'rxjs/operators';
import {Input, Component, OnInit} from '@angular/core';
import { Subject } from 'rxjs';
import {NgbModal, ModalDismissReasons, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {Patient} from '../../shared/model/patient.model';
import {PatientService} from './patient.service';
import {ResponseWrapper} from '../../shared/model/response-wrapper.model';


@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.scss']
})
export class PatientComponent implements OnInit {
  closeResult: string;
  patient: Patient = new Patient();
  patientList: Patient[] = [];
  pagination : any ;
  previousPage: any;
  itemsPerPage : any ;
  predicate : any;
  reverse : any ;
  totalItems: any;



  constructor(config: NgbModalConfig, private modalService: NgbModal , private router: Router , private patientService : PatientService ) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {
    this.searchPatientDetail( );
  }
  open(content) {
    this.modalService.open(content);
  }
  btnClick = function () {
    this.router.navigateByUrl('/component/detailPatientTest');
    this.modalService.dismissAll();
  };
  searchPatient(){
    alert('hi search patient')
    this.transition();
  }

  loadPage(page: number, type: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      //this.transition(type);
    }
    this.transition();
  }

  transition() {
    this.router.navigate(['/component/patient'], {
      queryParams:
        {
          page: this.pagination,
          size: this.itemsPerPage,
          sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }
    });
    this.searchPatientDetail();
  }

  searchPatientDetail(){
    this.patientService.search(
      this.patient,
      this.pagination-1,
      this.itemsPerPage,
      this.sort()).
    subscribe(
      // (res: ResponseWrapper) => { this.onSuccess(res.json(), res.headers)},
      // (res: ResponseWrapper) => this.onError(res.json)
    );
  }

  sort(){
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }
  // trackId(index: number, item: Position) {
  //   return item.id;
  // }
}
