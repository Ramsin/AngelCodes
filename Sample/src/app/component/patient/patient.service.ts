import { Injectable } from '@angular/core';
import {BaseRequestOptions, Http, RequestOptions, Response, URLSearchParams} from '@angular/http';
import { SERVER_API_URL } from '../../app.constants';
import {Photo} from '../../shared/model/photo.model';
import {map} from 'rxjs/operators';
type EntityResponseType = HttpResponse<Photo>;
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Category} from '../../shared/model/category.model';
import {Patient} from '../../shared/model/patient.model';
import {ResponseWrapper} from '../../shared/model/response-wrapper.model';


@Injectable()
export class PatientService {
    private resourceUrl = SERVER_API_URL + 'api/patient/search';



  constructor(private httpClient: HttpClient ) { }

  search(page: Patient, pageNumber?: any, pageSize?: any, pageSort?: any): Observable<ResponseWrapper> {

    // let postData = new FormData();
    // const body = JSON.stringify(page);
    //
    // postData.append('pageNumber', pageNumber);
    // postData.append('size', pageSize);
    // postData.append('sort', pageSort);
    //
    // // this.spinner.show();

    return this.httpClient.get(`${this.resourceUrl}?pageNumber=${pageNumber}&size=${pageSize}&sort=${pageSort}`).map((res: Response) => {
      // this.spinner.hide();
      return res;

    });
  }










}
