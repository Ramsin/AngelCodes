import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JsonpModule } from '@angular/http';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ComponentsRoutes } from './component.routing';
import { NgbdpregressbarBasicComponent } from './progressbar/progressbar.component';
import { NgbdpaginationBasicComponent } from './pagination/pagination.component';
import { NgbdAccordionBasicComponent } from './accordion/accordion.component';
import { NgbdAlertBasicComponent } from './alert/alert.component';
import { NgbdCarouselBasicComponent } from './carousel/carousel.component';
import { NgbdDatepickerBasicComponent } from './datepicker/datepicker.component';
import { NgbdDropdownBasicComponent } from './dropdown-collapse/dropdown-collapse.component';
import { NgbdModalBasicComponent } from './modal/modal.component';
import { NgbdPopTooltipComponent } from './popover-tooltip/popover-tooltip.component';
import { NgbdratingBasicComponent } from './rating/rating.component';
import { NgbdtabsBasicComponent } from './tabs/tabs.component';
import { NgbdtimepickerBasicComponent } from './timepicker/timepicker.component';
import { NgbdtypeheadBasicComponent } from './typehead/typehead.component';
import { CardsComponent } from './card/card.component';
import { ButtonsComponent } from './buttons/buttons.component';
import {PatientComponent} from './patient/patient.component';
import {DetailPatientComponent} from './detail-patient/detail-patient.component';
import {GalleryComponent} from './gallery/gallery.component';
import {UploadImageComponent} from './upload-image/upload-image.component';
import {UploadImageService} from './upload-image/upload-image.service';
import {DetailPatientTestComponent} from './detail-patient-test/detail-patient-test.component';
import{DetailPatientPhotoComponent} from './detail-patient-test/detail-patient-photo.component';
import {PatientService} from './patient/patient.service';
import {ResponsiveTabComponent} from './responsive-tab/responsive-tab.component';
import {LightboxModule} from 'ngx-lightbox';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ComponentsRoutes),
    FormsModule,
    ReactiveFormsModule,
    JsonpModule,
    NgbModule,
    LightboxModule
  ],
  declarations: [
    NgbdpregressbarBasicComponent,
    NgbdpaginationBasicComponent,
    NgbdAccordionBasicComponent,
    NgbdAlertBasicComponent,
    NgbdCarouselBasicComponent,
    NgbdDatepickerBasicComponent,
    NgbdDropdownBasicComponent,
    NgbdModalBasicComponent,
    NgbdPopTooltipComponent,
    NgbdratingBasicComponent,
    NgbdtabsBasicComponent,
    NgbdtimepickerBasicComponent,
    NgbdtypeheadBasicComponent,
    CardsComponent,
    ButtonsComponent,
    PatientComponent,
    DetailPatientComponent,
    GalleryComponent,
    UploadImageComponent,
    DetailPatientTestComponent,
    DetailPatientPhotoComponent,
    ResponsiveTabComponent

  ],
  providers: [ UploadImageService , PatientService ],
})
export class ComponentsModule {}
