import { debounceTime } from 'rxjs/operators';
import {Input, Component, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {NgbModal, ModalDismissReasons, NgbModalConfig, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {Photo} from '../../shared/model/photo.model';
import {UploadImageService} from './upload-image.service';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Category} from '../../shared/model/category.model';
import {isNullOrUndefined} from 'util';


@Component({
  selector: 'app-upload-image',
  templateUrl: './upload-image.component.html',
  styleUrls: ['./upload-image.component.scss']
})
export class UploadImageComponent implements OnInit {
  @Input()
  photo: Photo = new Photo();
  formPhoto: Photo[];
  appCategory: Category = new Category();
  isSaving: boolean;
  confirmSaveMsgForm: boolean;
  errorSaveMsgForm: boolean;
  patient_id: any ;
  photoPatient: any ;
  photoCat: any ;
  cat_id: any ;
  surgery_id: any ;
  catGroup: FormGroup;

  itemName: AbstractControl;

  editForm = this.fb.group({
    id: [],
    title: [null, [Validators.required]],
    image: [null, [Validators.required]],
    imageContentType: [],
    amazonId: [],
    amazonLink: [],
  });
  private imageSrc = '';
  constructor( private  uploadImageService: UploadImageService , private fb: FormBuilder , protected activatedRoute: ActivatedRoute ) {
    this.catGroup = fb.group({
      'itemName': ['', Validators.compose([])],
    });
    this.itemName = this.catGroup.controls['itemName'];
  }

  ngOnInit() {
    this.loadPhotoPatientId(5);
    this.loadAllCategory();
    this.loadPhotoCatId(5 , 1);
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ photo }) => {
      this.updateForm(photo);
      this.photo = photo;
    });

  }

  btnClick = function () {
    this.router.navigateByUrl('/component/detailPatient');
    this.modalService.dismissAll();
  };

//............start  upload.............................................
  selectedFile: File = null;
  private res2: string;


  handleInputChange(e) {
    console.log(e);
    console.log(e.target.files[0]);
    alert('hiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiibabe.........' + e.target.files[0].name)
    const file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    alert('================================>> ' + e.target.files[0].name.split('.').pop().toLowerCase());
    const imageContentType = e.target.files[0].name.split('.').pop().toLowerCase();
    const pattern = /image-*/;

    this.photo.imageContentType = 'image/' + imageContentType ;
    //alert('===============================>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>' + this.photo.imageContentType);

    //this.photo.image = this.getBase64(file);
    //alert('===============================>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>' + this.photo.imageContentType);

    const reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }
  getBase64(file) {
    //alert('sraaaaaaaaaaaaaaaa=============>>.' )

    const reader = new FileReader();
    reader.readAsDataURL(file);
   // alert('sraaaaaaaaaaaaaaaa=============>>.' +  reader.readAsDataURL(file)),
    reader.onload = function () {
      console.log(reader.result);
    };
    reader.onerror = function (error) {
      console.log('Error: ', error);
    };
  }
  _handleReaderLoaded(e) {
    const reader = e.target;
    this.selectedFile = <File>reader.result;
   // alert('--- >>>> ' +  this.selectedFile.name);
    this.imageSrc = reader.result;
    // var res = this.imageSrc.substring(0,this.imageSrc.indexOf(','));
    this.res2 = this.imageSrc.substring(this.imageSrc.indexOf(',') + 1);
    //alert('hiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii vfv f' + this.res2);

    console.log(this.imageSrc);
  }

  //.................................end upload .........................

  updateForm(photo: Photo) {
    this.editForm.patchValue({
      id: photo.id,
      title: photo.title,
      image: photo.image,
      imageContentType: photo.imageContentType,
      amazonLink: photo.amazonLink,
      amazonId: photo.amazonId,
    });
  }
  save() {
    this.isSaving = true;
    const photo = this.createFromForm();
    this.subscribeToSaveResponse(this.uploadImageService.create( photo ));
    // }
  }
  savePatientId() {
    this.isSaving = true;
    const photo = this.createFromForm();
    this.uploadImageService.createPatient( photo , this.patient_id = 5).subscribe();
    // this.subscribeToSaveResponse();
    // }
  }
  saveCatId() {
    this.isSaving = true;
    const photo = this.createFromForm();
    this.uploadImageService.createCatID( photo , this.patient_id = 5 , this.cat_id = 3).subscribe();
}


  saveSurgeryId() {
    this.isSaving = true;
    const photo = this.createFromForm();
    this.uploadImageService.createSurgeryID( photo , this.patient_id = 5 , this.cat_id = 3 , this.surgery_id = 1 ).subscribe();
  }

  deletePhoto(id: number) {
    alert('id delete === >> ' + id)
    this.uploadImageService.deleteOwnerId(id = 14 ).subscribe();
  }
  deleteCategory(id: number) {
    this.uploadImageService.deleteCatId(id = 13 ).subscribe();
  }

  loadPhotoPatientId(id) {
    //  alert('id=' + id);
    this.uploadImageService.find(id).subscribe((photo) => {
      this.photoPatient = photo;
      // alert(JSON.stringify(photo));
     // = this.photo.amazonLink.text();
    });
  }
  loadAllCategory() {
    //  alert('id=' + id);
    this.uploadImageService.findAllCategory().subscribe((appCategory) => {
      this.appCategory = appCategory;
      // alert(JSON.stringify(photo));
      // = this.photo.amazonLink.text();
    });

  }
  loadPhotoCatId(id , cat_id) {
    //  alert('id=' + id);
    this.uploadImageService.findCatId(id , cat_id).subscribe((photo) => {
      this.photoCat = photo;
      // alert(JSON.stringify(photo));
      // = this.photo.amazonLink.text();
    });
  }

  private createFromForm(): Photo {
    const entity = {
      ...new Photo(),
      id: this.editForm.get(['id']).value,
      title: this.editForm.get(['title']).value,
      amazonLink: this.editForm.get(['amazonLink']).value,
      amazonId: this.editForm.get(['amazonId']).value,
      image: this.res2,
      imageContentType: this.photo.imageContentType,
    };
    return entity;
  }

  saveCategory() {
    this.isSaving = true;
    if (!isNullOrUndefined(this.appCategory.id)) {
      this.subscribeToSaveResponseCategory(this.uploadImageService.updateCategory( this.appCategory ));

    }else {
      this.subscribeToSaveResponseCategory(this.uploadImageService.createCategory( this.appCategory ));

    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<Photo>>) {
    result.subscribe((res: HttpResponse<Photo>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  private subscribeToSaveResponseCategory(result: Observable<Category>) {
    result.subscribe((res: Category) =>
      this.onSaveSuccessCat( res ), (res: Response) => {
      this.onSaveError();
    });
  }
  protected onSaveSuccessCat(result: Category) {
    this.appCategory = result ;
    this.isSaving = false;
    this.confirmSaveMsgForm = true;
    this.previousState();
  }
  protected onSaveSuccess() {
    alert('hi sara')
    this.isSaving = false;
    this.confirmSaveMsgForm = true;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
    this.confirmSaveMsgForm = false;
  }
  previousState() {
    window.history.back();
  }
}
