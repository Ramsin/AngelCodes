import { Injectable } from '@angular/core';
import {BaseRequestOptions, Http, RequestOptions, Response, URLSearchParams} from '@angular/http';
import { SERVER_API_URL } from '../../app.constants';
import {Photo} from '../../shared/model/photo.model';
import {map} from 'rxjs/operators';
type EntityResponseType = HttpResponse<Photo>;
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Category} from '../../shared/model/category.model';


@Injectable()
export class UploadImageService {
    private resourceUrl = SERVER_API_URL + 'api/image/photo';
    private resourceUrl1 = SERVER_API_URL + 'api/image/patient/photo';
    private resourceUrlCatId = SERVER_API_URL + 'api/image/patient/photo-category';
    private resourceUrlSurgeryId = SERVER_API_URL + 'api/image/patient/photo-category-surgery';
    private resourceUrlDeleteOwner = SERVER_API_URL + 'api/image/owner';
    private resourceUrlSaveCategory = SERVER_API_URL + 'api/image/category';


  constructor(private httpClient: HttpClient ) { }

  create(photo: Photo  ): Observable<EntityResponseType> {
    // const copy = this.convertDateFromClient(photo);
    return this.httpClient
      .post<Photo>(this.resourceUrl, photo, { observe: 'response' });
      // .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }
  find(id): Observable<Photo> {
    return this.httpClient.get(`${this.resourceUrl1}?patient_id=${id}`);
  }
  findAllCategory(): Observable<Category> {
    return this.httpClient.get(`${this.resourceUrlSaveCategory}`);
  }
  findCatId(id , cat_id: any): Observable<Photo> {
    return this.httpClient.get(`${this.resourceUrlCatId}?patient_id=${id}&cat_id=${cat_id}`);
  }
  createPatient(photo: Photo , patient_id: any): Observable<Photo> {
    const header = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Methods': 'GET',
      'Access-Control-Allow-Origin': '*'
    };
    const httpOptions = new HttpHeaders(header);
    return this.httpClient.post(`${this.resourceUrl1}?patient_id=${patient_id}`, photo  , {headers: httpOptions});
  }

  createCategory(category: Category): Observable<Category> {
    const header = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Methods': 'GET',
      'Access-Control-Allow-Origin': '*'
    };
    const httpOptions = new HttpHeaders(header);
    return this.httpClient.post(`${this.resourceUrlSaveCategory}` , category , {headers: httpOptions});
  }
  updateCategory(category: Category): Observable<Category> {
    const header = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Methods': 'GET',
      'Access-Control-Allow-Origin': '*'
    };
    const httpOptions = new HttpHeaders(header);
    return this.httpClient.put(`${this.resourceUrlSaveCategory}` , category , {headers: httpOptions});
  }

  createCatID(photo: Photo , patient_id: any , cat_id: any): Observable<Photo> {
    const header = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Methods': 'GET',
      'Access-Control-Allow-Origin': '*'
    };
    const httpOptions = new HttpHeaders(header);
    return this.httpClient.post(`${this.resourceUrlCatId}?patient_id=${patient_id}&cat_id=${cat_id}`, photo  , {headers: httpOptions});
  }
  createSurgeryID(photo: Photo , patient_id: any , cat_id: any , surgery_id: any): Observable<Photo> {
    const header = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Methods': 'GET',
      'Access-Control-Allow-Origin': '*'
    };
    const httpOptions = new HttpHeaders(header);
    return this.httpClient.post(`${this.resourceUrlSurgeryId}?patient_id=${patient_id}&cat_id=${cat_id}&surgery_id=${surgery_id}`, photo  , {headers: httpOptions});
  }

  deleteOwnerId(id: number): Observable<Photo> {
    return this.httpClient.delete(`${this.resourceUrlDeleteOwner}/${id}` );
  }
  deleteCatId(id: number): Observable<Category> {
    return this.httpClient.delete(`${this.resourceUrlSaveCategory}/${id}` );
  }
  private convertItemFromServer(json: any): Photo {
    const entity: Photo = Object.assign(new Photo(), json);
    // entity.creationDate = this.dateUtils
    //     .convertLocalDateFromServer(json.creationDate);
    return entity;
  }
  private convert(photo: Photo): Photo {
    const copy: Photo = Object.assign({}, photo);
    return copy;
  }

}
