import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import * as $ from 'jquery';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';



@Component({
  selector: 'app-detail-patient-test',
  templateUrl: './detail-patient-test.component.html',
  styleUrls: ['./detail-patient-test.component.scss']
})
export class DetailPatientTestComponent implements OnInit {
  closeResult: string;
  public imageSrc = 'https://image.freepik.com/free-icon/upload-arrow_318-26670.jpg';

  public patients = [
    { id: 11, name: 'Dr Nice'},
    { id: 12, name: 'mahsa'},
    { id: 13, name: 'smaira'},
    { id: 14, name: 'nasrin'},
    { id: 15, name: 'nastran'},
    { id: 16, name: 'shima'},
    { id: 17, name: 'shiva'},
  ];
  constructor(config: NgbModalConfig, private modalService: NgbModal ) {
    config.backdrop = 'static';
    config.keyboard = false;

  }

  ngOnInit() {
  }
  openFolder(content) {
    this.modalService.open(content);

  }
  openFolderSearch(contentSearch) {
    this.modalService.open(contentSearch);

  }





}
