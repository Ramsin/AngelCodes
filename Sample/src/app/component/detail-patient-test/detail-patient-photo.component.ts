import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import * as $ from 'jquery';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { Lightbox } from 'ngx-lightbox';



@Component({
  selector: 'app-detail-patient-photo',
  templateUrl: './detail-patient-photo.component.html',
  styleUrls: ['./detail-patient-photo.component.scss']
})
export class DetailPatientPhotoComponent implements OnInit {
  closeResult: string;
  public imageSrc = 'https://image.freepik.com/free-icon/upload-arrow_318-26670.jpg';
  // private _albums: Array = [];
  // private _albums = new Array<any>()

  public photoImg = [
    { id: 11, image: 'https://unsplash.it/100'},
    { id: 12, image: 'https://unsplash.it/200'},
    { id: 13, image: 'https://unsplash.it/300'},
    { id: 14, image: 'https://unsplash.it/400'},
    { id: 15, image: 'https://unsplash.it/500'},
    { id: 16, image: 'https://unsplash.it/600'},
    { id: 17, image: 'https://unsplash.it/700'},
  ];

  public itemSurgeryes = [
    {name1 : 'Liposuction' , name2 : 'Eyes-surgery' , name3 : 'Microneedling' } ,
    // {name2 : 'Eyes-surgery' } ,
    // {name3 : 'Microneedling' } ,
  ];

  constructor(config: NgbModalConfig, private modalService: NgbModal , configCarousel: NgbCarouselConfig , private _lightbox: Lightbox) {


    // for (let i = 1; i <= 4; i++) {
    //   const src = 'demo/img/image' + i + '.jpg';
    //   const caption = 'Image ' + i + ' caption here';
    //   const thumb = 'demo/img/image' + i + '-thumb.jpg';
    //   const album = {
    //     src: src,
    //     caption: caption,
    //     thumb: thumb
    //   };
    //
    //   this._albums.push(album);
    // }


    config.backdrop = 'static';
    config.keyboard = false;


    configCarousel.interval = 10000;
    configCarousel.wrap = false;
    configCarousel.keyboard = false;

    configCarousel.showNavigationArrows = true;
    configCarousel.showNavigationIndicators = true;
  }

  // open(index: number): void {
  //   // open lightbox
  //   this._lightbox.open(this._albums, index);
  // }
  //
  // close(): void {
  //   // close lightbox programmatically
  //   this._lightbox.close();
  // }

  ngOnInit() {
    this.openPicture()
    //.....start carousel...............

    $( '.img-wrapper' ).hover(
      function() {
        $(this).find('.img-overlay').animate({opacity: 1}, 600);
      }, function() {
        $(this).find('.img-overlay').animate({opacity: 0}, 600);
      }
    );
    //start .........tab 2
    $('.tabMain').click(function(event) {
      $('.chosen').removeClass('chosen');
      $(this).addClass('chosen');
      const index = $('.tabMain').index($(this));
      $('.shown').removeClass('shown');
      $('.pageMain').eq(index).addClass('shown');
    });

    // ... start gallery Lightbox
//     let $overlay = $('<div id="overlay" style="background: rgba(0,0,0,0.7);width: 100%;height: 100%;position: fixed;top: 0;left: 0;display: flex; justify-content: center;align-items: center; z-index: 999;-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;"></div>');
//     const $image = $('<img style=" margin: 0; width: 80%;height: auto;object-fit: contain;padding: 5%;@media screen and (min-width:768px) {  width: 60%;} @media screen and (min-width:1200px) {  width: 50%;}">' );
//     const $prevButton = $('<div id="prevButton" style="color: #fff;font-size: 2em;transition: opacity 0.8s;margin-right: 20px;@media screen and (min-width:768px) {font-size: 3em;};"><i class="fa fa-chevron-left"></i></div>');
//     const $nextButton = $('<div id="nextButton" style="color: #fff;font-size: 2em;transition: opacity 0.8s;margin-left: 20px;@media screen and (min-width:768px) {font-size: 3em; }"><i class="fa fa-chevron-right"></i></div>');
//     const $exitButton = $('<div id="exitButton" style="color: #fff;font-size: 2em;transition: opacity 0.8s;position: absolute;top: 15px;right: 15px ;@media screen and (min-width:768px) {font-size: 3em;}"><i class="fa fa-times"></i></div>');
//
// // Add overlay
//     $overlay.append($image).prepend($prevButton).append($nextButton).append($exitButton);
//     $('#gallery').append($overlay);
//
// // Hide overlay on default
//     $overlay.hide();
//
// // When an image is clicked
//     $('.img-overlay').click(function(event) {
//       alert('hi sarararararrararr')
//
//       // Prevents default behavior
//       event.preventDefault();
//       // Adds href attribute to variable
//       const imageLocation = $(this).prev().attr('href');
//       // Add the image src to $image
//       $image.attr('src', imageLocation);
//       // Fade in the overlay
//       $overlay.fadeIn('slow');
//     });
//
// // When the overlay is clicked
//     $overlay.click(function() {
//       // alert('hiii')
//       // Fade out the overlay
//       $(this).fadeOut('slow');
//     });
//
// // When next button is clicked
//     $nextButton.click(function(event) {
//       // Hide the current image
//       $('#overlay img').hide();
//       // Overlay image location
//       const $currentImgSrc = $('#overlay img').attr('src');
//       // Image with matching location of the overlay image
//       const $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
//       // Finds the next image
//       const $nextImg = $($currentImg.closest('.image').next().find('img'));
//       // All of the images in the gallery
//       const $images = $('#image-gallery img');
//       // If there is a next image
//       if ($nextImg.length > 0) {
//         // Fade in the next image
//         $('#overlay img').attr('src', $nextImg.attr('src')).fadeIn(800);
//       } else {
//         // Otherwise fade in the first image
//         $('#overlay img').attr('src', $($images[0]).attr('src')).fadeIn(800);
//       }
//       // Prevents overlay from being hidden
//       event.stopPropagation();
//     });
//
// // When previous button is clicked
//     $prevButton.click(function(event) {
//       // Hide the current image
//       $('#overlay img').hide();
//       // Overlay image location
//       const $currentImgSrc = $('#overlay img').attr('src');
//       // Image with matching location of the overlay image
//       const $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
//       // Finds the next image
//       const $nextImg = $($currentImg.closest('.image').prev().find('img'));
//       // Fade in the next image
//       $('#overlay img').attr('src', $nextImg.attr('src')).fadeIn(800);
//       // Prevents overlay from being hidden
//       event.stopPropagation();
//     });
//
// // When the exit button is clicked
//     $exitButton.click(function() {
//       // Fade out the overlay
//       $('#overlay').fadeOut('slow');
//     });

    //..................................start copmare image.....................
    // drag and drop component........................
    // Call & init
    $(document).ready(function() {
      var each = $('.ba-slider').each(function() {
        var cur = $(this);
        // Adjust the slider
        var width = cur.width() + 'px';
        cur.find('.resize img').css('width', width);
        // Bind dragging events
        drags(cur.find('.handle'), cur.find('.resize'), cur);
      });
    });

// Update sliders on resize.
// Because we all do this: i.imgur.com/YkbaV.gif
    $(window).resize(function() {
      $('.ba-slider').each(function() {
        var cur = $(this);
        var width = cur.width() + 'px';
        cur.find('.resize img').css('width', width);
      });
    });

    function drags(dragElement, resizeElement, container) {

      // Initialize the dragging event on mousedown.
      dragElement.on('mousedown touchstart', function(e) {

        dragElement.addClass('draggable');
        resizeElement.addClass('resizable');

        // Check if it's a mouse or touch event and pass along the correct value
        var startX = (e.pageX) ? e.pageX : e.originalEvent.touches[0].pageX;

        // Get the initial position
        var dragWidth = dragElement.outerWidth(),
          posX = dragElement.offset().left + dragWidth - startX,
          containerOffset = container.offset().left,
          containerWidth = container.outerWidth();

        // Set limits
        var minLeft;
        minLeft = containerOffset + 10;
        var maxLeft;
        maxLeft = containerOffset + containerWidth - dragWidth - 10;

        // Calculate the dragging distance on mousemove.
        dragElement.parents().on('mousemove touchmove', function(e) {

          // Check if it's a mouse or touch event and pass along the correct value
          var moveX = (e.pageX) ? e.pageX : e.originalEvent.touches[0].pageX;

          var leftValue;
          leftValue = moveX + posX - dragWidth;

          // Prevent going off limits
          if ( leftValue < minLeft) {
            leftValue = minLeft;
          } else if (leftValue > maxLeft) {
            leftValue = maxLeft;
          }

          // Translate the handle's left value to masked divs width.
          var widthValue;
          widthValue = (leftValue + dragWidth / 2 - containerOffset) * 100 / containerWidth + '%';

          // Set the new values for the slider and the handle.
          // Bind mouseup events to stop dragging.
          $('.draggable').css('left', widthValue).on('mouseup touchend touchcancel', function() {
            $(this).removeClass('draggable');
            resizeElement.removeClass('resizable');
          });
          $('.resizable').css('width', widthValue);
        }).on('mouseup touchend touchcancel', function() {
          dragElement.removeClass('draggable');
          resizeElement.removeClass('resizable');
        });
        e.preventDefault();
      }).on('mouseup touchend touchcancel', function(e) {
        dragElement.removeClass('draggable');
        resizeElement.removeClass('resizable');
      });
    }

    //.......................filter Images......................
    //........start multi select...............

    function buildDropDown(name, index, array) {
      const opt = document.createElement('option');
      opt.value = name;
      opt.innerHTML = name;
      dropdown.appendChild(opt);
    }
    const container = document.getElementById('dynaflex'),
      array = container.getElementsByTagName('figure'),
      selectionBar = document.getElementById('selectionbar'),
      categories = [];
    for (let i = 0; i < array.length ; i++) {
      categories[i] = array[i].dataset.group;
    }
    const unique = categories.filter(function(item, i, ar) { return ar.indexOf(item) === i; });
    unique.reverse();
    unique.unshift('all');

    const dropdown = document.createElement('select');
    dropdown.id = 'categories';
    const dropdownLabel = document.createElement('label');
    dropdownLabel.htmlFor = dropdown.id;
    dropdownLabel.innerHTML = 'Show : ';
    unique.forEach(buildDropDown);
    selectionBar.appendChild(dropdownLabel);
    selectionBar.appendChild(dropdown);

    dropdown.addEventListener('change', function() {
        console.log('Change');
        if (this.value === 'all') {
          for (let i = 0; i < array.length; ++i) {	array[i].classList.remove('diminish');
          }
        } else {
          const hide = document.querySelectorAll('#dynaflex figure:not([data-group="' + this.value + '"])');
          for (let j = 0; j < hide.length; ++j) {
            hide[j].classList.add('diminish');
          }
          const show = document.querySelectorAll('#dynaflex figure[data-group="' + this.value + '"]');
          for (let k = 0; k < show.length; ++k) {	show[k].classList.remove('diminish');
          }
        }
      }
    );
  }
  openFolder(content) {
    this.modalService.open(content);

  }
  openFolderSearch(contentSearch) {
    this.modalService.open(contentSearch);

  }
  //...............................start compare 2 image...................................................
  onDropHandler(object) {
    console.log('event' + JSON.stringify(object));
    this.imageSrc = object.event.target.result;
  }

  allowDrop(ev) {
    ev.preventDefault();
  }

  drag(ev) {
    // alert('drag=====>' + ev.target.id)
    ev.dataTransfer.setData('text', ev.target.id);
  }

  drop(ev) {
    ev.preventDefault();
    const data = ev.dataTransfer.getData('text');
    const x = ev.target.appendChild(document.getElementById(data));
    if (x && x.style) {
      x.style.height = '400px';
      x.style.width = '800px';
    }
  }
  //...............................end compare 2 image.......................

  //..........................start print.............................................................

  print(): void {

    let printContents, popupWin;

    printContents = document.getElementById('demo').innerHTML
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();

  }


  openPicture(){
    alert('hi kavehhh');

    // ... start gallery Lightbox
    let $overlay = $('<div id="overlay" style="background: rgba(0,0,0,0.7);width: 100%;height: 100%;position: fixed;top: 0;left: 0;display: flex; justify-content: center;align-items: center; z-index: 999;-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;"></div>');
    const $image = $('<img style=" margin: 0; width: 80%;height: auto;object-fit: contain;padding: 5%;@media screen and (min-width:768px) {  width: 60%;} @media screen and (min-width:1200px) {  width: 50%;}">' );
    const $prevButton = $('<div id="prevButton" style="color: #fff;font-size: 2em;transition: opacity 0.8s;margin-right: 20px;@media screen and (min-width:768px) {font-size: 3em;};"><i class="fa fa-chevron-left"></i></div>');
    const $nextButton = $('<div id="nextButton" style="color: #fff;font-size: 2em;transition: opacity 0.8s;margin-left: 20px;@media screen and (min-width:768px) {font-size: 3em; }"><i class="fa fa-chevron-right"></i></div>');
    const $exitButton = $('<div id="exitButton" style="color: #fff;font-size: 2em;transition: opacity 0.8s;position: absolute;top: 15px;right: 15px ;@media screen and (min-width:768px) {font-size: 3em;}"><i class="fa fa-times"></i></div>');

// Add overlay
    $overlay.append($image).prepend($prevButton).append($nextButton).append($exitButton);
    $('#gallery').append($overlay);

// Hide overlay on default
    $overlay.hide();

// When an image is clicked
    $('.img-overlay').click(function(event) {
      // alert('hi sarararararrararr')

      // Prevents default behavior
      event.preventDefault();
      // Adds href attribute to variable
      const imageLocation = $(this).prev().attr('href');
      // Add the image src to $image
      $image.attr('src', imageLocation);
      // Fade in the overlay
      $overlay.fadeIn('slow');
    });

// When the overlay is clicked
    $overlay.click(function() {
      // alert('hiii')
      // Fade out the overlay
      $(this).fadeOut('slow');
    });

// When next button is clicked
    $nextButton.click(function(event) {
      // Hide the current image
      $('#overlay img').hide();
      // Overlay image location
      const $currentImgSrc = $('#overlay img').attr('src');
      // Image with matching location of the overlay image
      const $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
      // Finds the next image
      const $nextImg = $($currentImg.closest('.image').next().find('img'));
      // All of the images in the gallery
      const $images = $('#image-gallery img');
      // If there is a next image
      if ($nextImg.length > 0) {
        // Fade in the next image
        $('#overlay img').attr('src', $nextImg.attr('src')).fadeIn(800);
      } else {
        // Otherwise fade in the first image
        $('#overlay img').attr('src', $($images[0]).attr('src')).fadeIn(800);
      }
      // Prevents overlay from being hidden
      event.stopPropagation();
    });

// When previous button is clicked
    $prevButton.click(function(event) {
      // Hide the current image
      $('#overlay img').hide();
      // Overlay image location
      const $currentImgSrc = $('#overlay img').attr('src');
      // Image with matching location of the overlay image
      const $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
      // Finds the next image
      const $nextImg = $($currentImg.closest('.image').prev().find('img'));
      // Fade in the next image
      $('#overlay img').attr('src', $nextImg.attr('src')).fadeIn(800);
      // Prevents overlay from being hidden
      event.stopPropagation();
    });

// When the exit button is clicked
    $exitButton.click(function() {
      // Fade out the overlay
      alert('closeeeeeeeeeeeeeeee')
      $('#overlay').fadeOut('slow');
    });

  }



}
