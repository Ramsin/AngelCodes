
export interface IPhoto {
  id?: number;
  title?: string;
  description?: any;
  imageContentType?: string;
  image?: any;
  height?: number;
  width?: number;
  isArchived?: boolean;
  amazonLink?: string;
  amazonId?: string;
}

export class Photo implements IPhoto {
  constructor(
    public id?: number,
    public title?: string,
    public description?: any,
    public imageContentType?: string,
    public image?: any,
    public height?: number,
    public width?: number,
    public isArchived?: boolean,
    public amazonLink?: string,
    public amazonId?: string,
  ) {
    this.isArchived = this.isArchived || false;
  }
}
