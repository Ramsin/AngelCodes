export interface ICategory {
  id?: number;
  itemName?: string;
}

export class Category implements ICategory {
  constructor(
    public id?: number,
    public itemName?: string
  ) {}
}
