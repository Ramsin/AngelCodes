import {BaseEntity} from './base-entity';

export const enum Status {
    'Active',
    'Passi'
}
export const enum Gender {
    'Male',
    'Female'
}
export class Patient implements BaseEntity {
    constructor(
        public id?: number,
        public firstname?: string,
        public lastname?: string,
        public dateofbirth?: any,
        public address1?: string,
        public address2?: string,
        public age?: number,
        public altEmail?: string,
        public emailAddress?: string,
        public cellPhone?: number,
        public city?: string,
        public displayName?: string,
        public emergencyContactAddress?: string,
        public emergencyContactCity?: string,
        public emergencyContactName?: string,
        public emergencyContactPhone?: number,
        public emergencyContactState?: string,
        public emergencyContactZip?: number,
        public fax?: number,
        public country?: string,
        public gender?: Gender,
        public height?: number,
        public homePhone?: number,
        public officePhone?: number,
        public middlename?: string,
        public otherPhone?: number,
        public postfix?: string,
        public prefix?: string,
        public primaryEmail?: string,
        public primaryPhone?: number,
        // public state?: string,
        // public status?: Status,
        public suitNumber?: number,
        public weight?: number,
        public website?: string,
        public zip?: number,

    ) {
    }
}
