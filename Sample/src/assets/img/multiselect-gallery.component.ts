import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-multiselect-gallery',
  templateUrl: './multiselect-gallery.component.html',
  styleUrls: ['./multiselect-gallery.component.css']
})
export class MultiselectGalleryComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    function buildDropDown(name, index, array) {
      const opt = document.createElement('option');
      opt.value = name;
      opt.innerHTML = name;
      dropdown.appendChild(opt);
    }
    const container = document.getElementById('dynaflex'),
      array = container.getElementsByTagName('figure'),
      selectionBar = document.getElementById('selectionbar'),
      categories = [];
    for (let i = 0; i < array.length ; i++) {
      categories[i] = array[i].dataset.group;
    }
    const unique = categories.filter(function(item, i, ar) { return ar.indexOf(item) === i; });
    unique.reverse();
    unique.unshift('all');

    const dropdown = document.createElement('select');
    dropdown.id = 'categories';
    const dropdownLabel = document.createElement('label');
    dropdownLabel.htmlFor = dropdown.id;
    dropdownLabel.innerHTML = 'Show : ';
    unique.forEach(buildDropDown);
    selectionBar.appendChild(dropdownLabel);
    selectionBar.appendChild(dropdown);

    dropdown.addEventListener('change', function() {
        console.log('Change');
        if (this.value == 'all') {
          for (let i = 0; i < array.length; ++i) {	array[i].classList.remove('diminish');
          }
        } else {
          const hide = document.querySelectorAll('#dynaflex figure:not([data-group="' + this.value + '"])');
          for (let j = 0; j < hide.length; ++j) {
            hide[j].classList.add('diminish');
          }
          const show = document.querySelectorAll('#dynaflex figure[data-group="' + this.value + '"]');
          for (let k = 0; k < show.length; ++k) {	show[k].classList.remove('diminish');
          }
        }
      }
    );

  }

}
