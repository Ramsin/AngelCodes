import {Component, OnInit, OnDestroy, Input} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';
import {Patient} from "../../shared/model/patient.model";
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {DetailPatientTestComponent} from '../detail-patient-test/detail-patient-test.component';

@Component({
    selector: 'app-photo-management-popup',
    templateUrl: './photo-management-popup.component.html',
    styleUrls: ['./photo-management-popup.component.css'],
})
export class PhotoManagementPopupComponent implements OnInit {
    @Input() patientEdit;

    patient: Patient = new Patient();
    patientList: Patient[] = [];
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        public activeModal: NgbActiveModal,public modalService: NgbModal , private router: Router
    ) {}


    ngOnInit() {

    }
    showDetail(id) {
         // alert(' in edit id =' + id);
        this.router.navigateByUrl('/pages/detailPatientTest/' + id);
        this.activeModal.dismiss('cancel');
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }
}
