import { debounceTime } from 'rxjs/operators';
import {Input, Component, OnInit} from '@angular/core';
import { Subject } from 'rxjs';
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {Patient} from '../../shared/model/patient.model';
// import {PatientService} from './patient.service';
import {PhotoManagementService} from './photo-management.service';
import {ResponseWrapper} from '../../shared/model/response-wrapper.model';
import {JhiEventManager, JhiAlertService, JhiDataUtils, JhiDateUtils, JhiParseLinks} from 'ng-jhipster';
import {PatientDialogComponent} from '../patient/patient-dialog.component';
import {PhotoManagementPopupComponent} from './photo-management-popup.component';



@Component({
    selector: 'app-photo-management',
    templateUrl: './photo-management.component.html',
    styleUrls: ['./photo-management.component.scss']
})
export class PhotoManagementComponent implements OnInit {
    closeResult: string;
    patient: Patient = new Patient();
    patientList: Patient[] = [];
    sortedData : Patient[];
    pagination : any ;
    previousPage: any;
    itemsPerPage : any ;
    predicate : any;
    reverse : any ;
    totalItems: any;
    sortList: any;
    patient1: any;
    sortPredeict: any;
    resultAvailabe: boolean = false;
    links: any;
    link: string;
    queryCount : any ;
    public data = [];
    fromDate: string ;
    toDate: string ;





    constructor(  public modalService: NgbModal , private router: Router,private parseLinks: JhiParseLinks , private dateUtils: JhiDateUtils , private photoManagementService : PhotoManagementService  ) {

        this.itemsPerPage = 10;
        this.sortList = 'id';
        this.sortPredeict = 'asc';
    }

    ngOnInit() {
        // this.searchPatientDetail();

    }

    patientSurgery(id) {
         // alert(' in edit id =' + id);
        const modalRef = this.modalService.open(PhotoManagementPopupComponent , {windowClass: 'lg' , backdrop: 'static'});
        modalRef.componentInstance.patientEdit = id;
    }

    searchPatient(){
        this.resultAvailabe = false;
        this.pagination = 1;
        // alert('hi search patient')
        this.transition();
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            //this.transition(type);
        }
        this.transition();
    }

    loadPage2(param) {
        // alert('parameter ==== >>  ' + param)

        if (this.sortList === param) {
            if (this.sortPredeict === 'asc') {
                this.sortPredeict = 'desc';
            } else {
                this.sortPredeict = 'asc';
            }
        } else {
            this.sortPredeict = 'asc';
        }
        this.sortList = param;
        this.transition();
    }

    transition() {
        this.router.navigate(['/pages/photoManagement'], {
            queryParams:
                {
                    page: this.pagination,
                    size: this.itemsPerPage,
                    sort: this.sortPredeict + ',' + (this.reverse ? 'asc' : 'desc'),

                }
        });
        this.searchPatientDetail();
    }

    searchPatientDetail(){
      // this.patient1 = JSON.stringify(this.patient) ;
      //   alert('bimara' + JSON.stringify(this.patient));
        const dateStart = this.fromDate ;
        const dateEnd = this.toDate ;

        this.photoManagementService.search({
            patient1 : this.patient ,
            page: this.pagination - 1,
            size: this.itemsPerPage,
            sort: this.sort(this.sortList, this.sortPredeict) ,dateEnd , dateStart }).
        subscribe(
            (res: ResponseWrapper) => { this.onSuccess(res.json(), res.headers)},
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }


    private onSuccess(data, headers) {
        // alert(' in onSuccess');

        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.patientList = data;
        this.data = data;
        this.resultAvailabe = true;
        // alert(' this.positions=' + this.positions);
    }
    onError(error){
    }

    sort(sort, predicate) {
        let result = ['clinicId,desc'];
        if (sort) {
            this.reverse = 'desc';
            result = [sort + ',' + predicate];
            return result;
        }
    }
    clickReset(){
        this.patient.firstname = '' ;
        this.patient.lastname = '' ;
        this.patient.height = null ;
        this.patient.gender = null ;
        this.patient.id = null ;
        this.fromDate = '' ;
        this.toDate = '' ;

    }

}
