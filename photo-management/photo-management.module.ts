import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import {CommonModule} from '@angular/common';
import {HttpModule} from '@angular/http';
import {AgGridModule} from 'ag-grid-angular';
import {HttpClientModule} from '@angular/common/http';
import {MultiselectDropdownModule} from 'angular-2-dropdown-multiselect';
import {PhotoManagementComponent} from './photo-management.component';
import {NgbActiveModal, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormsModule, ReactiveFormsModule, Validators} from '@angular/forms';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {NgJhipsterModule} from 'ng-jhipster';
import {PhotoManagementService} from './photo-management.service';
import {AppRoutingModule} from './photo-management-routing.module';
import {customHttpProvider} from '../../shared/interceptor/http.provider';
import {GeneralService} from '../../shared/service/general.service';
import {PhotoManagementPopupComponent} from './photo-management-popup.component';


export const routes = [
    {
        path: '', component: PhotoManagementComponent, pathMatch: 'full',
        // children: [
        //     {path: '', redirectTo: '/personal', pathMatch: 'full'},
        //
        //     // {path: '/personal', component: PersonalComponent}
        // ]
    }
];

@NgModule({
    imports: [
        CommonModule, HttpModule, HttpClientModule, RouterModule.forChild(routes), FormsModule, NgbModule.forRoot(), MultiselectDropdownModule
        , AngularMultiSelectModule, NgJhipsterModule ,
        ReactiveFormsModule,
      AppRoutingModule
    ],
    declarations: [
        PhotoManagementComponent , PhotoManagementPopupComponent ,
    ],
    entryComponents: [
        PhotoManagementPopupComponent ,
    ],
    providers: [  PhotoManagementService, customHttpProvider(), GeneralService

    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PhotoManagementModule {



}
