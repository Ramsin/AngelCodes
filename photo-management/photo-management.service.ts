import { Injectable } from '@angular/core';
import {BaseRequestOptions, Http, RequestOptions, Response, URLSearchParams} from '@angular/http';
import { SERVER_API_URL } from '../../app.constants';
// import {Photo} from '../../shared/model/photo.model';
import {map} from 'rxjs/operators';
// type EntityResponseType = HttpResponse<Photo>;
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
// import {Category} from '../../shared/model/category.model';
import {Patient} from '../../shared/model/patient.model';
import {ResponseWrapper} from '../../shared/model/response-wrapper.model';


@Injectable()
export class PhotoManagementService {
    private resourceUrl = SERVER_API_URL + 'api/patient/search';
    private resourceUrlPatientId = SERVER_API_URL + 'api/patients';



    constructor(private http: Http ) { }

    search(req: any): Observable<ResponseWrapper> {
        const params: URLSearchParams = new URLSearchParams();
        params.set('pageNumber', req.page);
        params.set('size', req.size);
        params.set('sort', req.sort);
        // alert('sortttt=====>>>>  ' + JSON.stringify(req.sort))
        if(req.dateStart){
            params.set('creationTimeGreater', req.dateStart);
        }
        if(req.dateEnd){
            params.set('creationTimeLess', req.dateEnd);
        }
        if(req.patient1.firstname){
            params.set('firstname', req.patient1.firstname);
        }
        if(req.patient1.lastname){
            params.set('lastname', req.patient1.lastname);
        }
        if(req.patient1.id){
            params.set('id', req.patient1.id);
        }
        if(req.patient1.gender){
            params.set('gender', req.patient1.gender);
        }
        if(req.patient1.height){
            params.set('height', req.patient1.height);
        }

        const options = {
            search: params
        };
        return this.http.get(this.resourceUrl, options).map((res: Response) => {
            return res;

        });

    }



    search1(patient: Patient, pageNumber?: any, pageSize?: any, pageSort?: any): Observable<ResponseWrapper> {
        // alert('page numbaer ====> =' + pageNumber) ;
        // alert('page numbaer ====> =' + pageSize) ;
        // alert('page numbaer ====> =' + pageSort) ;
        // let postData = new FormData();
        // const body = JSON.stringify(patient);
        //
        // postData.append('pageNumber', pageNumber);
        // postData.append('size', pageSize);
        // postData.append('sort', pageSort);
        //
        // // this.spinner.show();

        return this.http.get(`${this.resourceUrl}?firstname=${patient.firstname}&id=${patient.id}&pageNumber=${pageNumber}&size=${pageSize}&sort=${pageSort}`).map((res: Response) => {
            return res;

        });

    }

    find(id: number): Observable<Patient> {
        //alert(id);
        return this.http.get(`${this.resourceUrlPatientId}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }


    private convertItemFromServer(json: any): Patient {
        const entity: Patient = Object.assign(new Patient(), json);
        // entity.creationDate = this.dateUtils
        //     .convertLocalDateFromServer(json.creationDate);
        return entity;
    }






}
