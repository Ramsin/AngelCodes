import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {ActivatedRoute, Router, RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {HttpModule} from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import {MultiselectDropdownModule} from 'angular-2-dropdown-multiselect';
import {NgbModal, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormsModule, ReactiveFormsModule, Validators} from '@angular/forms';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';
import {JhiDateUtils, NgJhipsterModule} from 'ng-jhipster';
import {CredentialApplicationFormService} from '../form/credential-application-form.service';
import {CredentialAOSMService} from '../form/credential-aosm.service';
import {TrainingFellowshipsService} from '../form/training-fellowships.service';
import {HospitalUniversityAffiliationService} from '../form/hospital-university-affiliation.service';
import {FormService} from '../form/form.service';
//import {RejectEmailPopUpComponent} from './rejectemail.popup.component';
import {CKEditorModule} from 'ng2-ckeditor';
import {DocumentVersionComponent} from '../document-version/document-version.component';
import {customHttpProvider} from '../../shared/interceptor/http.provider';
import {GeneralService} from '../../shared/service/general.service';
import {CredentialInternshipService} from '../form/credential-internship.service';
import {DocumentCRService} from '../report-document/document-cr.service';
import {EmployeeService} from '../employee/employee.service';
import {Principal} from '../../shared/auth/principal.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {EmployeeFileService} from '../employee/employee-file.service';
import {ToastrService} from 'ngx-toastr';
import {EmployeeFileDialogComponent} from '../employee/employee-file-dialog.component';
import {FileDropModule} from 'ngx-file-drop';
import {CredentialMlCertification1Service} from '../form/credential-ml-certification1.service';
import {CredentialMlCertification2Service} from '../form/credential-ml-certification2.service';
import {CredentialFormNewComponent} from './CredentialFormNew.component';


export const routes = [
    {
        path: '', component: CredentialFormNewComponent, pathMatch: 'full',
        // children: [
        //     {path: '', redirectTo: '/personal', pathMatch: 'full'},
        //
        //     // {path: '/personal', component: PersonalComponent}
        // ]
    }
];



// private formService: FormService, private modalService: NgbModal, private trainingService: TrainingFellowshipsService, private dateUtils: JhiDateUtils,
//     private generalService: GeneralService, private credentialAOSMService: CredentialAOSMService,private credentialInternshipService:CredentialInternshipService, private hospitalService: HospitalUniversityAffiliationService,
//     private  fb: FormBuilder, private credentialApplicationFormService: CredentialApplicationFormService,


// private formService: FormService, private modalService: NgbModal, private trainingService: TrainingFellowshipsService, private dateUtils: JhiDateUtils,
//     private generalService: GeneralService, private credentialAOSMService: CredentialAOSMService,private credentialInternshipService:CredentialInternshipService, private hospitalService: HospitalUniversityAffiliationService,
//     private  fb: FormBuilder, private credentialApplicationFormService: CredentialApplicationFormService,
//     private employeeService: EmployeeService,
//     private documentCRService: DocumentCRService,
//     private activatedRoute: ActivatedRoute,
//     private employeeFileService: EmployeeFileService,
//     private router: Router,
//     private principal: Principal,
//     private spinner: NgxSpinnerService,
//     public toastrService: ToastrService

// @NgModule({
//    imports: [
//              CommonModule, HttpModule, HttpClientModule, FormsModule, NgbModule.forRoot(), MultiselectDropdownModule
//         , AngularMultiSelectModule, NgJhipsterModule,
//         ReactiveFormsModule, CKEditorModule, AppRoutingModule, FileDropModule
//        , RouterModule.forChild(routes)
//        // , RouterModule
//     ],
//     declarations: [
//          ProfileComponent,  RejectEmailPopUpComponent, EmployeeFileDialogComponent
//     ],
//     entryComponents: [
//          // ProfileComponent,
//     RejectEmailPopUpComponent, EmployeeFileDialogComponent
//     ],
//     providers: [
//         FormService,
//         CredentialAOSMService,
//         TrainingFellowshipsService, HospitalUniversityAffiliationService, CredentialApplicationFormService
//         , GeneralService, CredentialInternshipService, EmployeeFileService, DocumentCRService, EmployeeService
//         , customHttpProvider()
//     ],
//     schemas: [CUSTOM_ELEMENTS_SCHEMA]
// })
// export class ProfileModule {
//
//
//
// }



@NgModule({
    imports: [
        CommonModule, HttpModule, HttpClientModule, FormsModule, RouterModule.forChild(routes), MultiselectDropdownModule
        , AngularMultiSelectModule, NgJhipsterModule,NgbModule.forRoot(),
        ReactiveFormsModule, CKEditorModule
    ],
    declarations: [
        CredentialFormNewComponent //,  RejectEmailPopUpComponent, EmployeeFileDialogComponent
    ],
    entryComponents: [
        CredentialFormNewComponent //, RejectEmailPopUpComponent, EmployeeFileDialogComponent
    ],
    providers: [

                FormService,
        CredentialAOSMService,
        TrainingFellowshipsService, HospitalUniversityAffiliationService, CredentialApplicationFormService
        , GeneralService, CredentialInternshipService, CredentialMlCertification1Service, CredentialMlCertification2Service, EmployeeFileService, DocumentCRService, EmployeeService
        , customHttpProvider()
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CredentialFormNewModule {



}