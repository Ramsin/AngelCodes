import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {ResponseWrapper} from '../../shared/model/response-wrapper.model';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbDateParserFormatter, NgbDateStruct, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {GeneralService} from '../../shared/service/general.service';
import {Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import {JhiEventManager, JhiAlertService, JhiDataUtils, JhiDateUtils} from 'ng-jhipster';
import {AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Position} from '../../shared/model/position.model';
import {CredentialAOSM} from '../../shared/model/form-models/credential-aosm.model';
import {Page1} from '../../shared/model/form-models/credentialing-app-page-1.model';
import {Page2} from '../../shared/model/form-models/credentialing-app-page-2.model';
import {until} from 'selenium-webdriver';
import {Page3} from '../../shared/model/form-models/credentialing-app-page-3.model';
import {TrainingFellowships} from '../../shared/model/form-models/training-fellowships.model';
import {Page5} from '../../shared/model/form-models/credentialing-app-page-5.model';
import {HospitalUniversityAffiliation} from '../../shared/model/form-models/hospital-university-affiliation.model';
import {Page4} from 'app/shared/model/form-models/credentialing-app-page-4.model';
import {Page6} from '../../shared/model/form-models/credentialing-app-page-6.model';
import {Page7} from '../../shared/model/form-models/credentialing-app-page-7.model';
import {Page8} from '../../shared/model/form-models/credentialing-app-page-8.model';
import {Page9} from 'app/shared/model/form-models/credentialing-app-page-9.model';
import {DatePipe} from '@angular/common';
import {isNullOrUndefined} from 'util';
import {CredentialApplicationForm} from '../../shared/model/form-models/credential-application-form.model';
import {FormService} from '../form/form.service';
import {CredentialApplicationFormService} from '../form/credential-application-form.service';
import {HospitalUniversityAffiliationService} from '../form/hospital-university-affiliation.service';
import {CredentialAOSMService} from '../form/credential-aosm.service';
import {TrainingFellowshipsService} from '../form/training-fellowships.service';
import {EmployeeService} from '../employee/employee.service';
import {Employee} from '../../shared/model/employee.model';
import {Clinic} from '../../shared/model/clinic.model';
// import {EmployeeFileService} from '../employee/employee-file.service';
// import {EmployeeFile} from '../../shared/model/employee-file.model';
// import {EmployeeFileDialogComponent} from '../employee/employee-file-dialog.component';
import {Principal} from '../../shared/auth/principal.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {NgbDateFRParserFormatter} from '../employee/NgbDateFRParserFormatter';
import {DocumentCR} from '../../shared/model/document-cr.model';
import {DocumentCRService} from '../report-document/document-cr.service';
import {ToastrService} from 'ngx-toastr';
import {EMAIL_ALREADY_USED_TYPE, EMPLOYEE_NAME_ALREADY_USED_TYPE, LOGIN_ALREADY_USED_TYPE} from '../../shared/constants/error.constants';
//import {RejectEmailPopUpComponent} from './rejectemail.popup.component';
import {CredentialInternship} from '../../shared/model/form-models/credential-internship.model';
import {CredentialInternshipService} from '../form/credential-internship.service';
import {State} from '../../shared/model/state.model';
import {CredentialMLCertification1} from '../../shared/model/form-models/credential-mlCertification1.model';
import {CredentialMLCertification2} from '../../shared/model/form-models/credential-mlCertification2.model';
import{CredentialMlCertification1Service} from '../form/credential-ml-certification1.service';
import{CredentialMlCertification2Service} from '../form/credential-ml-certification2.service';
import {V} from '@angular/core/src/render3';
import {FormNewApproachModel} from '../../shared/model/form-models/formNewApproach.model';


@Component({
    selector: 'app-credentialFormNew',
    templateUrl: './credentialFormNew.component.html',
    styleUrls: ['./credentialFormNew.component.scss'],
    encapsulation: ViewEncapsulation.None
    , providers: [{provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter}]

})
export class CredentialFormNewComponent implements OnInit {
    @Input() employeeDetail;


    positions: Position[];
    clinics: Clinic[];

    isClicked = [];

    param1: any;
    public serverValidationError = false;
    public serverValidationErrorName = false;


    employee: Employee = new Employee();

    clinicsDropdownSettings = {};
    selectedItems = [];

    links: any;
    link: string;
    documentCrs: DocumentCR[];

    expirationDate_from: string;
    expirationDate_to: string;
    public genders = ['Male', 'Female'];

    minDate = {year: 1900, month: 1, day: 1};
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    sortList: any;
    sortPredeict: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    showForm: Boolean = false;
    showFormInfo: any = 'info';
    readonlyForm: Boolean = false;
    itemName_p: String = 'test';
    // file: EmployeeFile = new EmployeeFile();

    fixedNumberOfAosms = 20;

    profileImg: any = 'assets/img/users/default-user.jpg';

    isCredential = true;
    page1: Page1 = new Page1();
    page2: Page2 = new Page2();
    page3: Page3 = new Page3();
    page4: Page4 = new Page4();
    page5: Page5 = new Page5();
    page6: Page6 = new Page6();
    page7: Page7 = new Page7();
    page8: Page8 = new Page8();
    page9: Page9 = new Page9();
    isSaving: boolean;
    saveForPage1 : boolean = false;
    saveForPage2 : boolean = false;
    saveForPage3 : boolean = false;
    saveForPage4 : boolean = false;
    saveForPage5 : boolean = false;
    saveForPage6 : boolean = false;
    saveForPage7 : boolean = false;
    saveForPage8 : boolean = false;
    saveForPage9 : boolean = false;
    isSubmitted: boolean = false;
    loaderPage1 = false;
    loaderPage2 = false;
    loaderPage3 = false;
    loaderPage4 = false;
    loaderPage5 = false;
    loaderPage7 = false;
    loaderPage9 = false;
    // isSubmitted = false;
    disableForm = false;
    currentUserForm: CredentialApplicationForm = new CredentialApplicationForm();
    dateOfBirthDp: any;
    expiresDate1Db: any;
    expiresDate2Db: any;
    expiresDate3Db: any;
    originalDateIssueDb: any;
    dateOfGraduationPreMedicalDb: any;
    dateOfGraduationMedicalDb: any;
    dateOfGraduationPofessionalEduDb: any;
    originalDateOfIssueGrid: any;
    datesAttendendResidencyProgramDb: any;
    datesPracticingPreviousGroupDb: any;
    datesPracticingPreviousGroup2Db: any;
    datesPracticingPreviousGroup3Db: any;
    dateOfCertificationDb: any;
    professionalLiabilityEffectiveDateDb: any;
    professionalLiabilityExpirationDateDb: any;
    dateDb: any
    states:State[];
    duplicatedName = false;
    formPage1: FormGroup;
    formPage2: FormGroup;
    formPage3: FormGroup;
    formPage4: FormGroup;
    formPage5: FormGroup;
    formPage6: FormGroup;
    formPage7: FormGroup;
    formPage8: FormGroup;
    formPage9: FormGroup;
    employeeForm: FormGroup;
    emailControl: AbstractControl;
    cellPhoneControl: AbstractControl;

    id: AbstractControl;
    firstName1: AbstractControl;
    lastName1: AbstractControl;
    middle1: AbstractControl;
    lastName2: AbstractControl;
    firstName2: AbstractControl;
    middle2: AbstractControl;
    ss: AbstractControl;
    npiNumber: AbstractControl;
    dateOfBirth: AbstractControl;
    placeOfBirth: AbstractControl;
    citizenship: AbstractControl;
    yearsAssociated: AbstractControl;
    yearsAssociatedEnd: AbstractControl;
    primaryProfessionalGNA: AbstractControl;
    cityInfo1: AbstractControl;
    stateInfo1: AbstractControl;
    zipInfo1: AbstractControl;
    telephoneNumber: AbstractControl;
    faxInfo1: AbstractControl;
    emailInfo1: AbstractControl;
    homeAddress: AbstractControl;
    homeTelNumber: AbstractControl;
    cityInfo2: AbstractControl;
    stateInfo2: AbstractControl;
    zipInfo2: AbstractControl;
    stateAndLicenseNumber: AbstractControl;
    licenseNumberMedicalLicensure: AbstractControl;
    expiresDate1: AbstractControl;
    originalDateIssue: AbstractControl;
    collegeUniversity: AbstractControl;
    degreesHonorsPreMedical: AbstractControl;
    dateOfGraduationPreMedical: AbstractControl;
    addressPreMedical: AbstractControl;
    cityPreMedical: AbstractControl;
    statePreMedical: AbstractControl;
    zipPreMedical: AbstractControl;
    professionalSchoolmedical: AbstractControl;
    degreesHonorsMedical: AbstractControl;
    dateOfGraduationMedical: AbstractControl;
    addressMedical: AbstractControl;
    cityMedical: AbstractControl;
    stateMedical: AbstractControl;
    zipMedical: AbstractControl;
    nameInstitution: AbstractControl;
    degreesHonorsPofessionalEdu: AbstractControl;
    dateOfGraduationPofessionalEdu: AbstractControl;
    addressPofessionalEdu: AbstractControl;
    cityPofessionalEdu: AbstractControl;
    statePofessionalEdu: AbstractControl;
    zipPofessionalEdu: AbstractControl;

    nameInstitutionResidencyProgram: AbstractControl;
    isNaResidencyPrograms: AbstractControl;
    datesAttendendResidencyProgram: AbstractControl;
    addressResidencyProgram: AbstractControl;
    cityResidencyProgram: AbstractControl;
    stateResidencyProgram: AbstractControl;
    zipResidencyProgram: AbstractControl;
    typeOfResidency: AbstractControl;
    fullNameOfProgramDirectorResidencyProgram: AbstractControl;
    isProgramSuccessfullyCompletedResidency: AbstractControl;
    programSuccessfullyCompletedResidency: AbstractControl;

    typeOfOrganizationPreviousGroup: AbstractControl;
    nameOfOrganizationPreviousGroup: AbstractControl;
    addressOfOrganizationPreviousGroup: AbstractControl;
    cityOrganizationPreviousGroup: AbstractControl;
    stateOrganizationPreviousGroup: AbstractControl;
    zipOrganizationPreviousGroup: AbstractControl;
    datesPracticingPreviousGroup: AbstractControl;
    datesPracticingPreviousGroupEnd: AbstractControl;
    typeOfOrganizationPreviousGroup2: AbstractControl;
    nameOfOrganizationPreviousGroup2: AbstractControl;
    addressOfOrganizationPreviousGroup2: AbstractControl;
    cityOrganizationPreviousGroup2: AbstractControl;
    stateOrganizationPreviousGroup2: AbstractControl;
    zipOrganizationPreviousGroup2: AbstractControl;
    datesPracticingPreviousGroup2: AbstractControl;
    datesPracticingPreviousGroupEnd2: AbstractControl;
    typeOfOrganizationPreviousGroup3: AbstractControl;
    nameOfOrganizationPreviousGroup3: AbstractControl;
    addressOfOrganizationPreviousGroup3: AbstractControl;
    cityOrganizationPreviousGroup3: AbstractControl;
    stateOrganizationPreviousGroup3: AbstractControl;
    zipOrganizationPreviousGroup3: AbstractControl;
    datesPracticingPreviousGroup3: AbstractControl;
    datesPracticingPreviousGroupEnd3: AbstractControl;
    certifiedByAmericanBoardOf: AbstractControl;
    certificationNumber: AbstractControl;
    dateOfCertification: AbstractControl;
    dateOfCertification2: AbstractControl;
    subSpecialityBoardStatus: AbstractControl;
    certificationNumber2: AbstractControl;
    ifNotCertifiedGivePresentStatus: AbstractControl;
    ifNotCertifiedGivePresentStatusDate: AbstractControl;
    ifNotCertifiedGivePresentStatusDateOfExam: AbstractControl;
    listAllMemberShipPastAndPresentAndPEnding: AbstractControl;
    professionalPeerReferenceLastName1: AbstractControl;
    professionalPeerReferenceFirstName1: AbstractControl;
    professionalPeerReferenceMiddleName1: AbstractControl;
    professionalPeerReferenceTitle1: AbstractControl;
    professionalPeerReferenceRelationShip1: AbstractControl;
    professionalPeerReferenceDegree1: AbstractControl;
    professionalPeerReferenceSpecialities1: AbstractControl;
    professionalPeerReferenceYearsKnown1: AbstractControl;
    professionalPeerReferenceAddress1: AbstractControl;
    professionalPeerReferenceCity1: AbstractControl;
    professionalPeerReferenceState1: AbstractControl;
    professionalPeerReferenceZip1: AbstractControl;
    professionalPeerReferencePhone1: AbstractControl;
    professionalPeerReferencefax1: AbstractControl;
    professionalPeerReferenceEmail1: AbstractControl;
    professionalPeerReferenceLastName2: AbstractControl;
    professionalPeerReferenceFirstName2: AbstractControl;
    professionalPeerReferenceMiddleName2: AbstractControl;
    professionalPeerReferenceTitle2: AbstractControl;
    professionalPeerReferenceRelationShip2: AbstractControl;
    professionalPeerReferenceDegree2: AbstractControl;
    professionalPeerReferenceSpecialities2: AbstractControl;
    professionalPeerReferenceYearsKnown2: AbstractControl;
    professionalPeerReferenceAddress2: AbstractControl;
    professionalPeerReferenceCity2: AbstractControl;
    professionalPeerReferenceState2: AbstractControl;
    professionalPeerReferenceZip2: AbstractControl;
    professionalPeerReferencePhone2: AbstractControl;
    professionalPeerReferencefax2: AbstractControl;
    professionalPeerReferenceEmail2: AbstractControl;
    professionalPeerReferenceLastName3: AbstractControl;
    professionalPeerReferenceFirstName3: AbstractControl;
    professionalPeerReferenceMiddleName3: AbstractControl;
    professionalPeerReferenceTitle3: AbstractControl;
    professionalPeerReferenceRelationShip3: AbstractControl;
    professionalPeerReferenceDegree3: AbstractControl;
    professionalPeerReferenceSpecialities3: AbstractControl;
    professionalPeerReferenceYearsKnown3: AbstractControl;
    professionalPeerReferenceAddress3: AbstractControl;
    professionalPeerReferenceCity3: AbstractControl;
    professionalPeerReferenceState3: AbstractControl;
    professionalPeerReferenceZip3: AbstractControl;
    professionalPeerReferencePhone3: AbstractControl;
    professionalPeerReferenceFax3: AbstractControl;
    professionalPeerReferenceEmail3: AbstractControl;
    professionalLiabilityInsuranceCarrier: AbstractControl;
    professionalLiabilityAddress: AbstractControl;
    professionalLiabilityCity: AbstractControl;
    professionalLiabilityState: AbstractControl;
    professionalLiabilityZip: AbstractControl;
    professionalLiabilityPolicyLimits: AbstractControl;
    professionalLiabilityPerOccurrence: AbstractControl;
    professionalLiabilityAggregate: AbstractControl;
    professionalLiabilityPolicyNumber: AbstractControl;
    professionalLiabilityAgent: AbstractControl;
    professionalLiabilityEffectiveDate: AbstractControl;
    professionalLiabilityExpirationDate: AbstractControl;
    professionalLiabilityClaimsMade: AbstractControl;
    professionalLiabilityLawSuitAgainstYouYes: AbstractControl;
    professionalLiabilityLawSuitAgainstYouNo: AbstractControl;
    professionalLiabilityAnyNowPendingYes: AbstractControl;
    professionalLiabilityAnyNowPendingNo: AbstractControl;
    professionalLiabilityAnyJudgementOrPaymentOrClaimYes: AbstractControl;
    professionalLiabilityAnyJudgementOrPaymentOrClaimNo: AbstractControl;
    professionalLiabilityDeniedProfessionalInsuranceYes: AbstractControl;
    professionalLiabilityDeniedProfessionalInsuranceNo: AbstractControl;

    personalSanctionsLicenseToPracticeDeniedYes: AbstractControl;
    personalSanctionsLicenseToPracticeDeniedNo: AbstractControl;
    personalSanctionsLicenseToReprimandedSubjectYes: AbstractControl;
    personalSanctionsLicenseToReprimandedSubjectNo: AbstractControl;
    personalSanctionsLostBoardYes: AbstractControl;
    personalSanctionsLostBoardNo: AbstractControl;
    personalSanctionsCapitalCertifyingYes: AbstractControl;
    personalSanctionsCapitalCertifyingNo: AbstractControl;
    personalSanctionsInformationPertainingYes: AbstractControl;
    personalSanctionsInformationPertainingNo: AbstractControl;
    personalSanctionsFederalDEANumberYes: AbstractControl;
    personalSanctionsFederalDEANumberNo: AbstractControl;
    personalSanctionsHospitalAmbulatoryYes: AbstractControl;
    personalSanctionsHospitalAmbulatoryNo: AbstractControl;
    personalSanctionsVoluntarilyAndInVoluntarilyYes: AbstractControl;
    personalSanctionsVoluntarilyAndInVoluntarilyNo: AbstractControl;
    personalSanctionsDisciplinaryActionYes: AbstractControl;
    personalSanctionsDisciplinaryActionNo: AbstractControl;
    personalSanctionsCensuredOrExcludeOrSuspendActionYes: AbstractControl;
    personalSanctionsCensuredOrExcludeOrSuspendActionNo: AbstractControl;
    personalSanctionsMedicareMedicaidChampusYes: AbstractControl;
    personalSanctionsMedicareMedicaidChampusNo: AbstractControl;
    personalSanctionsDeniedMembershipSubjectYes: AbstractControl;
    personalSanctionsDeniedMembershipSubjectNo: AbstractControl;
    personalSanctionsWithdrawnAnApplicationYes: AbstractControl;
    personalSanctionsWithdrawnAnApplicationNo: AbstractControl;
    personalSanctionsChargedOrConvictedForACrimeYes: AbstractControl;
    personalSanctionsChargedOrConvictedForACrimeNo: AbstractControl;
    personalSanctionsSubjectOfCivilOrCriminalYes: AbstractControl;
    personalSanctionsSubjectOfCivilOrCriminalNo: AbstractControl;
    personalSanctionsHaveMedicalConditionYes: AbstractControl;
    personalSanctionsHaveMedicalConditionNo: AbstractControl;
    personalSanctionsUnableToPerformEssentialYes: AbstractControl;
    personalSanctionsUnableToPerformEssentialNo: AbstractControl;
    personalSanctionsAnyReligiousYesRestrictionYes: AbstractControl;
    personalSanctionsAnyReligiousYesRestrictionNo: AbstractControl;
    personalSanctionsAnyReligiousYesExplain: AbstractControl;

    personalSanctionsCurrentlyEngagedYes: AbstractControl;
    personalSanctionsCurrentlyEngagedNo: AbstractControl;
    personalSanctionsChemicalSubstancesYes: AbstractControl;
    personalSanctionsChemicalSubstancesNo: AbstractControl;
    printName: AbstractControl;
    date: AbstractControl;
    personalSanctionsIfYesExplain: AbstractControl;
    // dataList:Car[] = [];
    // i: number = 0;
    aosmList: CredentialAOSM[] = [new CredentialAOSM()];
    aosmCounter = 0;
    test: CredentialAOSM = new CredentialAOSM();

    internshipList: CredentialInternship[] = [ new CredentialInternship()];
    internshipCounter = 0;
    testInternship: CredentialInternship = new CredentialInternship();


    public ListClinics = [{id: 1, itemName: 'Atlanta'}];

    // employeeFiles: EmployeeFile[] = [];
    // employeeFileImg: EmployeeFile = new EmployeeFile();

    mlCertification1SList: CredentialMLCertification1[] = [ new CredentialMLCertification1()];
    mlCertification1SCounter = 0;
    testMlCertification1S: CredentialMLCertification1 = new CredentialMLCertification1();

    mlCertification2SList: CredentialMLCertification2[] = [ new CredentialMLCertification2()];
    mlCertification2SCounter = 0;
    testMlCertification2S: CredentialMLCertification2 = new CredentialMLCertification2();

    constructor(private formService: FormService, private modalService: NgbModal, private trainingService: TrainingFellowshipsService, private dateUtils: JhiDateUtils,
                private generalService: GeneralService, private credentialAOSMService: CredentialAOSMService,private  credentialMlCertification1Service: CredentialMlCertification1Service ,
                private  credentialMlCertification2Service: CredentialMlCertification2Service , private credentialInternshipService:CredentialInternshipService, private hospitalService: HospitalUniversityAffiliationService,
                private  fb: FormBuilder, private credentialApplicationFormService: CredentialApplicationFormService,
                private employeeService: EmployeeService,
                private documentCRService: DocumentCRService,
                private activatedRoute: ActivatedRoute,
                //  private employeeFileService: EmployeeFileService,
                private router: Router,
                private principal: Principal,
                private spinner: NgxSpinnerService,
                public toastrService: ToastrService) {


        this.formPage1 = fb.group({
            'id': ['', Validators.compose([])],
            'firstName1': ['', [Validators.required]],
            'lastName1': ['', [Validators.required]],
            'middle1': ['', []],
            'lastName2': ['', []],
            'firstName2': ['', []],
            'middle2': ['', []],
            'ss': ['', [Validators.required]],
            'npiNumber': ['', []],
            'dateOfBirth': ['', [Validators.required]],
            'placeOfBirth': ['', [Validators.required]],
            'citizenship': ['', [Validators.required]],
            'primaryProfessionalGNA': ['', [Validators.required]],
            'yearsAssociated': ['', [Validators.required]],
            'yearsAssociatedEnd': ['', [Validators.required]],
            'cityInfo1': ['', [Validators.required]],
            'stateInfo1': ['', [Validators.required]],
            'zipInfo1': ['', [Validators.required]],
            'telephoneNumber': ['', [Validators.required]],
            'faxInfo1': ['', []],
            'emailInfo1': ['', [Validators.required, Validators.email]],
            'homeAddress': ['', [Validators.required]],
            'homeTelNumber': ['', [Validators.required]],
            'cityInfo2': ['', [Validators.required]],
            'stateInfo2': ['', [Validators.required]],
            'zipInfo2': ['', [Validators.required]],
            'stateAndLicenseNumber': ['', [Validators.required]],
            'licenseNumberMedicalLicensure': ['', [Validators.required]],
            'expiresDate1': ['', [Validators.required]],
            'originalDateIssue': ['', [Validators.required]],
            mlCertification1s: this.fb.array([
                this.initMlCertification1s(),
            ]),

            mlCertification2s: this.fb.array([
                this.initMlCertification2s(),
            ]),
        });
        this.formPage2 = fb.group({
            'collegeUniversity': ['', [Validators.required]],
            'degreesHonorsPreMedical': ['', [Validators.required]],
            'dateOfGraduationPreMedical': ['', [Validators.required]],
            'addressPreMedical': ['', [Validators.required]],
            'cityPreMedical': ['', [Validators.required]],
            'statePreMedical': ['', [Validators.required]],
            'zipPreMedical': ['', Validators.compose([Validators.required])],
            'professionalSchoolmedical': ['', Validators.compose([Validators.required])],
            'degreesHonorsMedical': ['', Validators.compose([Validators.required])],
            'dateOfGraduationMedical': ['', Validators.compose([Validators.required])],
            'addressMedical': ['', Validators.compose([Validators.required])],
            'cityMedical': ['', Validators.compose([Validators.required])],
            'stateMedical': ['', Validators.compose([Validators.required])],
            'zipMedical': ['', Validators.compose([Validators.required])],
            'nameInstitution': ['', []],
            'degreesHonorsPofessionalEdu': ['', []],
            'dateOfGraduationPofessionalEdu': ['', []],
            'addressPofessionalEdu': ['', []],
            'cityPofessionalEdu': ['', []],
            'statePofessionalEdu': ['', []],
            'zipPofessionalEdu': ['', []],

            aosms: this.fb.array([
                this.initAosm(),
            ]),

            internships: this.fb.array([
                this.initInternship(),
            ])

        });
        this.formPage3 = fb.group({
            'nameInstitutionResidencyProgram': ['', []],
            'isNaResidencyPrograms': ['', []],
            'datesAttendendResidencyProgram': ['', []],
            'addressResidencyProgram': ['', [Validators.required]],
            'cityResidencyProgram': ['', [Validators.required]],
            'stateResidencyProgram': ['', [Validators.required]],
            'zipResidencyProgram': ['', [Validators.required]],
            'typeOfResidency': ['', [Validators.required]],
            'fullNameOfProgramDirectorResidencyProgram': ['', Validators.compose([Validators.required])],
            'isProgramSuccessfullyCompletedResidency': ['', Validators.compose([])],
            'programSuccessfullyCompletedResidency': ['', Validators.compose([])],

            trainings: this.fb.array([
                this.initTrainings(),
            ])

        });
        this.formPage5 = fb.group({
            'typeOfOrganizationPreviousGroup': ['', Validators.compose([])],
            'nameOfOrganizationPreviousGroup': ['', Validators.compose([])],
            'addressOfOrganizationPreviousGroup': ['', Validators.compose([])],
            'cityOrganizationPreviousGroup': ['', Validators.compose([])],
            'stateOrganizationPreviousGroup': ['', Validators.compose([])],
            'zipOrganizationPreviousGroup': ['', Validators.compose([])],
            'datesPracticingPreviousGroup': ['', Validators.compose([])],
            'datesPracticingPreviousGroupEnd': ['', Validators.compose([])],
            'typeOfOrganizationPreviousGroup2': ['', Validators.compose([])],
            'nameOfOrganizationPreviousGroup2': ['', Validators.compose([])],
            'addressOfOrganizationPreviousGroup2': ['', Validators.compose([])],
            'cityOrganizationPreviousGroup2': ['', Validators.compose([])],
            'stateOrganizationPreviousGroup2': ['', Validators.compose([])],
            'zipOrganizationPreviousGroup2': ['', Validators.compose([])],
            'datesPracticingPreviousGroup2': ['', Validators.compose([])],
            'datesPracticingPreviousGroupEnd2': ['', Validators.compose([])],
            'typeOfOrganizationPreviousGroup3': ['', Validators.compose([])],
            'nameOfOrganizationPreviousGroup3': ['', Validators.compose([])],
            'addressOfOrganizationPreviousGroup3': ['', Validators.compose([])],
            'cityOrganizationPreviousGroup3': ['', Validators.compose([])],
            'stateOrganizationPreviousGroup3': ['', Validators.compose([])],
            'zipOrganizationPreviousGroup3': ['', Validators.compose([])],
            'datesPracticingPreviousGroup3': ['', Validators.compose([])],
            'datesPracticingPreviousGroupEnd3': ['', Validators.compose([])],
            'certifiedByAmericanBoardOf': ['', Validators.compose([])],
            'certificationNumber': ['', Validators.compose([])],
            'dateOfCertification': ['', Validators.compose([])],
            'dateOfCertification2': ['', Validators.compose([])],
            'subSpecialityBoardStatus': ['', Validators.compose([])],
            'certificationNumber2': ['', Validators.compose([])],
            'ifNotCertifiedGivePresentStatus': ['', Validators.compose([])],
            'ifNotCertifiedGivePresentStatusDate': ['', Validators.compose([])],
            'ifNotCertifiedGivePresentStatusDateOfExam': ['', Validators.compose([])],
            'listAllMemberShipPastAndPresentAndPEnding': ['', Validators.compose([])],

        });
        this.formPage6 = fb.group({
            'professionalPeerReferenceLastName1': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceFirstName1': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceMiddleName1': ['', Validators.compose([])],
            'professionalPeerReferenceTitle1': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceRelationShip1': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceDegree1': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceSpecialities1': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceYearsKnown1': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceAddress1': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceCity1': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceState1': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceZip1': ['', Validators.compose([Validators.required])],
            'professionalPeerReferencePhone1': ['', Validators.compose([Validators.required])],
            'professionalPeerReferencefax1': ['', Validators.compose([])],
            'professionalPeerReferenceEmail1': ['', Validators.compose([Validators.required ,Validators.email])],
            'professionalPeerReferenceLastName2': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceFirstName2': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceMiddleName2': ['', Validators.compose([])],
            'professionalPeerReferenceTitle2': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceRelationShip2': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceDegree2': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceSpecialities2': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceYearsKnown2': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceAddress2': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceCity2': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceState2': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceZip2': ['', Validators.compose([Validators.required])],
            'professionalPeerReferencePhone2': ['', Validators.compose([Validators.required])],
            'professionalPeerReferencefax2': ['', Validators.compose([])],
            'professionalPeerReferenceEmail2': ['', Validators.compose([Validators.required , Validators.email])],
            'professionalPeerReferenceLastName3': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceFirstName3': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceMiddleName3': ['', Validators.compose([])],
            'professionalPeerReferenceTitle3': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceRelationShip3': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceDegree3': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceSpecialities3': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceYearsKnown3': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceAddress3': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceCity3': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceState3': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceZip3': ['', Validators.compose([Validators.required])],
            'professionalPeerReferencePhone3': ['', Validators.compose([Validators.required])],
            'professionalPeerReferenceFax3': ['', Validators.compose([])],
            'professionalPeerReferenceEmail3': ['', Validators.compose([Validators.required , Validators.email])],

        });
        this.formPage7 = fb.group({
            'professionalLiabilityInsuranceCarrier': ['', Validators.compose([Validators.required])],
            'professionalLiabilityAddress': ['', Validators.compose([])],
            'professionalLiabilityCity': ['', Validators.compose([])],
            'professionalLiabilityState': ['', Validators.compose([])],
            'professionalLiabilityZip': ['', Validators.compose([])],
            'professionalLiabilityPolicyLimits': ['', Validators.compose([Validators.required])],
            'professionalLiabilityPerOccurrence': ['', Validators.compose([Validators.required])],
            'professionalLiabilityAggregate': ['', Validators.compose([Validators.required])],
            'professionalLiabilityPolicyNumber': ['', Validators.compose([Validators.required])],
            'professionalLiabilityAgent': ['', Validators.compose([Validators.required])],
            'professionalLiabilityEffectiveDate': ['', Validators.compose([Validators.required])],
            'professionalLiabilityExpirationDate': ['', Validators.compose([Validators.required])],
            'professionalLiabilityClaimsMade': ['', Validators.compose([Validators.required])],
            'professionalLiabilityLawSuitAgainstYouYes': ['', Validators.compose([Validators.required])],
            'professionalLiabilityLawSuitAgainstYouNo': ['', Validators.compose([])],
            'professionalLiabilityAnyNowPendingYes': ['', Validators.compose([Validators.required])],
            'professionalLiabilityAnyNowPendingNo': ['', Validators.compose([])],
            'professionalLiabilityAnyJudgementOrPaymentOrClaimYes': ['', Validators.compose([Validators.required])],
            'professionalLiabilityAnyJudgementOrPaymentOrClaimNo': ['', Validators.compose([])],
            'professionalLiabilityDeniedProfessionalInsuranceYes': ['', Validators.compose([Validators.required])],
            'professionalLiabilityDeniedProfessionalInsuranceNo': ['', Validators.compose([])],


        });
        this.formPage8 = fb.group({
            'personalSanctionsLicenseToPracticeDeniedYes': ['', Validators.compose([])],
            'personalSanctionsLicenseToPracticeDeniedNo': ['', Validators.compose([])],
            'personalSanctionsLicenseToReprimandedSubjectYes': ['', Validators.compose([])],
            'personalSanctionsLicenseToReprimandedSubjectNo': ['', Validators.compose([])],
            'personalSanctionsLostBoardYes': ['', Validators.compose([])],
            'personalSanctionsLostBoardNo': ['', Validators.compose([])],
            'personalSanctionsCapitalCertifyingYes': ['', Validators.compose([])],
            'personalSanctionsCapitalCertifyingNo': ['', Validators.compose([])],
            'personalSanctionsInformationPertainingYes': ['', Validators.compose([])],
            'personalSanctionsInformationPertainingNo': ['', Validators.compose([])],
            'personalSanctionsFederalDEANumberYes': ['', Validators.compose([])],
            'personalSanctionsFederalDEANumberNo': ['', Validators.compose([])],
            'personalSanctionsHospitalAmbulatoryYes': ['', Validators.compose([])],
            'personalSanctionsHospitalAmbulatoryNo': ['', Validators.compose([])],
            'personalSanctionsVoluntarilyAndInVoluntarilyYes': ['', Validators.compose([])],
            'personalSanctionsVoluntarilyAndInVoluntarilyNo': ['', Validators.compose([])],
            'personalSanctionsDisciplinaryActionYes': ['', Validators.compose([])],
            'personalSanctionsDisciplinaryActionNo': ['', Validators.compose([])],
            'personalSanctionsCensuredOrExcludeOrSuspendActionYes': ['', Validators.compose([])],
            'personalSanctionsCensuredOrExcludeOrSuspendActionNo': ['', Validators.compose([])],
            'personalSanctionsMedicareMedicaidChampusYes': ['', Validators.compose([])],
            'personalSanctionsMedicareMedicaidChampusNo': ['', Validators.compose([])],
            'personalSanctionsDeniedMembershipSubjectYes': ['', Validators.compose([])],
            'personalSanctionsDeniedMembershipSubjectNo': ['', Validators.compose([])],
            'personalSanctionsWithdrawnAnApplicationYes': ['', Validators.compose([])],
            'personalSanctionsWithdrawnAnApplicationNo': ['', Validators.compose([])],
            'personalSanctionsChargedOrConvictedForACrimeYes': ['', Validators.compose([])],
            'personalSanctionsChargedOrConvictedForACrimeNo': ['', Validators.compose([])],
            'personalSanctionsSubjectOfCivilOrCriminalYes': ['', Validators.compose([])],
            'personalSanctionsSubjectOfCivilOrCriminalNo': ['', Validators.compose([])],
            'personalSanctionsHaveMedicalConditionYes': ['', Validators.compose([])],
            'personalSanctionsHaveMedicalConditionNo': ['', Validators.compose([])],
            'personalSanctionsUnableToPerformEssentialYes': ['', Validators.compose([])],
            'personalSanctionsUnableToPerformEssentialNo': ['', Validators.compose([])],
            'personalSanctionsAnyReligiousYesRestrictionYes': ['', Validators.compose([])],
            'personalSanctionsAnyReligiousYesRestrictionNo': ['', Validators.compose([])],
            'personalSanctionsAnyReligiousYesExplain': ['', Validators.compose([])],
        });
        this.formPage9 = fb.group({
            'personalSanctionsCurrentlyEngagedYes': ['', Validators.compose([])],
            'personalSanctionsCurrentlyEngagedNo': ['', Validators.compose([])],
            'personalSanctionsChemicalSubstancesYes': ['', Validators.compose([])],
            'personalSanctionsChemicalSubstancesNo': ['', Validators.compose([])],
            'date': ['', Validators.compose([Validators.required])],
            'printName': ['', Validators.compose([Validators.required])],
            'personalSanctionsIfYesExplain': ['', Validators.compose([])],
        });
        this.employeeForm = fb.group({
            'cellPhoneControl': ['', Validators.compose([])],
            'emailControl': ['', [Validators.required, Validators.email]],

            // 'cellPhoneControl': ['', Validators.compose([Validators.required])],

        });
        this.formPage4 = fb.group({
            hospitals: this.fb.array([
                this.initHospitals(),
            ])
        });

        this.firstName1 = this.formPage1.controls['firstName1'];
        this.lastName1 = this.formPage1.controls['lastName1'];
        this.middle1 = this.formPage1.controls['middle1'];
        this.lastName2 = this.formPage1.controls['lastName2'];
        this.firstName2 = this.formPage1.controls['firstName2'];
        this.middle2 = this.formPage1.controls['middle2'];
        this.ss = this.formPage1.controls['ss'];
        this.npiNumber = this.formPage1.controls['npiNumber'];
        this.dateOfBirth = this.formPage1.controls['dateOfBirth'];
        this.placeOfBirth = this.formPage1.controls['placeOfBirth'];
        this.citizenship = this.formPage1.controls['citizenship'];
        this.primaryProfessionalGNA = this.formPage1.controls['primaryProfessionalGNA'];
        this.yearsAssociated = this.formPage1.controls['yearsAssociated'];
        this.yearsAssociatedEnd = this.formPage1.controls['yearsAssociatedEnd'];
        this.cityInfo1 = this.formPage1.controls['cityInfo1'];
        this.stateInfo1 = this.formPage1.controls['stateInfo1'];
        this.zipInfo1 = this.formPage1.controls['zipInfo1'];
        this.telephoneNumber = this.formPage1.controls['telephoneNumber'];
        this.faxInfo1 = this.formPage1.controls['faxInfo1'];
        this.emailInfo1 = this.formPage1.controls['emailInfo1'];
        this.homeAddress = this.formPage1.controls['homeAddress'];
        this.homeTelNumber = this.formPage1.controls['homeTelNumber'];
        this.cityInfo2 = this.formPage1.controls['cityInfo2'];
        this.stateInfo2 = this.formPage1.controls['stateInfo2'];
        this.zipInfo2 = this.formPage1.controls['zipInfo2'];
        this.stateAndLicenseNumber = this.formPage1.controls['stateAndLicenseNumber'];
        this.licenseNumberMedicalLicensure = this.formPage1.controls['licenseNumberMedicalLicensure'];
        this.expiresDate1 = this.formPage1.controls['expiresDate1'];
        this.originalDateIssue = this.formPage1.controls['originalDateIssue'];


        this.collegeUniversity = this.formPage2.controls['collegeUniversity'];
        this.degreesHonorsPreMedical = this.formPage2.controls['degreesHonorsPreMedical'];
        this.dateOfGraduationPreMedical = this.formPage2.controls['dateOfGraduationPreMedical'];
        this.addressPreMedical = this.formPage2.controls['addressPreMedical'];
        this.cityPreMedical = this.formPage2.controls['cityPreMedical'];
        this.statePreMedical = this.formPage2.controls['statePreMedical'];
        this.zipPreMedical = this.formPage2.controls['zipPreMedical'];
        this.professionalSchoolmedical = this.formPage2.controls['professionalSchoolmedical'];
        this.degreesHonorsMedical = this.formPage2.controls['degreesHonorsMedical'];
        this.dateOfGraduationMedical = this.formPage2.controls['dateOfGraduationMedical'];
        this.addressMedical = this.formPage2.controls['addressMedical'];
        this.cityMedical = this.formPage2.controls['cityMedical'];
        this.stateMedical = this.formPage2.controls['stateMedical'];
        this.zipMedical = this.formPage2.controls['zipMedical'];
        this.nameInstitution = this.formPage2.controls['nameInstitution'];
        this.degreesHonorsPofessionalEdu = this.formPage2.controls['degreesHonorsPofessionalEdu'];
        this.dateOfGraduationPofessionalEdu = this.formPage2.controls['dateOfGraduationPofessionalEdu'];
        this.addressPofessionalEdu = this.formPage2.controls['addressPofessionalEdu'];
        this.cityPofessionalEdu = this.formPage2.controls['cityPofessionalEdu'];
        this.statePofessionalEdu = this.formPage2.controls['statePofessionalEdu'];
        this.zipPofessionalEdu = this.formPage2.controls['zipPofessionalEdu'];

        this.nameInstitutionResidencyProgram = this.formPage3.controls['nameInstitutionResidencyProgram'];
        this.isNaResidencyPrograms = this.formPage3.controls['isNaResidencyPrograms'];
        this.datesAttendendResidencyProgram = this.formPage3.controls['datesAttendendResidencyProgram'];
        this.addressResidencyProgram = this.formPage3.controls['addressResidencyProgram'];
        this.cityResidencyProgram = this.formPage3.controls['cityResidencyProgram'];
        this.stateResidencyProgram = this.formPage3.controls['stateResidencyProgram'];
        this.zipResidencyProgram = this.formPage3.controls['zipResidencyProgram'];
        this.typeOfResidency = this.formPage3.controls['typeOfResidency'];
        this.fullNameOfProgramDirectorResidencyProgram = this.formPage3.controls['fullNameOfProgramDirectorResidencyProgram'];
        this.isProgramSuccessfullyCompletedResidency = this.formPage3.controls['isProgramSuccessfullyCompletedResidency'];
        this.programSuccessfullyCompletedResidency = this.formPage3.controls['programSuccessfullyCompletedResidency'];

        this.typeOfOrganizationPreviousGroup = this.formPage5.controls['typeOfOrganizationPreviousGroup'];
        this.nameOfOrganizationPreviousGroup = this.formPage5.controls['nameOfOrganizationPreviousGroup'];
        this.addressOfOrganizationPreviousGroup = this.formPage5.controls['addressOfOrganizationPreviousGroup'];
        this.cityOrganizationPreviousGroup = this.formPage5.controls['cityOrganizationPreviousGroup'];
        this.stateOrganizationPreviousGroup = this.formPage5.controls['stateOrganizationPreviousGroup'];
        this.zipOrganizationPreviousGroup = this.formPage5.controls['zipOrganizationPreviousGroup'];
        this.datesPracticingPreviousGroup = this.formPage5.controls['datesPracticingPreviousGroup'];
        this.datesPracticingPreviousGroupEnd = this.formPage5.controls['datesPracticingPreviousGroupEnd'];
        this.typeOfOrganizationPreviousGroup2 = this.formPage5.controls['typeOfOrganizationPreviousGroup2'];
        this.nameOfOrganizationPreviousGroup2 = this.formPage5.controls['nameOfOrganizationPreviousGroup2'];
        this.addressOfOrganizationPreviousGroup2 = this.formPage5.controls['addressOfOrganizationPreviousGroup2'];
        this.cityOrganizationPreviousGroup2 = this.formPage5.controls['cityOrganizationPreviousGroup2'];
        this.stateOrganizationPreviousGroup2 = this.formPage5.controls['stateOrganizationPreviousGroup2'];
        this.zipOrganizationPreviousGroup2 = this.formPage5.controls['zipOrganizationPreviousGroup2'];
        this.datesPracticingPreviousGroup2 = this.formPage5.controls['datesPracticingPreviousGroup2'];
        this.datesPracticingPreviousGroupEnd2 = this.formPage5.controls['datesPracticingPreviousGroupEnd2'];
        this.typeOfOrganizationPreviousGroup3 = this.formPage5.controls['typeOfOrganizationPreviousGroup3'];
        this.nameOfOrganizationPreviousGroup3 = this.formPage5.controls['nameOfOrganizationPreviousGroup3'];
        this.addressOfOrganizationPreviousGroup3 = this.formPage5.controls['addressOfOrganizationPreviousGroup3'];
        this.cityOrganizationPreviousGroup3 = this.formPage5.controls['cityOrganizationPreviousGroup3'];
        this.stateOrganizationPreviousGroup3 = this.formPage5.controls['stateOrganizationPreviousGroup3'];
        this.zipOrganizationPreviousGroup3 = this.formPage5.controls['zipOrganizationPreviousGroup3'];
        this.datesPracticingPreviousGroup3 = this.formPage5.controls['datesPracticingPreviousGroup3'];
        this.datesPracticingPreviousGroupEnd3 = this.formPage5.controls['datesPracticingPreviousGroupEnd3'];
        this.certifiedByAmericanBoardOf = this.formPage5.controls['certifiedByAmericanBoardOf'];
        this.certificationNumber = this.formPage5.controls['certificationNumber'];
        this.dateOfCertification = this.formPage5.controls['dateOfCertification'];
        this.dateOfCertification2 = this.formPage5.controls['dateOfCertification2'];
        this.subSpecialityBoardStatus = this.formPage5.controls['subSpecialityBoardStatus'];
        this.certificationNumber2 = this.formPage5.controls['certificationNumber2'];
        this.ifNotCertifiedGivePresentStatus = this.formPage5.controls['ifNotCertifiedGivePresentStatus'];
        this.ifNotCertifiedGivePresentStatusDate = this.formPage5.controls['ifNotCertifiedGivePresentStatusDate'];
        this.ifNotCertifiedGivePresentStatusDateOfExam = this.formPage5.controls['ifNotCertifiedGivePresentStatusDateOfExam'];
        this.listAllMemberShipPastAndPresentAndPEnding = this.formPage5.controls['listAllMemberShipPastAndPresentAndPEnding'];

        this.professionalPeerReferenceLastName1 = this.formPage6.controls['professionalPeerReferenceLastName1'];
        this.professionalPeerReferenceFirstName1 = this.formPage6.controls['professionalPeerReferenceFirstName1'];
        this.professionalPeerReferenceMiddleName1 = this.formPage6.controls['professionalPeerReferenceMiddleName1'];
        this.professionalPeerReferenceTitle1 = this.formPage6.controls['professionalPeerReferenceTitle1'];
        this.professionalPeerReferenceRelationShip1 = this.formPage6.controls['professionalPeerReferenceRelationShip1'];
        this.professionalPeerReferenceDegree1 = this.formPage6.controls['professionalPeerReferenceDegree1'];
        this.professionalPeerReferenceSpecialities1 = this.formPage6.controls['professionalPeerReferenceSpecialities1'];
        this.professionalPeerReferenceYearsKnown1 = this.formPage6.controls['professionalPeerReferenceYearsKnown1'];
        this.professionalPeerReferenceAddress1 = this.formPage6.controls['professionalPeerReferenceAddress1'];
        this.professionalPeerReferenceCity1 = this.formPage6.controls['professionalPeerReferenceCity1'];
        this.professionalPeerReferenceState1 = this.formPage6.controls['professionalPeerReferenceState1'];
        this.professionalPeerReferenceZip1 = this.formPage6.controls['professionalPeerReferenceZip1'];
        this.professionalPeerReferencePhone1 = this.formPage6.controls['professionalPeerReferencePhone1'];
        this.professionalPeerReferencefax1 = this.formPage6.controls['professionalPeerReferencefax1'];
        this.professionalPeerReferenceEmail1 = this.formPage6.controls['professionalPeerReferenceEmail1'];
        this.professionalPeerReferenceLastName2 = this.formPage6.controls['professionalPeerReferenceLastName2'];
        this.professionalPeerReferenceFirstName2 = this.formPage6.controls['professionalPeerReferenceFirstName2'];
        this.professionalPeerReferenceMiddleName2 = this.formPage6.controls['professionalPeerReferenceMiddleName2'];
        this.professionalPeerReferenceTitle2 = this.formPage6.controls['professionalPeerReferenceTitle2'];
        this.professionalPeerReferenceRelationShip2 = this.formPage6.controls['professionalPeerReferenceRelationShip2'];
        this.professionalPeerReferenceDegree2 = this.formPage6.controls['professionalPeerReferenceDegree2'];
        this.professionalPeerReferenceSpecialities2 = this.formPage6.controls['professionalPeerReferenceSpecialities2'];
        this.professionalPeerReferenceYearsKnown2 = this.formPage6.controls['professionalPeerReferenceYearsKnown2'];
        this.professionalPeerReferenceAddress2 = this.formPage6.controls['professionalPeerReferenceAddress2'];
        this.professionalPeerReferenceCity2 = this.formPage6.controls['professionalPeerReferenceCity2'];
        this.professionalPeerReferenceState2 = this.formPage6.controls['professionalPeerReferenceState2'];
        this.professionalPeerReferenceZip2 = this.formPage6.controls['professionalPeerReferenceZip2'];
        this.professionalPeerReferencePhone2 = this.formPage6.controls['professionalPeerReferencePhone2'];
        this.professionalPeerReferencefax2 = this.formPage6.controls['professionalPeerReferencefax2'];
        this.professionalPeerReferenceEmail2 = this.formPage6.controls['professionalPeerReferenceEmail2'];
        this.professionalPeerReferenceLastName3 = this.formPage6.controls['professionalPeerReferenceLastName3'];
        this.professionalPeerReferenceFirstName3 = this.formPage6.controls['professionalPeerReferenceFirstName3'];
        this.professionalPeerReferenceMiddleName3 = this.formPage6.controls['professionalPeerReferenceMiddleName3'];
        this.professionalPeerReferenceTitle3 = this.formPage6.controls['professionalPeerReferenceTitle3'];
        this.professionalPeerReferenceRelationShip3 = this.formPage6.controls['professionalPeerReferenceRelationShip3'];
        this.professionalPeerReferenceDegree3 = this.formPage6.controls['professionalPeerReferenceDegree3'];
        this.professionalPeerReferenceSpecialities3 = this.formPage6.controls['professionalPeerReferenceSpecialities3'];
        this.professionalPeerReferenceYearsKnown3 = this.formPage6.controls['professionalPeerReferenceYearsKnown3'];
        this.professionalPeerReferenceAddress3 = this.formPage6.controls['professionalPeerReferenceAddress3'];
        this.professionalPeerReferenceCity3 = this.formPage6.controls['professionalPeerReferenceCity3'];
        this.professionalPeerReferenceState3 = this.formPage6.controls['professionalPeerReferenceState3'];
        this.professionalPeerReferenceZip3 = this.formPage6.controls['professionalPeerReferenceZip3'];
        this.professionalPeerReferencePhone3 = this.formPage6.controls['professionalPeerReferencePhone3'];
        this.professionalPeerReferenceFax3 = this.formPage6.controls['professionalPeerReferenceFax3'];
        this.professionalPeerReferenceEmail3 = this.formPage6.controls['professionalPeerReferenceEmail3'];

        this.professionalLiabilityInsuranceCarrier = this.formPage7.controls['professionalLiabilityInsuranceCarrier'];
        this.professionalLiabilityAddress = this.formPage7.controls['professionalLiabilityAddress'];
        this.professionalLiabilityCity = this.formPage7.controls['professionalLiabilityCity'];
        this.professionalLiabilityState = this.formPage7.controls['professionalLiabilityState'];
        this.professionalLiabilityZip = this.formPage7.controls['professionalLiabilityZip'];
        this.professionalLiabilityPolicyLimits = this.formPage7.controls['professionalLiabilityPolicyLimits'];
        this.professionalLiabilityPerOccurrence = this.formPage7.controls['professionalLiabilityPerOccurrence'];
        this.professionalLiabilityAggregate = this.formPage7.controls['professionalLiabilityAggregate'];
        this.professionalLiabilityPolicyNumber = this.formPage7.controls['professionalLiabilityPolicyNumber'];
        this.professionalLiabilityAgent = this.formPage7.controls['professionalLiabilityAgent'];
        this.professionalLiabilityEffectiveDate = this.formPage7.controls['professionalLiabilityEffectiveDate'];
        this.professionalLiabilityExpirationDate = this.formPage7.controls['professionalLiabilityExpirationDate'];
        this.professionalLiabilityClaimsMade = this.formPage7.controls['professionalLiabilityClaimsMade'];
        this.professionalLiabilityLawSuitAgainstYouYes = this.formPage7.controls['professionalLiabilityLawSuitAgainstYouYes'];
        this.professionalLiabilityLawSuitAgainstYouNo = this.formPage7.controls['professionalLiabilityLawSuitAgainstYouNo'];
        this.professionalLiabilityAnyNowPendingYes = this.formPage7.controls['professionalLiabilityAnyNowPendingYes'];
        this.professionalLiabilityAnyNowPendingNo = this.formPage7.controls['professionalLiabilityAnyNowPendingNo'];
        this.professionalLiabilityAnyJudgementOrPaymentOrClaimYes = this.formPage7.controls['professionalLiabilityAnyJudgementOrPaymentOrClaimYes'];
        this.professionalLiabilityAnyJudgementOrPaymentOrClaimNo = this.formPage7.controls['professionalLiabilityAnyJudgementOrPaymentOrClaimNo'];
        this.professionalLiabilityDeniedProfessionalInsuranceYes = this.formPage7.controls['professionalLiabilityDeniedProfessionalInsuranceYes'];
        this.professionalLiabilityDeniedProfessionalInsuranceNo = this.formPage7.controls['professionalLiabilityDeniedProfessionalInsuranceNo'];


        this.personalSanctionsLicenseToPracticeDeniedYes = this.formPage8.controls['personalSanctionsLicenseToPracticeDeniedYes'];
        this.personalSanctionsLicenseToPracticeDeniedNo = this.formPage8.controls['personalSanctionsLicenseToPracticeDeniedNo'];
        this.personalSanctionsLicenseToReprimandedSubjectYes = this.formPage8.controls['personalSanctionsLicenseToReprimandedSubjectYes'];
        this.personalSanctionsLicenseToReprimandedSubjectNo = this.formPage8.controls['personalSanctionsLicenseToReprimandedSubjectNo'];
        this.personalSanctionsLostBoardYes = this.formPage8.controls['personalSanctionsLostBoardYes'];
        this.personalSanctionsLostBoardNo = this.formPage8.controls['personalSanctionsLostBoardNo'];
        this.personalSanctionsCapitalCertifyingYes = this.formPage8.controls['personalSanctionsCapitalCertifyingYes'];
        this.personalSanctionsCapitalCertifyingNo = this.formPage8.controls['personalSanctionsCapitalCertifyingNo'];
        this.personalSanctionsInformationPertainingYes = this.formPage8.controls['personalSanctionsInformationPertainingYes'];
        this.personalSanctionsInformationPertainingNo = this.formPage8.controls['personalSanctionsInformationPertainingNo'];
        this.personalSanctionsFederalDEANumberYes = this.formPage8.controls['personalSanctionsFederalDEANumberYes'];
        this.personalSanctionsFederalDEANumberNo = this.formPage8.controls['personalSanctionsFederalDEANumberNo'];
        this.personalSanctionsHospitalAmbulatoryYes = this.formPage8.controls['personalSanctionsHospitalAmbulatoryYes'];
        this.personalSanctionsHospitalAmbulatoryNo = this.formPage8.controls['personalSanctionsHospitalAmbulatoryNo'];
        this.personalSanctionsVoluntarilyAndInVoluntarilyYes = this.formPage8.controls['personalSanctionsVoluntarilyAndInVoluntarilyYes'];
        this.personalSanctionsVoluntarilyAndInVoluntarilyNo = this.formPage8.controls['personalSanctionsVoluntarilyAndInVoluntarilyNo'];
        this.personalSanctionsDisciplinaryActionYes = this.formPage8.controls['personalSanctionsDisciplinaryActionYes'];
        this.personalSanctionsDisciplinaryActionNo = this.formPage8.controls['personalSanctionsDisciplinaryActionNo'];
        this.personalSanctionsCensuredOrExcludeOrSuspendActionYes = this.formPage8.controls['personalSanctionsCensuredOrExcludeOrSuspendActionYes'];
        this.personalSanctionsCensuredOrExcludeOrSuspendActionNo = this.formPage8.controls['personalSanctionsCensuredOrExcludeOrSuspendActionNo'];
        this.personalSanctionsMedicareMedicaidChampusYes = this.formPage8.controls['personalSanctionsMedicareMedicaidChampusYes'];
        this.personalSanctionsMedicareMedicaidChampusNo = this.formPage8.controls['personalSanctionsMedicareMedicaidChampusNo'];
        this.personalSanctionsDeniedMembershipSubjectYes = this.formPage8.controls['personalSanctionsDeniedMembershipSubjectYes'];
        this.personalSanctionsDeniedMembershipSubjectNo = this.formPage8.controls['personalSanctionsDeniedMembershipSubjectNo'];
        this.personalSanctionsWithdrawnAnApplicationYes = this.formPage8.controls['personalSanctionsWithdrawnAnApplicationYes'];
        this.personalSanctionsWithdrawnAnApplicationNo = this.formPage8.controls['personalSanctionsWithdrawnAnApplicationNo'];
        this.personalSanctionsChargedOrConvictedForACrimeYes = this.formPage8.controls['personalSanctionsChargedOrConvictedForACrimeYes'];
        this.personalSanctionsChargedOrConvictedForACrimeNo = this.formPage8.controls['personalSanctionsChargedOrConvictedForACrimeNo'];
        this.personalSanctionsSubjectOfCivilOrCriminalYes = this.formPage8.controls['personalSanctionsSubjectOfCivilOrCriminalYes'];
        this.personalSanctionsSubjectOfCivilOrCriminalNo = this.formPage8.controls['personalSanctionsSubjectOfCivilOrCriminalNo'];
        this.personalSanctionsHaveMedicalConditionYes = this.formPage8.controls['personalSanctionsHaveMedicalConditionYes'];
        this.personalSanctionsHaveMedicalConditionNo = this.formPage8.controls['personalSanctionsHaveMedicalConditionNo'];
        this.personalSanctionsUnableToPerformEssentialYes = this.formPage8.controls['personalSanctionsUnableToPerformEssentialYes'];
        this.personalSanctionsUnableToPerformEssentialNo = this.formPage8.controls['personalSanctionsUnableToPerformEssentialNo'];
        this.personalSanctionsAnyReligiousYesRestrictionYes = this.formPage8.controls['personalSanctionsAnyReligiousYesRestrictionYes'];
        this.personalSanctionsAnyReligiousYesRestrictionNo = this.formPage8.controls['personalSanctionsAnyReligiousYesRestrictionNo'];


        this.personalSanctionsCurrentlyEngagedYes = this.formPage9.controls['personalSanctionsCurrentlyEngagedYes'];
        this.personalSanctionsCurrentlyEngagedNo = this.formPage9.controls['personalSanctionsCurrentlyEngagedNo'];
        this.personalSanctionsChemicalSubstancesYes = this.formPage9.controls['personalSanctionsChemicalSubstancesYes'];
        this.personalSanctionsChemicalSubstancesNo = this.formPage9.controls['personalSanctionsChemicalSubstancesNo'];
        this.date = this.formPage9.controls['date'];
        this.printName = this.formPage9.controls['printName'];
        this.personalSanctionsIfYesExplain = this.formPage9.controls['personalSanctionsIfYesExplain'];

        this.emailControl = this.employeeForm.controls['emailControl'];
        this.cellPhoneControl = this.employeeForm.controls['cellPhoneControl'];


        this.itemsPerPage = 500;
        this.page = 1;
        this.sortList = 'id';
        this.sortPredeict = 'desc';

    }

    sortListF() {
        const result = ['itemName,asc'];
        return result;
    }

    ngOnInit() {


        //  this.spinner.show();
        //page 1 ml certification 1...
        const controlCertification1 = <FormArray>this.formPage1.controls['mlCertification1s'];


        if((<FormGroup>controlCertification1.at(0)).controls.isNaMLCertification1.value === ''){
            (<FormGroup>controlCertification1.at(0)).controls.controlledRCertificationNumber.setValidators([Validators.required]);
            (<FormGroup>controlCertification1.at(0)).controls.expiresDate2.setValidators([Validators.required]);
        }

        //...page 1 ml certification 2...
        const controlCertification2 = <FormArray>this.formPage1.controls['mlCertification2s'];

        if((<FormGroup>controlCertification2.at(0)).controls.isNaMLCertification2.value === ''){
            (<FormGroup>controlCertification2.at(0)).controls.deaRegistrationNumber.setValidators([Validators.required]);
            (<FormGroup>controlCertification2.at(0)).controls.expiresDate3.setValidators([Validators.required]);
        }

        // first load for page3
        if(this.page3.isNaResidencyPrograms === false) {
            this.formPage3.controls.nameInstitutionResidencyProgram.setValidators([Validators.required]);
            this.formPage3.controls.datesAttendendResidencyProgram.setValidators([Validators.required]);
            this.formPage3.controls.addressResidencyProgram.setValidators([Validators.required]);
            this.formPage3.controls.cityResidencyProgram.setValidators([Validators.required]);
            this.formPage3.controls.stateResidencyProgram.setValidators([Validators.required]);
            this.formPage3.controls.zipResidencyProgram.setValidators([Validators.required]);
            this.formPage3.controls.typeOfResidency.setValidators([Validators.required]);
            this.formPage3.controls.fullNameOfProgramDirectorResidencyProgram.setValidators([Validators.required]);
        }

        // check null for page 2 & internship
        const controlInternship = <FormArray>this.formPage2.controls['internships'];
        //alert('ffffffffffffffff===>>>' + (<FormGroup>controlInternship.at(0)).controls.isNaInternship.value)
        if((<FormGroup>controlInternship.at(0)).controls.isNaInternship.value === ''){
            (<FormGroup>controlInternship.at(0)).controls.nameInstitutionInternship.setValidators([Validators.required]);
            (<FormGroup>controlInternship.at(0)).controls.addressInternship.setValidators(Validators.required);
            (<FormGroup>controlInternship.at(0)).controls.programDirectorNameInternship.setValidators(Validators.required);
            (<FormGroup>controlInternship.at(0)).controls.typeInternship.setValidators(Validators.required);
            (<FormGroup>controlInternship.at(0)).controls.datesAttendendInternship.setValidators(Validators.required);
            (<FormGroup>controlInternship.at(0)).controls.datesAttendendInternshipEnd.setValidators(Validators.required);
        }

        ///.........page 8 ..............................


        //........Q1.............................
        if(this.page8.personalSanctionsLicenseToPracticeDeniedYes === true){
            this.formPage8.controls.personalSanctionsLicenseToPracticeDeniedNo.setValidators([Validators.required]);
        }
        //......Q2......
        if(this.page8.personalSanctionsLicenseToReprimandedSubjectYes === true){
            this.formPage8.controls.personalSanctionsLicenseToReprimandedSubjectNo.setValidators([Validators.required]);
        }
        //.....................Q3.....
        if(this.page8.personalSanctionsLostBoardYes === true){
            this.formPage8.controls.personalSanctionsLostBoardNo.setValidators([Validators.required]);
        }
        //..................Q4...............
        if(this.page8.personalSanctionsCapitalCertifyingYes === true){
            this.formPage8.controls.personalSanctionsCapitalCertifyingNo.setValidators([Validators.required]);
        }
        //....Q5........................
        if(this.page8.personalSanctionsInformationPertainingYes === true){
            this.formPage8.controls.personalSanctionsInformationPertainingNo.setValidators([Validators.required]);
        }
        //......Q6........
        if(this.page8.personalSanctionsFederalDEANumberYes === true){
            this.formPage8.controls.personalSanctionsFederalDEANumberNo.setValidators([Validators.required]);
        }
        //......Q7.........
        if(this.page8.personalSanctionsHospitalAmbulatoryYes === true){
            this.formPage8.controls.personalSanctionsHospitalAmbulatoryNo.setValidators([Validators.required]);
        }
        //.....Q8....................
        if(this.page8.personalSanctionsVoluntarilyAndInVoluntarilyYes === true){
            this.formPage8.controls.personalSanctionsVoluntarilyAndInVoluntarilyNo.setValidators([Validators.required]);
        }
        //........Q9.....................
        if(this.page8.personalSanctionsDisciplinaryActionYes === true){
            this.formPage8.controls.personalSanctionsDisciplinaryActionNo.setValidators([Validators.required]);
        }
        //......Q10.............................
        if(this.page8.personalSanctionsCensuredOrExcludeOrSuspendActionYes === true){
            this.formPage8.controls.personalSanctionsCensuredOrExcludeOrSuspendActionNo.setValidators([Validators.required]);
        }
        //.....Q11.......
        if(this.page8.personalSanctionsMedicareMedicaidChampusYes === true){
            this.formPage8.controls.personalSanctionsMedicareMedicaidChampusNo.setValidators([Validators.required]);
        }
        //.......Q12..........
        if(this.page8.personalSanctionsDeniedMembershipSubjectYes === true){
            this.formPage8.controls.personalSanctionsDeniedMembershipSubjectNo.setValidators([Validators.required]);
        }
        //......Q13..................
        if(this.page8.personalSanctionsWithdrawnAnApplicationYes === true){
            this.formPage8.controls.personalSanctionsWithdrawnAnApplicationNo.setValidators([Validators.required]);
        }
        //.....Q14..................
        if(this.page8.personalSanctionsChargedOrConvictedForACrimeYes === true){
            this.formPage8.controls.personalSanctionsChargedOrConvictedForACrimeNo.setValidators([Validators.required]);
        }
        //.....Q15..................
        if(this.page8.personalSanctionsSubjectOfCivilOrCriminalYes === true){
            this.formPage8.controls.personalSanctionsSubjectOfCivilOrCriminalNo.setValidators([Validators.required]);
        }
        //.......Q B1......
        if(this.page8.personalSanctionsHaveMedicalConditionYes === true){
            this.formPage8.controls.personalSanctionsHaveMedicalConditionNo.setValidators([Validators.required]);
        }
        //.....Q B2.......
        if(this.page8.personalSanctionsUnableToPerformEssentialYes === true){
            this.formPage8.controls.personalSanctionsUnableToPerformEssentialNo.setValidators([Validators.required]);
        }
        ///...Q C1.....
        if(this.page8.personalSanctionsAnyReligiousYesRestrictionYes === true){
            this.formPage8.controls.personalSanctionsAnyReligiousYesRestrictionNo.setValidators([Validators.required]);
        }

        //.....page 7.....

        //......Q1........

        if(this.page7.professionalLiabilityLawSuitAgainstYouYes === true){
            this.formPage7.controls.professionalLiabilityLawSuitAgainstYouNo.setValidators([Validators.required]);
        }
        //....Q2.......
        if(this.page7.professionalLiabilityAnyNowPendingYes === true){
            this.formPage7.controls.professionalLiabilityAnyNowPendingNo.setValidators([Validators.required]);
        }
        //.....Q3....
        if(this.page7.professionalLiabilityAnyJudgementOrPaymentOrClaimYes === true){
            this.formPage7.controls.professionalLiabilityAnyJudgementOrPaymentOrClaimNo.setValidators([Validators.required]);
        }
        //....Q4.....
        if(this.page7.professionalLiabilityDeniedProfessionalInsuranceYes === true){
            this.formPage7.controls.professionalLiabilityDeniedProfessionalInsuranceNo.setValidators([Validators.required]);
        }
        //.......page 9......

        //.....Q1.......
        if(this.page9.personalSanctionsCurrentlyEngagedYes === true){
            this.formPage9.controls.personalSanctionsCurrentlyEngagedNo.setValidators([Validators.required]);
        }
        //....Q2....
        if(this.page9.personalSanctionsChemicalSubstancesYes === true){
            this.formPage9.controls.personalSanctionsChemicalSubstancesNo.setValidators([Validators.required]);
        }



        this.generalService.queryState({
            page: 0,
            size: 300,

        }).subscribe((res: ResponseWrapper) => {
            this.states = res.json;
        }, (res: ResponseWrapper) => this.onError(res.json));

        //**************** $$$$$$$$$
        // this.employeeService.getCurrent().subscribe(res => {
        //
        //     if (res.status == 'pending' || res.status == 'Pending')
        //         this.isSubmitted = true;
        // })


        // alert(' principal ' + this.principal.hasAnyAuthorityDirect(['CREDENTIALING_MANAGER']));
        if (this.principal.hasAnyAuthorityDirect(['CREDENTIALING_MANAGER']) || this.principal.hasAnyAuthorityDirect(['ROLE_ADMIN'])) {
            this.isCredential = true;

        } else {
            this.isCredential = false;

        }
        this.clinicsDropdownSettings = {
            singleSelection: false,
            text: 'Select Clinics',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: 'myclass custom-class'
        };
        this.generalService.queryPosition({
            page: 0,
            size: this.itemsPerPage,
            sort: this.sortListF()
        })
            .subscribe((res: ResponseWrapper) => {
                this.positions = res.json;
            }, (res: ResponseWrapper) => this.onError(res.json));

        this.generalService.queryClinic({
            page: 0,
            size: this.itemsPerPage,
            sort: this.sortListF()
        })
            .subscribe((res: ResponseWrapper) => {
                this.clinics = res.json;

                for (let x = 0; x < this.clinics.length; x++) {
                    const temp = {id: this.clinics[x].id, itemName: this.clinics[x].itemName};
                    this.ListClinics[x] = temp;
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        // fetching form from DB if it exists
        this.spinner.show();
        this.activatedRoute.queryParams.subscribe((params) => {
            this.param1 = params['formNewAppId'];

            //alert(this.param1 + " ooooo ----- >>>>>>>> ");
            //   if (typeof this.param1 !== 'undefined') {

            // this.load(this.employee.id);
            // this.loadFormByFormNewApp(this.param1);
            //    } else {
            this.employeeService.getCurrent().subscribe((employee) => {
                this.employee = employee;

                // this.load(this.employee.id);
                this.checkCurrentUserStatus();
                this.loadFormByFormNewApp(this.param1);
                // this.spinner.hide();
                // this.load(this.employee.id);
                // this.loadData(this.employee.id);
            });
            //  }

        });
        // this.spinner.hide();

    }

    // loadDocuments(id) {
    //     this.spinner.show();
    //     const employeeId = id;
    //     this.documentCRService.findDocs({
    //         page: 0,
    //         size: 500,
    //         sort: this.sort('documentType.itemName', 'asc')
    //         , employeeId
    //     }).subscribe(
    //         (res: ResponseWrapper) => this.onSuccessDocument(res.json, res.headers),
    //         (res: ResponseWrapper) => this.onError(res.json)
    //     );
    //
    // }

    checkCurrentUserStatus(){

        if(this.employee && this.employee.rejected === true) {
            // alert(' in if ');
            this.disableForm = true ;
        } else {
            this.disableForm = false;
        }
        this.employee.dateOfBirth = this.dateUtils.convertLocalDateFromServer(this.employee.dateOfBirth);

        const dp = new DatePipe(navigator.language);
        const p = 'y-MM-dd'; // YYYY-MM-DD
        const dtr = dp.transform(this.employee.dateOfBirth, p);
        this.employee.dateOfBirth = dtr;

        if (this.employee && this.employee.status && this.employee.position) {
            if (this.employee.position.itemName === 'Physician') {
                if (this.employee.status !== 'New') {
                    this.showForm = true;
                    this.showFormInfo = 'all';
                    if (this.employee.status !== 'Pending') {

                        this.readonlyForm = true;
                    }

                }
            }
        }
    }

    // load(id) {
    //     // alert(' in load id == ' + id);
    //     // alert(' in load this.employee.rejected == ' + this.employee.rejected);
    //     this.employeeService.find(id).subscribe((employee) => {
    //
    //       //  alert(JSON.stringify(employee) + '   TTTGGGGGGTTTTT ')
    //
    //             this.employee = employee;
    //         if(this.employee && this.employee.rejected === true) {
    //             // alert(' in if ');
    //             this.disableForm = true ;
    //         } else {
    //             this.disableForm = false;
    //         }
    //             this.employee.dateOfBirth = this.dateUtils.convertLocalDateFromServer(this.employee.dateOfBirth);
    //
    //             const dp = new DatePipe(navigator.language);
    //             const p = 'y-MM-dd'; // YYYY-MM-DD
    //             const dtr = dp.transform(this.employee.dateOfBirth, p);
    //             this.employee.dateOfBirth = dtr;
    //
    //             if (this.employee && this.employee.status && this.employee.position) {
    //                 if (this.employee.position.itemName === 'Physician') {
    //                     if (this.employee.status !== 'New') {
    //                         this.showForm = true;
    //                         this.showFormInfo = 'all';
    //                         if (this.employee.status !== 'Pending') {
    //
    //                             this.readonlyForm = true;
    //                         }
    //                     }
    //                 }
    //             }
    //
    //             // this.loadFiles(this.employee.id);
    //             // this.loadFileImg(this.employee.id);
    //          //   this.loadDocuments(this.employee.id);
    //
    //         }
    //     );
    // }

    loadFormByFormNewApp(id) {
        // fetching form from DB if it exists
        this.spinner.show();
        this.credentialApplicationFormService.getFormByFormNewApproach(id).subscribe(res => {

            //  alert(JSON.stringify(res) + '   LoadForm ')
            this.currentUserForm = res;

            // check the CRD form is submitted or Not
           let formNewApproach : FormNewApproachModel;

           formNewApproach  = res.formNewApproach;

            if(formNewApproach.submit)
                this.isSubmitted = true;


            // Loading first page
            if (!isNullOrUndefined(res.credentialingAppPage1)) {
                this.saveForPage1 = true;
                this.page1 = res.credentialingAppPage1;
                const copy: Page1 = Object.assign(new Page1(), res.credentialingAppPage1);

                // new ml certification1....
                this.credentialMlCertification1Service.findByPage1Id(copy.id).subscribe(res =>
                {

                    let mlCertification1Array = new Array<CredentialMLCertification1>()

                    let s = new Array<String>()


                    let mlCertification1ArrayTemp: any = <CredentialMLCertification1[]>res.json;

                    let length: any = (<CredentialMLCertification1[]>res.json).length;
                    const control = <FormArray>this.formPage1.controls['mlCertification1s'];
                    for (let i = 0; i < mlCertification1ArrayTemp.length - 1; i++)
                        this.addMLCertification1()

                    for (let i = 0; i < length; i++) {
                        (<FormGroup>control.at(i)).controls.controlledRCertificationNumber.setValue(mlCertification1ArrayTemp[i].controlledRCertificationNumber);
                        (<FormGroup>control.at(i)).controls.isNaMLCertification1.setValue(mlCertification1ArrayTemp[i].isNaMLCertification1);
                        let dp = new DatePipe(navigator.language);
                        let p = 'y-MM-dd';
                        let dateExpire2 = dp.transform(mlCertification1ArrayTemp[i].expiresDate2, p);
                        (<FormGroup>control.at(i)).controls.expiresDate2.setValue(dateExpire2);

                        if((<FormGroup>control.at(i)).controls.isNaMLCertification1.value === true) {
                            (<FormGroup>control.at(i)).controls.controlledRCertificationNumber.clearValidators();
                            (<FormGroup>control.at(i)).controls.controlledRCertificationNumber.setErrors(null);
                            (<FormGroup>control.at(i)).controls.controlledRCertificationNumber.reset();
                            (<FormGroup>control.at(i)).controls.controlledRCertificationNumber.setValue('');


                            (<FormGroup>control.at(i)).controls.expiresDate2.clearValidators();
                            (<FormGroup>control.at(i)).controls.expiresDate2.setErrors(null);
                            (<FormGroup>control.at(i)).controls.expiresDate2.reset();
                            (<FormGroup>control.at(i)).controls.expiresDate2.setValue('');


                        }

                        var x = document.getElementById("hiddenButton");
                        var y = document.getElementById("incrementId1");
                        var z = document.getElementById("noticeId1");
                        if ((<FormGroup>control.at(i)).controls.isNaMLCertification1.value === true) {
                            x.style.display = "none";
                            y.style.display = "none";
                            z.style.display = "block";
                        } else {
                            x.style.display = "block";
                            y.style.display = "block";
                            z.style.display = "none";
                        }

                        if ( mlCertification1ArrayTemp.length == 1){
                            z.style.display = "block";
                        }

                    }
                }, err => alert(err))

                // end new ml certification1....

                //......ml2.......

                this.credentialMlCertification2Service.findByPage2Id(copy.id).subscribe(res =>
                {

                    let mlCertification2Array = new Array<CredentialMLCertification2>()

                    let s = new Array<String>()


                    let mlCertification2ArrayTemp: any = <CredentialMLCertification2[]>res.json;

                    let length: any = (<CredentialMLCertification2[]>res.json).length;
                    const control = <FormArray>this.formPage1.controls['mlCertification2s'];
                    for (let i = 0; i < mlCertification2ArrayTemp.length - 1; i++)
                        this.addMLCertification2()

                    for (let i = 0; i < length; i++) {
                        (<FormGroup>control.at(i)).controls.deaRegistrationNumber.setValue(mlCertification2ArrayTemp[i].deaRegistrationNumber);
                        (<FormGroup>control.at(i)).controls.isNaMLCertification2.setValue(mlCertification2ArrayTemp[i].isNaMLCertification2);
                        let dp = new DatePipe(navigator.language);
                        let p = 'y-MM-dd';
                        let dateExpire3 = dp.transform(mlCertification2ArrayTemp[i].expiresDate3, p);
                        (<FormGroup>control.at(i)).controls.expiresDate3.setValue(dateExpire3);


                        if((<FormGroup>control.at(i)).controls.isNaMLCertification2.value === true) {
                            (<FormGroup>control.at(i)).controls.deaRegistrationNumber.clearValidators();
                            (<FormGroup>control.at(i)).controls.deaRegistrationNumber.setErrors(null);
                            (<FormGroup>control.at(i)).controls.deaRegistrationNumber.reset();
                            (<FormGroup>control.at(i)).controls.deaRegistrationNumber.setValue('');

                            (<FormGroup>control.at(i)).controls.expiresDate3.clearValidators();
                            (<FormGroup>control.at(i)).controls.expiresDate3.setErrors(null);
                            (<FormGroup>control.at(i)).controls.expiresDate3.reset();
                            (<FormGroup>control.at(i)).controls.expiresDate3.setValue('');

                        }

                        var x = document.getElementById("hiddenButton2");
                        var y = document.getElementById("incrementId2");
                        var z = document.getElementById("noticeId2");
                        if ((<FormGroup>control.at(i)).controls.isNaMLCertification2.value === true) {
                            x.style.display = "none";
                            y.style.display = "none";
                            z.style.display = "block";
                        } else {
                            x.style.display = "block";
                            y.style.display = "block";
                            z.style.display = "none";
                        }

                        if ( mlCertification2ArrayTemp.length == 1){
                            z.style.display = "block";
                        }


                    }
                })

                const dp = new DatePipe(navigator.language);
                const p = 'y-MM-dd'; // YYYY-MM-DD
                const dtr = dp.transform(this.page1.dateOfBirth, p);
                this.formPage1.get('dateOfBirth').setValue(dtr);
                const dtAssociated = dp.transform(this.page1.yearsAssociated, p);
                this.formPage1.get('yearsAssociated').setValue(dtAssociated);
                const dtAssociatedEnd = dp.transform(this.page1.yearsAssociatedEnd, p);
                this.formPage1.get('yearsAssociatedEnd').setValue(dtAssociatedEnd);
                const dtr1 = dp.transform(this.page1.expiresDate1, p);
                this.formPage1.get('expiresDate1').setValue(dtr1);
                const dtr4 = dp.transform(this.page1.originalDateIssue, p);
                this.formPage1.get('originalDateIssue').setValue(dtr4);

                this.loaderPage1 = true;
            }
            else {
                this.loaderPage1 = true;
                this.saveForPage1 = false;
            }

            // Loading second page

            if (!isNullOrUndefined(res.credentialingAppPage2)) {
                this.saveForPage2 = true;
                this.page2 = res.credentialingAppPage2;
                const copy2: Page2 = Object.assign(new Page2(), res.credentialingAppPage2);
                this.credentialAOSMService.findByPage2Id(copy2.id).subscribe(res => {

                    const aosmArray = new Array<CredentialAOSM>()

                    const s = new Array<String>()


                    const aosmArrayTemp: any = <CredentialAOSM[]>res.json;

                    const length: any = (<CredentialAOSM[]>res.json).length;


                    const control = <FormArray>this.formPage2.controls['aosms'];

                    for (let i = 0; i < aosmArrayTemp.length - 1; i++) {
                        this.addAosm()
                    }

                    for (let i = 0; i < length; i++) {
                        (<FormGroup>control.at(i)).controls.stateLicenseNumber.setValue(aosmArrayTemp[i].stateLicenseNumber);
                        (<FormGroup>control.at(i)).controls.licenseNumberMedical.setValue(aosmArrayTemp[i].licenseNumberMedical);
                        (<FormGroup>control.at(i)).controls.currentlyPracticeState.setValue(aosmArrayTemp[i].currentlyPracticeState);
                        (<FormGroup>control.at(i)).controls.naOtherStateMedicalLicenses.setValue(aosmArrayTemp[i].naOtherStateMedicalLicenses);
                        (<FormGroup>control.at(i)).controls.explain.setValue(aosmArrayTemp[i].explain);

                        // (<FormGroup>control.at(i)).controls.explain.setValidators([Validators.required]);

                        if((<FormGroup>control.at(i)).controls.currentlyPracticeState.value === false){
                            (<FormGroup>control.at(i)).controls.explain.clearValidators();
                            (<FormGroup>control.at(i)).controls.explain.setErrors(null);
                            (<FormGroup>control.at(i)).controls.explain.reset();
                        }

                        if((<FormGroup>control.at(i)).controls.naOtherStateMedicalLicenses.value === true && (<FormGroup>control.at(i)).controls.currentlyPracticeState.value === true ){
                            (<FormGroup>control.at(i)).controls.explain.clearValidators();
                            (<FormGroup>control.at(i)).controls.explain.setErrors(null);
                            (<FormGroup>control.at(i)).controls.explain.reset();
                        }

                        const dp = new DatePipe(navigator.language);
                        const p = 'y-MM-dd'; // YYYY-MM-DD
                        const dateOfIssue = dp.transform(aosmArrayTemp[i].originalDateOfIssue, p);
                        // alert(' bososeeeeeee ' + dateOfIssue);
                        (<FormGroup>control.at(i)).controls.originalDateOfIssue.setValue(dateOfIssue);


                    }



                })

                //..................internship.....
                this.credentialInternshipService.findByPage2Id(copy2.id).subscribe(res => {

                    let internshipArray = new Array<CredentialInternship>()

                    let s = new Array<String>()

                    let internshipArrayTemp: any = <CredentialInternship[]>res.json;

                    let length: any = (<CredentialInternship[]>res.json).length;


                    const control = <FormArray>this.formPage2.controls['internships'];

                    for (let i = 0; i < internshipArrayTemp.length - 1; i++)
                        this.addInternship()

                    for (let i = 0; i < length; i++) {
                        (<FormGroup>control.at(i)).controls.nameInstitutionInternship.setValue(internshipArrayTemp[i].nameInstitutionInternship);
                        (<FormGroup>control.at(i)).controls.isNaInternship.setValue(internshipArrayTemp[i].isNaInternship);
                        (<FormGroup>control.at(i)).controls.addressInternship.setValue(internshipArrayTemp[i].addressInternship);
                        (<FormGroup>control.at(i)).controls.programDirectorNameInternship.setValue(internshipArrayTemp[i].programDirectorNameInternship);
                        (<FormGroup>control.at(i)).controls.typeInternship.setValue(internshipArrayTemp[i].typeInternship);
                        (<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedInternship.setValue(internshipArrayTemp[i].isProgramSuccessfullyCompletedInternship);
                        (<FormGroup>control.at(i)).controls.isSubjectOfDisciplinaryActionInternship.setValue(internshipArrayTemp[i].isSubjectOfDisciplinaryActionInternship);
                        (<FormGroup>control.at(i)).controls.rotatingOrStraight.setValue(internshipArrayTemp[i].rotatingOrStraight);
                        (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.setValue(internshipArrayTemp[i].programSuccessfullyCompletedInternship);
                        (<FormGroup>control.at(i)).controls.listSpeciality.setValue(internshipArrayTemp[i].listSpeciality);
                        (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.setValue(internshipArrayTemp[i].subjectOfDisciplinaryActionInternship);


                        //....load complete internship.....
                        (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.setValidators([Validators.required]);

                        if((<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedInternship.value === true || (<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedInternship.value === null){
                            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.clearValidators();
                            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.setErrors(null);
                            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.reset();
                        }

                        //....load rotating or straight internship.....
                        (<FormGroup>control.at(i)).controls.listSpeciality.setValidators([Validators.required]);

                        if((<FormGroup>control.at(i)).controls.rotatingOrStraight.value === true || (<FormGroup>control.at(i)).controls.rotatingOrStraight.value === null){
                            (<FormGroup>control.at(i)).controls.listSpeciality.clearValidators();
                            (<FormGroup>control.at(i)).controls.listSpeciality.setErrors(null);
                            (<FormGroup>control.at(i)).controls.listSpeciality.reset();
                        }

                        //.....load Disciplinary Action Internship
                        (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.setValidators([Validators.required]);

                        if((<FormGroup>control.at(i)).controls.isSubjectOfDisciplinaryActionInternship.value === false || (<FormGroup>control.at(i)).controls.isSubjectOfDisciplinaryActionInternship.value === null){
                            (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.clearValidators();
                            (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.setErrors(null);
                            (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.reset();
                        }

                        //.....start na...

                        if((<FormGroup>control.at(i)).controls.isNaInternship.value === true) {
                            (<FormGroup>control.at(i)).controls.nameInstitutionInternship.clearValidators();
                            (<FormGroup>control.at(i)).controls.nameInstitutionInternship.setErrors(null);
                            (<FormGroup>control.at(i)).controls.nameInstitutionInternship.reset();

                            (<FormGroup>control.at(i)).controls.datesAttendendInternship.clearValidators();
                            (<FormGroup>control.at(i)).controls.datesAttendendInternship.setErrors(null);
                            (<FormGroup>control.at(i)).controls.datesAttendendInternship.reset();

                            (<FormGroup>control.at(i)).controls.datesAttendendInternshipEnd.clearValidators();
                            (<FormGroup>control.at(i)).controls.datesAttendendInternshipEnd.setErrors(null);
                            (<FormGroup>control.at(i)).controls.datesAttendendInternshipEnd.reset();

                            (<FormGroup>control.at(i)).controls.addressInternship.clearValidators();
                            (<FormGroup>control.at(i)).controls.addressInternship.setErrors(null);
                            (<FormGroup>control.at(i)).controls.addressInternship.reset();

                            (<FormGroup>control.at(i)).controls.programDirectorNameInternship.clearValidators();
                            (<FormGroup>control.at(i)).controls.programDirectorNameInternship.setErrors(null);
                            (<FormGroup>control.at(i)).controls.programDirectorNameInternship.reset();

                            (<FormGroup>control.at(i)).controls.typeInternship.clearValidators();
                            (<FormGroup>control.at(i)).controls.typeInternship.setErrors(null);
                            (<FormGroup>control.at(i)).controls.typeInternship.reset();

                        }
                        if((<FormGroup>control.at(i)).controls.isNaInternship.value === true && (<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedInternship.value === false ||
                            (<FormGroup>control.at(i)).controls.isNaInternship.value === true && (<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedInternship.value === null) {
                            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.clearValidators();
                            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.setErrors(null);
                            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.reset();
                        }

                        if((<FormGroup>control.at(i)).controls.isNaInternship.value === true && (<FormGroup>control.at(i)).controls.rotatingOrStraight.value === false ||
                            (<FormGroup>control.at(i)).controls.isNaInternship.value === true && (<FormGroup>control.at(i)).controls.rotatingOrStraight.value === null){
                            (<FormGroup>control.at(i)).controls.listSpeciality.clearValidators();
                            (<FormGroup>control.at(i)).controls.listSpeciality.setErrors(null);
                            (<FormGroup>control.at(i)).controls.listSpeciality.reset();
                        }

                        if((<FormGroup>control.at(i)).controls.isNaInternship.value === true && (<FormGroup>control.at(i)).controls.isSubjectOfDisciplinaryActionInternship.value === true ||
                            (<FormGroup>control.at(i)).controls.isNaInternship.value === true && (<FormGroup>control.at(i)).controls.isSubjectOfDisciplinaryActionInternship.value === null){
                            (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.clearValidators();
                            (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.setErrors(null);
                            (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.reset();
                        }




                        let dp = new DatePipe(navigator.language);
                        let p = 'y-MM-dd';
                        let datesAttendend = dp.transform(internshipArrayTemp[i].datesAttendendInternship, p);
                        (<FormGroup>control.at(i)).controls.datesAttendendInternship.setValue(datesAttendend);

                        let datesAttendendEnd = dp.transform(internshipArrayTemp[i].datesAttendendInternshipEnd, p);
                        (<FormGroup>control.at(i)).controls.datesAttendendInternshipEnd.setValue(datesAttendendEnd);

                    }
                })

                //..................internship.....

                const dp = new DatePipe(navigator.language);
                const p = 'y-MM-dd'; // YYYY-MM-DD
                const dtrPreMedical = dp.transform(this.page2.dateOfGraduationPreMedical, p);
                this.formPage2.get('dateOfGraduationPreMedical').setValue(dtrPreMedical);
                const dtrGraduationMedical = dp.transform(this.page2.dateOfGraduationMedical, p);
                this.formPage2.get('dateOfGraduationMedical').setValue(dtrGraduationMedical);
                const dtrPofessionalEdu = dp.transform(this.page2.dateOfGraduationPofessionalEdu, p);
                this.formPage2.get('dateOfGraduationPofessionalEdu').setValue(dtrPofessionalEdu);




                this.page2 = copy2;
                this.loaderPage2 = true;
            }
            else {
                this.loaderPage2 = true;
                this.saveForPage2 = false ;
            }
            //first load...


            // Loading 3rth page
            if (!isNullOrUndefined(res.credentialingAppPage3)) {
                this.saveForPage3 = true;

                this.formPage3.controls.nameInstitutionResidencyProgram.setValidators([Validators.required]);
                this.formPage3.controls.datesAttendendResidencyProgram.setValidators([Validators.required]);
                this.formPage3.controls.addressResidencyProgram.setValidators([Validators.required]);
                this.formPage3.controls.cityResidencyProgram.setValidators([Validators.required]);
                this.formPage3.controls.stateResidencyProgram.setValidators([Validators.required]);
                this.formPage3.controls.zipResidencyProgram.setValidators([Validators.required]);
                this.formPage3.controls.typeOfResidency.setValidators([Validators.required]);
                this.formPage3.controls.fullNameOfProgramDirectorResidencyProgram.setValidators([Validators.required]);

                this.formPage3.controls.programSuccessfullyCompletedResidency.setValidators([Validators.required]);


                this.page3 = res.credentialingAppPage3;
                const copy3: Page3 = Object.assign(new Page3(), res.credentialingAppPage3);



                if(this.page3.isNaResidencyPrograms === true){
                    //alert('sssaaaaaaaaaaaaaaaaaaa')
                    this.formPage3.controls.nameInstitutionResidencyProgram.clearValidators();
                    this.formPage3.controls.nameInstitutionResidencyProgram.setErrors(null);
                    this.formPage3.controls.nameInstitutionResidencyProgram.reset();

                    this.formPage3.controls.datesAttendendResidencyProgram.clearValidators();
                    this.formPage3.controls.datesAttendendResidencyProgram.setErrors(null);
                    this.formPage3.controls.datesAttendendResidencyProgram.reset();

                    this.formPage3.controls.addressResidencyProgram.clearValidators();
                    this.formPage3.controls.addressResidencyProgram.setErrors(null);
                    this.formPage3.controls.addressResidencyProgram.reset();

                    this.formPage3.controls.cityResidencyProgram.clearValidators();
                    this.formPage3.controls.cityResidencyProgram.setErrors(null);
                    this.formPage3.controls.cityResidencyProgram.reset();

                    this.formPage3.controls.stateResidencyProgram.clearValidators();
                    this.formPage3.controls.stateResidencyProgram.setErrors(null);
                    this.formPage3.controls.stateResidencyProgram.reset();

                    this.formPage3.controls.zipResidencyProgram.clearValidators();
                    this.formPage3.controls.zipResidencyProgram.setErrors(null);
                    this.formPage3.controls.zipResidencyProgram.reset();

                    this.formPage3.controls.typeOfResidency.clearValidators();
                    this.formPage3.controls.typeOfResidency.setErrors(null);
                    this.formPage3.controls.typeOfResidency.reset();

                    this.formPage3.controls.fullNameOfProgramDirectorResidencyProgram.clearValidators();
                    this.formPage3.controls.fullNameOfProgramDirectorResidencyProgram.setErrors(null);
                    this.formPage3.controls.fullNameOfProgramDirectorResidencyProgram.reset();
                }

                if(this.page3.isProgramSuccessfullyCompletedResidency === true && this.page3.isNaResidencyPrograms === false || this.page3.isProgramSuccessfullyCompletedResidency === true && this.page3.isNaResidencyPrograms === true || this.page3.isProgramSuccessfullyCompletedResidency === false && this.page3.isNaResidencyPrograms === true  ){
                    this.formPage3.controls.programSuccessfullyCompletedResidency.clearValidators();
                    this.formPage3.controls.programSuccessfullyCompletedResidency.setErrors(null);
                    this.formPage3.controls.programSuccessfullyCompletedResidency.reset();
                }




                this.page3 = copy3;
                const dp = new DatePipe(navigator.language);
                const p = 'y-MM-dd'; // YYYY-MM-DD
                const dateResidencyProgram = dp.transform(this.page3.datesAttendendResidencyProgram, p);
                this.formPage3.get('datesAttendendResidencyProgram').setValue(dateResidencyProgram);


                this.trainingService.findByPage3Id(copy3.id).subscribe(res => {
                    //     alert(' this is the page3 trainings   ' + JSON.stringify(res.json))

                    const trainArray = new Array<TrainingFellowships>()

                    const s = new Array<String>()

                    const trainingArrayTemp: any = <TrainingFellowships[]>res.json;

                    const length: any = (<TrainingFellowships[]>res.json).length;

                    const control = <FormArray>this.formPage3.controls['trainings'];

                    for (let i = 0; i < trainingArrayTemp.length - 1; i++) {
                        this.addTraining()
                    }

                    // fill in the trainings
                    for (let i = 0; i < length; i++) {

                        (<FormGroup>control.at(i)).controls.addressTrainingFellowship.setValue(trainingArrayTemp[i].addressTrainingFellowship);
                        (<FormGroup>control.at(i)).controls.isNaTrainingFellowship.setValue(trainingArrayTemp[i].isNaTrainingFellowship);
                        (<FormGroup>control.at(i)).controls.cityTrainingFellowship.setValue(trainingArrayTemp[i].cityTrainingFellowship);
                        (<FormGroup>control.at(i)).controls.nameOfInstitutionTrainingFellowship.setValue(trainingArrayTemp[i].nameOfInstitutionTrainingFellowship);
                        (<FormGroup>control.at(i)).controls.nameOfImmediateSupervisorTrainingFellowship.setValue(trainingArrayTemp[i].nameOfImmediateSupervisorTrainingFellowship);
                        (<FormGroup>control.at(i)).controls.stateTrainingFellowship.setValue(trainingArrayTemp[i].stateTrainingFellowship);
                        (<FormGroup>control.at(i)).controls.zipTrainingFellowship.setValue(trainingArrayTemp[i].zipTrainingFellowship);
                        (<FormGroup>control.at(i)).controls.typeOfFellowship.setValue(trainingArrayTemp[i].typeOfFellowship);
                        (<FormGroup>control.at(i)).controls.disciplinaryTrainingFellowship.setValue(trainingArrayTemp[i].disciplinaryTrainingFellowship);
                        (<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedTrainingFellowship.setValue(trainingArrayTemp[i].isProgramSuccessfullyCompletedTrainingFellowship);
                        (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.setValue(trainingArrayTemp[i].programSuccessfullyCompletedTrainingFellowship);
                        (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.setValue(trainingArrayTemp[i].attachAnExplanationTrainingFellowship);
                        //....load Completed Training Fellowship..
                        (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.setValidators([Validators.required]);
                        if((<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedTrainingFellowship.value === true || (<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedTrainingFellowship.value === null){
                            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.clearValidators();
                            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.setErrors(null);
                            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.reset();
                        }

                        //.... load disciplinary Training Fellowship....
                        (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.setValidators([Validators.required]);

                        if((<FormGroup>control.at(i)).controls.disciplinaryTrainingFellowship.value === false || (<FormGroup>control.at(i)).controls.disciplinaryTrainingFellowship.value === null){
                            (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.clearValidators();
                            (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.setErrors(null);
                            (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.reset();
                        }

                        //...........for na new...

                        if((<FormGroup>control.at(i)).controls.isNaTrainingFellowship.value === true &&  (<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedTrainingFellowship.value === false){
                            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.clearValidators();
                            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.setErrors(null);
                            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.reset();
                        }
                        if((<FormGroup>control.at(i)).controls.isNaTrainingFellowship.value === true &&  (<FormGroup>control.at(i)).controls.disciplinaryTrainingFellowship.value === true){
                            (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.clearValidators();
                            (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.setErrors(null);
                            (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.reset();
                        }

                        //....end na.....
                        let dp = new DatePipe(navigator.language);
                        let p = 'y-MM-dd'; // YYYY-MM-DD
                        let dateFellowship = dp.transform(trainingArrayTemp[i].datesAttendedTrainingFellowship, p);
                        (<FormGroup>control.at(i)).controls.datesAttendedTrainingFellowship.setValue(dateFellowship);

                    }
                })

                this.page3 = copy3;
                this.loaderPage3 = true;
            }
            else {

                this.loaderPage3 = true;
                this.saveForPage3 = false;
            }


            // Loading FourthPage page

            // check if the page4 has the previous record or not
            if (!isNullOrUndefined(res.credentialingAppPage4)) {
                this.saveForPage4 = true;
                this.page4 = res.credentialingAppPage4;

                const copy4: Page4 = Object.assign(new Page4(), res.credentialingAppPage4);

                this.hospitalService.findByPage4Id(copy4.id).subscribe(res => {

                    const hospitalArrayTemp: any = <HospitalUniversityAffiliation[]>res.json;

                    const length: any = (<HospitalUniversityAffiliation[]>res.json).length;

                    const control = <FormArray>this.formPage4.controls['hospitals'];

                    for (let i = 0; i < hospitalArrayTemp.length - 1; i++) {
                        this.addHospital()
                    }

                    // fill in the trainings
                    for (let i = 0; i < length; i++) {
                        (<FormGroup>control.at(i)).controls.addressHospitalAndUniversity.setValue(hospitalArrayTemp[i].addressHospitalAndUniversity);
                        (<FormGroup>control.at(i)).controls.cityHospitalAndUniversity.setValue(hospitalArrayTemp[i].cityHospitalAndUniversity);
                        (<FormGroup>control.at(i)).controls.stateHospitalAndUniversity.setValue(hospitalArrayTemp[i].stateHospitalAndUniversity);
                        (<FormGroup>control.at(i)).controls.zipHospitalAndUniversity.setValue(hospitalArrayTemp[i].zipHospitalAndUniversity);
                        (<FormGroup>control.at(i)).controls.departmentHospitalAndUniversity.setValue(hospitalArrayTemp[i].departmentHospitalAndUniversity);
                        (<FormGroup>control.at(i)).controls.membershipStatusHospitalAndUniversity.setValue(hospitalArrayTemp[i].membershipStatusHospitalAndUniversity);
                        (<FormGroup>control.at(i)).controls.nameOfInstitutionHospitalAndUniversity.setValue(hospitalArrayTemp[i].nameOfInstitutionHospitalAndUniversity);
                        (<FormGroup>control.at(i)).controls.departmentChiefHospitalAndUniversity.setValue(hospitalArrayTemp[i].departmentChiefHospitalAndUniversity);
                        (<FormGroup>control.at(i)).controls.privilegesAtInstitutionHospitalAndUniversity.setValue(hospitalArrayTemp[i].privilegesAtInstitutionHospitalAndUniversity);
                        (<FormGroup>control.at(i)).controls.typePrivilegesGrantedHospitalAndUniversity.setValue(hospitalArrayTemp[i].typePrivilegesGrantedHospitalAndUniversity);

                        (<FormGroup>control.at(i)).controls.typePrivilegesGrantedHospitalAndUniversity.setValidators([Validators.required]);
                        if ((<FormGroup>control.at(i)).controls.privilegesAtInstitutionHospitalAndUniversity.value === false || (<FormGroup>control.at(i)).controls.privilegesAtInstitutionHospitalAndUniversity.value === null) {
                            (<FormGroup>control.at(i)).controls.typePrivilegesGrantedHospitalAndUniversity.clearValidators();
                            (<FormGroup>control.at(i)).controls.typePrivilegesGrantedHospitalAndUniversity.setErrors(null);
                            (<FormGroup>control.at(i)).controls.typePrivilegesGrantedHospitalAndUniversity.reset();
                        }

                        const dp = new DatePipe(navigator.language);
                        const p = 'y-MM-dd'; // YYYY-MM-DD
                        const dateAndUniversity = dp.transform(hospitalArrayTemp[i].datesAffiliateHospitalAndUniversity, p);
                        (<FormGroup>control.at(i)).controls.datesAffiliateHospitalAndUniversity.setValue(dateAndUniversity);
                        const dateAndUniversityEnd = dp.transform(hospitalArrayTemp[i].datesAffiliateHospitalAndUniversityEnd, p);
                        (<FormGroup>control.at(i)).controls.datesAffiliateHospitalAndUniversityEnd.setValue(dateAndUniversityEnd);

                    }


                })

                this.page4 = copy4;
                this.loaderPage4 = true;
            }
            else {
                this.loaderPage4 = true;
                this.saveForPage4 = false;
            }

            // Loading FifthPage page
            if (!isNullOrUndefined(res.credentialingAppPage5)) {
                this.saveForPage5 = true;
                // this.page5 = res.credentialingAppPage5;

                const entity: CredentialApplicationForm = res;

                const copy: Page5 = Object.assign(new Page5(), res.credentialingAppPage5);


                copy.datesPracticingPreviousGroup = this.dateUtils.convertLocalDateFromServer((<Page5>res.credentialingAppPage5).datesPracticingPreviousGroup)

                this.page5 = copy;


                const dp = new DatePipe(navigator.language);
                const p = 'y-MM-dd'; // YYYY-MM-DD
                let datePreviousGroup = dp.transform(this.page5.datesPracticingPreviousGroup, p);
                this.formPage5.get('datesPracticingPreviousGroup').setValue(datePreviousGroup);
                let datePreviousGroupEnd = dp.transform(this.page5.datesPracticingPreviousGroupEnd, p);
                this.formPage5.get('datesPracticingPreviousGroupEnd').setValue(datePreviousGroupEnd);
                let datePreviousGroup2 = dp.transform(this.page5.datesPracticingPreviousGroup2, p);
                this.formPage5.get('datesPracticingPreviousGroup2').setValue(datePreviousGroup2);
                let datePreviousGroupEnd2 = dp.transform(this.page5.datesPracticingPreviousGroupEnd2, p);
                this.formPage5.get('datesPracticingPreviousGroupEnd2').setValue(datePreviousGroupEnd2);
                let datePreviousGroup3 = dp.transform(this.page5.datesPracticingPreviousGroup3, p);
                this.formPage5.get('datesPracticingPreviousGroup3').setValue(datePreviousGroup3);
                let datePreviousGroupEnd3 = dp.transform(this.page5.datesPracticingPreviousGroupEnd3, p);
                this.formPage5.get('datesPracticingPreviousGroupEnd3').setValue(datePreviousGroupEnd3);
                const dateCertification = dp.transform(this.page5.dateOfCertification, p);
                this.formPage5.get('dateOfCertification').setValue(dateCertification);
                const dateCertification2 = dp.transform(this.page5.dateOfCertification2, p);
                this.formPage5.get('dateOfCertification2').setValue(dateCertification2);
                const dateStatusDate = dp.transform(this.page5.ifNotCertifiedGivePresentStatusDate, p);
                this.formPage5.get('ifNotCertifiedGivePresentStatusDate').setValue(dateStatusDate);
                const dateStatusOfExam = dp.transform(this.page5.ifNotCertifiedGivePresentStatusDateOfExam, p);
                this.formPage5.get('ifNotCertifiedGivePresentStatusDateOfExam').setValue(dateStatusOfExam);

                this.loaderPage5 = true;
            }
            else {

                this.loaderPage5 = true;
                this.saveForPage5 = false ;
            }


            if (!isNullOrUndefined(res.credentialingAppPage6)) {
                this.saveForPage6 = true;
                this.page6 = res.credentialingAppPage6;
            }
            else {
                this.saveForPage6 = false;
            }

            // // Loading seventhPage page
            // this.page7 = res.credentialingAppPage7;

            if (!isNullOrUndefined(res.credentialingAppPage7)) {
                this.saveForPage7 = true;
                this.page7 = res.credentialingAppPage7;
                if(this.page7.professionalLiabilityLawSuitAgainstYouYes === false){
                    this.formPage7.controls.professionalLiabilityLawSuitAgainstYouNo.clearValidators();
                    this.formPage7.controls.professionalLiabilityLawSuitAgainstYouNo.setErrors(null);
                    this.formPage7.controls.professionalLiabilityLawSuitAgainstYouNo.reset();
                }
                this.formPage7.controls.professionalLiabilityLawSuitAgainstYouNo.setValidators([Validators.required]);
                //change7-2
                if(this.page7.professionalLiabilityAnyNowPendingYes === false){
                    this.formPage7.controls.professionalLiabilityAnyNowPendingNo.clearValidators();
                    this.formPage7.controls.professionalLiabilityAnyNowPendingNo.setErrors(null);
                    this.formPage7.controls.professionalLiabilityAnyNowPendingNo.reset();
                }
                this.formPage7.controls.professionalLiabilityAnyNowPendingNo.setValidators([Validators.required]);
                //change7-3
                if(this.page7.professionalLiabilityAnyJudgementOrPaymentOrClaimYes === false){
                    this.formPage7.controls.professionalLiabilityAnyJudgementOrPaymentOrClaimNo.clearValidators();
                    this.formPage7.controls.professionalLiabilityAnyJudgementOrPaymentOrClaimNo.setErrors(null);
                    this.formPage7.controls.professionalLiabilityAnyJudgementOrPaymentOrClaimNo.reset();
                }
                this.formPage7.controls.professionalLiabilityAnyJudgementOrPaymentOrClaimNo.setValidators([Validators.required]);
                //change7-4
                if(this.page7.professionalLiabilityDeniedProfessionalInsuranceYes === false){
                    this.formPage7.controls.professionalLiabilityDeniedProfessionalInsuranceNo.clearValidators();
                    this.formPage7.controls.professionalLiabilityDeniedProfessionalInsuranceNo.setErrors(null);
                    this.formPage7.controls.professionalLiabilityDeniedProfessionalInsuranceNo.reset();
                }
                this.formPage7.controls.professionalLiabilityDeniedProfessionalInsuranceNo.setValidators([Validators.required]);


                const entity: CredentialApplicationForm = res;

                const copy: Page7 = Object.assign(new Page7(), res.credentialingAppPage7);

                this.page7 = copy;
                const dp = new DatePipe(navigator.language);
                const p = 'y-MM-dd'; // YYYY-MM-DD
                const dateEffective = dp.transform(this.page7.professionalLiabilityEffectiveDate, p);
                this.formPage7.get('professionalLiabilityEffectiveDate').setValue(dateEffective);
                const dateExpire = dp.transform(this.page7.professionalLiabilityExpirationDate, p);
                this.formPage7.get('professionalLiabilityExpirationDate').setValue(dateExpire);
                this.loaderPage7 = true;
            } else {
                this.loaderPage7 = true;
                this.saveForPage7 = false;
            }



            // Loading eightPage page
            if (!isNullOrUndefined(res.credentialingAppPage8)) {
                this.saveForPage8 = true;
                this.page8 = res.credentialingAppPage8;
                //change1
                if(this.page8.personalSanctionsLicenseToPracticeDeniedYes === false ){
                    this.formPage8.controls.personalSanctionsLicenseToPracticeDeniedNo.clearValidators();
                    this.formPage8.controls.personalSanctionsLicenseToPracticeDeniedNo.setErrors(null);
                    this.formPage8.controls.personalSanctionsLicenseToPracticeDeniedNo.reset();
                }
                //change2
                if(this.page8.personalSanctionsLicenseToReprimandedSubjectYes === false){
                    this.formPage8.controls.personalSanctionsLicenseToReprimandedSubjectNo.clearValidators();
                    this.formPage8.controls.personalSanctionsLicenseToReprimandedSubjectNo.setErrors(null);
                    this.formPage8.controls.personalSanctionsLicenseToReprimandedSubjectNo.reset();
                }
                //change3
                if(this.page8.personalSanctionsLostBoardYes === false){
                    this.formPage8.controls.personalSanctionsLostBoardNo.clearValidators();
                    this.formPage8.controls.personalSanctionsLostBoardNo.setErrors(null);
                    this.formPage8.controls.personalSanctionsLostBoardNo.reset();
                }
                //change4
                if(this.page8.personalSanctionsCapitalCertifyingYes === false){
                    this.formPage8.controls.personalSanctionsCapitalCertifyingNo.clearValidators();
                    this.formPage8.controls.personalSanctionsCapitalCertifyingNo.setErrors(null);
                    this.formPage8.controls.personalSanctionsCapitalCertifyingNo.reset();
                }
                //change5
                if(this.page8.personalSanctionsInformationPertainingYes === false){
                    this.formPage8.controls.personalSanctionsInformationPertainingNo.clearValidators();
                    this.formPage8.controls.personalSanctionsInformationPertainingNo.setErrors(null);
                    this.formPage8.controls.personalSanctionsInformationPertainingNo.reset();
                }
                //change6
                if(this.page8.personalSanctionsFederalDEANumberYes === false){
                    this.formPage8.controls.personalSanctionsFederalDEANumberNo.clearValidators();
                    this.formPage8.controls.personalSanctionsFederalDEANumberNo.setErrors(null);
                    this.formPage8.controls.personalSanctionsFederalDEANumberNo.reset();
                }
                //change7
                if (this.page8.personalSanctionsHospitalAmbulatoryYes === false) {
                    this.formPage8.controls.personalSanctionsHospitalAmbulatoryNo.clearValidators();
                    this.formPage8.controls.personalSanctionsHospitalAmbulatoryNo.setErrors(null);
                    this.formPage8.controls.personalSanctionsHospitalAmbulatoryNo.reset();
                }
                //change8
                if(this.page8.personalSanctionsVoluntarilyAndInVoluntarilyYes === false){
                    this.formPage8.controls.personalSanctionsVoluntarilyAndInVoluntarilyNo.clearValidators();
                    this.formPage8.controls.personalSanctionsVoluntarilyAndInVoluntarilyNo.setErrors(null);
                    this.formPage8.controls.personalSanctionsVoluntarilyAndInVoluntarilyNo.reset();
                }
                //change9
                if(this.page8.personalSanctionsDisciplinaryActionYes === false){
                    this.formPage8.controls.personalSanctionsDisciplinaryActionNo.clearValidators();
                    this.formPage8.controls.personalSanctionsDisciplinaryActionNo.setErrors(null);
                    this.formPage8.controls.personalSanctionsDisciplinaryActionNo.reset();
                }
                //change10
                if(this.page8.personalSanctionsCensuredOrExcludeOrSuspendActionYes === false){
                    this.formPage8.controls.personalSanctionsCensuredOrExcludeOrSuspendActionNo.clearValidators();
                    this.formPage8.controls.personalSanctionsCensuredOrExcludeOrSuspendActionNo.setErrors(null);
                    this.formPage8.controls.personalSanctionsCensuredOrExcludeOrSuspendActionNo.reset();
                }
                //change11
                if(this.page8.personalSanctionsMedicareMedicaidChampusYes === false){
                    this.formPage8.controls.personalSanctionsMedicareMedicaidChampusNo.clearValidators();
                    this.formPage8.controls.personalSanctionsMedicareMedicaidChampusNo.setErrors(null);
                    this.formPage8.controls.personalSanctionsMedicareMedicaidChampusNo.reset();
                }
                //change12
                if(this.page8.personalSanctionsDeniedMembershipSubjectYes === false){
                    this.formPage8.controls.personalSanctionsDeniedMembershipSubjectNo.clearValidators();
                    this.formPage8.controls.personalSanctionsDeniedMembershipSubjectNo.setErrors(null);
                    this.formPage8.controls.personalSanctionsDeniedMembershipSubjectNo.reset();
                }
                //change13
                if(this.page8.personalSanctionsWithdrawnAnApplicationYes === false){
                    this.formPage8.controls.personalSanctionsWithdrawnAnApplicationNo.clearValidators();
                    this.formPage8.controls.personalSanctionsWithdrawnAnApplicationNo.setErrors(null);
                    this.formPage8.controls.personalSanctionsWithdrawnAnApplicationNo.reset();
                }
                //change14
                if(this.page8.personalSanctionsChargedOrConvictedForACrimeYes === false){
                    this.formPage8.controls.personalSanctionsChargedOrConvictedForACrimeNo.clearValidators();
                    this.formPage8.controls.personalSanctionsChargedOrConvictedForACrimeNo.setErrors(null);
                    this.formPage8.controls.personalSanctionsChargedOrConvictedForACrimeNo.reset();
                }
                //change15
                if(this.page8.personalSanctionsSubjectOfCivilOrCriminalYes === false){
                    this.formPage8.controls.personalSanctionsSubjectOfCivilOrCriminalNo.clearValidators();
                    this.formPage8.controls.personalSanctionsSubjectOfCivilOrCriminalNo.setErrors(null);
                    this.formPage8.controls.personalSanctionsSubjectOfCivilOrCriminalNo.reset();
                }
                //change1b
                if(this.page8.personalSanctionsHaveMedicalConditionYes === false){
                    this.formPage8.controls.personalSanctionsHaveMedicalConditionNo.clearValidators();
                    this.formPage8.controls.personalSanctionsHaveMedicalConditionNo.setErrors(null);
                    this.formPage8.controls.personalSanctionsHaveMedicalConditionNo.reset();
                }
                //change2b
                if(this.page8.personalSanctionsUnableToPerformEssentialYes === false){
                    this.formPage8.controls.personalSanctionsUnableToPerformEssentialNo.clearValidators();
                    this.formPage8.controls.personalSanctionsUnableToPerformEssentialNo.setErrors(null);
                    this.formPage8.controls.personalSanctionsUnableToPerformEssentialNo.reset();
                }
                //change1c
                if(this.page8.personalSanctionsAnyReligiousYesRestrictionYes === false){
                    this.formPage8.controls.personalSanctionsAnyReligiousYesRestrictionNo.clearValidators();
                    this.formPage8.controls.personalSanctionsAnyReligiousYesRestrictionNo.setErrors(null);
                    this.formPage8.controls.personalSanctionsAnyReligiousYesRestrictionNo.reset();
                }



                // this.formPage8.get('personalSanctionsCapitalCertifyingYes').setValue(1);
            }
            else {
                this.saveForPage8 = false;
            }

            // Loading ninthPage page
            if (!isNullOrUndefined(res.credentialingAppPage9)) {
                this.saveForPage9 = true;
                this.page9 = res.credentialingAppPage9;
                //changeEngaged
                if(this.page9.personalSanctionsCurrentlyEngagedYes === false){
                    this.formPage9.controls.personalSanctionsCurrentlyEngagedNo.clearValidators();
                    this.formPage9.controls.personalSanctionsCurrentlyEngagedNo.setErrors(null);
                    this.formPage9.controls.personalSanctionsCurrentlyEngagedNo.reset();
                }
                //changeSubstances
                if(this.page9.personalSanctionsChemicalSubstancesYes === false){
                    this.formPage9.controls.personalSanctionsChemicalSubstancesNo.clearValidators();
                    this.formPage9.controls.personalSanctionsChemicalSubstancesNo.setErrors(null);
                    this.formPage9.controls.personalSanctionsChemicalSubstancesNo.reset();
                }
                const entity: CredentialApplicationForm = res;

                const copy: Page9 = Object.assign(new Page9(), res.credentialingAppPage9);

                this.page9 = copy;
                const dp = new DatePipe(navigator.language);
                const p = 'y-MM-dd'; // YYYY-MM-DD
                const dateDate = dp.transform(this.page9.date, p);
                this.formPage9.get('date').setValue(dateDate);

                this.loaderPage9 = true;
            } else {
                this.loaderPage9 = true;
                this.saveForPage9 = false;
            }; this.spinner.hide();
        }, rej => {

            //  alert(' ---->>>>>>>>>>>  ' + rej)
            // in the case that current user dosnot have a form this reject will be called
            this.loaderPage1 = true;
            this.loaderPage2 = true;
            this.loaderPage3 = true;
            this.loaderPage4 = true;
            this.loaderPage9 = true;
            this.spinner.hide();

        });
    }

    // loadData(id) {
    //     // fetching form from DB if it exists
    //     this.credentialApplicationFormService.getCurrentFormpProfile(id).subscribe(res => {
    //
    //
    //       //  alert(JSON.stringify(res) + '   LoadForm ')
    //         this.currentUserForm = res;
    //         // Loading first page
    //         if (!isNullOrUndefined(res.credentialingAppPage1)) {
    //             this.saveForPage1 = true;
    //             this.page1 = res.credentialingAppPage1;
    //             const copy: Page1 = Object.assign(new Page1(), res.credentialingAppPage1);
    //
    //             // new ml certification1....
    //             this.credentialMlCertification1Service.findByPage1Id(copy.id).subscribe(res =>
    //             {
    //
    //                 let mlCertification1Array = new Array<CredentialMLCertification1>()
    //
    //                 let s = new Array<String>()
    //
    //
    //                 let mlCertification1ArrayTemp: any = <CredentialMLCertification1[]>res.json;
    //
    //                 let length: any = (<CredentialMLCertification1[]>res.json).length;
    //                 const control = <FormArray>this.formPage1.controls['mlCertification1s'];
    //                 for (let i = 0; i < mlCertification1ArrayTemp.length - 1; i++)
    //                     this.addMLCertification1()
    //
    //                 for (let i = 0; i < length; i++) {
    //                     (<FormGroup>control.at(i)).controls.controlledRCertificationNumber.setValue(mlCertification1ArrayTemp[i].controlledRCertificationNumber);
    //                     (<FormGroup>control.at(i)).controls.isNaMLCertification1.setValue(mlCertification1ArrayTemp[i].isNaMLCertification1);
    //                     let dp = new DatePipe(navigator.language);
    //                     let p = 'y-MM-dd';
    //                     let dateExpire2 = dp.transform(mlCertification1ArrayTemp[i].expiresDate2, p);
    //                     (<FormGroup>control.at(i)).controls.expiresDate2.setValue(dateExpire2);
    //
    //                     if((<FormGroup>control.at(i)).controls.isNaMLCertification1.value === true) {
    //                         (<FormGroup>control.at(i)).controls.controlledRCertificationNumber.clearValidators();
    //                         (<FormGroup>control.at(i)).controls.controlledRCertificationNumber.setErrors(null);
    //                         (<FormGroup>control.at(i)).controls.controlledRCertificationNumber.reset();
    //
    //                         (<FormGroup>control.at(i)).controls.expiresDate2.clearValidators();
    //                         (<FormGroup>control.at(i)).controls.expiresDate2.setErrors(null);
    //                         (<FormGroup>control.at(i)).controls.expiresDate2.reset();
    //
    //                     }
    //
    //                     var x = document.getElementById("hiddenButton");
    //                     var y = document.getElementById("incrementId1");
    //                     var z = document.getElementById("noticeId1");
    //                     if ((<FormGroup>control.at(i)).controls.isNaMLCertification1.value === true) {
    //                         x.style.display = "none";
    //                         y.style.display = "none";
    //                         z.style.display = "block";
    //                     } else {
    //                         x.style.display = "block";
    //                         y.style.display = "block";
    //                         z.style.display = "none";
    //                     }
    //
    //                 }
    //             }, err => alert(err))
    //
    //             // end new ml certification1....
    //
    //             //......ml2.......
    //
    //             this.credentialMlCertification2Service.findByPage2Id(copy.id).subscribe(res =>
    //             {
    //
    //                 let mlCertification2Array = new Array<CredentialMLCertification2>()
    //
    //                 let s = new Array<String>()
    //
    //
    //                 let mlCertification2ArrayTemp: any = <CredentialMLCertification2[]>res.json;
    //
    //                 let length: any = (<CredentialMLCertification2[]>res.json).length;
    //                 const control = <FormArray>this.formPage1.controls['mlCertification2s'];
    //                 for (let i = 0; i < mlCertification2ArrayTemp.length - 1; i++)
    //                     this.addMLCertification2()
    //
    //                 for (let i = 0; i < length; i++) {
    //                     (<FormGroup>control.at(i)).controls.deaRegistrationNumber.setValue(mlCertification2ArrayTemp[i].deaRegistrationNumber);
    //                     (<FormGroup>control.at(i)).controls.isNaMLCertification2.setValue(mlCertification2ArrayTemp[i].isNaMLCertification2);
    //                     let dp = new DatePipe(navigator.language);
    //                     let p = 'y-MM-dd';
    //                     let dateExpire3 = dp.transform(mlCertification2ArrayTemp[i].expiresDate3, p);
    //                     (<FormGroup>control.at(i)).controls.expiresDate3.setValue(dateExpire3);
    //
    //
    //                     if((<FormGroup>control.at(i)).controls.isNaMLCertification2.value === true) {
    //                         (<FormGroup>control.at(i)).controls.deaRegistrationNumber.clearValidators();
    //                         (<FormGroup>control.at(i)).controls.deaRegistrationNumber.setErrors(null);
    //                         (<FormGroup>control.at(i)).controls.deaRegistrationNumber.reset();
    //
    //                         (<FormGroup>control.at(i)).controls.expiresDate3.clearValidators();
    //                         (<FormGroup>control.at(i)).controls.expiresDate3.setErrors(null);
    //                         (<FormGroup>control.at(i)).controls.expiresDate3.reset();
    //
    //                     }
    //
    //                     var x = document.getElementById("hiddenButton2");
    //                     var y = document.getElementById("incrementId2");
    //                     var z = document.getElementById("noticeId2");
    //                     if ((<FormGroup>control.at(i)).controls.isNaMLCertification2.value === true) {
    //                         x.style.display = "none";
    //                         y.style.display = "none";
    //                         z.style.display = "block";
    //                     } else {
    //                         x.style.display = "block";
    //                         y.style.display = "block";
    //                         z.style.display = "none";
    //                     }
    //
    //
    //                 }
    //             })
    //
    //             const dp = new DatePipe(navigator.language);
    //             const p = 'y-MM-dd'; // YYYY-MM-DD
    //             const dtr = dp.transform(this.page1.dateOfBirth, p);
    //             this.formPage1.get('dateOfBirth').setValue(dtr);
    //             const dtAssociated = dp.transform(this.page1.yearsAssociated, p);
    //             this.formPage1.get('yearsAssociated').setValue(dtAssociated);
    //             const dtAssociatedEnd = dp.transform(this.page1.yearsAssociatedEnd, p);
    //             this.formPage1.get('yearsAssociatedEnd').setValue(dtAssociatedEnd);
    //             const dtr1 = dp.transform(this.page1.expiresDate1, p);
    //             this.formPage1.get('expiresDate1').setValue(dtr1);
    //             const dtr4 = dp.transform(this.page1.originalDateIssue, p);
    //             this.formPage1.get('originalDateIssue').setValue(dtr4);
    //
    //             this.loaderPage1 = true;
    //         }
    //         else {
    //             this.loaderPage1 = true;
    //             this.saveForPage1 = false;
    //         }
    //
    //         // Loading second page
    //
    //         if (!isNullOrUndefined(res.credentialingAppPage2)) {
    //             this.saveForPage2 = true;
    //             this.page2 = res.credentialingAppPage2;
    //             const copy2: Page2 = Object.assign(new Page2(), res.credentialingAppPage2);
    //             this.credentialAOSMService.findByPage2Id(copy2.id).subscribe(res => {
    //
    //                 const aosmArray = new Array<CredentialAOSM>()
    //
    //                 const s = new Array<String>()
    //
    //
    //                 const aosmArrayTemp: any = <CredentialAOSM[]>res.json;
    //
    //                 const length: any = (<CredentialAOSM[]>res.json).length;
    //
    //
    //                 const control = <FormArray>this.formPage2.controls['aosms'];
    //
    //                 for (let i = 0; i < aosmArrayTemp.length - 1; i++) {
    //                     this.addAosm()
    //                 }
    //
    //                 for (let i = 0; i < length; i++) {
    //                     (<FormGroup>control.at(i)).controls.stateLicenseNumber.setValue(aosmArrayTemp[i].stateLicenseNumber);
    //                     (<FormGroup>control.at(i)).controls.licenseNumberMedical.setValue(aosmArrayTemp[i].licenseNumberMedical);
    //                     (<FormGroup>control.at(i)).controls.currentlyPracticeState.setValue(aosmArrayTemp[i].currentlyPracticeState);
    //                     (<FormGroup>control.at(i)).controls.naOtherStateMedicalLicenses.setValue(aosmArrayTemp[i].naOtherStateMedicalLicenses);
    //                     (<FormGroup>control.at(i)).controls.explain.setValue(aosmArrayTemp[i].explain);
    //
    //                     // (<FormGroup>control.at(i)).controls.explain.setValidators([Validators.required]);
    //
    //                     if((<FormGroup>control.at(i)).controls.currentlyPracticeState.value === false){
    //                         (<FormGroup>control.at(i)).controls.explain.clearValidators();
    //                         (<FormGroup>control.at(i)).controls.explain.setErrors(null);
    //                         (<FormGroup>control.at(i)).controls.explain.reset();
    //                     }
    //
    //                     if((<FormGroup>control.at(i)).controls.naOtherStateMedicalLicenses.value === true && (<FormGroup>control.at(i)).controls.currentlyPracticeState.value === true ){
    //                         (<FormGroup>control.at(i)).controls.explain.clearValidators();
    //                         (<FormGroup>control.at(i)).controls.explain.setErrors(null);
    //                         (<FormGroup>control.at(i)).controls.explain.reset();
    //                     }
    //
    //                     const dp = new DatePipe(navigator.language);
    //                     const p = 'y-MM-dd'; // YYYY-MM-DD
    //                     const dateOfIssue = dp.transform(aosmArrayTemp[i].originalDateOfIssue, p);
    //                     // alert(' bososeeeeeee ' + dateOfIssue);
    //                     (<FormGroup>control.at(i)).controls.originalDateOfIssue.setValue(dateOfIssue);
    //
    //
    //                 }
    //
    //
    //
    //             })
    //
    //             //..................internship.....
    //             this.credentialInternshipService.findByPage2Id(copy2.id).subscribe(res => {
    //
    //                 let internshipArray = new Array<CredentialInternship>()
    //
    //                 let s = new Array<String>()
    //
    //                 let internshipArrayTemp: any = <CredentialInternship[]>res.json;
    //
    //                 let length: any = (<CredentialInternship[]>res.json).length;
    //
    //
    //                 const control = <FormArray>this.formPage2.controls['internships'];
    //
    //                 for (let i = 0; i < internshipArrayTemp.length - 1; i++)
    //                     this.addInternship()
    //
    //                 for (let i = 0; i < length; i++) {
    //                     (<FormGroup>control.at(i)).controls.nameInstitutionInternship.setValue(internshipArrayTemp[i].nameInstitutionInternship);
    //                     (<FormGroup>control.at(i)).controls.isNaInternship.setValue(internshipArrayTemp[i].isNaInternship);
    //                     (<FormGroup>control.at(i)).controls.addressInternship.setValue(internshipArrayTemp[i].addressInternship);
    //                     (<FormGroup>control.at(i)).controls.programDirectorNameInternship.setValue(internshipArrayTemp[i].programDirectorNameInternship);
    //                     (<FormGroup>control.at(i)).controls.typeInternship.setValue(internshipArrayTemp[i].typeInternship);
    //                     (<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedInternship.setValue(internshipArrayTemp[i].isProgramSuccessfullyCompletedInternship);
    //                     (<FormGroup>control.at(i)).controls.isSubjectOfDisciplinaryActionInternship.setValue(internshipArrayTemp[i].isSubjectOfDisciplinaryActionInternship);
    //                     (<FormGroup>control.at(i)).controls.rotatingOrStraight.setValue(internshipArrayTemp[i].rotatingOrStraight);
    //                     (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.setValue(internshipArrayTemp[i].programSuccessfullyCompletedInternship);
    //                     (<FormGroup>control.at(i)).controls.listSpeciality.setValue(internshipArrayTemp[i].listSpeciality);
    //                     (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.setValue(internshipArrayTemp[i].subjectOfDisciplinaryActionInternship);
    //
    //
    //                     //....load complete internship.....
    //                     (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.setValidators([Validators.required]);
    //
    //                     if((<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedInternship.value === true || (<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedInternship.value === null){
    //                         (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.clearValidators();
    //                         (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.setErrors(null);
    //                         (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.reset();
    //                     }
    //
    //                     //....load rotating or straight internship.....
    //                     (<FormGroup>control.at(i)).controls.listSpeciality.setValidators([Validators.required]);
    //
    //                     if((<FormGroup>control.at(i)).controls.rotatingOrStraight.value === true || (<FormGroup>control.at(i)).controls.rotatingOrStraight.value === null){
    //                         (<FormGroup>control.at(i)).controls.listSpeciality.clearValidators();
    //                         (<FormGroup>control.at(i)).controls.listSpeciality.setErrors(null);
    //                         (<FormGroup>control.at(i)).controls.listSpeciality.reset();
    //                     }
    //
    //                     //.....load Disciplinary Action Internship
    //                     (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.setValidators([Validators.required]);
    //
    //                     if((<FormGroup>control.at(i)).controls.isSubjectOfDisciplinaryActionInternship.value === false || (<FormGroup>control.at(i)).controls.isSubjectOfDisciplinaryActionInternship.value === null){
    //                         (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.clearValidators();
    //                         (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.setErrors(null);
    //                         (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.reset();
    //                     }
    //
    //                     //.....start na...
    //
    //                     if((<FormGroup>control.at(i)).controls.isNaInternship.value === true) {
    //                         (<FormGroup>control.at(i)).controls.nameInstitutionInternship.clearValidators();
    //                         (<FormGroup>control.at(i)).controls.nameInstitutionInternship.setErrors(null);
    //                         (<FormGroup>control.at(i)).controls.nameInstitutionInternship.reset();
    //
    //                         (<FormGroup>control.at(i)).controls.datesAttendendInternship.clearValidators();
    //                         (<FormGroup>control.at(i)).controls.datesAttendendInternship.setErrors(null);
    //                         (<FormGroup>control.at(i)).controls.datesAttendendInternship.reset();
    //
    //                         (<FormGroup>control.at(i)).controls.datesAttendendInternshipEnd.clearValidators();
    //                         (<FormGroup>control.at(i)).controls.datesAttendendInternshipEnd.setErrors(null);
    //                         (<FormGroup>control.at(i)).controls.datesAttendendInternshipEnd.reset();
    //
    //                         (<FormGroup>control.at(i)).controls.addressInternship.clearValidators();
    //                         (<FormGroup>control.at(i)).controls.addressInternship.setErrors(null);
    //                         (<FormGroup>control.at(i)).controls.addressInternship.reset();
    //
    //                         (<FormGroup>control.at(i)).controls.programDirectorNameInternship.clearValidators();
    //                         (<FormGroup>control.at(i)).controls.programDirectorNameInternship.setErrors(null);
    //                         (<FormGroup>control.at(i)).controls.programDirectorNameInternship.reset();
    //
    //                         (<FormGroup>control.at(i)).controls.typeInternship.clearValidators();
    //                         (<FormGroup>control.at(i)).controls.typeInternship.setErrors(null);
    //                         (<FormGroup>control.at(i)).controls.typeInternship.reset();
    //
    //                     }
    //                     if((<FormGroup>control.at(i)).controls.isNaInternship.value === true && (<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedInternship.value === false ||
    //                         (<FormGroup>control.at(i)).controls.isNaInternship.value === true && (<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedInternship.value === null) {
    //                         (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.clearValidators();
    //                         (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.setErrors(null);
    //                         (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.reset();
    //                     }
    //
    //                     if((<FormGroup>control.at(i)).controls.isNaInternship.value === true && (<FormGroup>control.at(i)).controls.rotatingOrStraight.value === false ||
    //                         (<FormGroup>control.at(i)).controls.isNaInternship.value === true && (<FormGroup>control.at(i)).controls.rotatingOrStraight.value === null){
    //                         (<FormGroup>control.at(i)).controls.listSpeciality.clearValidators();
    //                         (<FormGroup>control.at(i)).controls.listSpeciality.setErrors(null);
    //                         (<FormGroup>control.at(i)).controls.listSpeciality.reset();
    //                     }
    //
    //                     if((<FormGroup>control.at(i)).controls.isNaInternship.value === true && (<FormGroup>control.at(i)).controls.isSubjectOfDisciplinaryActionInternship.value === true ||
    //                         (<FormGroup>control.at(i)).controls.isNaInternship.value === true && (<FormGroup>control.at(i)).controls.isSubjectOfDisciplinaryActionInternship.value === null){
    //                         (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.clearValidators();
    //                         (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.setErrors(null);
    //                         (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.reset();
    //                     }
    //
    //
    //
    //
    //                     let dp = new DatePipe(navigator.language);
    //                     let p = 'y-MM-dd';
    //                     let datesAttendend = dp.transform(internshipArrayTemp[i].datesAttendendInternship, p);
    //                     (<FormGroup>control.at(i)).controls.datesAttendendInternship.setValue(datesAttendend);
    //
    //                     let datesAttendendEnd = dp.transform(internshipArrayTemp[i].datesAttendendInternshipEnd, p);
    //                     (<FormGroup>control.at(i)).controls.datesAttendendInternshipEnd.setValue(datesAttendendEnd);
    //
    //                 }
    //             })
    //
    //             //..................internship.....
    //
    //                 const dp = new DatePipe(navigator.language);
    //                 const p = 'y-MM-dd'; // YYYY-MM-DD
    //                 const dtrPreMedical = dp.transform(this.page2.dateOfGraduationPreMedical, p);
    //                 this.formPage2.get('dateOfGraduationPreMedical').setValue(dtrPreMedical);
    //                 const dtrGraduationMedical = dp.transform(this.page2.dateOfGraduationMedical, p);
    //                 this.formPage2.get('dateOfGraduationMedical').setValue(dtrGraduationMedical);
    //                 const dtrPofessionalEdu = dp.transform(this.page2.dateOfGraduationPofessionalEdu, p);
    //                 this.formPage2.get('dateOfGraduationPofessionalEdu').setValue(dtrPofessionalEdu);
    //
    //
    //
    //
    //             this.page2 = copy2;
    //             this.loaderPage2 = true;
    //         }
    //         else {
    //             this.loaderPage2 = true;
    //             this.saveForPage2 = false ;
    //         }
    //         //first load...
    //
    //
    //         // Loading 3rth page
    //         if (!isNullOrUndefined(res.credentialingAppPage3)) {
    //             this.saveForPage3 = true;
    //
    //             this.formPage3.controls.nameInstitutionResidencyProgram.setValidators([Validators.required]);
    //             this.formPage3.controls.datesAttendendResidencyProgram.setValidators([Validators.required]);
    //             this.formPage3.controls.addressResidencyProgram.setValidators([Validators.required]);
    //             this.formPage3.controls.cityResidencyProgram.setValidators([Validators.required]);
    //             this.formPage3.controls.stateResidencyProgram.setValidators([Validators.required]);
    //             this.formPage3.controls.zipResidencyProgram.setValidators([Validators.required]);
    //             this.formPage3.controls.typeOfResidency.setValidators([Validators.required]);
    //             this.formPage3.controls.fullNameOfProgramDirectorResidencyProgram.setValidators([Validators.required]);
    //
    //             this.formPage3.controls.programSuccessfullyCompletedResidency.setValidators([Validators.required]);
    //
    //
    //             this.page3 = res.credentialingAppPage3;
    //             const copy3: Page3 = Object.assign(new Page3(), res.credentialingAppPage3);
    //
    //
    //
    //             if(this.page3.isNaResidencyPrograms === true){
    //                 //alert('sssaaaaaaaaaaaaaaaaaaa')
    //                 this.formPage3.controls.nameInstitutionResidencyProgram.clearValidators();
    //                 this.formPage3.controls.nameInstitutionResidencyProgram.setErrors(null);
    //                 this.formPage3.controls.nameInstitutionResidencyProgram.reset();
    //
    //                 this.formPage3.controls.datesAttendendResidencyProgram.clearValidators();
    //                 this.formPage3.controls.datesAttendendResidencyProgram.setErrors(null);
    //                 this.formPage3.controls.datesAttendendResidencyProgram.reset();
    //
    //                 this.formPage3.controls.addressResidencyProgram.clearValidators();
    //                 this.formPage3.controls.addressResidencyProgram.setErrors(null);
    //                 this.formPage3.controls.addressResidencyProgram.reset();
    //
    //                 this.formPage3.controls.cityResidencyProgram.clearValidators();
    //                 this.formPage3.controls.cityResidencyProgram.setErrors(null);
    //                 this.formPage3.controls.cityResidencyProgram.reset();
    //
    //                 this.formPage3.controls.stateResidencyProgram.clearValidators();
    //                 this.formPage3.controls.stateResidencyProgram.setErrors(null);
    //                 this.formPage3.controls.stateResidencyProgram.reset();
    //
    //                 this.formPage3.controls.zipResidencyProgram.clearValidators();
    //                 this.formPage3.controls.zipResidencyProgram.setErrors(null);
    //                 this.formPage3.controls.zipResidencyProgram.reset();
    //
    //                 this.formPage3.controls.typeOfResidency.clearValidators();
    //                 this.formPage3.controls.typeOfResidency.setErrors(null);
    //                 this.formPage3.controls.typeOfResidency.reset();
    //
    //                 this.formPage3.controls.fullNameOfProgramDirectorResidencyProgram.clearValidators();
    //                 this.formPage3.controls.fullNameOfProgramDirectorResidencyProgram.setErrors(null);
    //                 this.formPage3.controls.fullNameOfProgramDirectorResidencyProgram.reset();
    //             }
    //
    //             if(this.page3.isProgramSuccessfullyCompletedResidency === true && this.page3.isNaResidencyPrograms === false || this.page3.isProgramSuccessfullyCompletedResidency === true && this.page3.isNaResidencyPrograms === true || this.page3.isProgramSuccessfullyCompletedResidency === false && this.page3.isNaResidencyPrograms === true  ){
    //                 this.formPage3.controls.programSuccessfullyCompletedResidency.clearValidators();
    //                 this.formPage3.controls.programSuccessfullyCompletedResidency.setErrors(null);
    //                 this.formPage3.controls.programSuccessfullyCompletedResidency.reset();
    //             }
    //
    //
    //
    //
    //             this.page3 = copy3;
    //             const dp = new DatePipe(navigator.language);
    //             const p = 'y-MM-dd'; // YYYY-MM-DD
    //             const dateResidencyProgram = dp.transform(this.page3.datesAttendendResidencyProgram, p);
    //             this.formPage3.get('datesAttendendResidencyProgram').setValue(dateResidencyProgram);
    //
    //
    //             this.trainingService.findByPage3Id(copy3.id).subscribe(res => {
    //                 //     alert(' this is the page3 trainings   ' + JSON.stringify(res.json))
    //
    //                 const trainArray = new Array<TrainingFellowships>()
    //
    //                 const s = new Array<String>()
    //
    //                 const trainingArrayTemp: any = <TrainingFellowships[]>res.json;
    //
    //                 const length: any = (<TrainingFellowships[]>res.json).length;
    //
    //                 const control = <FormArray>this.formPage3.controls['trainings'];
    //
    //                 for (let i = 0; i < trainingArrayTemp.length - 1; i++) {
    //                     this.addTraining()
    //                 }
    //
    //                 // fill in the trainings
    //                 for (let i = 0; i < length; i++) {
    //
    //                     (<FormGroup>control.at(i)).controls.addressTrainingFellowship.setValue(trainingArrayTemp[i].addressTrainingFellowship);
    //                     (<FormGroup>control.at(i)).controls.isNaTrainingFellowship.setValue(trainingArrayTemp[i].isNaTrainingFellowship);
    //                     (<FormGroup>control.at(i)).controls.cityTrainingFellowship.setValue(trainingArrayTemp[i].cityTrainingFellowship);
    //                     (<FormGroup>control.at(i)).controls.nameOfInstitutionTrainingFellowship.setValue(trainingArrayTemp[i].nameOfInstitutionTrainingFellowship);
    //                     (<FormGroup>control.at(i)).controls.nameOfImmediateSupervisorTrainingFellowship.setValue(trainingArrayTemp[i].nameOfImmediateSupervisorTrainingFellowship);
    //                     (<FormGroup>control.at(i)).controls.stateTrainingFellowship.setValue(trainingArrayTemp[i].stateTrainingFellowship);
    //                     (<FormGroup>control.at(i)).controls.zipTrainingFellowship.setValue(trainingArrayTemp[i].zipTrainingFellowship);
    //                     (<FormGroup>control.at(i)).controls.typeOfFellowship.setValue(trainingArrayTemp[i].typeOfFellowship);
    //                     (<FormGroup>control.at(i)).controls.disciplinaryTrainingFellowship.setValue(trainingArrayTemp[i].disciplinaryTrainingFellowship);
    //                     (<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedTrainingFellowship.setValue(trainingArrayTemp[i].isProgramSuccessfullyCompletedTrainingFellowship);
    //                     (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.setValue(trainingArrayTemp[i].programSuccessfullyCompletedTrainingFellowship);
    //                     (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.setValue(trainingArrayTemp[i].attachAnExplanationTrainingFellowship);
    //                     //....load Completed Training Fellowship..
    //                     (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.setValidators([Validators.required]);
    //                     if((<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedTrainingFellowship.value === true || (<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedTrainingFellowship.value === null){
    //                         (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.clearValidators();
    //                         (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.setErrors(null);
    //                         (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.reset();
    //                     }
    //
    //                     //.... load disciplinary Training Fellowship....
    //                     (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.setValidators([Validators.required]);
    //
    //                     if((<FormGroup>control.at(i)).controls.disciplinaryTrainingFellowship.value === false || (<FormGroup>control.at(i)).controls.disciplinaryTrainingFellowship.value === null){
    //                         (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.clearValidators();
    //                         (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.setErrors(null);
    //                         (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.reset();
    //                     }
    //
    //                     //...........for na new...
    //
    //                     if((<FormGroup>control.at(i)).controls.isNaTrainingFellowship.value === true &&  (<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedTrainingFellowship.value === false){
    //                         (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.clearValidators();
    //                         (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.setErrors(null);
    //                         (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.reset();
    //                     }
    //                     if((<FormGroup>control.at(i)).controls.isNaTrainingFellowship.value === true &&  (<FormGroup>control.at(i)).controls.disciplinaryTrainingFellowship.value === true){
    //                         (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.clearValidators();
    //                         (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.setErrors(null);
    //                         (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.reset();
    //                     }
    //
    //                     //....end na.....
    //                     let dp = new DatePipe(navigator.language);
    //                     let p = 'y-MM-dd'; // YYYY-MM-DD
    //                     let dateFellowship = dp.transform(trainingArrayTemp[i].datesAttendedTrainingFellowship, p);
    //                     (<FormGroup>control.at(i)).controls.datesAttendedTrainingFellowship.setValue(dateFellowship);
    //
    //                 }
    //             })
    //
    //             this.page3 = copy3;
    //             this.loaderPage3 = true;
    //         }
    //         else {
    //
    //             this.loaderPage3 = true;
    //             this.saveForPage3 = false;
    //         }
    //
    //
    //         // Loading FourthPage page
    //
    //         // check if the page4 has the previous record or not
    //         if (!isNullOrUndefined(res.credentialingAppPage4)) {
    //             this.saveForPage4 = true;
    //             this.page4 = res.credentialingAppPage4;
    //
    //             const copy4: Page4 = Object.assign(new Page4(), res.credentialingAppPage4);
    //
    //             this.hospitalService.findByPage4Id(copy4.id).subscribe(res => {
    //
    //                 const hospitalArrayTemp: any = <HospitalUniversityAffiliation[]>res.json;
    //
    //                 const length: any = (<HospitalUniversityAffiliation[]>res.json).length;
    //
    //                 const control = <FormArray>this.formPage4.controls['hospitals'];
    //
    //                 for (let i = 0; i < hospitalArrayTemp.length - 1; i++) {
    //                     this.addHospital()
    //                 }
    //
    //                 // fill in the trainings
    //                 for (let i = 0; i < length; i++) {
    //                     (<FormGroup>control.at(i)).controls.addressHospitalAndUniversity.setValue(hospitalArrayTemp[i].addressHospitalAndUniversity);
    //                     (<FormGroup>control.at(i)).controls.cityHospitalAndUniversity.setValue(hospitalArrayTemp[i].cityHospitalAndUniversity);
    //                     (<FormGroup>control.at(i)).controls.stateHospitalAndUniversity.setValue(hospitalArrayTemp[i].stateHospitalAndUniversity);
    //                     (<FormGroup>control.at(i)).controls.zipHospitalAndUniversity.setValue(hospitalArrayTemp[i].zipHospitalAndUniversity);
    //                     (<FormGroup>control.at(i)).controls.departmentHospitalAndUniversity.setValue(hospitalArrayTemp[i].departmentHospitalAndUniversity);
    //                     (<FormGroup>control.at(i)).controls.membershipStatusHospitalAndUniversity.setValue(hospitalArrayTemp[i].membershipStatusHospitalAndUniversity);
    //                     (<FormGroup>control.at(i)).controls.nameOfInstitutionHospitalAndUniversity.setValue(hospitalArrayTemp[i].nameOfInstitutionHospitalAndUniversity);
    //                     (<FormGroup>control.at(i)).controls.departmentChiefHospitalAndUniversity.setValue(hospitalArrayTemp[i].departmentChiefHospitalAndUniversity);
    //                     (<FormGroup>control.at(i)).controls.privilegesAtInstitutionHospitalAndUniversity.setValue(hospitalArrayTemp[i].privilegesAtInstitutionHospitalAndUniversity);
    //                     (<FormGroup>control.at(i)).controls.typePrivilegesGrantedHospitalAndUniversity.setValue(hospitalArrayTemp[i].typePrivilegesGrantedHospitalAndUniversity);
    //
    //                     (<FormGroup>control.at(i)).controls.typePrivilegesGrantedHospitalAndUniversity.setValidators([Validators.required]);
    //                     if ((<FormGroup>control.at(i)).controls.privilegesAtInstitutionHospitalAndUniversity.value === false || (<FormGroup>control.at(i)).controls.privilegesAtInstitutionHospitalAndUniversity.value === null) {
    //                         (<FormGroup>control.at(i)).controls.typePrivilegesGrantedHospitalAndUniversity.clearValidators();
    //                         (<FormGroup>control.at(i)).controls.typePrivilegesGrantedHospitalAndUniversity.setErrors(null);
    //                         (<FormGroup>control.at(i)).controls.typePrivilegesGrantedHospitalAndUniversity.reset();
    //                     }
    //
    //                     const dp = new DatePipe(navigator.language);
    //                     const p = 'y-MM-dd'; // YYYY-MM-DD
    //                     const dateAndUniversity = dp.transform(hospitalArrayTemp[i].datesAffiliateHospitalAndUniversity, p);
    //                     (<FormGroup>control.at(i)).controls.datesAffiliateHospitalAndUniversity.setValue(dateAndUniversity);
    //                     const dateAndUniversityEnd = dp.transform(hospitalArrayTemp[i].datesAffiliateHospitalAndUniversityEnd, p);
    //                     (<FormGroup>control.at(i)).controls.datesAffiliateHospitalAndUniversityEnd.setValue(dateAndUniversityEnd);
    //
    //                 }
    //
    //
    //             })
    //
    //             this.page4 = copy4;
    //             this.loaderPage4 = true;
    //         }
    //         else {
    //             this.loaderPage4 = true;
    //             this.saveForPage4 = false;
    //         }
    //
    //         // Loading FifthPage page
    //         if (!isNullOrUndefined(res.credentialingAppPage5)) {
    //             this.saveForPage5 = true;
    //             // this.page5 = res.credentialingAppPage5;
    //
    //             const entity: CredentialApplicationForm = res;
    //
    //             const copy: Page5 = Object.assign(new Page5(), res.credentialingAppPage5);
    //
    //
    //             copy.datesPracticingPreviousGroup = this.dateUtils.convertLocalDateFromServer((<Page5>res.credentialingAppPage5).datesPracticingPreviousGroup)
    //
    //             this.page5 = copy;
    //
    //
    //             const dp = new DatePipe(navigator.language);
    //             const p = 'y-MM-dd'; // YYYY-MM-DD
    //             let datePreviousGroup = dp.transform(this.page5.datesPracticingPreviousGroup, p);
    //             this.formPage5.get('datesPracticingPreviousGroup').setValue(datePreviousGroup);
    //             let datePreviousGroupEnd = dp.transform(this.page5.datesPracticingPreviousGroupEnd, p);
    //             this.formPage5.get('datesPracticingPreviousGroupEnd').setValue(datePreviousGroupEnd);
    //             let datePreviousGroup2 = dp.transform(this.page5.datesPracticingPreviousGroup2, p);
    //             this.formPage5.get('datesPracticingPreviousGroup2').setValue(datePreviousGroup2);
    //             let datePreviousGroupEnd2 = dp.transform(this.page5.datesPracticingPreviousGroupEnd2, p);
    //             this.formPage5.get('datesPracticingPreviousGroupEnd2').setValue(datePreviousGroupEnd2);
    //             let datePreviousGroup3 = dp.transform(this.page5.datesPracticingPreviousGroup3, p);
    //             this.formPage5.get('datesPracticingPreviousGroup3').setValue(datePreviousGroup3);
    //             let datePreviousGroupEnd3 = dp.transform(this.page5.datesPracticingPreviousGroupEnd3, p);
    //             this.formPage5.get('datesPracticingPreviousGroupEnd3').setValue(datePreviousGroupEnd3);
    //             const dateCertification = dp.transform(this.page5.dateOfCertification, p);
    //             this.formPage5.get('dateOfCertification').setValue(dateCertification);
    //             const dateStatusDate = dp.transform(this.page5.ifNotCertifiedGivePresentStatusDate, p);
    //             this.formPage5.get('ifNotCertifiedGivePresentStatusDate').setValue(dateStatusDate);
    //             const dateStatusOfExam = dp.transform(this.page5.ifNotCertifiedGivePresentStatusDateOfExam, p);
    //             this.formPage5.get('ifNotCertifiedGivePresentStatusDateOfExam').setValue(dateStatusOfExam);
    //
    //             this.loaderPage5 = true;
    //         }
    //         else {
    //
    //             this.loaderPage5 = true;
    //             this.saveForPage5 = false ;
    //         }
    //
    //
    //         if (!isNullOrUndefined(res.credentialingAppPage6)) {
    //             this.saveForPage6 = true;
    //             this.page6 = res.credentialingAppPage6;
    //         }
    //         else {
    //             this.saveForPage6 = false;
    //         }
    //
    //         // // Loading seventhPage page
    //             // this.page7 = res.credentialingAppPage7;
    //
    //             if (!isNullOrUndefined(res.credentialingAppPage7)) {
    //                 this.saveForPage7 = true;
    //               this.page7 = res.credentialingAppPage7;
    //                 if(this.page7.professionalLiabilityLawSuitAgainstYouYes === false){
    //                     this.formPage7.controls.professionalLiabilityLawSuitAgainstYouNo.clearValidators();
    //                     this.formPage7.controls.professionalLiabilityLawSuitAgainstYouNo.setErrors(null);
    //                     this.formPage7.controls.professionalLiabilityLawSuitAgainstYouNo.reset();
    //                 }
    //                 this.formPage7.controls.professionalLiabilityLawSuitAgainstYouNo.setValidators([Validators.required]);
    //                 //change7-2
    //                 if(this.page7.professionalLiabilityAnyNowPendingYes === false){
    //                     this.formPage7.controls.professionalLiabilityAnyNowPendingNo.clearValidators();
    //                     this.formPage7.controls.professionalLiabilityAnyNowPendingNo.setErrors(null);
    //                     this.formPage7.controls.professionalLiabilityAnyNowPendingNo.reset();
    //                 }
    //                 this.formPage7.controls.professionalLiabilityAnyNowPendingNo.setValidators([Validators.required]);
    //                 //change7-3
    //                 if(this.page7.professionalLiabilityAnyJudgementOrPaymentOrClaimYes === false){
    //                     this.formPage7.controls.professionalLiabilityAnyJudgementOrPaymentOrClaimNo.clearValidators();
    //                     this.formPage7.controls.professionalLiabilityAnyJudgementOrPaymentOrClaimNo.setErrors(null);
    //                     this.formPage7.controls.professionalLiabilityAnyJudgementOrPaymentOrClaimNo.reset();
    //                 }
    //                 this.formPage7.controls.professionalLiabilityAnyJudgementOrPaymentOrClaimNo.setValidators([Validators.required]);
    //                 //change7-4
    //                 if(this.page7.professionalLiabilityDeniedProfessionalInsuranceYes === false){
    //                     this.formPage7.controls.professionalLiabilityDeniedProfessionalInsuranceNo.clearValidators();
    //                     this.formPage7.controls.professionalLiabilityDeniedProfessionalInsuranceNo.setErrors(null);
    //                     this.formPage7.controls.professionalLiabilityDeniedProfessionalInsuranceNo.reset();
    //                 }
    //                 this.formPage7.controls.professionalLiabilityDeniedProfessionalInsuranceNo.setValidators([Validators.required]);
    //
    //
    //                 const entity: CredentialApplicationForm = res;
    //
    //                 const copy: Page7 = Object.assign(new Page7(), res.credentialingAppPage7);
    //
    //                 this.page7 = copy;
    //                 const dp = new DatePipe(navigator.language);
    //                 const p = 'y-MM-dd'; // YYYY-MM-DD
    //                 const dateEffective = dp.transform(this.page7.professionalLiabilityEffectiveDate, p);
    //                 this.formPage7.get('professionalLiabilityEffectiveDate').setValue(dateEffective);
    //                 const dateExpire = dp.transform(this.page7.professionalLiabilityExpirationDate, p);
    //                 this.formPage7.get('professionalLiabilityExpirationDate').setValue(dateExpire);
    //                 this.loaderPage7 = true;
    //             } else {
    //                 this.loaderPage7 = true;
    //                 this.saveForPage7 = false;
    //             }
    //
    //
    //
    //         // Loading eightPage page
    //         if (!isNullOrUndefined(res.credentialingAppPage8)) {
    //             this.saveForPage8 = true;
    //             this.page8 = res.credentialingAppPage8;
    //             //change1
    //             if(this.page8.personalSanctionsLicenseToPracticeDeniedYes === false ){
    //                 this.formPage8.controls.personalSanctionsLicenseToPracticeDeniedNo.clearValidators();
    //                 this.formPage8.controls.personalSanctionsLicenseToPracticeDeniedNo.setErrors(null);
    //                 this.formPage8.controls.personalSanctionsLicenseToPracticeDeniedNo.reset();
    //             }
    //             //change2
    //             if(this.page8.personalSanctionsLicenseToReprimandedSubjectYes === false){
    //                 this.formPage8.controls.personalSanctionsLicenseToReprimandedSubjectNo.clearValidators();
    //                 this.formPage8.controls.personalSanctionsLicenseToReprimandedSubjectNo.setErrors(null);
    //                 this.formPage8.controls.personalSanctionsLicenseToReprimandedSubjectNo.reset();
    //             }
    //             //change3
    //             if(this.page8.personalSanctionsLostBoardYes === false){
    //                 this.formPage8.controls.personalSanctionsLostBoardNo.clearValidators();
    //                 this.formPage8.controls.personalSanctionsLostBoardNo.setErrors(null);
    //                 this.formPage8.controls.personalSanctionsLostBoardNo.reset();
    //             }
    //             //change4
    //             if(this.page8.personalSanctionsCapitalCertifyingYes === false){
    //                 this.formPage8.controls.personalSanctionsCapitalCertifyingNo.clearValidators();
    //                 this.formPage8.controls.personalSanctionsCapitalCertifyingNo.setErrors(null);
    //                 this.formPage8.controls.personalSanctionsCapitalCertifyingNo.reset();
    //             }
    //             //change5
    //             if(this.page8.personalSanctionsInformationPertainingYes === false){
    //                 this.formPage8.controls.personalSanctionsInformationPertainingNo.clearValidators();
    //                 this.formPage8.controls.personalSanctionsInformationPertainingNo.setErrors(null);
    //                 this.formPage8.controls.personalSanctionsInformationPertainingNo.reset();
    //             }
    //             //change6
    //             if(this.page8.personalSanctionsFederalDEANumberYes === false){
    //                 this.formPage8.controls.personalSanctionsFederalDEANumberNo.clearValidators();
    //                 this.formPage8.controls.personalSanctionsFederalDEANumberNo.setErrors(null);
    //                 this.formPage8.controls.personalSanctionsFederalDEANumberNo.reset();
    //             }
    //             //change7
    //             if (this.page8.personalSanctionsHospitalAmbulatoryYes === false) {
    //                 this.formPage8.controls.personalSanctionsHospitalAmbulatoryNo.clearValidators();
    //                 this.formPage8.controls.personalSanctionsHospitalAmbulatoryNo.setErrors(null);
    //                 this.formPage8.controls.personalSanctionsHospitalAmbulatoryNo.reset();
    //             }
    //             //change8
    //             if(this.page8.personalSanctionsVoluntarilyAndInVoluntarilyYes === false){
    //                 this.formPage8.controls.personalSanctionsVoluntarilyAndInVoluntarilyNo.clearValidators();
    //                 this.formPage8.controls.personalSanctionsVoluntarilyAndInVoluntarilyNo.setErrors(null);
    //                 this.formPage8.controls.personalSanctionsVoluntarilyAndInVoluntarilyNo.reset();
    //             }
    //             //change9
    //             if(this.page8.personalSanctionsDisciplinaryActionYes === false){
    //                 this.formPage8.controls.personalSanctionsDisciplinaryActionNo.clearValidators();
    //                 this.formPage8.controls.personalSanctionsDisciplinaryActionNo.setErrors(null);
    //                 this.formPage8.controls.personalSanctionsDisciplinaryActionNo.reset();
    //             }
    //             //change10
    //             if(this.page8.personalSanctionsCensuredOrExcludeOrSuspendActionYes === false){
    //                 this.formPage8.controls.personalSanctionsCensuredOrExcludeOrSuspendActionNo.clearValidators();
    //                 this.formPage8.controls.personalSanctionsCensuredOrExcludeOrSuspendActionNo.setErrors(null);
    //                 this.formPage8.controls.personalSanctionsCensuredOrExcludeOrSuspendActionNo.reset();
    //             }
    //             //change11
    //             if(this.page8.personalSanctionsMedicareMedicaidChampusYes === false){
    //                 this.formPage8.controls.personalSanctionsMedicareMedicaidChampusNo.clearValidators();
    //                 this.formPage8.controls.personalSanctionsMedicareMedicaidChampusNo.setErrors(null);
    //                 this.formPage8.controls.personalSanctionsMedicareMedicaidChampusNo.reset();
    //             }
    //             //change12
    //             if(this.page8.personalSanctionsDeniedMembershipSubjectYes === false){
    //                 this.formPage8.controls.personalSanctionsDeniedMembershipSubjectNo.clearValidators();
    //                 this.formPage8.controls.personalSanctionsDeniedMembershipSubjectNo.setErrors(null);
    //                 this.formPage8.controls.personalSanctionsDeniedMembershipSubjectNo.reset();
    //             }
    //             //change13
    //             if(this.page8.personalSanctionsWithdrawnAnApplicationYes === false){
    //                 this.formPage8.controls.personalSanctionsWithdrawnAnApplicationNo.clearValidators();
    //                 this.formPage8.controls.personalSanctionsWithdrawnAnApplicationNo.setErrors(null);
    //                 this.formPage8.controls.personalSanctionsWithdrawnAnApplicationNo.reset();
    //             }
    //             //change14
    //             if(this.page8.personalSanctionsChargedOrConvictedForACrimeYes === false){
    //                 this.formPage8.controls.personalSanctionsChargedOrConvictedForACrimeNo.clearValidators();
    //                 this.formPage8.controls.personalSanctionsChargedOrConvictedForACrimeNo.setErrors(null);
    //                 this.formPage8.controls.personalSanctionsChargedOrConvictedForACrimeNo.reset();
    //             }
    //             //change15
    //             if(this.page8.personalSanctionsSubjectOfCivilOrCriminalYes === false){
    //                 this.formPage8.controls.personalSanctionsSubjectOfCivilOrCriminalNo.clearValidators();
    //                 this.formPage8.controls.personalSanctionsSubjectOfCivilOrCriminalNo.setErrors(null);
    //                 this.formPage8.controls.personalSanctionsSubjectOfCivilOrCriminalNo.reset();
    //             }
    //             //change1b
    //             if(this.page8.personalSanctionsHaveMedicalConditionYes === false){
    //                 this.formPage8.controls.personalSanctionsHaveMedicalConditionNo.clearValidators();
    //                 this.formPage8.controls.personalSanctionsHaveMedicalConditionNo.setErrors(null);
    //                 this.formPage8.controls.personalSanctionsHaveMedicalConditionNo.reset();
    //             }
    //             //change2b
    //             if(this.page8.personalSanctionsUnableToPerformEssentialYes === false){
    //                 this.formPage8.controls.personalSanctionsUnableToPerformEssentialNo.clearValidators();
    //                 this.formPage8.controls.personalSanctionsUnableToPerformEssentialNo.setErrors(null);
    //                 this.formPage8.controls.personalSanctionsUnableToPerformEssentialNo.reset();
    //             }
    //             //change1c
    //             if(this.page8.personalSanctionsAnyReligiousYesRestrictionYes === false){
    //                 this.formPage8.controls.personalSanctionsAnyReligiousYesRestrictionNo.clearValidators();
    //                 this.formPage8.controls.personalSanctionsAnyReligiousYesRestrictionNo.setErrors(null);
    //                 this.formPage8.controls.personalSanctionsAnyReligiousYesRestrictionNo.reset();
    //             }
    //
    //
    //
    //             // this.formPage8.get('personalSanctionsCapitalCertifyingYes').setValue(1);
    //         }
    //         else {
    //             this.saveForPage8 = false;
    //         }
    //
    //         // Loading ninthPage page
    //         if (!isNullOrUndefined(res.credentialingAppPage9)) {
    //             this.saveForPage9 = true;
    //             this.page9 = res.credentialingAppPage9;
    //             //changeEngaged
    //             if(this.page9.personalSanctionsCurrentlyEngagedYes === false){
    //                 this.formPage9.controls.personalSanctionsCurrentlyEngagedNo.clearValidators();
    //                 this.formPage9.controls.personalSanctionsCurrentlyEngagedNo.setErrors(null);
    //                 this.formPage9.controls.personalSanctionsCurrentlyEngagedNo.reset();
    //             }
    //             //changeSubstances
    //             if(this.page9.personalSanctionsChemicalSubstancesYes === false){
    //                 this.formPage9.controls.personalSanctionsChemicalSubstancesNo.clearValidators();
    //                 this.formPage9.controls.personalSanctionsChemicalSubstancesNo.setErrors(null);
    //                 this.formPage9.controls.personalSanctionsChemicalSubstancesNo.reset();
    //             }
    //             const entity: CredentialApplicationForm = res;
    //
    //             const copy: Page9 = Object.assign(new Page9(), res.credentialingAppPage9);
    //
    //             this.page9 = copy;
    //             const dp = new DatePipe(navigator.language);
    //             const p = 'y-MM-dd'; // YYYY-MM-DD
    //             const dateDate = dp.transform(this.page9.date, p);
    //             this.formPage9.get('date').setValue(dateDate);
    //
    //             this.loaderPage9 = true;
    //         } else {
    //             this.loaderPage9 = true;
    //             this.saveForPage9 = false;
    //         }
    //     }, rej => {
    //
    //       //  alert(' ---->>>>>>>>>>>  ' + rej)
    //         // in the case that current user dosnot have a form this reject will be called
    //         this.loaderPage1 = true;
    //         this.loaderPage2 = true;
    //         this.loaderPage3 = true;
    //         this.loaderPage4 = true;
    //         this.loaderPage9 = true;
    //
    //
    //     });
    // }

    changeNaMlCertification1(i){
        const control = <FormArray>this.formPage1.controls['mlCertification1s'];

        if((<FormGroup>control.at(i)).controls.isNaMLCertification1.value === true) {
            (<FormGroup>control.at(i)).controls.controlledRCertificationNumber.clearValidators();
            (<FormGroup>control.at(i)).controls.controlledRCertificationNumber.setErrors(null);
            (<FormGroup>control.at(i)).controls.controlledRCertificationNumber.reset();
            (<FormGroup>control.at(i)).controls.controlledRCertificationNumber.setValue('');


            (<FormGroup>control.at(i)).controls.expiresDate2.clearValidators();
            (<FormGroup>control.at(i)).controls.expiresDate2.setErrors(null);
            (<FormGroup>control.at(i)).controls.expiresDate2.reset();
            (<FormGroup>control.at(i)).controls.expiresDate2.setValue('');


        }
        else if((<FormGroup>control.at(i)).controls.isNaMLCertification1.value === false){
            (<FormGroup>control.at(i)).controls.controlledRCertificationNumber.setValidators([Validators.required]);
            (<FormGroup>control.at(i)).controls.controlledRCertificationNumber.reset();

            (<FormGroup>control.at(i)).controls.expiresDate2.setValidators([Validators.required]);
            (<FormGroup>control.at(i)).controls.expiresDate2.reset();
        }

        var x = document.getElementById("hiddenButton");
        var y = document.getElementById("incrementId1");
        if ((<FormGroup>control.at(i)).controls.isNaMLCertification1.value === true) {
            x.style.display = "none";
            y.style.display = "none";
        } else {
            x.style.display = "block";
            y.style.display = "block";
        }
    }

    changeNaMlCertification2(i){
        const control = <FormArray>this.formPage1.controls['mlCertification2s'];

        if((<FormGroup>control.at(i)).controls.isNaMLCertification2.value === true) {
            (<FormGroup>control.at(i)).controls.deaRegistrationNumber.clearValidators();
            (<FormGroup>control.at(i)).controls.deaRegistrationNumber.setErrors(null);
            (<FormGroup>control.at(i)).controls.deaRegistrationNumber.reset();
            (<FormGroup>control.at(i)).controls.deaRegistrationNumber.setValue('');

            (<FormGroup>control.at(i)).controls.expiresDate3.clearValidators();
            (<FormGroup>control.at(i)).controls.expiresDate3.setErrors(null);
            (<FormGroup>control.at(i)).controls.expiresDate3.reset();
            (<FormGroup>control.at(i)).controls.expiresDate3.setValue('');

        }
        else if((<FormGroup>control.at(i)).controls.isNaMLCertification2.value === false){
            (<FormGroup>control.at(i)).controls.deaRegistrationNumber.setValidators([Validators.required]);
            (<FormGroup>control.at(i)).controls.deaRegistrationNumber.reset();

            (<FormGroup>control.at(i)).controls.expiresDate3.setValidators([Validators.required]);
            (<FormGroup>control.at(i)).controls.expiresDate3.reset();
        }

        var x = document.getElementById("hiddenButton2");
        var y = document.getElementById("incrementId2");
        if ((<FormGroup>control.at(i)).controls.isNaMLCertification2.value === true) {
            x.style.display = "none";
            y.style.display = "none";
        } else {
            x.style.display = "block";
            y.style.display = "block";
        }
    }

    save() {
        this.isSaving = true;
        this.saveForPage1 = true ;
        //console.log(this.page1);

        const control1 = <FormArray>this.formPage1.controls['mlCertification1s'];
        for( let i = 0 ; i < control1.length ; i++ ) {

            (<FormGroup>control1.at(i)).controls.certification1OrderId.setValue(i+1);
        }
        const control = <FormArray>this.formPage1.controls['mlCertification2s'];
        for( let i = 0 ; i < control.length ; i++ ) {

            (<FormGroup>control.at(i)).controls.certification2OrderId.setValue(i+1);
        }

        if (!isNullOrUndefined(this.page1.id)) {
            this.subscribeToSaveResponsePage1(
                this.formService.update(this.page1, this.currentUserForm.id));
        } else {
            this.subscribeToSaveResponsePage1(
                this.formService.create(this.page1, this.currentUserForm.id));
        }
    }

    changeNaStateMedicalLicenses(i){
        const control = <FormArray>this.formPage2.controls['aosms'];
        if((<FormGroup>control.at(i)).controls.naOtherStateMedicalLicenses.value === true && (<FormGroup>control.at(i)).controls.currentlyPracticeState.value === true ){
            (<FormGroup>control.at(i)).controls.explain.clearValidators();
            (<FormGroup>control.at(i)).controls.explain.setErrors(null);
            (<FormGroup>control.at(i)).controls.explain.reset();
        }

        if((<FormGroup>control.at(i)).controls.naOtherStateMedicalLicenses.value === false && (<FormGroup>control.at(i)).controls.currentlyPracticeState.value === true ){
            (<FormGroup>control.at(i)).controls.explain.setValidators([Validators.required]);
            (<FormGroup>control.at(i)).controls.explain.reset();
        }

    }

    changeState(i){
        // let i
        const control = <FormArray>this.formPage2.controls['aosms'];
        const x = control.length;
        // for(let i=0 ; i < x ; i++){
        if((<FormGroup>control.at(i)).controls.currentlyPracticeState.value === true){
            (<FormGroup>control.at(i)).controls.explain.setValidators([Validators.required]);
            (<FormGroup>control.at(i)).controls.explain.reset();
        }
        else if((<FormGroup>control.at(i)).controls.currentlyPracticeState.value === false){
            (<FormGroup>control.at(i)).controls.explain.clearValidators();
            (<FormGroup>control.at(i)).controls.explain.setErrors(null);
            (<FormGroup>control.at(i)).controls.explain.reset();
        }

        // }

    }

    changeCompletedInternship(i){

        const control = <FormArray>this.formPage2.controls['internships'];
        if((<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedInternship.value === false){
            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.setValidators([Validators.required]);
            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.reset();
        }
        else if((<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedInternship.value === true){
            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.clearValidators();
            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.setErrors(null);
            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.reset();
        }
    }

    changeRotatingOrStraight(i){

        const control = <FormArray>this.formPage2.controls['internships'];
        if((<FormGroup>control.at(i)).controls.rotatingOrStraight.value === false){
            (<FormGroup>control.at(i)).controls.listSpeciality.setValidators([Validators.required]);
            (<FormGroup>control.at(i)).controls.listSpeciality.reset();
        }
        else if((<FormGroup>control.at(i)).controls.rotatingOrStraight.value === true){
            (<FormGroup>control.at(i)).controls.listSpeciality.clearValidators();
            (<FormGroup>control.at(i)).controls.listSpeciality.setErrors(null);
            (<FormGroup>control.at(i)).controls.listSpeciality.reset();
        }
    }

    changeDisciplinaryActionInternship(i){
        const control = <FormArray>this.formPage2.controls['internships'];
        if((<FormGroup>control.at(i)).controls.isSubjectOfDisciplinaryActionInternship.value === true){
            (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.setValidators([Validators.required]);
            (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.reset();
        }
        else if((<FormGroup>control.at(i)).controls.isSubjectOfDisciplinaryActionInternship.value === false){
            (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.clearValidators();
            (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.setErrors(null);
            (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.reset();
        }
    }

    changeNaInternship(i){
        const control = <FormArray>this.formPage2.controls['internships'];

        if((<FormGroup>control.at(i)).controls.isNaInternship.value === true) {
            (<FormGroup>control.at(i)).controls.nameInstitutionInternship.clearValidators();
            (<FormGroup>control.at(i)).controls.nameInstitutionInternship.setErrors(null);
            (<FormGroup>control.at(i)).controls.nameInstitutionInternship.reset();

            (<FormGroup>control.at(i)).controls.datesAttendendInternship.clearValidators();
            (<FormGroup>control.at(i)).controls.datesAttendendInternship.setErrors(null);
            (<FormGroup>control.at(i)).controls.datesAttendendInternship.reset();

            (<FormGroup>control.at(i)).controls.datesAttendendInternshipEnd.clearValidators();
            (<FormGroup>control.at(i)).controls.datesAttendendInternshipEnd.setErrors(null);
            (<FormGroup>control.at(i)).controls.datesAttendendInternshipEnd.reset();

            (<FormGroup>control.at(i)).controls.addressInternship.clearValidators();
            (<FormGroup>control.at(i)).controls.addressInternship.setErrors(null);
            (<FormGroup>control.at(i)).controls.addressInternship.reset();

            (<FormGroup>control.at(i)).controls.programDirectorNameInternship.clearValidators();
            (<FormGroup>control.at(i)).controls.programDirectorNameInternship.setErrors(null);
            (<FormGroup>control.at(i)).controls.programDirectorNameInternship.reset();

            (<FormGroup>control.at(i)).controls.typeInternship.clearValidators();
            (<FormGroup>control.at(i)).controls.typeInternship.setErrors(null);
            (<FormGroup>control.at(i)).controls.typeInternship.reset();

        }
        else if((<FormGroup>control.at(i)).controls.isNaInternship.value === false){
            (<FormGroup>control.at(i)).controls.nameInstitutionInternship.setValidators([Validators.required]);
            (<FormGroup>control.at(i)).controls.nameInstitutionInternship.reset();

            (<FormGroup>control.at(i)).controls.datesAttendendInternship.setValidators([Validators.required]);
            (<FormGroup>control.at(i)).controls.datesAttendendInternship.reset();

            (<FormGroup>control.at(i)).controls.datesAttendendInternshipEnd.setValidators([Validators.required]);
            (<FormGroup>control.at(i)).controls.datesAttendendInternshipEnd.reset();

            (<FormGroup>control.at(i)).controls.addressInternship.setValidators([Validators.required]);
            (<FormGroup>control.at(i)).controls.addressInternship.reset();

            (<FormGroup>control.at(i)).controls.programDirectorNameInternship.setValidators([Validators.required]);
            (<FormGroup>control.at(i)).controls.programDirectorNameInternship.reset();

            (<FormGroup>control.at(i)).controls.typeInternship.setValidators([Validators.required]);
            (<FormGroup>control.at(i)).controls.typeInternship.reset();
        }
        if((<FormGroup>control.at(i)).controls.isNaInternship.value === true && (<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedInternship.value === false) {
            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.clearValidators();
            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.setErrors(null);
            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.reset();
        }

        if((<FormGroup>control.at(i)).controls.isNaInternship.value === true && (<FormGroup>control.at(i)).controls.rotatingOrStraight.value === false){
            (<FormGroup>control.at(i)).controls.listSpeciality.clearValidators();
            (<FormGroup>control.at(i)).controls.listSpeciality.setErrors(null);
            (<FormGroup>control.at(i)).controls.listSpeciality.reset();
        }

        if((<FormGroup>control.at(i)).controls.isNaInternship.value === true && (<FormGroup>control.at(i)).controls.isSubjectOfDisciplinaryActionInternship.value === true){
            (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.clearValidators();
            (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.setErrors(null);
            (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.reset();
        }

        if((<FormGroup>control.at(i)).controls.isNaInternship.value === false && (<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedInternship.value === false) {
            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.setValidators([Validators.required]);
            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedInternship.reset();
        }

        if((<FormGroup>control.at(i)).controls.isNaInternship.value === false && (<FormGroup>control.at(i)).controls.rotatingOrStraight.value === false){
            (<FormGroup>control.at(i)).controls.listSpeciality.setValidators([Validators.required]);
            (<FormGroup>control.at(i)).controls.listSpeciality.reset();
        }

        if((<FormGroup>control.at(i)).controls.isNaInternship.value === false && (<FormGroup>control.at(i)).controls.isSubjectOfDisciplinaryActionInternship.value === true){
            (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.setValidators([Validators.required]);
            (<FormGroup>control.at(i)).controls.subjectOfDisciplinaryActionInternship.reset();
        }

    }


    savePage2() {
        this.isSaving = true;
        this.saveForPage2 = true;

        let aosmArray = new Array<CredentialAOSM>()

        // aosmArray = this.formPage2.controls['aosms'].value;
        //
        // let internshipArray = new Array<CredentialInternship>()

        const controlInternship = <FormArray>this.formPage2.controls['internships'];
        for( let i = 0 ; i < controlInternship.length ; i++ ) {
            (<FormGroup>controlInternship.at(i)).controls.internshipOrderId.setValue(i+1);
        }
        // internshipArray = this.formPage2.controls['internships'].value;
        const control = <FormArray>this.formPage2.controls['aosms'];

        // alert ('before    ++++ >>>>   ' + (<FormGroup>control.at(control.length-1)).controls.naOtherStateMedicalLicenses.value);
        for( let i = 0 ; i < control.length ; i++ ) {
            if( (<FormGroup>control.at(i)).controls.naOtherStateMedicalLicenses.value === ''){
                (<FormGroup>control.at(i)).controls.naOtherStateMedicalLicenses.setValue(0) ;
            }
            (<FormGroup>control.at(i)).controls.aosmOrderId.setValue(i+1);
        }

        // alert ('after    ++++ >>>>   ' + (<FormGroup>control.at(control.length-1)).controls.naOtherStateMedicalLicenses.value);

        if (!isNullOrUndefined(this.page2.id )) {
            this.subscribeToSaveResponsePage2(
                this.formService.updatePage2(this.page2,this.currentUserForm.id));
        } else {
            this.subscribeToSaveResponsePage2(
                this.formService.createPage2(this.page2, this.currentUserForm.id));
        }

    }

    private subscribeToSaveResponsePage1(result: Observable<Page1>) {
        result.subscribe((res: Page1) =>
            this.onSaveSuccessPage1(res), (res: Response) => {
            this.onSaveErrorPage1();
            if (res.status === 400) {
                this.duplicatedName = true;
            }
            this.onSaveErrorPage1()
        });
    }

    private onSaveSuccessPage1(result: Page1) {

        //
        // // setting the created page in the parent form
        // this.credentialApplicationFormService.getCurrentForm().subscribe(form => {
        //     form.credentialingAppPage1 = result;
        //     this.page1 = result;
        //     this.credentialApplicationFormService.update(form).subscribe();
        // })

        this.isSaving = false;
        this.toastrService.success('This form was edited.', 'Successfully saved !');
        // setting Id of the create page1 to current page1
        this.page1.id = result.id;

        // .....start new ml certification...
        let mlCertification1Array = new Array<CredentialMLCertification1>();

        mlCertification1Array = this.formPage1.controls['mlCertification1s'].value;


        const tmpMLCertification1 = new CredentialMLCertification1();

        const forgeinKeyMLCertification1: any =   Number.parseFloat(result.id.toString()); // result.id;

        this.formService.findPage1(forgeinKeyMLCertification1).subscribe((res: Page1) => {

            // deleting previously added aosms to add new ones
            this.credentialMlCertification1Service.deleteAllByPageId(res.id).subscribe(del => {// adding relation to the object

                for (let i = 0 ; i < mlCertification1Array.length; i++) {


                    mlCertification1Array[i].credentialingAppPage1 = res;


                    this.credentialMlCertification1Service.create(mlCertification1Array[i]).subscribe((res: CredentialMLCertification1) =>
                        this.onSaveSuccessMLCertification1(res), (res: Response) => {
                        this.onSaveErrorMLCertification1();
                        if (res.status === 400) {
                            this.duplicatedName = true;
                        }

                    });
                }
            })


        } , (res: Response) => {})
        // .....end new ml certification...

        //  ...ml2

        let mlCertification2Array = new Array<CredentialMLCertification2>();

        mlCertification2Array = this.formPage1.controls['mlCertification2s'].value;


        const tmpMLCertification2 = new CredentialMLCertification2();

        const forgeinKeyMLCertification2: any =   Number.parseFloat(result.id.toString()); // result.id;

        this.formService.findPage1(forgeinKeyMLCertification2).subscribe((res: Page1) => {

            // deleting previously added aosms to add new ones
            this.credentialMlCertification2Service.deleteAllByPageId(res.id).subscribe(del => {// adding relation to the object

                for (let i = 0 ; i < mlCertification2Array.length; i++) {


                    mlCertification2Array[i].credentialingAppPage1 = res;


                    this.credentialMlCertification2Service.create(mlCertification2Array[i]).subscribe((res: CredentialMLCertification2) =>
                        this.onSaveSuccessMLCertification2(res), (res: Response) => {
                        this.onSaveErrorMLCertification2();
                        if (res.status === 400) {
                            this.duplicatedName = true;
                        }

                    });
                }
            })


        } , (res: Response) => {})



    }

    private onSaveErrorPage1() {
        this.isSaving = false;
    }

    private subscribeToSaveResponsePage2(result: Observable<Page2>) {
        result.subscribe((res: Page2) => {
            this.onSaveSuccessPage2(res)
        }, (res: Response) => {
            this.onSaveErrorPage2();
            if (res.status === 400) {
                this.duplicatedName = true;
            }
            this.onSaveErrorPage2()
        });
    }

    private onSaveSuccessPage2(result: Page2) {


        // // setting the created page in the parent form
        // this.credentialApplicationFormService.getCurrentForm().subscribe(form => {
        //     form.credentialingAppPage2 = result;
        //     this.page2 = result;
        //     this.credentialApplicationFormService.update(form).subscribe();
        // })

        // this.eventManager.broadcast({ name: 'clinicListModification', content: 'OK'});
        this.isSaving = false;
        this.toastrService.success('This form was edited.', 'Successfully saved !');


        // setting Id of the create page2 to current page2
        this.page2.id = result.id;

        // after saving page2 we have to save inner entities (an array of aosms)
        let aosmArray = new Array<CredentialAOSM>();

        aosmArray = this.formPage2.controls['aosms'].value;

        const tmp = new CredentialAOSM();

        const forgeinKey: any = Number.parseFloat(result.id.toString()); // result.id;

        // for (let i = 0 ; i < aosmArray.length; i++) {
        //    console.log('there are the aosm Arraies : ' + aosmArray[i].stateLicenseNumber + ' ***** ' + aosmArray[i]);

        this.formService.findPage2(forgeinKey).subscribe((res: Page2) => {

            // deleting previously added aosms to add new ones
            this.credentialAOSMService.deleteAllByPageId(res.id).subscribe(del => {// adding relation to the object

                for (let i = 0; i < aosmArray.length; i++) {

                    // add related page top the aosmes
                    aosmArray[i].credentialingAppPage2 = res;

                    this.credentialAOSMService.create(aosmArray[i]).subscribe((res: CredentialAOSM) =>
                        this.onSaveSuccessAosm(res), (res: Response) => {
                        this.onSaveErrorAosm();
                        if (res.status === 400) {
                            this.duplicatedName = true;
                        }

                    });
                }
            })


        }, (res: Response) => {})

        //......internship...

        let internshipArray = new Array<CredentialInternship>();

        internshipArray = this.formPage2.controls['internships'].value;

        const tmpInternship = new CredentialInternship();

        const forgeinKeyInternship: any =   Number.parseFloat(result.id.toString()); // result.id;

        this.formService.findPage2(forgeinKeyInternship).subscribe((res: Page2) => {

            this.credentialInternshipService.deleteAllByPageId(res.id).subscribe(del => {// adding relation to the object

                for (let i = 0 ; i < internshipArray.length; i++) {


                    internshipArray[i].credentialingAppPage2 = res;

                    this.credentialInternshipService.create(internshipArray[i]).subscribe((res: CredentialInternship) =>
                        this.onSaveSuccessInternship(res), (res: Response) => {
                        this.onSaveErrorInternship();
                        if (res.status === 400) {
                            this.duplicatedName = true;
                        }

                    });
                }
            })


        } , (res: Response) => {})

        //.....end internship
    }



    private onSaveSuccessAosm(result: CredentialAOSM) {
        this.isSaving = false;
    }

    initAosm() {
        return this.fb.group({
            stateLicenseNumber: [''],
            aosmOrderId: [''],
            licenseNumberMedical: [''],
            originalDateOfIssue: [''],
            currentlyPracticeState: [''],
            naOtherStateMedicalLicenses: [''],
            explain: [''],
        });
    }

    private onSaveErrorAosm() {
        this.isSaving = false;
    }

    private onSaveErrorPage3() {
        this.isSaving = false;
    }

    //start mlCertification1s.................................

    private onSaveSuccessMLCertification1(result: CredentialMLCertification1) {
        this.isSaving = false;
    }

    initMlCertification1s() {
        return this.fb.group({
            controlledRCertificationNumber: ['' ,[]],
            isNaMLCertification1: ['' ,[]],
            expiresDate2: ['' ,[]],
            certification1OrderId: ['' ,[]],
        });
    }

    private onSaveErrorMLCertification1() {
        this.isSaving = false;
    }


    //...end mlCertification1s.....................

    //....start ml2 ....
    private onSaveSuccessMLCertification2(result: CredentialMLCertification2) {
        this.isSaving = false;
    }

    initMlCertification2s() {
        return this.fb.group({
            deaRegistrationNumber: ['' ,[]],
            isNaMLCertification2: ['' ,[]],
            expiresDate3: ['' ,[]],
            certification2OrderId: ['' ,[]],
        });
    }

    private onSaveErrorMLCertification2() {
        this.isSaving = false;
    }
    //...end ml2 ....


    //    start fun internship................
    private onSaveSuccessInternship(result: CredentialInternship) {
        this.isSaving = false;
    }

    initInternship() {
        return this.fb.group({
            nameInstitutionInternship: ['', []],
            datesAttendendInternship: ['' , []],
            datesAttendendInternshipEnd: ['' , []],
            addressInternship: ['' , []],
            programDirectorNameInternship: ['' , []],
            typeInternship: ['' , []],
            isProgramSuccessfullyCompletedInternship: [''],
            isNaInternship: [''],
            isSubjectOfDisciplinaryActionInternship: [''],
            rotatingOrStraight: [''],
            programSuccessfullyCompletedInternship: [''],
            subjectOfDisciplinaryActionInternship: [''],
            listSpeciality: [''],
            internshipOrderId: [''],

        });
    }

    private onSaveErrorInternship() {
        this.isSaving = false;
    }



    //    start end internship................

    private onSaveErrorTraining() {
        this.isSaving = false;
    }

    private onSaveSuccessTraining(result: TrainingFellowships) {
        this.isSaving = false;
    }

    private onError(error: any) {
        // this.jhiAlertService.error(error.message, null, null);
    }

    // validationReset() {
    //     this.duplicatedName = false;
    // }
    /// ...page 1 ml certification

    addMLCertification1() {
        const control = <FormArray>this.formPage1.controls['mlCertification1s'];
        control.push(this.initMlCertification1s());
        (<FormGroup>control.at(control.length-1)).controls.controlledRCertificationNumber.setValidators([Validators.required]);
        (<FormGroup>control.at(control.length-1)).controls.expiresDate2.setValidators([Validators.required]);

    }

    removeMlCertification1(i: number) {
        const control = <FormArray>this.formPage1.controls['mlCertification1s'];
        control.removeAt(i);


    }
    //......end

    //start ml2/.....

    addMLCertification2() {
        const control = <FormArray>this.formPage1.controls['mlCertification2s'];
        control.push(this.initMlCertification2s());
        (<FormGroup>control.at(control.length-1)).controls.deaRegistrationNumber.setValidators([Validators.required]);
        (<FormGroup>control.at(control.length-1)).controls.expiresDate3.setValidators([Validators.required]);
    }

    removeMlCertification2(i: number) {
        const control = <FormArray>this.formPage1.controls['mlCertification2s'];
        control.removeAt(i);
    }

    addAosm() {
        const control = <FormArray>this.formPage2.controls['aosms'];
        control.push(this.initAosm());
    }

    removeAosm(i: number) {
        const control = <FormArray>this.formPage2.controls['aosms'];
        control.removeAt(i);
    }

    //...internship........start.....

    removeInternship(i: number) {
        const control = <FormArray>this.formPage2.controls['internships'];
        control.removeAt(i);
    }

    addInternship() {
        const control = <FormArray>this.formPage2.controls['internships'];
        control.push(this.initInternship());
        (<FormGroup>control.at(control.length-1)).controls.nameInstitutionInternship.setValidators([Validators.required]);
        (<FormGroup>control.at(control.length-1)).controls.datesAttendendInternship.setValidators([Validators.required]);
        (<FormGroup>control.at(control.length-1)).controls.datesAttendendInternshipEnd.setValidators([Validators.required]);
        (<FormGroup>control.at(control.length-1)).controls.addressInternship.setValidators([Validators.required]);
        (<FormGroup>control.at(control.length-1)).controls.programDirectorNameInternship.setValidators([Validators.required]);
        (<FormGroup>control.at(control.length-1)).controls.typeInternship.setValidators([Validators.required]);

    }

    //...internship........end.....

    addTraining() {
        const control = <FormArray>this.formPage3.controls['trainings'];
        control.push(this.initTrainings());
    }

    removeTraining(i: number) {
        const control = <FormArray>this.formPage3.controls['trainings'];
        control.removeAt(i);
    }
    // ......page3
    changeResidency(){
        if(this.page3.isProgramSuccessfullyCompletedResidency === false){
            this.formPage3.controls.programSuccessfullyCompletedResidency.setValidators([Validators.required]);
            this.formPage3.controls.programSuccessfullyCompletedResidency.reset();
        }
        else if(this.page3.isProgramSuccessfullyCompletedResidency === true){
            this.formPage3.controls.programSuccessfullyCompletedResidency.clearValidators();
            this.formPage3.controls.programSuccessfullyCompletedResidency.setErrors(null);
            this.formPage3.controls.programSuccessfullyCompletedResidency.reset();
        }
    }


    changeNaResidency(){
        // alert('checkbox =====>' + this.page3.isNaResidencyPrograms == false)
        if(this.page3.isNaResidencyPrograms === false){
            this.formPage3.controls.nameInstitutionResidencyProgram.setValidators([Validators.required]);
            this.formPage3.controls.nameInstitutionResidencyProgram.reset();

            this.formPage3.controls.datesAttendendResidencyProgram.setValidators([Validators.required]);
            this.formPage3.controls.datesAttendendResidencyProgram.reset();

            this.formPage3.controls.addressResidencyProgram.setValidators([Validators.required]);
            this.formPage3.controls.addressResidencyProgram.reset();

            this.formPage3.controls.cityResidencyProgram.setValidators([Validators.required]);
            this.formPage3.controls.cityResidencyProgram.reset();

            this.formPage3.controls.stateResidencyProgram.setValidators([Validators.required]);
            this.formPage3.controls.stateResidencyProgram.reset();

            this.formPage3.controls.zipResidencyProgram.setValidators([Validators.required]);
            this.formPage3.controls.zipResidencyProgram.reset();

            this.formPage3.controls.typeOfResidency.setValidators([Validators.required]);
            this.formPage3.controls.typeOfResidency.reset();

            this.formPage3.controls.fullNameOfProgramDirectorResidencyProgram.setValidators([Validators.required]);
            this.formPage3.controls.fullNameOfProgramDirectorResidencyProgram.reset();

        }
        else if(this.page3.isNaResidencyPrograms === true){
            this.formPage3.controls.nameInstitutionResidencyProgram.clearValidators();
            this.formPage3.controls.nameInstitutionResidencyProgram.setErrors(null);
            this.formPage3.controls.nameInstitutionResidencyProgram.reset();

            this.formPage3.controls.datesAttendendResidencyProgram.clearValidators();
            this.formPage3.controls.datesAttendendResidencyProgram.setErrors(null);
            this.formPage3.controls.datesAttendendResidencyProgram.reset();

            this.formPage3.controls.addressResidencyProgram.clearValidators();
            this.formPage3.controls.addressResidencyProgram.setErrors(null);
            this.formPage3.controls.addressResidencyProgram.reset();

            this.formPage3.controls.cityResidencyProgram.clearValidators();
            this.formPage3.controls.cityResidencyProgram.setErrors(null);
            this.formPage3.controls.cityResidencyProgram.reset();

            this.formPage3.controls.stateResidencyProgram.clearValidators();
            this.formPage3.controls.stateResidencyProgram.setErrors(null);
            this.formPage3.controls.stateResidencyProgram.reset();

            this.formPage3.controls.zipResidencyProgram.clearValidators();
            this.formPage3.controls.zipResidencyProgram.setErrors(null);
            this.formPage3.controls.zipResidencyProgram.reset();

            this.formPage3.controls.typeOfResidency.clearValidators();
            this.formPage3.controls.typeOfResidency.setErrors(null);
            this.formPage3.controls.typeOfResidency.reset();

            this.formPage3.controls.fullNameOfProgramDirectorResidencyProgram.clearValidators();
            this.formPage3.controls.fullNameOfProgramDirectorResidencyProgram.setErrors(null);
            this.formPage3.controls.fullNameOfProgramDirectorResidencyProgram.reset();


        }

        if(this.page3.isProgramSuccessfullyCompletedResidency === false && this.page3.isNaResidencyPrograms === false ){
            this.formPage3.controls.programSuccessfullyCompletedResidency.setValidators([Validators.required]);
            this.formPage3.controls.programSuccessfullyCompletedResidency.reset();
        }
        else if(this.page3.isProgramSuccessfullyCompletedResidency === false && this.page3.isNaResidencyPrograms === true){
            this.formPage3.controls.programSuccessfullyCompletedResidency.clearValidators();
            this.formPage3.controls.programSuccessfullyCompletedResidency.setErrors(null);
            this.formPage3.controls.programSuccessfullyCompletedResidency.reset();
        }
        if(this.page3.isProgramSuccessfullyCompletedResidency === true && this.page3.isNaResidencyPrograms === false || this.page3.isProgramSuccessfullyCompletedResidency === true && this.page3.isNaResidencyPrograms === true ){
            this.formPage3.controls.programSuccessfullyCompletedResidency.clearValidators();
            this.formPage3.controls.programSuccessfullyCompletedResidency.setErrors(null);
            this.formPage3.controls.programSuccessfullyCompletedResidency.reset();
        }

    }

    changeCompletedTrainingFellowship(i){
        const control = <FormArray>this.formPage3.controls['trainings'];
        if((<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedTrainingFellowship.value === false){
            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.setValidators([Validators.required]);
            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.reset();
        }
        else if((<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedTrainingFellowship.value === true){
            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.clearValidators();
            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.setErrors(null);
            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.reset();
        }
    }
    changeDisciplinaryTrainingFellowship(i){
        const control = <FormArray>this.formPage3.controls['trainings'];
        if((<FormGroup>control.at(i)).controls.disciplinaryTrainingFellowship.value === true){
            (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.setValidators([Validators.required]);
            (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.reset();
        }
        else if((<FormGroup>control.at(i)).controls.disciplinaryTrainingFellowship.value === false){
            (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.clearValidators();
            (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.setErrors(null);
            (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.reset();
        }
    }

    changeNaTrainingFellowship(i){
        const control = <FormArray>this.formPage3.controls['trainings'];
        if((<FormGroup>control.at(i)).controls.isNaTrainingFellowship.value === true &&  (<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedTrainingFellowship.value === false){
            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.clearValidators();
            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.setErrors(null);
            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.reset();
        }
        if((<FormGroup>control.at(i)).controls.isNaTrainingFellowship.value === false &&  (<FormGroup>control.at(i)).controls.isProgramSuccessfullyCompletedTrainingFellowship.value === false){
            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.setValidators([Validators.required]);
            (<FormGroup>control.at(i)).controls.programSuccessfullyCompletedTrainingFellowship.reset();
        }
        if((<FormGroup>control.at(i)).controls.isNaTrainingFellowship.value === true &&  (<FormGroup>control.at(i)).controls.disciplinaryTrainingFellowship.value === true){
            (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.clearValidators();
            (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.setErrors(null);
            (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.reset();
        }
        if((<FormGroup>control.at(i)).controls.isNaTrainingFellowship.value === false &&  (<FormGroup>control.at(i)).controls.disciplinaryTrainingFellowship.value === true){
            (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.setValidators([Validators.required]);
            (<FormGroup>control.at(i)).controls.attachAnExplanationTrainingFellowship.reset();
        }
    }


    savePage3() {
        this.isSaving = true;
        this.saveForPage3 = true;

        let trainingArray = new Array<TrainingFellowships>()

        trainingArray = this.formPage3.controls['trainings'].value;

        const control = <FormArray>this.formPage3.controls['trainings'];

        // alert ('before    ++++ >>>>   ' + (<FormGroup>control.at(control.length-1)).controls.isNaTrainingFellowship.value);
        for(let i = 0 ; i < control.length ; i++ ){
            if( (<FormGroup>control.at(i)).controls.isNaTrainingFellowship.value === ''){
                (<FormGroup>control.at(i)).controls.isNaTrainingFellowship.setValue(0) ;
            }
            (<FormGroup>control.at(i)).controls.trainingOrderId.setValue(i+1)
        }

        // alert ('after    ++++ >>>>   ' + (<FormGroup>control.at(control.length-1)).controls.isNaTrainingFellowship.value);
        if (!isNullOrUndefined(this.page3.id)) {
            this.subscribeToSaveResponsePage3(
                this.formService.updatePage3(this.page3, this.currentUserForm.id));
        } else {
            this.subscribeToSaveResponsePage3(
                this.formService.createPage3(this.page3, this.currentUserForm.id));
        }

    }

    private subscribeToSaveResponsePage3(result: Observable<Page3>) {
        result.subscribe((res: Page3) => {
            this.onSaveSuccessPage3(res)
        }, (res: Response) => {
            this.onSaveErrorPage3();
            if (res.status === 400) {
                this.duplicatedName = true;
            }
            this.onSaveErrorPage3()
        });
    }

    private onSaveSuccessPage3(result: Page3) {

        // // setting the created page in the parent form
        // this.credentialApplicationFormService.getCurrentForm().subscribe(form => {
        //     form.credentialingAppPage3 = result;
        //     this.page3 = result;
        //     this.credentialApplicationFormService.update(form).subscribe();
        // })


        // this.eventManager.broadcast({ name: 'clinicListModification', content: 'OK'});
        this.isSaving = false;
        this.toastrService.success('This form was edited.', 'Successfully saved !');

        // setting Id of the create page3 to current page3
        this.page3.id = result.id;

        // after saving page3 we have to save inner entities (an array of trainings)
        let trainingsArray = new Array<TrainingFellowships>();

        trainingsArray = this.formPage3.controls['trainings'].value;

        const forgeinKey: any = Number.parseFloat(result.id.toString()); // result.id;

        //     for (let i = 0 ; i < trainingsArray.length; i++) {

        this.formService.findPage3(forgeinKey).subscribe((res: Page3) => {

            //  alert( ' ---------   >> '+ JSON.stringify(res))

            // deleting previously added trainings to add new ones
            this.trainingService.deleteAllByPageId(res.id).subscribe(del => {// adding relation to the object

                for (let i = 0; i < trainingsArray.length; i++) {

                    //  alert('------ >>>>>>  ' + trainingsArray.length)

                    // adding relation to the object
                    trainingsArray[i].credentialingAppPage3 = res;

                    this.trainingService.create(trainingsArray[i]).subscribe((res: TrainingFellowships) =>
                        this.onSaveSuccessTraining(res), (res: Response) => {
                        this.onSaveErrorTraining();
                        if (res.status === 400) {
                            this.duplicatedName = true;
                        }
                    });
                }

            })


        }, (res: Response) => {
        })

        // }
    }

    private onSaveErrorPage2() {
        this.isSaving = false;
    }

    initTrainings() {
        return this.fb.group({
            nameOfInstitutionTrainingFellowship: ['' ],
            isNaTrainingFellowship: ['' ],
            datesAttendedTrainingFellowship: [''],
            nameOfImmediateSupervisorTrainingFellowship: [''],
            addressTrainingFellowship: [''],
            cityTrainingFellowship: [''],
            stateTrainingFellowship: [''],
            zipTrainingFellowship: [''],
            typeOfFellowship: [''],
            isProgramSuccessfullyCompletedTrainingFellowship: [''],
            programSuccessfullyCompletedTrainingFellowship: [''],
            disciplinaryTrainingFellowship: [''],
            attachAnExplanationTrainingFellowship: [''],
            trainingOrderId: [''],
        });
    }

    // .....start page 5................

    savePage5() {
        this.isSaving = true;
        this.saveForPage5 = true;

        if (!isNullOrUndefined(this.page5.id)) {
            this.subscribeToSaveResponsePage5(
                this.formService.updatePage5(this.page5, this.currentUserForm.id));
        } else {
            this.subscribeToSaveResponsePage5(
                this.formService.createPage5(this.page5, this.currentUserForm.id));
        }

    }

    private subscribeToSaveResponsePage5(result: Observable<Page5>) {
        result.subscribe((res: Page5) =>
            this.onSaveSuccessPage5(res), (res: Response) => {
            this.onSaveErrorPage5();
            if (res.status === 400) {
                this.duplicatedName = true;
            }
            this.onSaveErrorPage5()
        });
    }

    private onSaveErrorPage5() {
        this.isSaving = false;
    }

    private onSaveSuccessPage5(result: Page5) {

        // // setting the created page in the parent form
        // this.credentialApplicationFormService.getCurrentForm().subscribe(form => {
        //     form.credentialingAppPage5 = result;
        //     this.page5 = result;
        //     this.credentialApplicationFormService.update(form).subscribe();
        // })

        //   this.currentUserForm.credentialingAppPage5 = result;

        // setting the created page in the parent form
        //  this.credentialApplicationFormService.update(this.currentUserForm).subscribe();

        this.isSaving = false;
        this.toastrService.success('This form was edited.', 'Successfully saved !');

        // setting Id of the create page5 to current page5
        this.page5.id = result.id;
    }

    initHospitals() {
        return this.fb.group({
            nameOfInstitutionHospitalAndUniversity: [''],
            addressHospitalAndUniversity: [''],
            cityHospitalAndUniversity: [''],
            stateHospitalAndUniversity: [''],
            zipHospitalAndUniversity: [''],
            datesAffiliateHospitalAndUniversity: [''],
            datesAffiliateHospitalAndUniversityEnd: [''],
            departmentHospitalAndUniversity: [''],
            membershipStatusHospitalAndUniversity: [''],
            departmentChiefHospitalAndUniversity: [''],
            privilegesAtInstitutionHospitalAndUniversity: [''],
            typePrivilegesGrantedHospitalAndUniversity: [''],
            hospitalOrderId: [''],
        });
    }

    addHospital() {
        const control = <FormArray>this.formPage4.controls['hospitals'];
        control.push(this.initHospitals());
    }


    removeHospital(i: number) {
        const control = <FormArray>this.formPage4.controls['hospitals'];
        control.removeAt(i);
    }
    //...page 4
    changeInstitutionHospitalAndUniversity(i){
        const control = <FormArray>this.formPage4.controls['hospitals'];
        if((<FormGroup>control.at(i)).controls.privilegesAtInstitutionHospitalAndUniversity.value === true){
            (<FormGroup>control.at(i)).controls.typePrivilegesGrantedHospitalAndUniversity.setValidators([Validators.required]);
            (<FormGroup>control.at(i)).controls.typePrivilegesGrantedHospitalAndUniversity.reset();
        }
        else if((<FormGroup>control.at(i)).controls.privilegesAtInstitutionHospitalAndUniversity.value === false){
            (<FormGroup>control.at(i)).controls.typePrivilegesGrantedHospitalAndUniversity.clearValidators();
            (<FormGroup>control.at(i)).controls.typePrivilegesGrantedHospitalAndUniversity.setErrors(null);
            (<FormGroup>control.at(i)).controls.typePrivilegesGrantedHospitalAndUniversity.reset();
        }
    }

    savePage4() {
        this.isSaving = true;
        this.saveForPage4 = true;

        // let hospitalArray = new Array<HospitalUniversityAffiliation>()
        //
        // hospitalArray = this.formPage3.controls['trainings'].value;

        const control = <FormArray>this.formPage4.controls['hospitals'];

        for(let i = 0 ; i < control.length ; i++ ){
            (<FormGroup>control.at(i)).controls.hospitalOrderId.setValue(i+1)
        }
        if (!isNullOrUndefined(this.page4.id)) {
            this.subscribeToSaveResponsePage4(
                this.formService.updatePage4(this.page4,this.currentUserForm.id));
        } else {
            // alert(JSON.stringify(this.page4));
            this.subscribeToSaveResponsePage4(
                this.formService.createPage4(this.page4, this.currentUserForm.id));
        }

    }

    private subscribeToSaveResponsePage4(result: Observable<Page4>) {
        result.subscribe((res: Page4) => {
            this.onSaveSuccessPage4(res)
        }, (res: Response) => {
            this.onSaveErrorPage4();
            if (res.status === 400) {
                this.duplicatedName = true;
            }
            this.onSaveErrorPage4()
        });
    }

    private onSaveErrorPage4() {
        this.isSaving = false;
    }

    private onSaveSuccessHospital(result: TrainingFellowships) {
        this.isSaving = false;
    }

    private onSaveSuccessPage4(result: Page4) {

        // this.eventManager.broadcast({ name: 'clinicListModification', content: 'OK'});

        // // setting the created page in the parent form
        // this.credentialApplicationFormService.getCurrentForm().subscribe(form => {
        //     form.credentialingAppPage4 = result;
        //     this.page4 = result;
        //     this.credentialApplicationFormService.update(form).subscribe();
        // });

        this.isSaving = false;
        this.toastrService.success('This form was edited.', 'Successfully saved !');

        // setting Id of the create page4 to current page4
        this.page4.id = result.id;


        // alert(' rhis is the form entity  ' + JSON.stringify(this.currentUserForm))

        // setting the created page in the parent form
        //  this.credentialApplicationFormService.update(this.currentUserForm).subscribe();


        // after saving page2 we have to save inner entities (an array of aosms)
        let hospitalArray = new Array<HospitalUniversityAffiliation>();

        hospitalArray = this.formPage4.controls['hospitals'].value;

        const forgeinKey: any = Number.parseFloat(result.id.toString()); // result.id;

        //  for (let i = 0 ; i<hospitalArray.length; i++) {
        this.formService.findPage4(forgeinKey).subscribe((res: Page4) => {

            this.hospitalService.deleteAllByPageId(res.id).subscribe(del => {// adding relation to the object


                for (let i = 0; i < hospitalArray.length; i++) {

                    // adding relation to the object
                    hospitalArray[i].credentialingAppPage4 = res;

                    this.hospitalService.create(hospitalArray[i]).subscribe((res: HospitalUniversityAffiliation) =>
                        this.onSaveSuccessHospital(res), (res: Response) => {
                        this.onSaveErrorHospital();
                        if (res.status === 400) {
                            this.duplicatedName = true;
                        }
                    })
                }
            });


        }, (res: Response) => {
        })

        // }
    }

    private onSaveErrorHospital() {
        this.isSaving = false;
    }

    //............................START FOR PAGE6
    savePage6() {
        this.isSaving = true;
        this.saveForPage6 = true;

        if (!isNullOrUndefined(this.page6.id)) {
            this.subscribeToSaveResponsePage6(
                this.formService.updatePage6(this.page6, this.currentUserForm.id));
        } else {
            this.subscribeToSaveResponsePage6(
                this.formService.createPage6(this.page6, this.currentUserForm.id));
        }

    }

    private subscribeToSaveResponsePage6(result: Observable<Page6>) {
        result.subscribe((res: Page6) =>
            this.onSaveSuccessPage6(res), (res: Response) => {
            this.onSaveErrorPage6();
            if (res.status === 400) {
                this.duplicatedName = true;
            }
            this.onSaveErrorPage6()
        });
    }

    private onSaveErrorPage6() {
        this.isSaving = false;
    }

    private onSaveSuccessPage6(result: Page6) {
        // this.currentUserForm.credentialingAppPage6 = result;
        //
        // // setting the created page in the parent form
        // this.credentialApplicationFormService.update(this.currentUserForm).subscribe();

        // // setting the created page in the parent form
        // this.credentialApplicationFormService.getCurrentForm().subscribe(form => {
        //     form.credentialingAppPage6 = result;
        //     this.page6 = result;
        //     this.credentialApplicationFormService.update(form).subscribe();
        // })


        this.isSaving = false;
        this.toastrService.success('This form was edited.', 'Successfully saved !');
        // setting Id of the create page6 to current page6
        this.page6.id = result.id;
    }

    //..........................PAGE 7............

    //..........................PAGE 7............
    changeSuitAgainst(){
        if(this.page7.professionalLiabilityLawSuitAgainstYouYes === true){
            this.formPage7.controls.professionalLiabilityLawSuitAgainstYouNo.setValidators([Validators.required]);
            this.formPage7.controls.professionalLiabilityLawSuitAgainstYouNo.reset();
        }
        else if(this.page7.professionalLiabilityLawSuitAgainstYouYes === false){
            this.formPage7.controls.professionalLiabilityLawSuitAgainstYouNo.clearValidators();
            this.formPage7.controls.professionalLiabilityLawSuitAgainstYouNo.setErrors(null);
            this.formPage7.controls.professionalLiabilityLawSuitAgainstYouNo.reset();
        }
    }
    changeNowPending(){
        if(this.page7.professionalLiabilityAnyNowPendingYes === true){
            this.formPage7.controls.professionalLiabilityAnyNowPendingNo.setValidators([Validators.required]);
            this.formPage7.controls.professionalLiabilityAnyNowPendingNo.reset();
        }
        else if(this.page7.professionalLiabilityAnyNowPendingYes === false){
            this.formPage7.controls.professionalLiabilityAnyNowPendingNo.clearValidators();
            this.formPage7.controls.professionalLiabilityAnyNowPendingNo.setErrors(null);
            this.formPage7.controls.professionalLiabilityAnyNowPendingNo.reset();
        }
    }
    changeClaim(){
        if(this.page7.professionalLiabilityAnyJudgementOrPaymentOrClaimYes === true){
            this.formPage7.controls.professionalLiabilityAnyJudgementOrPaymentOrClaimNo.setValidators([Validators.required]);
            this.formPage7.controls.professionalLiabilityAnyJudgementOrPaymentOrClaimNo.reset();
        }
        else if(this.page7.professionalLiabilityAnyJudgementOrPaymentOrClaimYes === false){
            this.formPage7.controls.professionalLiabilityAnyJudgementOrPaymentOrClaimNo.clearValidators();
            this.formPage7.controls.professionalLiabilityAnyJudgementOrPaymentOrClaimNo.setErrors(null);
            this.formPage7.controls.professionalLiabilityAnyJudgementOrPaymentOrClaimNo.reset();
        }
    }

    changeInsurance(){
        if(this.page7.professionalLiabilityDeniedProfessionalInsuranceYes === true){
            this.formPage7.controls.professionalLiabilityDeniedProfessionalInsuranceNo.setValidators([Validators.required]);
            this.formPage7.controls.professionalLiabilityDeniedProfessionalInsuranceNo.reset();
        }
        else if(this.page7.professionalLiabilityDeniedProfessionalInsuranceYes === false){
            this.formPage7.controls.professionalLiabilityDeniedProfessionalInsuranceNo.clearValidators();
            this.formPage7.controls.professionalLiabilityDeniedProfessionalInsuranceNo.setErrors(null);
            this.formPage7.controls.professionalLiabilityDeniedProfessionalInsuranceNo.reset();
        }
    }



    savePage7() {
        this.isSaving = true;
        this.saveForPage7 = true;

        if (!isNullOrUndefined(this.page7.id)) {
            this.subscribeToSaveResponsePage7(
                this.formService.updatePage7(this.page7, this.currentUserForm.id));
        } else {
            this.subscribeToSaveResponsePage7(
                this.formService.createPage7(this.page7, this.currentUserForm.id));
        }

    }

    private subscribeToSaveResponsePage7(result: Observable<Page7>) {
        result.subscribe((res: Page7) =>
            this.onSaveSuccessPage7(res), (res: Response) => {
            this.onSaveErrorPage7();
            if (res.status === 400) {
                this.duplicatedName = true;
            }
            this.onSaveErrorPage7()
        });
    }

    private onSaveErrorPage7() {
        this.isSaving = false;
    }

    private onSaveSuccessPage7(result: Page7) {

        // // setting the created page in the parent form
        // this.credentialApplicationFormService.getCurrentForm().subscribe(form => {
        //     form.credentialingAppPage7 = result;
        //     this.page7 = result;
        //     this.credentialApplicationFormService.update(form).subscribe();
        // })

        this.isSaving = false;
        this.toastrService.success('This form was edited.', 'Successfully saved !');
        // setting Id of the create page7 to current page7
        this.page7.id = result.id;
    }

    //...................start page8
    change1() {
        // alert(' in change ');
        // alert(' in change value == ' + this.page8.personalSanctionsLicenseToPracticeDeniedYes);
        if(this.page8.personalSanctionsLicenseToPracticeDeniedYes === true ){
            // alert(' in if ');

            this.formPage8.controls.personalSanctionsLicenseToPracticeDeniedNo.setValidators([Validators.required]);
            this.formPage8.controls.personalSanctionsLicenseToPracticeDeniedNo.reset();
            // alert(' in if 2222');

        }else if(this.page8.personalSanctionsLicenseToPracticeDeniedYes === false ){
            this.formPage8.controls.personalSanctionsLicenseToPracticeDeniedNo.clearValidators();
            this.formPage8.controls.personalSanctionsLicenseToPracticeDeniedNo.setErrors(null);
            this.formPage8.controls.personalSanctionsLicenseToPracticeDeniedNo.reset();
        }
        // alert(' out if ');
    }
    change2(){
        if(this.page8.personalSanctionsLicenseToReprimandedSubjectYes === true){
            this.formPage8.controls.personalSanctionsLicenseToReprimandedSubjectNo.setValidators([Validators.required]);
            this.formPage8.controls.personalSanctionsLicenseToReprimandedSubjectNo.reset();
        }
        else if(this.page8.personalSanctionsLicenseToReprimandedSubjectYes === false){
            this.formPage8.controls.personalSanctionsLicenseToReprimandedSubjectNo.clearValidators();
            this.formPage8.controls.personalSanctionsLicenseToReprimandedSubjectNo.setErrors(null);
            this.formPage8.controls.personalSanctionsLicenseToReprimandedSubjectNo.reset();
        }
    }
    change3(){
        if(this.page8.personalSanctionsLostBoardYes === true){
            this.formPage8.controls.personalSanctionsLostBoardNo.setValidators([Validators.required]);
            this.formPage8.controls.personalSanctionsLostBoardNo.reset();
        }
        else if(this.page8.personalSanctionsLostBoardYes === false){
            this.formPage8.controls.personalSanctionsLostBoardNo.clearValidators();
            this.formPage8.controls.personalSanctionsLostBoardNo.setErrors(null);
            this.formPage8.controls.personalSanctionsLostBoardNo.reset();
        }
    }
    change4(){
        if(this.page8.personalSanctionsCapitalCertifyingYes === true){
            this.formPage8.controls.personalSanctionsCapitalCertifyingNo.setValidators([Validators.required]);
            this.formPage8.controls.personalSanctionsCapitalCertifyingNo.reset();
        }
        else if(this.page8.personalSanctionsCapitalCertifyingYes === false){
            this.formPage8.controls.personalSanctionsCapitalCertifyingNo.clearValidators();
            this.formPage8.controls.personalSanctionsCapitalCertifyingNo.setErrors(null);
            this.formPage8.controls.personalSanctionsCapitalCertifyingNo.reset();
        }
    }
    change5(){
        if(this.page8.personalSanctionsInformationPertainingYes === true){
            this.formPage8.controls.personalSanctionsInformationPertainingNo.setValidators([Validators.required]);
            this.formPage8.controls.personalSanctionsInformationPertainingNo.reset();
        }
        else if(this.page8.personalSanctionsInformationPertainingYes === false){
            this.formPage8.controls.personalSanctionsInformationPertainingNo.clearValidators();
            this.formPage8.controls.personalSanctionsInformationPertainingNo.setErrors(null);
            this.formPage8.controls.personalSanctionsInformationPertainingNo.reset();
        }
    }
    change6(){
        if(this.page8.personalSanctionsFederalDEANumberYes === true){
            this.formPage8.controls.personalSanctionsFederalDEANumberNo.setValidators([Validators.required]);
            this.formPage8.controls.personalSanctionsFederalDEANumberNo.reset();
        }
        else if(this.page8.personalSanctionsFederalDEANumberYes === false){
            this.formPage8.controls.personalSanctionsFederalDEANumberNo.clearValidators();
            this.formPage8.controls.personalSanctionsFederalDEANumberNo.setErrors(null);
            this.formPage8.controls.personalSanctionsFederalDEANumberNo.reset();
        }
    }

    change7() {
        if (this.page8.personalSanctionsHospitalAmbulatoryYes === true) {
            this.formPage8.controls.personalSanctionsHospitalAmbulatoryNo.setValidators([Validators.required]);
            this.formPage8.controls.personalSanctionsHospitalAmbulatoryNo.reset();
        }
        else if (this.page8.personalSanctionsHospitalAmbulatoryYes === false) {
            this.formPage8.controls.personalSanctionsHospitalAmbulatoryNo.clearValidators();
            this.formPage8.controls.personalSanctionsHospitalAmbulatoryNo.setErrors(null);
            this.formPage8.controls.personalSanctionsHospitalAmbulatoryNo.reset();
        }
    }

    change8(){
        if(this.page8.personalSanctionsVoluntarilyAndInVoluntarilyYes === true){
            this.formPage8.controls.personalSanctionsVoluntarilyAndInVoluntarilyNo.setValidators([Validators.required]);
            this.formPage8.controls.personalSanctionsVoluntarilyAndInVoluntarilyNo.reset();
        }
        else if(this.page8.personalSanctionsVoluntarilyAndInVoluntarilyYes === false){
            this.formPage8.controls.personalSanctionsVoluntarilyAndInVoluntarilyNo.clearValidators();
            this.formPage8.controls.personalSanctionsVoluntarilyAndInVoluntarilyNo.setErrors(null);
            this.formPage8.controls.personalSanctionsVoluntarilyAndInVoluntarilyNo.reset();
        }
    }
    change9(){
        if(this.page8.personalSanctionsDisciplinaryActionYes === true){
            this.formPage8.controls.personalSanctionsDisciplinaryActionNo.setValidators([Validators.required]);
            this.formPage8.controls.personalSanctionsDisciplinaryActionNo.reset();
        }
        else if(this.page8.personalSanctionsDisciplinaryActionYes === false){
            this.formPage8.controls.personalSanctionsDisciplinaryActionNo.clearValidators();
            this.formPage8.controls.personalSanctionsDisciplinaryActionNo.setErrors(null);
            this.formPage8.controls.personalSanctionsDisciplinaryActionNo.reset();
        }
    }

    change10(){
        if(this.page8.personalSanctionsCensuredOrExcludeOrSuspendActionYes === true){
            this.formPage8.controls.personalSanctionsCensuredOrExcludeOrSuspendActionNo.setValidators([Validators.required]);
            this.formPage8.controls.personalSanctionsCensuredOrExcludeOrSuspendActionNo.reset();
        }
        else if(this.page8.personalSanctionsCensuredOrExcludeOrSuspendActionYes === false){
            this.formPage8.controls.personalSanctionsCensuredOrExcludeOrSuspendActionNo.clearValidators();
            this.formPage8.controls.personalSanctionsCensuredOrExcludeOrSuspendActionNo.setErrors(null);
            this.formPage8.controls.personalSanctionsCensuredOrExcludeOrSuspendActionNo.reset();
        }
    }
    change11(){
        if(this.page8.personalSanctionsMedicareMedicaidChampusYes === true){
            this.formPage8.controls.personalSanctionsMedicareMedicaidChampusNo.setValidators([Validators.required]);
            this.formPage8.controls.personalSanctionsMedicareMedicaidChampusNo.reset();
        }
        else if(this.page8.personalSanctionsMedicareMedicaidChampusYes === false){
            this.formPage8.controls.personalSanctionsMedicareMedicaidChampusNo.clearValidators();
            this.formPage8.controls.personalSanctionsMedicareMedicaidChampusNo.setErrors(null);
            this.formPage8.controls.personalSanctionsMedicareMedicaidChampusNo.reset();
        }
    }
    change12(){
        if(this.page8.personalSanctionsDeniedMembershipSubjectYes === true){
            this.formPage8.controls.personalSanctionsDeniedMembershipSubjectNo.setValidators([Validators.required]);
            this.formPage8.controls.personalSanctionsDeniedMembershipSubjectNo.reset();
        }
        else if(this.page8.personalSanctionsDeniedMembershipSubjectYes === false){
            this.formPage8.controls.personalSanctionsDeniedMembershipSubjectNo.clearValidators();
            this.formPage8.controls.personalSanctionsDeniedMembershipSubjectNo.setErrors(null);
            this.formPage8.controls.personalSanctionsDeniedMembershipSubjectNo.reset();
        }
    }
    change13(){
        if(this.page8.personalSanctionsWithdrawnAnApplicationYes === true){
            this.formPage8.controls.personalSanctionsWithdrawnAnApplicationNo.setValidators([Validators.required]);
            this.formPage8.controls.personalSanctionsWithdrawnAnApplicationNo.reset();
        }
        else if(this.page8.personalSanctionsWithdrawnAnApplicationYes === false){
            this.formPage8.controls.personalSanctionsWithdrawnAnApplicationNo.clearValidators();
            this.formPage8.controls.personalSanctionsWithdrawnAnApplicationNo.setErrors(null);
            this.formPage8.controls.personalSanctionsWithdrawnAnApplicationNo.reset();
        }
    }

    change14(){
        if(this.page8.personalSanctionsChargedOrConvictedForACrimeYes === true){
            this.formPage8.controls.personalSanctionsChargedOrConvictedForACrimeNo.setValidators([Validators.required]);
            this.formPage8.controls.personalSanctionsChargedOrConvictedForACrimeNo.reset();
        }
        else if(this.page8.personalSanctionsChargedOrConvictedForACrimeYes === false){
            this.formPage8.controls.personalSanctionsChargedOrConvictedForACrimeNo.clearValidators();
            this.formPage8.controls.personalSanctionsChargedOrConvictedForACrimeNo.setErrors(null);
            this.formPage8.controls.personalSanctionsChargedOrConvictedForACrimeNo.reset();
        }
    }
    change15(){
        if(this.page8.personalSanctionsSubjectOfCivilOrCriminalYes === true){
            this.formPage8.controls.personalSanctionsSubjectOfCivilOrCriminalNo.setValidators([Validators.required]);
            this.formPage8.controls.personalSanctionsSubjectOfCivilOrCriminalNo.reset();
        }
        else if(this.page8.personalSanctionsSubjectOfCivilOrCriminalYes === false){
            this.formPage8.controls.personalSanctionsSubjectOfCivilOrCriminalNo.clearValidators();
            this.formPage8.controls.personalSanctionsSubjectOfCivilOrCriminalNo.setErrors(null);
            this.formPage8.controls.personalSanctionsSubjectOfCivilOrCriminalNo.reset();
        }
    }
    change1b(){
        if(this.page8.personalSanctionsHaveMedicalConditionYes === true){
            this.formPage8.controls.personalSanctionsHaveMedicalConditionNo.setValidators([Validators.required]);
            this.formPage8.controls.personalSanctionsHaveMedicalConditionNo.reset();
        }
        else if(this.page8.personalSanctionsHaveMedicalConditionYes === false){
            this.formPage8.controls.personalSanctionsHaveMedicalConditionNo.clearValidators();
            this.formPage8.controls.personalSanctionsHaveMedicalConditionNo.setErrors(null);
            this.formPage8.controls.personalSanctionsHaveMedicalConditionNo.reset();
        }
    }
    change2b(){
        if(this.page8.personalSanctionsUnableToPerformEssentialYes === true){
            this.formPage8.controls.personalSanctionsUnableToPerformEssentialNo.setValidators([Validators.required]);
            this.formPage8.controls.personalSanctionsUnableToPerformEssentialNo.reset();
        }
        else if(this.page8.personalSanctionsUnableToPerformEssentialYes === false){
            this.formPage8.controls.personalSanctionsUnableToPerformEssentialNo.clearValidators();
            this.formPage8.controls.personalSanctionsUnableToPerformEssentialNo.setErrors(null);
            this.formPage8.controls.personalSanctionsUnableToPerformEssentialNo.reset();
        }
    }
    change1c(){
        if(this.page8.personalSanctionsAnyReligiousYesRestrictionYes === true){
            this.formPage8.controls.personalSanctionsAnyReligiousYesRestrictionNo.setValidators([Validators.required]);
            this.formPage8.controls.personalSanctionsAnyReligiousYesRestrictionNo.reset();
        }
        else if(this.page8.personalSanctionsAnyReligiousYesRestrictionYes === false){
            this.formPage8.controls.personalSanctionsAnyReligiousYesRestrictionNo.clearValidators();
            this.formPage8.controls.personalSanctionsAnyReligiousYesRestrictionNo.setErrors(null);
            this.formPage8.controls.personalSanctionsAnyReligiousYesRestrictionNo.reset();
        }
    }


    savePage8() {
        this.isSaving = true;
        this.saveForPage8 = true;

        if (!isNullOrUndefined(this.page8.id)) {
            this.subscribeToSaveResponsePage8(
                this.formService.updatePage8(this.page8, this.currentUserForm.id));
        } else {
            this.subscribeToSaveResponsePage8(
                this.formService.createPage8(this.page8, this.currentUserForm.id));
        }

    }

    private subscribeToSaveResponsePage8(result: Observable<Page8>) {
        result.subscribe((res: Page8) =>
            this.onSaveSuccessPage8(res), (res: Response) => {
            this.onSaveErrorPage8();
            if (res.status === 400) {
                this.duplicatedName = true;
            }
            this.onSaveErrorPage8()
        });
    }

    private onSaveErrorPage8() {
        this.isSaving = false;
    }

    private onSaveSuccessPage8(result: Page8) {
        // // setting the created page in the parent form
        // this.credentialApplicationFormService.getCurrentForm().subscribe(form => {
        //     form.credentialingAppPage8 = result;
        //     this.page8 = result;
        //     this.credentialApplicationFormService.update(form).subscribe();
        // })

        this.isSaving = false;
        this.toastrService.success('This form was edited.', 'Successfully saved !');
        // setting Id of the create page8 to current page8
        this.page8.id = result.id;
    }


    //.............START PAGE 9

    changeEngaged(){
        if(this.page9.personalSanctionsCurrentlyEngagedYes === true){
            this.formPage9.controls.personalSanctionsCurrentlyEngagedNo.setValidators([Validators.required]);
            this.formPage9.controls.personalSanctionsCurrentlyEngagedNo.reset();
        }
        else if(this.page9.personalSanctionsCurrentlyEngagedYes === false){
            this.formPage9.controls.personalSanctionsCurrentlyEngagedNo.clearValidators();
            this.formPage9.controls.personalSanctionsCurrentlyEngagedNo.setErrors(null);
            this.formPage9.controls.personalSanctionsCurrentlyEngagedNo.reset();
        }
    }

    changeSubstances(){
        if(this.page9.personalSanctionsChemicalSubstancesYes === true){
            this.formPage9.controls.personalSanctionsChemicalSubstancesNo.setValidators([Validators.required]);
            this.formPage9.controls.personalSanctionsChemicalSubstancesNo.reset();
        }
        else if(this.page9.personalSanctionsChemicalSubstancesYes === false){
            this.formPage9.controls.personalSanctionsChemicalSubstancesNo.clearValidators();
            this.formPage9.controls.personalSanctionsChemicalSubstancesNo.setErrors(null);
            this.formPage9.controls.personalSanctionsChemicalSubstancesNo.reset();
        }
    }

    savePage9() {
        this.isSaving = true;
        this.saveForPage9 = true;

        if (!isNullOrUndefined(this.page9.id)) {
            this.subscribeToSaveResponsePage9(
                this.formService.updatePage9(this.page9, this.currentUserForm.id));
        } else {
            this.subscribeToSaveResponsePage9(
                this.formService.createPage9(this.page9, this.currentUserForm.id));
        }

    }

    private subscribeToSaveResponsePage9(result: Observable<Page9>) {
        result.subscribe((res: Page9) =>
            this.onSaveSuccessPage9(res), (res: Response) => {
            this.onSaveErrorPage9();
            if (res.status === 400) {
                this.duplicatedName = true;
            }
            this.onSaveErrorPage9()
        });
    }

    private onSaveErrorPage9() {
        this.isSaving = false;
    }

    private onSaveSuccessPage9(result: Page9) {

        // // setting the created page in the parent form
        // this.credentialApplicationFormService.getCurrentForm().subscribe(form => {
        //     form.credentialingAppPage9 = result;
        //     this.page9 = result;
        //     this.credentialApplicationFormService.update(form).subscribe();
        // })

        this.isSaving = false;
        this.toastrService.success('This form was edited.', 'Successfully saved !');
        // setting Id of the create page9 to current page9
        this.page9.id = result.id;
    }

    submit() {

        //   this.credentialApplicationFormService.getCurrentForm().subscribe(res => {
        this.spinner.show();
        this.credentialApplicationFormService.getFormByFormNewApproach(this.param1).subscribe( res => {

            let forms: any[] = [];

            if (res.credentialingAppPage9 == null)
                forms.push('Page9')
            if (res.credentialingAppPage8 == null)
                forms.push('Page8');
            if (res.credentialingAppPage7 == null)
                forms.push('Page7')
            if (res.credentialingAppPage6 == null)
                forms.push('Page6');
            if (res.credentialingAppPage5 == null)
                forms.push('Page5');
            if (res.credentialingAppPage4 == null)
                forms.push('Page4');
            if (res.credentialingAppPage3 == null)
                forms.push('Page3');
            if (res.credentialingAppPage2 == null)
                forms.push('Page2');
            if (res.credentialingAppPage1 == null)
                forms.push('Page1');
            if (forms.length == 0) {
                let employee: Employee = new Employee();
                employee.status = 'Pending';
                employee.id = this.employee.id;
         //      this.employeeService.updateSelfStatus(employee, this.param1).subscribe(res => {
                this.credentialApplicationFormService.submitCrdForm(employee, this.param1).subscribe(res => {
                    this.isSubmitted = true;
                    this.spinner.hide();
                    this.toastrService.success('The Credential-Form Submitted', 'Successfully Submitted !');

                }, rej => {
                    this.toastrService.error('The Credential-Form Submittion ERROR', 'Error Submitting !');
                    this.spinner.hide();
                })

            } else {
                this.spinner.hide();
                this.toastrService.error('First Save All The pages Then Submit', 'Error Submitting !');

            }

        }, reject => {
            // this.jump('Page1')
            this.toastrService.error('The Credential-Form Submittion ERROR', 'Error Submitting !');
            this.spinner.hide();

        })


    }

    // onKeyUpChangeEmail() {
    //     this.serverValidationError = false;
    // }

    saveEmployee() {

        // this.isSaving = true;
        if (this.employee.id !== undefined) {
            this.subscribeToSaveResponseEmployee(
                this.employeeService.update(this.employee));
        } else {
            this.subscribeToSaveResponseEmployee(
                this.employeeService.create(this.employee));
        }
    }


    private subscribeToSaveResponseEmployee(result: Observable<Employee>) {

        result.subscribe((res: Employee) =>
            this.onSaveSuccessEmployee(res), (res: Response) => {
            this.onSaveErrorEmployee();
            this.processError(res);

        })
    }

    private onSaveSuccessEmployee(result: Employee) {
        this.isSaving = false;
        this.serverValidationErrorName = false;
        this.toastrService.success('This employee was edited.', 'Successfully saved !');
    }

    private onSaveErrorEmployee() {
        this.isSaving = false;
    }


    onItemSelect(item: any) {
        console.log(item);
        console.log(this.selectedItems);
    }

    OnItemDeSelect(item: any) {
        console.log(item);
        console.log(this.selectedItems);
    }

    onSelectAll(items: any) {
        console.log(items);
    }

    onDeSelectAll(items: any) {
        console.log(items);
    }

    sort(sort, predicate) {
        let result = ['id,desc'];
        if (sort) {
            this.reverse = 'desc';
            result = [sort + ',' + predicate];
            return result;
        }
    }

    // loadFileImg(id) {
    //
    //     this.profileImg = 'assets/img/users/default-user.jpg';
    //
    //     this.employeeFileService.findImg(id).subscribe((file) => {
    //         this.employeeFileImg = file;
    //         if (file) {
    //             if (this.employeeFileImg.imgProfile === true) {
    //                 this.employeeFileService.getDownloadUrl(file.id).subscribe(res => {
    //                     this.profileImg = res.text();
    //                     // alert(' 66 thirdth this.profileImg ==' + this.profileImg);
    //
    //                     this.spinner.hide();
    //                 })
    //             }
    //         } else {
    //
    //             this.spinner.hide();
    //
    //         }
    //
    //     });
    // }
    //
    // private onSuccessDocument(data, headers) {
    //     this.documentCrs = data;
    // }
    //
    // private onSuccess(data, headers) {
    //     this.totalItems = headers.get('X-Total-Count');
    //     this.queryCount = this.totalItems;
    //     this.employeeFiles = data;
    //     this.spinner.hide();
    // }
    //
    // loadFiles(employeeId) {
    //     this.spinner.show();
    //     const itemName = this.file.itemName;
    //     const status = this.file.status;
    //     const type = this.file.fileContentType;
    //     const document = this.file.documentTitle;
    //     const expirationDate_from = this.dateUtils
    //         .convertLocalDateToServer(this.expirationDate_from);
    //
    //     const expirationDate_to = this.dateUtils
    //         .convertLocalDateToServer(this.expirationDate_to);
    //     this.employeeFileService.queryFind({
    //         page: this.page - 1,
    //         size: this.itemsPerPage,
    //         sort: this.sort(this.sortList, this.sortPredeict),
    //         employeeId,
    //         itemName, type, status, document, expirationDate_from, expirationDate_to
    //     }).subscribe(
    //         (res: ResponseWrapper) => this.onSuccess(res.json, res.headers)
    //         // (res: ResponseWrapper) => {
    //         //     this.employeeFiles = res.json;
    //         //
    //         //     // this.employeeFiles.forEach((file) => {
    //         //     // })
    //         //     this.employeeFiles.forEach((file) => {
    //         //         // alert(' file.imgProfile === true==' + file.imgProfile );
    //         //         // && file.fileType === 'image/png'
    //         //
    //         //     });
    //         //     this.spinner.hide();
    //         //
    //         // }
    //         ,
    //         (res: ResponseWrapper) => this.onError(res.json)
    //     );
    // }
    //
    //
    // addFile(id: any) {
    //     const modalRef = this.modalService.open(EmployeeFileDialogComponent, {
    //         windowClass: 'lg', backdrop: 'static',
    //         keyboard: false
    //     });
    //     modalRef.componentInstance.employeeId = id;
    //     modalRef.componentInstance.action = 'add';
    //
    //     modalRef.result.then(res => {
    //         if (res === 'cancel') {
    //             if (res === true) {
    //                 this.loadFiles(this.employee.id);
    //             }
    //         } else if (res === 'save') {
    //             this.loadFiles(this.employee.id);
    //         }
    //     }, rejected => {
    //         this.loadFiles(this.employee.id);
    //     });
    // }
    //
    // addProfileImg(id: any) {
    //     const modalRef = this.modalService.open(EmployeeFileDialogComponent, {
    //         windowClass: 'lg', backdrop: 'static',
    //         keyboard: false
    //     });
    //     modalRef.componentInstance.employeeId = id;
    //     modalRef.componentInstance.action = 'img';
    //     modalRef.componentInstance.employeeFileImgId = (this.employeeFileImg && this.employeeFileImg.id ? this.employeeFileImg.id : null);
    //
    //
    //     modalRef.result.then(res => {
    //         if (res === 'cancel') {
    //             if (res === true) {
    //                 this.loadFileImg(this.employee.id);
    //             }
    //         } else if (res === 'save') {
    //             this.loadFileImg(this.employee.id);
    //         }
    //     }, rejected => {
    //         this.loadFileImg(this.employee.id);
    //     });
    // }
    //
    // open(id) {
    //
    //     this.employeeFileService.getDownloadUrl(id).subscribe(res => {
    //         this.link = res.text();
    //         const tabWindowId = window.open('about:blank', '_blank');
    //         tabWindowId.location.href = this.link;
    //     })
    // }


    approveEmployee() {

        // this.isSaving = true;
        this.employee.status = 'Processing'
        if (this.employee.id !== undefined) {
            this.subscribeToSaveResponseEmployee(
                this.employeeService.update(this.employee));
        } else {
            this.subscribeToSaveResponseEmployee(
                this.employeeService.create(this.employee));
        }
    }

    rejectEmployee() {

        //    this.modalService.open()
        //     const modalRef = this.modalService.open(RejectEmailPopUpComponent  , {windowClass: 'lg', backdrop: 'static'});
        //
        //     modalRef.componentInstance.employee = this.employee;
        //
        //
        //     // this.employee.status = 'Passive'
        //
        //     modalRef.result.then(res => {
        //         console.log(res + ' the result of popup');
        //         this.load(this.param1);
        //     }, rejected => {
        //         console.log(' rejected: ' + rejected.toString());
        //         this.load(this.param1);
        //     });

        // this.isSaving = true;
        // if (this.employee.id !== undefined) {
        //     this.subscribeToSaveResponseEmployee(
        //         this.employeeService.update(this.employee));
        // } else {
        //     this.subscribeToSaveResponseEmployee(
        //         this.employeeService.create(this.employee));
        // }
    }


    // approveFile(data: any, data2: any) {
    //     const modalRef = this.modalService.open(EmployeeFileDialogComponent, {windowClass: 'lg', backdrop: 'static'});
    //     modalRef.componentInstance.fileId = data;
    //     modalRef.componentInstance.employeeId = data2;
    //     modalRef.componentInstance.action = 'approve';
    //
    //     modalRef.result.then(res => {
    //         console.log(res + ' the result of popup');
    //         this.loadFiles(data2)
    //     }, rejected => {
    //         console.log(' rejected: ' + rejected.toString());
    //         this.loadFiles(data2)
    //     });
    //
    //
    // }
    //
    // editFile(data: any, data2: any, data3: any) {
    //     // alert(' data3 =='  + data3);
    //     const modalRef = this.modalService.open(EmployeeFileDialogComponent, {windowClass: 'lg', backdrop: 'static'});
    //     modalRef.componentInstance.fileId = data;
    //     modalRef.componentInstance.employeeId = data2;
    //     modalRef.componentInstance.docId = data3;
    //     modalRef.componentInstance.action = 'edit';
    //
    //     modalRef.result.then(res => {
    //         console.log(res + ' the result of popup');
    //         this.loadFiles(data2)
    //     }, rejected => {
    //         console.log(' rejected: ' + rejected.toString());
    //         this.loadFiles(data2)
    //     });
    //
    //
    // }
    //
    // deleteFileProfile(val, data: any) {
    //     const modalRef = this.modalService.open(EmployeeFileDialogComponent, {windowClass: 'lg', backdrop: 'static'});
    //     modalRef.componentInstance.fileId = val;
    //     modalRef.componentInstance.employeeId = data;
    //     modalRef.componentInstance.action = 'delete';
    //     modalRef.result.then(res => {
    //         console.log(res + ' the result of popup');
    //         this.loadFiles(data)
    //     }, rejected => {
    //         console.log(' rejected: ' + rejected.toString());
    //         this.loadFiles(data)
    //     });
    //
    // }
    //
    transition() {
        //    this.loadFiles(this.employee.id);
    }
    //
    // refresh() {
    //     this.loadFiles(this.employee.id);
    //     this.loadFileImg(this.employee.id);
    // }

    loadPage2(param) {
        if (this.sortList === param) {
            if (this.sortPredeict === 'asc') {
                this.sortPredeict = 'desc';
            } else {
                this.sortPredeict = 'asc';
            }
        } else {
            this.sortPredeict = 'asc';
        }
        this.sortList = param;
        this.transition();
    }


    back() {
        window.history.back();
    }


    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }


    keyUpInput() {

        this.page = 1;
        this.transition();
    }

    parse(value: any) {
        if (value) {
            const dateParts = value.trim().split('-');
            return (dateParts.length === 3 ? dateParts[1] + '/' + dateParts[2] + '/' + dateParts[0] : '-');
        }
        return '-';
    }

    check(value: any, value2) {
        // alert(value2 + '---' + value);
        if (value) {
            return value;
        }
        return '-';
    }


    onKeyUpChangeEmail(val) {
        this.serverValidationError = false;
        if (val) {
            this.employee.emailAddress = val;
        }
        this.subscribeToSaveResponse3(
            this.employeeService.createVE(this.employee));
    }

    onKeyUpChangeLastName(val) {
        this.serverValidationErrorName = false;
        if (val) {
            this.employee.lastName = val;
        }
        this.subscribeToSaveResponse2(
            this.employeeService.createV(this.employee));
    }

    onKeyUpChangeFirstName(val) {
        this.serverValidationErrorName = false;
        if (val) {
            this.employee.firstName = val;
        }
        this.subscribeToSaveResponse2(
            this.employeeService.createV(this.employee));
    }


    private subscribeToSaveResponse2(result: Observable<Employee>) {

        result.subscribe((res: Employee) =>
                this.onSaveSuccess2(res)
            , (response) => this.processError(response))

    }

    private processError(response) {
        if (response.status === 400 && response.json().type === EMAIL_ALREADY_USED_TYPE) {
            this.serverValidationError = true;
        } else if (response.status === 400 && response.json().type === EMPLOYEE_NAME_ALREADY_USED_TYPE) {
            this.serverValidationErrorName = true;
        }
    }

    private subscribeToSaveResponse3(result: Observable<Employee>) {

        result.subscribe((res: Employee) =>
                this.onSaveSuccess2(res)
            , (response) => this.processError(response)

        )
    }

    private onSaveSuccess2(result: Employee) {
        this.isSaving = false;
        this.toastrService.success('This form was edited.', 'Successfully saved !');

    }

    private onSaveError() {
        this.isSaving = false;
    }

    activeList(e){
        var elems = document.querySelectorAll(".active");
        [].forEach.call(elems, function(el) {
            el.classList.remove("active");
        });
        e.target.className = "active";
    }


}
