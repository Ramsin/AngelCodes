import { Injectable } from '@angular/core';
import {BaseRequestOptions, Http, RequestOptions, Response, URLSearchParams} from '@angular/http';
import { SERVER_API_URL } from '../../app.constants';
// import {Photo} from '../../shared/model/photo.model';
import {map} from 'rxjs/operators';
// type EntityResponseType = HttpResponse<Photo>;
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
// import {Category} from '../../shared/model/category.model';
import {Patient} from '../../shared/model/patient.model';
import {ResponseWrapper} from '../../shared/model/response-wrapper.model';
import {CategoryPhoto} from '../../shared/model/category.model';
import {createRequestOption} from '../../shared/model/request-util';
import {Photo} from '../../shared/model/photo.model';
type EntityResponseType = HttpResponse<Photo>;


@Injectable()
export class DetailPatientTestService {
    private resourceUrl = SERVER_API_URL + 'api/patients';
    private resourceCategory = SERVER_API_URL + 'api/image/category';
    private resourceUrlSavePhoto = SERVER_API_URL + 'api/image/photo';
    private resourceUrlCatId = SERVER_API_URL + 'api/image/patient/photo-category';
    private resourceUrlPatientPhoto = SERVER_API_URL + 'api/image/patient/photo';
    private resourceUrlDeletePhoto = SERVER_API_URL + 'api/image/photo';
    private resourceUrlSurgery = SERVER_API_URL + 'api/image/patient/photo-category-surgery';
    private resourceUrlSurgeryId = SERVER_API_URL + 'api/image/patient/photo-operation';




    constructor(private http: Http ) { }

    find(id: number): Observable<Patient> {
        //alert(id);
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            // alert('=====================>>>>' + JSON.stringify(res.json()))
            return this.convertItemFromServer(jsonResponse);
        });
    }

    // findPhotoByCatId1(id: number , catId : number): Observable<Photo> {
    //     //alert(id);
    //     return this.http.get(`${this.resourceUrlCatId}?patient_id=${id}&cat_id=${catId}`).map((res: Response) => {
    //         const jsonResponse = res.json();
    //         // alert('=====================>>>>' + JSON.stringify(res.json()))
    //         return this.convertItemFromServerPhotoPatient(jsonResponse);
    //     });
    // }

    findPhotoByCatId( id: number , catId : number): Observable<ResponseWrapper> {
        // alert('service patient photo')
        // const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrlCatId}?patient_id=${id}&cat_id=${catId}`)
            .map((res: Response) => this.convertResponsePhoto(res));
    }

    create(photo: Photo): Observable<Photo> {
        const copy = this.convert(photo);
        return this.http.post(this.resourceUrlSavePhoto, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServerPhoto(jsonResponse);
        });
    }
    createCatID(photo: Photo , patientId : any , catId: any ): Observable<Photo> {
        const copy = this.convert(photo);
        let option: RequestOptions;
        const options: BaseRequestOptions = new BaseRequestOptions();
        const params: URLSearchParams = new URLSearchParams();
        params.set('patient_id' , patientId ) ,
            params.set('cat_id' , catId ) ,
            options.params = params;
        return this.http.post(this.resourceUrlCatId , copy , options).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServerPhoto(jsonResponse);
        });
    }


    createSurgeryID(photo: Photo , patientId : any , catId: any ,surgery_id: any ): Observable<any> {
        const copy = this.convert(photo);
        let option: RequestOptions;
        const options: BaseRequestOptions = new BaseRequestOptions();
        const params: URLSearchParams = new URLSearchParams();
        params.set('patient_id' , patientId ) ,
            params.set('cat_id' , catId ) ,
            params.set('surgery_id' , surgery_id ) ,
            options.params = params;
        return this.http.post(this.resourceUrlSurgery , copy , options).map((res: Response) => {
            //const jsonResponse = res.json();
            //return this.convertItemFromServerPhoto(jsonResponse);
        });
    }




    private convert(Photo: Photo): Photo {
        const copy: Photo = Object.assign({}, Photo);
        return copy;
    }

    private convertItemFromServerPhoto(json: any): Photo {
        const entity: Photo = Object.assign(new Photo(), json);
        return entity;
    }

    private convertItemFromServer(json: any): Patient {
        const entity: Patient = Object.assign(new Patient(), json);
        // entity.creationDate = this.dateUtils
        //     .convertLocalDateFromServer(json.creationDate);
        return entity;
    }



    findAllCategory(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCategory, options)
            .map((res: Response) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServerCategory(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    private convertItemFromServerCategory(json: any): CategoryPhoto {
        const entity: CategoryPhoto = Object.assign(new CategoryPhoto(), json);
        // entity.creationDate = this.dateUtils
        //     .convertLocalDateFromServer(json.creationDate);
        return entity;
    }



    deletePhoto(id: number): Observable<Response> {
       // alert('service id photo======>>>>>' + id)
        return this.http.delete(`${this.resourceUrlDeletePhoto}/${id}` );
    }



    // findPhotoPatient(id: number): Observable<Photo> {
    //     return this.http.get(`${this.resourceUrlPatientPhoto}?patient_id=${id}`).map((res: Response) => {
    //         const jsonResponse = res.json();
    //         // alert('photoooooooooooooooooooooooooooo=====================>>>>' + JSON.stringify(res.json()))
    //         return this.convertItemFromServerPhoto(jsonResponse);
    //     });
    // }




    findPhotoPatient( id?:number ): Observable<ResponseWrapper> {
       // alert('service patient photo')
        // const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrlPatientPhoto}?patient_id=${id}`)
            .map((res: Response) => this.convertResponsePhoto(res));
    }

    findPhotoBySurgery( patientId?:number , surgeryId?:number): Observable<ResponseWrapper> {
        // alert('service patient surgery Id' + surgeryId)
        // const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrlSurgeryId}?patient_id=${patientId}&surgery_id=${surgeryId}&size=50&page=0`)
            .map((res: Response) => this.convertResponsePhotoSurgeryIddd(res));
    }

    private convertResponsePhotoSurgeryIddd(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        //alert('photoooooooooooooooooooooooooooo Service surgery Iddddddddddddd=====================>>>>' + JSON.stringify(res.json()))
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServerPhotoPatient(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }






    findPhotoBySurgeryAndCat( patientId?:number , catId?:number , surgeryId?:number): Observable<ResponseWrapper> {
        // alert('service patient photo')
        // const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrlSurgery}?patient_id=${patientId}&cat_id=${catId}&surgeryId=${surgeryId}`)
            .map((res: Response) => this.convertResponsePhotoSurgery(res));
    }





    private convertResponsePhoto(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
       //alert('photoooooooooooooooooooooooooooo Service =====================>>>>' + JSON.stringify(res.json()))
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServerPhotoPatient(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    private convertResponsePhotoSurgery(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
       //alert('photoooooooooooooooooooooooooooo Service surgery =====================>>>>' + JSON.stringify(res.json()))
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServerPhotoPatient(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    private convertItemFromServerPhotoPatient(json: any): Photo {
        const entity: Photo = Object.assign(new Photo(), json);
        // entity.creationDate = this.dateUtils
        //     .convertLocalDateFromServer(json.creationDate);
        return entity;
    }

}
