import {Component, OnInit, OnDestroy, Input} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';
import {Patient} from "../../shared/model/patient.model";
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {DetailPatientTestComponent} from '../detail-patient-test/detail-patient-test.component';
import {Photo} from '../../shared/model/photo.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DetailPatientTestService} from './detail-patient-test.service';
import {Observable} from 'rxjs';
import {Response} from '@angular/http';

@Component({
    selector: 'app-delete-image-popup',
    templateUrl: './delete-image-popup.component.html',
    styleUrls: ['./delete-image-popup.component.css'],
})
export class DeleteImagePopupComponent implements OnInit {

    @Input() photoDelete  ;



    constructor(
        public activeModal: NgbActiveModal, protected activatedRoute: ActivatedRoute , private detailPatientTestService: DetailPatientTestService ,public modalService: NgbModal , private router: Router ,private fb: FormBuilder) {

    }


    ngOnInit() {
       // alert('id cattttttttttttttt=======>>>' + this.photoDelete )

    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        // alert("id=" + id);
        this.detailPatientTestService.deletePhoto(id).subscribe((response) => {
            this.activeModal.dismiss(true);
        });
    }

}
