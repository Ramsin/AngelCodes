import {Component, OnInit, OnDestroy, Input} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';
import {Patient} from "../../shared/model/patient.model";
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {DetailPatientTestComponent} from '../detail-patient-test/detail-patient-test.component';
import {Photo} from '../../shared/model/photo.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DetailPatientTestService} from './detail-patient-test.service';
import {Observable} from 'rxjs';
import {Response} from '@angular/http';
import {ResponseWrapper} from '../../shared/model/response-wrapper.model';
import {Position} from '../../shared/model/position.model';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
    selector: 'app-upload-image-popup',
    templateUrl: './upload-image-popup.component.html',
    styleUrls: ['./upload-image-popup.component.css'],
})
export class UploadImagePopupComponent implements OnInit {
    @Input() catId  ;
    @Input() PatientId  ;
    @Input() surgery_id  ;

    patient: Patient = new Patient();
    patientList: Patient[] = [];
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    //.......from  upload photo them.....................
    photo: Photo = new Photo();
    formPhoto: Photo[];
    isSaving: boolean;
    confirmSaveMsgForm: boolean = false;
    errorSaveMsgForm: boolean;
    patient_id: any ;
    cat_id : any ;
    photoPatient: any ;
    photoCat: any ;
    surgeryId: any ;
    showPhotoByCat: any ;
    editForm: FormGroup;

    private imageSrc = '';

    constructor(
        private spinner: NgxSpinnerService  ,public activeModal: NgbActiveModal, protected activatedRoute: ActivatedRoute , private detailPatientTestService: DetailPatientTestService ,public modalService: NgbModal , private router: Router ,private fb: FormBuilder) {
        this.editForm = this.fb.group({
            'id': [''],
            'title': ['', [Validators.required]],
            'image': ['' , []],
            'imageContentType': [''],
            'amazonId': [''],
            'amazonLink': [''],
              // 'category': [''],
        });
    }


    ngOnInit() {
        //alert('id cattttttttttttttt=======>>>' + this.catId )
      //  alert('id patient=======>>>' + this.PatientId )
        // this.activatedRoute.data.subscribe(({ photo }) => {
        //     this.updateForm(photo);
        //     this.photo = photo;
        // });
        //alert('surgery Idddddd======>>>>' + this.surgery_id)
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }
    yesNoCheck1(){

       document.getElementById('dateCheck').style.display = 'block';
       document.getElementById('titleCheck').style.display = 'none';

    }
    yesNoCheck2(){
       document.getElementById('titleCheck').style.display = 'block';
       document.getElementById('dateCheck').style.display = 'none';

    }
    //............start  upload.............................................
    selectedFile: File = null;
    private res2: string;


    handleInputChange(e) {
        console.log(e);
        console.log(e.target.files[0]);
       // alert('hiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiibabe.........' + e.target.files[0].name)
        const file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
       // alert('================================>> ' + e.target.files[0].name.split('.').pop().toLowerCase());
        const imageContentType = e.target.files[0].name.split('.').pop().toLowerCase();
        const pattern = /image-*/;

        this.photo.imageContentType = 'image/' + imageContentType ;
        //alert('===============================>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>' + this.photo.imageContentType);

        //this.photo.image = this.getBase64(file);
        //alert('===============================>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>' + this.photo.imageContentType);

        const reader = new FileReader();
        if (!file.type.match(pattern)) {
           // alert('invalid format');
            return;
        }
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
    }
    getBase64(file) {
        //alert('sraaaaaaaaaaaaaaaa=============>>.' )

        const reader = new FileReader();
        reader.readAsDataURL(file);
        // alert('sraaaaaaaaaaaaaaaa=============>>.' +  reader.readAsDataURL(file)),
        reader.onload = function () {
            console.log(reader.result);
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }
    _handleReaderLoaded(e) {
        const reader = e.target;
        this.selectedFile = <File>reader.result;
        // alert('--- >>>> ' +  this.selectedFile.name);
        this.imageSrc = reader.result;
        // var res = this.imageSrc.substring(0,this.imageSrc.indexOf(','));
        this.res2 = this.imageSrc.substring(this.imageSrc.indexOf(',') + 1);
        //alert('hiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii vfv f' + this.res2);

        console.log(this.imageSrc);
    }

    //.................................end upload .........................

    updateForm(photo: Photo) {
        this.editForm.patchValue({
            id: photo.id,
            title: photo.title,
            image: photo.image,
            imageContentType: photo.imageContentType,
            amazonLink: photo.amazonLink,
            // category: photo.category,
            amazonId: photo.amazonId,
        });
    }


    // save() {
    //     this.isSaving = true;
    //     const photo = this.createFromForm();
    //     this.subscribeToSaveResponse(this.detailPatientTestService.create( photo ));
    // }


    // saveByCatId() {
    //      alert('patient Id ===== >>>> ' +   this.catId );
    //      alert('patient Id ===== >>>> ' +   this.PatientId);
    //     this.isSaving = true;
    //     const photo = this.createFromForm();
    //     this.detailPatientTestService.createCatID( photo , this.PatientId  , this.catId).subscribe();
    //     this.activeModal.dismiss('cancel');
    // }

    // saveBySurgeryId() {
    //     //alert('patient Id ===== >>>> ' +   this.catId );
    //     //alert('patient Id ===== >>>> ' +   this.PatientId);
    //     this.isSaving = true;
    //     const photo = this.createFromForm();
    //     this.detailPatientTestService.createSurgeryID( photo , this.PatientId  , this.catId , this.surgery_id).subscribe();
    //      //this.activeModal.dismiss('cancel');
    // }



    //..................start new save.............
    saveBySurgeryId() {
        this.isSaving = true;
        this.spinner.show();
        const photo = this.createFromForm();
        //if (this.patient.id !== undefined) {
            this.subscribeToSaveResponse(this.detailPatientTestService.createSurgeryID( photo , this.PatientId  , this.catId , this.surgery_id));
        //}
    }

    private subscribeToSaveResponse(result: Observable<Photo>) {
        result.subscribe((res: Photo) =>
            this.onSaveSuccess(res), (res: Response) => {
            this.onSaveError();
        });
    }

    private onSaveSuccess(result: Photo) {
        // this.eventManager.broadcast({ name: 'clinicListModification', content: 'OK'});
        //alert('success  ggg')
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }
    protected onSaveError() {
        this.isSaving = false;
        this.confirmSaveMsgForm = false;
    }
    //..................end new save.............
















    private createFromForm(): Photo {
        const entity = {
            ...new Photo(),
            id: this.editForm.get(['id']).value,
            title: this.editForm.get(['title']).value,
            amazonLink: this.editForm.get(['amazonLink']).value,
            amazonId: this.editForm.get(['amazonId']).value,
            // category: /.catId,
            image: this.res2,
            imageContentType: this.photo.imageContentType,
        };
        return entity;
    }
    // protected subscribeToSaveResponse(result: Observable<Photo>) {
    //     result.subscribe((res: Photo) => this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    // }


    // private onSaveSuccess(res: Photo) {
    //     // this.eventManager.broadcast({ name: 'clinicListModification', content: 'OK'});
    //     this.photo= res;
    //     this.isSaving = false;
    //     this.confirmSaveMsgForm = true;
    //     this.previousState();
    //
    // }


    previousState() {
        window.history.back();
    }
}
