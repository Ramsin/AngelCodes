import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import * as $ from 'jquery';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import {Subscription} from 'rxjs';
import {ResponseWrapper} from '../../shared/model/response-wrapper.model';
import {DetailPatientTestService} from './detail-patient-test.service';
import {DeleteImagePopupComponent} from './delete-image-popup.component';
import {UploadImagePopupComponent} from './upload-image-popup.component';
import {NgxSpinnerService} from 'ngx-spinner';



@Component({
    selector: 'app-detail-patient-photo',
    templateUrl: './detail-patient-photo.component.html',
    styleUrls: ['./detail-patient-photo.component.scss']
})
export class DetailPatientPhotoComponent implements OnInit {
    closeResult: string;
    public imageSrc = 'https://image.freepik.com/free-icon/upload-arrow_318-26670.jpg';
    routeSub: Subscription;
    param1: any;
    patient: any;
    patientDetail: any;
    photoSurgery: any;
    photoPatient: any;
    categories: any;
    photoSurgeryAndCat: any;
    selectedItem: any;
    private subscription: Subscription;
    getAllPhotoPatient : boolean = true;
    getAllPhotoBySurgery : boolean = false;
    getAllPhotoBySurgeryAndCat : boolean = false;
    showSubCategory : boolean = false;
    showCaretRight : boolean = true;
    showCaretDown : boolean = false;
    photoEmptyCat : boolean = false;
    photoEmptyAllImage : boolean = true;
    showUploadButton : boolean = false;
    photoEmptySurgeryImage : boolean = false;
    general_cat_id :any = -1;

    // public patients = {
    //     'Liposuction':[{id: 12, url: '../../../assets/images/lipo1.jpg'} ,{id: 12, url: '../../../assets/images/lipo1.jpg'} ] ,
    //     'Eyes-surgery': [{id: 13, url: '../../../assets/images/lipo1.jpg'},{id: 12, url: '../../../assets/images/lipo1.jpg'} ],
    //     'Microneedling': [{id: 14, url: '../../../assets/images/lipo1.jpg'} , {id: 12, url: '../../../assets/images/lipo1.jpg'} ] ,
    // };


    constructor(private spinner: NgxSpinnerService, private modalService: NgbModal , configCarousel: NgbCarouselConfig ,private route: ActivatedRoute ,private detailPatientTestService: DetailPatientTestService ) {
        // config.backdrop = 'static';
        // config.keyboard = false;


        configCarousel.interval = 10000;
        configCarousel.wrap = false;
        configCarousel.keyboard = false;

        // configCarousel.showNavigationArrows = true;
        // configCarousel.showNavigationIndicators = true;
    }

    ngOnInit() {

        this.loadAllCategory();

        //........start api...........................
        this.routeSub = this.route.params.subscribe((params) => {
            this.param1 = params['id'];
        });

        this.subscription = this.route.params.subscribe((params) => {
            this.load(this.param1);
        });

       // alert('patient photo test======>>>>>>' + this.param1)

        this.loadAllPhotoPatientId( this.param1);


        //.....start carousel...............
        var selector = '.caretRight td';

        $(selector).on('click', function(){
            $(selector).removeClass('active');
            $(this).addClass('active');
        });


        //start .........tab 2
        $('.tabMain').click(function(event) {
            $('.chosen').removeClass('chosen');
            $(this).addClass('chosen');
            const index = $('.tabMain').index($(this));
            $('.shown').removeClass('shown');
            $('.pageMain').eq(index).addClass('shown');
        });


        //..................................start copmare image.....................
        // drag and drop component........................
        // Call & init
        $(document).ready(function() {
            var each = $('.ba-slider').each(function() {
                var cur = $(this);
                // Adjust the slider
                var width = cur.width() + 'px';
                cur.find('.resize img').css('width', width);
                // Bind dragging events
                drags(cur.find('.handle'), cur.find('.resize'), cur);
            });
        });

// Update sliders on resize.
// Because we all do this: i.imgur.com/YkbaV.gif
        $(window).resize(function() {
            $('.ba-slider').each(function() {
                var cur = $(this);
                var width = cur.width() + 'px';
                cur.find('.resize img').css('width', width);
            });
        });

        function drags(dragElement, resizeElement, container) {

            // Initialize the dragging event on mousedown.
            dragElement.on('mousedown touchstart', function(e) {

                dragElement.addClass('draggable');
                resizeElement.addClass('resizable');

                // Check if it's a mouse or touch event and pass along the correct value
                var startX = (e.pageX) ? e.pageX : e.originalEvent.touches[0].pageX;

                // Get the initial position
                var dragWidth = dragElement.outerWidth(),
                    posX = dragElement.offset().left + dragWidth - startX,
                    containerOffset = container.offset().left,
                    containerWidth = container.outerWidth();

                // Set limits
                var minLeft;
                minLeft = containerOffset + 10;
                var maxLeft;
                maxLeft = containerOffset + containerWidth - dragWidth - 10;

                // Calculate the dragging distance on mousemove.
                dragElement.parents().on('mousemove touchmove', function(e) {

                    // Check if it's a mouse or touch event and pass along the correct value
                    var moveX = (e.pageX) ? e.pageX : e.originalEvent.touches[0].pageX;

                    var leftValue;
                    leftValue = moveX + posX - dragWidth;

                    // Prevent going off limits
                    if ( leftValue < minLeft) {
                        leftValue = minLeft;
                    } else if (leftValue > maxLeft) {
                        leftValue = maxLeft;
                    }

                    // Translate the handle's left value to masked divs width.
                    var widthValue;
                    widthValue = (leftValue + dragWidth / 2 - containerOffset) * 100 / containerWidth + '%';

                    // Set the new values for the slider and the handle.
                    // Bind mouseup events to stop dragging.
                    $('.draggable').css('left', widthValue).on('mouseup touchend touchcancel', function() {
                        $(this).removeClass('draggable');
                        resizeElement.removeClass('resizable');
                    });
                    $('.resizable').css('width', widthValue);
                }).on('mouseup touchend touchcancel', function() {
                    dragElement.removeClass('draggable');
                    resizeElement.removeClass('resizable');
                });
                e.preventDefault();
            }).on('mouseup touchend touchcancel', function(e) {
                dragElement.removeClass('draggable');
                resizeElement.removeClass('resizable');
            });
        }

        //.......................filter Images......................
    }




    openFolder(content) {
        this.modalService.open(content);

    }
    openFolderSearch(contentSearch) {
        this.modalService.open(contentSearch);

    }
    //...............................start compare 2 image...................................................
    onDropHandler(object) {
        console.log('event' + JSON.stringify(object));
        this.imageSrc = object.event.target.result;
    }

    allowDrop(ev) {
        ev.preventDefault();
    }

    drag(ev) {
        // alert('drag=====>' + ev.target.id)
        ev.dataTransfer.setData('text', ev.target.id);
    }

    drop(ev) {
        ev.preventDefault();
        const data = ev.dataTransfer.getData('text');
        const x = ev.target.appendChild(document.getElementById(data));
        if (x && x.style) {
            x.style.height = '400px';
            x.style.width = '800px';
        }
    }
    //...............................end compare 2 image.......................

    openPicture($E){

        // var target = event.target;
        // var idAttr = target.attributes.id;
        // alert('event first' + event )
        // alert(idAttr);
        // alert('hi kavehhh');

        // ... start gallery Lightbox
        let $overlay = $('<div id="overlay" style="background: rgba(0,0,0,0.7);width: 100%;height: 100%;position: fixed;top: 0;left: 0;display: flex; justify-content: center;align-items: center; z-index: 999;-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;"></div>');
        const $image = $('<img style=" margin: 0; width: 80%;height: auto;object-fit: contain;padding: 5%;@media screen and (min-width:768px) {  width: 60%;} @media screen and (min-width:1200px) {  width: 50%;}">' );
        const $prevButton = $('<div id="prevButton" style="color: #fff;font-size: 2em;transition: opacity 0.8s;margin-right: 20px;@media screen and (min-width:768px) {font-size: 3em;};"><i class="fa fa-chevron-left"></i></div>');
        const $nextButton = $('<div id="nextButton" style="color: #fff;font-size: 2em;transition: opacity 0.8s;margin-left: 20px;@media screen and (min-width:768px) {font-size: 3em; }"><i class="fa fa-chevron-right"></i></div>');
        const $exitButton = $('<div id="exitButton" style="color: #fff;font-size: 2em;transition: opacity 0.8s;position: absolute;top: 15px;right: 15px ;@media screen and (min-width:768px) {font-size: 3em;}"><i class="fa fa-times"></i></div>');

// Add overlay
        $overlay.append($image).prepend($prevButton).append($nextButton).append($exitButton);
        $('#gallery').append($overlay);

// Hide overlay on default
        $overlay.hide();

// When an image is clicked

          //  alert('hi sarararararrararr');



            // Prevents default behavior
            event.preventDefault();
            // Adds href attribute to variable
            const imageLocation = $E;

            // Add the image src to $image
            $image.attr('src', imageLocation);
            // Fade in the overlay
            $overlay.fadeIn('slow');


// When the overlay is clicked
        $overlay.click(function() {
            // alert('hiii')
            // Fade out the overlay
            $(this).fadeOut('slow');
        });

// When next button is clicked
        $nextButton.click(function(event) {
            // Hide the current image
            $('#overlay img').hide();
            // Overlay image location
            const $currentImgSrc = $('#overlay img').attr('src');
            // Image with matching location of the overlay image
            const $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
            // Finds the next image
            const $nextImg = $($currentImg.closest('.image').next().find('img'));
            // All of the images in the gallery
            const $images = $('#image-gallery img');
            // If there is a next image
            if ($nextImg.length > 0) {
                // Fade in the next image
                $('#overlay img').attr('src', $nextImg.attr('src')).fadeIn(800);
            } else {
                // Otherwise fade in the first image
                $('#overlay img').attr('src', $($images[0]).attr('src')).fadeIn(800);
            }
            // Prevents overlay from being hidden
            event.stopPropagation();
        });

// When previous button is clicked
        $prevButton.click(function(event) {
            // Hide the current image
            $('#overlay img').hide();
            // Overlay image location
            const $currentImgSrc = $('#overlay img').attr('src');
            // Image with matching location of the overlay image
            const $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
            // Finds the next image
            const $nextImg = $($currentImg.closest('.image').prev().find('img'));
            // Fade in the next image
            $('#overlay img').attr('src', $nextImg.attr('src')).fadeIn(800);
            // Prevents overlay from being hidden
            event.stopPropagation();
        });

// When the exit button is clicked
        $exitButton.click(function() {
            // Fade out the overlay
          //  alert('closeeeeeeeeeeeeeeee')
            $('#overlay').fadeOut('slow');
        });

    }






    openPictureBySurgery($E){
        // alert('event first' + event )
        // alert('hi kavehhh');

        // ... start gallery Lightbox
        let $overlay = $('<div id="overlay1" style="background: rgba(0,0,0,0.7);width: 100%;height: 100%;position: fixed;top: 0;left: 0;display: flex; justify-content: center;align-items: center; z-index: 999;-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;"></div>');
        const $image = $('<img style=" margin: 0; width: 80%;height: auto;object-fit: contain;padding: 5%;@media screen and (min-width:768px) {  width: 60%;} @media screen and (min-width:1200px) {  width: 50%;}">' );
        const $prevButton = $('<div id="prevButton1" style="color: #fff;font-size: 2em;transition: opacity 0.8s;margin-right: 20px;@media screen and (min-width:768px) {font-size: 3em;};"><i class="fa fa-chevron-left"></i></div>');
        const $nextButton = $('<div id="nextButton1" style="color: #fff;font-size: 2em;transition: opacity 0.8s;margin-left: 20px;@media screen and (min-width:768px) {font-size: 3em; }"><i class="fa fa-chevron-right"></i></div>');
        const $exitButton = $('<div id="exitButton1" style="color: #fff;font-size: 2em;transition: opacity 0.8s;position: absolute;top: 15px;right: 15px ;@media screen and (min-width:768px) {font-size: 3em;}"><i class="fa fa-times"></i></div>');

// Add overlay
        $overlay.append($image).prepend($prevButton).append($nextButton).append($exitButton);
        $('#gallery1').append($overlay);

// Hide overlay on default
        $overlay.hide();

// When an image is clicked

        //  alert('hi sarararararrararr');




        // Add the image src to $image

        // Fade in the overlay
        $overlay.fadeIn('slow');

        // Prevents default behavior
        event.preventDefault();
        // Adds href attribute to variable
        const imageLocation = $E;
        //alert(imageLocation);
        // Add the image src to $image
        $image.attr('src', imageLocation);
        // Fade in the overlay
        $overlay.fadeIn('slow');


// When the overlay is clicked
        $overlay.click(function() {
            // alert('hiii')
            // Fade out the overlay
            $(this).fadeOut('slow');
        });

// When next button is clicked
        $nextButton.click(function(event) {
            // Hide the current image
            $('#overlay1 img').hide();
            // Overlay image location
            const $currentImgSrc = $('#overlay1 img').attr('src');
            // Image with matching location of the overlay image
            const $currentImg = $('#image-gallery1 img[src="' + $currentImgSrc + '"]');
            // Finds the next image
            const $nextImg = $($currentImg.closest('.image').next().find('img'));
            // All of the images in the gallery
            const $images = $('#image-gallery1 img');
            // If there is a next image
            if ($nextImg.length > 0) {
                // Fade in the next image
                $('#overlay1 img').attr('src', $nextImg.attr('src')).fadeIn(800);
            } else {
                // Otherwise fade in the first image
                $('#overlay1 img').attr('src', $($images[0]).attr('src')).fadeIn(800);
            }
            // Prevents overlay from being hidden
            event.stopPropagation();
        });

// When previous button is clicked
        $prevButton.click(function(event) {
            // Hide the current image
            $('#overlay1 img').hide();
            // Overlay image location
            const $currentImgSrc = $('#overlay1 img').attr('src');
            // Image with matching location of the overlay image
            const $currentImg = $('#image-gallery1 img[src="' + $currentImgSrc + '"]');
            // Finds the next image
            const $nextImg = $($currentImg.closest('.image').prev().find('img'));
            // Fade in the next image
            $('#overlay1 img').attr('src', $nextImg.attr('src')).fadeIn(800);
            // Prevents overlay from being hidden
            event.stopPropagation();
        });

// When the exit button is clicked
        $exitButton.click(function() {
            // Fade out the overlay
              //alert('closeeeeeeeeeeeeeeee')
            $('#overlay1').fadeOut('slow');
        });

    }

    openPictureSurgeryAndCat($E){
        // ... start gallery Lightbox
        let $overlay = $('<div id="overlay2" style="background: rgba(0,0,0,0.7);width: 100%;height: 100%;position: fixed;top: 0;left: 0;display: flex; justify-content: center;align-items: center; z-index: 999;-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;"></div>');
        const $image = $('<img style=" margin: 0; width: 80%;height: auto;object-fit: contain;padding: 5%;@media screen and (min-width:768px) {  width: 60%;} @media screen and (min-width:1200px) {  width: 50%;}">' );
        const $prevButton = $('<div id="prevButton2" style="color: #fff;font-size: 2em;transition: opacity 0.8s;margin-right: 20px;@media screen and (min-width:768px) {font-size: 3em;};"><i class="fa fa-chevron-left"></i></div>');
        const $nextButton = $('<div id="nextButton2" style="color: #fff;font-size: 2em;transition: opacity 0.8s;margin-left: 20px;@media screen and (min-width:768px) {font-size: 3em; }"><i class="fa fa-chevron-right"></i></div>');
        const $exitButton = $('<div id="exitButton2" style="color: #fff;font-size: 2em;transition: opacity 0.8s;position: absolute;top: 15px;right: 15px ;@media screen and (min-width:768px) {font-size: 3em;}"><i class="fa fa-times"></i></div>');

// Add overlay
        $overlay.append($image).prepend($prevButton).append($nextButton).append($exitButton);
        $('#gallery2').append($overlay);

// Hide overlay on default
        $overlay.hide();

// When an image is clicked

        //  alert('hi sarararararrararr');



        // Prevents default behavior
        event.preventDefault();
        // Adds href attribute to variable
        const imageLocation = $E;

        // Add the image src to $image
        $image.attr('src', imageLocation);
        // Fade in the overlay
        $overlay.fadeIn('slow');


// When the overlay is clicked
        $overlay.click(function() {
            // alert('hiii')
            // Fade out the overlay
            $(this).fadeOut('slow');
        });

// When next button is clicked
        $nextButton.click(function(event) {
            // Hide the current image
            $('#overlay2 img').hide();
            // Overlay image location
            const $currentImgSrc = $('#overlay2 img').attr('src');
            // Image with matching location of the overlay image
            const $currentImg = $('#image-gallery2 img[src="' + $currentImgSrc + '"]');
            // Finds the next image
            const $nextImg = $($currentImg.closest('.image').next().find('img'));
            // All of the images in the gallery
            const $images = $('#image-gallery2 img');
            // If there is a next image
            if ($nextImg.length > 0) {
                // Fade in the next image
                $('#overlay2 img').attr('src', $nextImg.attr('src')).fadeIn(800);
            } else {
                // Otherwise fade in the first image
                $('#overlay2 img').attr('src', $($images[0]).attr('src')).fadeIn(800);
            }
            // Prevents overlay from being hidden
            event.stopPropagation();
        });

// When previous button is clicked
        $prevButton.click(function(event) {
            // Hide the current image
            $('#overlay2 img').hide();
            // Overlay image location
            const $currentImgSrc = $('#overlay2 img').attr('src');
            // Image with matching location of the overlay image
            const $currentImg = $('#image-gallery2 img[src="' + $currentImgSrc + '"]');
            // Finds the next image
            const $nextImg = $($currentImg.closest('.image').prev().find('img'));
            // Fade in the next image
            $('#overlay2 img').attr('src', $nextImg.attr('src')).fadeIn(800);
            // Prevents overlay from being hidden
            event.stopPropagation();
        });

// When the exit button is clicked
        $exitButton.click(function() {
            // Fade out the overlay
            //  alert('closeeeeeeeeeeeeeeee')
            $('#overlay2').fadeOut('slow');
        });

    }



    //..........................start print.............................................................

    print(): void {
        let  popupWin , a ;
        const array1 = [];
        const printContents = [] ;
        // let printContents, popupWin;
        $('.form-check-input:checked').each(function(){
            array1.push($(this).parent().parent().find('a').attr('id'));
        });
        console.log(array1);
        for (let i = 0 ; i < array1.length ; i++) {
            a = array1.valueOf();
            //alert('sararaaaa print====>>' + a);
            for (let j = 0 ; j < a.length ; j++) {
                printContents[j] = document.getElementById(a[j]).innerHTML;
            }
        }

        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
        );
        popupWin.document.close();

    }

    //......................start api.............................
    loadAllPhotoPatientId(id) {
        this.detailPatientTestService.findPhotoPatient( id).subscribe(
            (res: ResponseWrapper) => this.onSuccessPhoto(res.json),
            (res: ResponseWrapper) => this.onError(res.json));
    }

    private onSuccessPhoto(data) {
        // this.page = pagingParams.page;
        this.photoPatient  = data;
       // alert('photoooooo  test patientttttttttttttttt=====>>>' + JSON.stringify(this.photoPatient)  )
    }
    private onError(error) {
        // this.jhiAlertService.error(error.message, null, null);
    }

    load(id) {
        // alert('id patient Test========>>>>>' +id)
        this.detailPatientTestService.find(id).subscribe((patient) => {
            this.patient = patient;
        });
    }


    getPhotoBySurgery(patientId , SurgeryId){


       // alert('tssss surgery Id' + SurgeryId)
        this.getAllPhotoPatient  = false;
        this.getAllPhotoBySurgery = true;
        this.getAllPhotoBySurgeryAndCat = false;
        this.showUploadButton = false;
        this.photoEmptyCat = false;
        this.photoEmptyAllImage = false;
        this.photoEmptySurgeryImage = true;

        this.detailPatientTestService.findPhotoBySurgery( patientId , SurgeryId = 8).subscribe(
            (res: ResponseWrapper) => this.onSuccessPhotoBySurgery(res.json),
            (res: ResponseWrapper) => this.onError(res.json));
    }
    private onSuccessPhotoBySurgery(data) {
        // this.page = pagingParams.page;
        this.photoSurgery  = data;
       // alert('photoooooo  test photo Surgery  sedcsdcsdc=====>>>' + JSON.stringify(this.photoSurgery))
    }



    getPhotoBySurgeryAncCat(patientId , catId , SurgeryId ){
        //alert('asdasdas')

        this.spinner.hide();
        this.general_cat_id = catId;
        this.getAllPhotoPatient  = false;
        this.getAllPhotoBySurgery = false;
        this.getAllPhotoBySurgeryAndCat = true;
        this.showUploadButton = true;
        this.photoEmptyCat = true;
        this.photoEmptyAllImage = false;
        this.photoEmptySurgeryImage = false;
        this.detailPatientTestService.findPhotoBySurgeryAndCat( patientId , catId, SurgeryId = 8).subscribe(
            (res: ResponseWrapper) => this.onSuccessPhotoBySurgeryAndCat(res.json),
            (res: ResponseWrapper) => this.onError(res.json));


    }
    listClick(event, newValue) {
        //console.log(newValue);
        this.selectedItem = newValue;  // don't forget to update the model here
        // ... do other stuff here ...
    }

    private onSuccessPhotoBySurgeryAndCat(data) {
        // this.page = pagingParams.page;
        this.photoSurgeryAndCat  = data;
      //  alert('photoooooo  test photo Surgery And caregory*****=====>>>' + JSON.stringify(this.photoSurgeryAndCat))
    }




    loadAllCategory() {
        this.detailPatientTestService.findAllCategory({}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    private onSuccess(data) {

        // this.page = pagingParams.page;
        this.categories = data;
          //alert('categoriiiii=====>>>' + JSON.stringify(this.categories)  )
    }
    openCategori(){

        // document.getElementById('caretRight').style.display = 'none';
        // document.getElementById('createDown').style.display = 'block';
        this.showSubCategory = true ;
        this.showCaretRight = false ;
        this.showCaretDown = true ;
        // var x = document.getElementById("caretRight");
        // if (x.style.display === "none") {
        //     x.style.display = "block";
        // } else {
        //     x.style.display = "none";
        // }
    }

    closeCategory(){
        this.showSubCategory = false ;
        this.showCaretRight = true ;
        this.showCaretDown = false ;
    }

    deleteAllPhoto(id) {
        //alert('id =' + id);
        const modalRef = this.modalService.open(DeleteImagePopupComponent, {windowClass: 'lg', backdrop: 'static'});
        modalRef.componentInstance.photoDelete = id;
        // callback of the popup

        modalRef.result.then(res => {
            console.log(res + ' the result of popup');
            // alert(' in 1111')
            this.loadAllPhotoPatientId(this.param1);
            // alert('load all photo ==== >>' + this.loadAllPhotoPatientId(id) )
        }, rejected => {
            //alert(' in rrrr')

            console.log(' rejected: ' + rejected.toString());
            this.loadAllPhotoPatientId(this.param1);
        });
    }

    deleteAllPhotoBySurgery(id){
        const modalRef = this.modalService.open(DeleteImagePopupComponent, {windowClass: 'lg', backdrop: 'static'});
        modalRef.componentInstance.photoDelete = id;
        // callback of the popup

        modalRef.result.then(res => {
            console.log(res + ' the result of popup');
            // alert(' in 1111')
            this.getPhotoBySurgery(this.param1 , 8);
            // alert('load all photo ==== >>' + this.loadAllPhotoPatientId(id) )
        }, rejected => {
            //alert(' in rrrr')

            //console.log(' rejected: ' + rejected.toString());
            this.getPhotoBySurgery(this.param1 , 8);
        });
    }

    deletePhotoByCat(id) {
        // alert('id =' + id);
        const modalRef = this.modalService.open(DeleteImagePopupComponent, {windowClass: 'lg', backdrop: 'static'});
        modalRef.componentInstance.photoDelete = id;
        // callback of the popup

        modalRef.result.then(res => {
            // console.log(res + ' the result of popup');
            //alert(' in 1111')
            this.getPhotoBySurgeryAncCat(this.param1 , this.general_cat_id ,8);
            // alert('load all photo ==== >>' + this.loadAllPhotoPatientId(id) )
        }, rejected => {
            //alert(' in rrrr')

            //console.log(' rejected: ' + rejected.toString());
            this.getPhotoBySurgeryAncCat(this.param1 , this.general_cat_id , 8);
        });
    }

    uploadByCat(patientId, catId , surgery_id) {

        const modalRef = this.modalService.open(UploadImagePopupComponent, {windowClass: 'lg', backdrop: 'static'});
         // this.activeModal.dismiss('cancel');
        modalRef.componentInstance.catId = catId;
        modalRef.componentInstance.PatientId = patientId;
        modalRef.componentInstance.surgery_id = surgery_id;
        modalRef.result.then(res => {
            // console.log(res + ' the result of popup');
          // alert(' in 1111')
            this.getPhotoBySurgeryAncCat(this.param1 , this.general_cat_id ,8);
            // alert('load all photo ==== >>' + this.loadAllPhotoPatientId(id) )
        }, rejected => {
            // alert(' in rrrr');
            // console.log(' rejected: ' + rejected.toString());
            this.getPhotoBySurgeryAncCat(this.param1 , this.general_cat_id , 8);
        });
    }

     addactive(id) {
        $("#secondCarousel td.activeImage").removeClass('activeImage');
        $("#img"+id).addClass("activeImage");
    }

}
