import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
// import {PersonalComponent} from '../surgery-note/personal/personal.component';
// import {WorkflowGuard} from '../surgery-note/workflow/workflow-guard.service';
import {EmployeeService} from '../employee/employee.service';
// import {PhotoManagementComponent} from './photo-management.component';
import {DetailPatientTestComponent} from './detail-patient-test.component';
import {ProfileComponent} from '../profile/profile.component';
import {DetailPatientPhotoComponent} from './detail-patient-photo.component';


export const appRoutes: Routes = [
    { path: ':id', component: DetailPatientTestComponent},
    { path: ':id', component: DetailPatientPhotoComponent},
    { path: '',   redirectTo: '/pages/detailPatientTest', pathMatch: 'full' },
    { path: 'detailPatientTest',   redirectTo: '/pages/detailPatientTest', pathMatch: 'full' },
    // 6th Route
    { path: '**', component: DetailPatientTestComponent}
];

@NgModule({
    imports: [RouterModule.forChild(appRoutes)],
    exports: [RouterModule],
    providers: [EmployeeService,]
})

export class AppRoutingModule {}





//
//
// import { NgModule }             from '@angular/core';
// import { Routes, RouterModule } from '@angular/router';
//
// import { PersonalComponent }    from './personal/personal.component';
// import { WorkComponent }        from './work/work.component';
// import { AddressComponent }     from './address/address.component';
// import { ResultComponent }      from './result/result.component';
//
// import { WorkflowGuard }        from './workflow/workflow-guard.service';
// import { WorkflowService }      from './workflow/workflow.service';
// import {Address2Component} from './address2/address2.component';
//
//
// export const appRoutes: Routes = [
//     // 1st Route
//     { path: 'personal',  component: PersonalComponent },
//     // 2nd Route
//     { path: 'work',  component: WorkComponent, canActivate: [WorkflowGuard] },
//     // 3rd Route
//     { path: 'address',  component: AddressComponent, canActivate: [WorkflowGuard] },
//     // 4th Route
//     { path: 'address2',  component: Address2Component, canActivate: [WorkflowGuard] },
//
//     { path: 'result',  component: ResultComponent, canActivate: [WorkflowGuard] },
//     // 5th Route
//     { path: '',   redirectTo: '/personal', pathMatch: 'full' },
//     // 6th Route
//     { path: '**', component: PersonalComponent }
// ];
//
// @NgModule({
//     imports: [RouterModule.forRoot(appRoutes, { useHash: true} )],
//     exports: [RouterModule],
//     providers: [WorkflowGuard]
// })
//
// export class SurgeryNoteRoutingModule {}
