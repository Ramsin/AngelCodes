import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import {CommonModule} from '@angular/common';
import {HttpModule} from '@angular/http';
import {AgGridModule} from 'ag-grid-angular';
import {HttpClientModule} from '@angular/common/http';
import {MultiselectDropdownModule} from 'angular-2-dropdown-multiselect';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormsModule, ReactiveFormsModule, Validators} from '@angular/forms';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {NgJhipsterModule} from 'ng-jhipster';
import {DetailPatientTestService} from './detail-patient-test.service';
import {AppRoutingModule} from './detail-patient-test-routing.module';
import {customHttpProvider} from '../../shared/interceptor/http.provider';
import {GeneralService} from '../../shared/service/general.service';
import {DetailPatientTestComponent} from './detail-patient-test.component';
import {DetailPatientPhotoComponent} from './detail-patient-photo.component';
import {UploadImagePopupComponent} from './upload-image-popup.component';
import {DeleteImagePopupComponent} from './delete-image-popup.component';

export const routes = [
    {
        path: '', component: DetailPatientTestComponent, pathMatch: 'full',
        // children: [
        //     {path: '', redirectTo: '/personal', pathMatch: 'full'},
        //
        //     // {path: '/personal', component: PersonalComponent}
        // ]
    }
];

@NgModule({
    imports: [
        CommonModule, HttpModule, HttpClientModule, RouterModule.forChild(routes), FormsModule, NgbModule.forRoot(), MultiselectDropdownModule
        , AngularMultiSelectModule, NgJhipsterModule,
        ReactiveFormsModule,
      AppRoutingModule
    ],
    declarations: [
        DetailPatientTestComponent , DetailPatientPhotoComponent , UploadImagePopupComponent , DeleteImagePopupComponent
    ],
    entryComponents: [UploadImagePopupComponent ,DeleteImagePopupComponent
    ],
    providers: [  DetailPatientTestService, customHttpProvider(), GeneralService

    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DetailPatientTestModule {



}
