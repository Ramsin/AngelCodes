import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import * as $ from 'jquery';
import {NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';
import {Observable, Subscription} from 'rxjs';
import {DetailPatientTestService} from './detail-patient-test.service';
import {Patient} from '../../shared/model/patient.model';
import {CategoryPhoto} from '../../shared/model/category.model';
import {ResponseWrapper} from '../../shared/model/response-wrapper.model';
import {Photo} from '../../shared/model/photo.model';
import {FormBuilder, Validators} from '@angular/forms';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Position} from '../../shared/model/position.model';
import {Response} from '@angular/http';
import {PhotoManagementPopupComponent} from '../photo-management/photo-management-popup.component';
import {UploadImagePopupComponent} from './upload-image-popup.component';
import {PatientDeleteDialogComponent} from '../patient/patient-delete-dialog.component';
import {DeleteImagePopupComponent} from './delete-image-popup.component';
import {Location} from '@angular/common';


@Component({
    selector: 'app-detail-patient-test',
    templateUrl: './detail-patient-test.component.html',
    styleUrls: ['./detail-patient-test.component.scss']
})
export class DetailPatientTestComponent implements OnInit {
    // @Input() patientEdit;
    patient: Patient;
    categories: CategoryPhoto[];
    closeResult: string;
    param1: any;
    category_id: any;
    // categories: any = [];
    saveSurgeryId: any;
    routeSub: Subscription;
    showMyContainer: boolean = false;
    private subscription: Subscription;
    appCategory: CategoryPhoto = new CategoryPhoto();
    //.......from photo them.....................
    photo: Photo = new Photo();
    formPhoto: Photo[];
    isSaving: boolean;
    confirmSaveMsgForm: boolean;
    errorSaveMsgForm: boolean;
    patient_id: any;
    photoPatient: any;
    photoCat: any;
    photoSurgery: any;
    photoSurgeryAndCat: any;
    PhotoId: any;
   general_cat_id: any = -1;
    showAllPhotoSurgery : boolean = true;
    showAllPhotoSurgeryAndCat : boolean = false;

    constructor(private location: Location ,private modalService: NgbModal, private route: ActivatedRoute, private fb: FormBuilder, private detailPatientTestService: DetailPatientTestService, protected activatedRoute: ActivatedRoute) {
        // config.backdrop = 'static';
        // config.keyboard = false;

    }

    ngOnInit() {
        this.isSaving = false;
        this.loadAllCategory();


        this.routeSub = this.route.params.subscribe((params) => {
            this.param1 = params['id'];
        });

        this.subscription = this.route.params.subscribe((params) => {
            this.load(this.param1);
        });
        this.loadAllPhotoPatientId(this.param1);
        // this.findPhotoByCatId(findPhotoByCatId)
        this.getPhotoBySurgery(this.param1 , 8);

    }

    openFolder(content) {
        this.modalService.open(content);

    }

    openFolderSearch(contentSearch) {
        this.modalService.open(contentSearch);
    }

    load(id) {
        // alert('========>>>>>' +id)
        this.detailPatientTestService.find(id).subscribe((patient) => {
            this.patient = patient;
        });
    }

    loadAllPhotoPatientId(id) {
        //alert(' in loadAllPhotoPatientId id == ' + id);
        this.detailPatientTestService.findPhotoPatient(id).subscribe(
            (res: ResponseWrapper) => this.onSuccessPhoto(res.json),
            (res: ResponseWrapper) => this.onError(res.json));
    }


    private onSuccessPhoto(data) {

        // this.page = pagingParams.page;
        this.photoPatient = data;
        // alert('photoooooo=====>>>' + JSON.stringify(this.photoPatient)  )
    }


    showPhotoByCat(patientId, catId) {
        this.general_cat_id = catId;
        // document.getElementById('showPhotoByCat').style.display = 'block';
        // document.getElementById('showAllPhoto').style.display = 'none';
        // alert('catId====>' + catId) ;
        // alert('PatientId====>' + patientId) ;

        this.detailPatientTestService.findPhotoByCatId(patientId, catId).subscribe(
            (res: ResponseWrapper) => this.onSuccessPhotoByCatId(res.json),
            (res: ResponseWrapper) => this.onError(res.json));
    }


    private onSuccessPhotoByCatId(data) {

        // this.page = pagingParams.page;
        this.photoCat = data;
       // alert('photoooooo catttt=====>>>' + JSON.stringify(this.photoCat))
    }

    getPhotoBySurgeryAncCat(patientId , catId , SurgeryId){
         this.showAllPhotoSurgery  = false;
         this.showAllPhotoSurgeryAndCat = true;
        // this.getAllPhotoBySurgeryAndCat = true;
        this.general_cat_id = catId;

        this.detailPatientTestService.findPhotoBySurgeryAndCat( patientId , catId, SurgeryId = 8).subscribe(
            (res: ResponseWrapper) => this.onSuccessPhotoBySurgeryAndCat(res.json),
            (res: ResponseWrapper) => this.onError(res.json));
    }


    private onSuccessPhotoBySurgeryAndCat(data) {
        // this.page = pagingParams.page;
        this.photoSurgeryAndCat  = data;
        //  alert('photoooooo  test photo Surgery And caregory*****=====>>>' + JSON.stringify(this.photoSurgeryAndCat))
    }
    private onError(error) {
        // this.jhiAlertService.error(error.message, null, null);
    }


    getPhotoBySurgery(patientId , SurgeryId){
         this.showAllPhotoSurgery = true;
         this.showAllPhotoSurgeryAndCat = false;
        this.detailPatientTestService.findPhotoBySurgery( patientId , SurgeryId = 8).subscribe(
            (res: ResponseWrapper) => this.onSuccessPhotoBySurgery(res.json),
            (res: ResponseWrapper) => this.onError(res.json));
    }
    private onSuccessPhotoBySurgery(data) {
        // this.page = pagingParams.page;
        this.photoSurgery  = data;
        //alert('photoooooo  test photo Surgery=====>>>' + JSON.stringify(this.photoSurgery))
    }


    uploadByCat(patientId, catId) {
        //(' in edit id =' + catId);
        //alert('patient Iddd======>>' + patientId)
        const modalRef = this.modalService.open(UploadImagePopupComponent, {windowClass: 'lg', backdrop: 'static'});
        modalRef.componentInstance.catId = catId;
        modalRef.componentInstance.PatientId = patientId;
    }

    loadAllCategory() {
        this.detailPatientTestService.findAllCategory({}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    private onSuccess(data) {

        // this.page = pagingParams.page;
        this.categories = data;
        //  alert('categoriiiii=====>>>' + JSON.stringify(this.categories)  )
    }

    // backClicked(){
    //     this.location.back();
    // }


    protected onSaveError() {
        this.isSaving = false;
        this.confirmSaveMsgForm = false;
    }

    previousState() {
        window.history.back();
    }

    // deletePhoto(id : number){
    //     alert('photoo idddddddddddddddddddd =======>>' + id)
    //     this.detailPatientTestService.deletePhoto(id ).subscribe();
    // }

    deleteAllPhoto(id) {
        //alert('id =' + id);
        const modalRef = this.modalService.open(DeleteImagePopupComponent, {windowClass: 'lg', backdrop: 'static'});
        modalRef.componentInstance.photoDelete = id;
        // callback of the popup

        modalRef.result.then(res => {
            console.log(res + ' the result of popup');
           // alert(' in 1111')
            this.getPhotoBySurgery(this.param1 , 8);
            // alert('load all photo ==== >>' + this.loadAllPhotoPatientId(id) )
        }, rejected => {
            //alert(' in rrrr')

            //console.log(' rejected: ' + rejected.toString());
            this.getPhotoBySurgery(this.param1 , 8);
        });
    }


    deletePhotoByCat(id) {
       // alert('id =' + id);
        const modalRef = this.modalService.open(DeleteImagePopupComponent, {windowClass: 'lg', backdrop: 'static'});
        modalRef.componentInstance.photoDelete = id;
        // callback of the popup

        modalRef.result.then(res => {
           // console.log(res + ' the result of popup');
            //alert(' in 1111')
            this.getPhotoBySurgeryAncCat(this.param1 , this.general_cat_id ,8);
            // alert('load all photo ==== >>' + this.loadAllPhotoPatientId(id) )
        }, rejected => {
            //alert(' in rrrr')

            //console.log(' rejected: ' + rejected.toString());
            this.getPhotoBySurgeryAncCat(this.param1 , this.general_cat_id , 8);
        });
    }
}
